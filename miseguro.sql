-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-09-2018 a las 22:57:40
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `miseguro`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente`
--

CREATE TABLE `agente` (
  `id` int(11) NOT NULL,
  `id_distribuidor` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `agente` varchar(127) NOT NULL,
  `correo_electronico` varchar(255) NOT NULL,
  `telefono` varchar(32) NOT NULL,
  `comision` int(11) NOT NULL,
  `descuento` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `estado` text NOT NULL,
  `cp` int(11) NOT NULL,
  `ciudad` text NOT NULL,
  `calle_numero` varchar(511) NOT NULL,
  `razon_social` varchar(511) NOT NULL,
  `rfc` varchar(255) NOT NULL,
  `fac_correo_electronico` varchar(255) NOT NULL,
  `fac_telefono` varchar(32) NOT NULL,
  `fac_id_pais` int(11) NOT NULL,
  `fac_estado` text NOT NULL,
  `fac_cp` int(11) NOT NULL,
  `fac_ciudad` text NOT NULL,
  `fac_calle_numero` varchar(511) NOT NULL,
  `colonia` varchar(127) NOT NULL,
  `fac_colonia` varchar(127) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `agente`
--

INSERT INTO `agente` (`id`, `id_distribuidor`, `activo_inactivo`, `agente`, `correo_electronico`, `telefono`, `comision`, `descuento`, `id_pais`, `estado`, `cp`, `ciudad`, `calle_numero`, `razon_social`, `rfc`, `fac_correo_electronico`, `fac_telefono`, `fac_id_pais`, `fac_estado`, `fac_cp`, `fac_ciudad`, `fac_calle_numero`, `colonia`, `fac_colonia`) VALUES
(1, 2, 1, 'Carolina Sweet Martinez', 'americaseguros3@prodigy.net.mx', '', 15, 0, 1, 'Tamaulipas', 0, '', 'Calle Doce #107 Col. Aztlan', 'America Seguros', 'RADA6009227K7', 'americaseguros3@prodigy.net.mx', '', 1, '', 0, '', '', '', ''),
(2, 3, 1, 'Juan Villarreal Montemayor', 'info@setmex.com', '', 15, 0, 1, 'Nuevo León', 0, '', 'Ave. Roble 701', 'Asesores Profesionales De Seguros', '', 'info@setmex.com', '', 1, '', 0, '', '', 'Valle del Campestre', ''),
(3, 4, 1, 'Carlos A. Muñoz', 'caram1@prodigy.net.mx', '', 15, 0, 1, 'Quintana Roo', 0, 'Cancún', 'Pecari 3 16  5M 20 ', 'Carlos A. Muñoz', '', 'caram1@prodigy.net.mx', '', 1, '', 0, '', '', '', ''),
(4, 6, 1, 'CETYS Universidad', '', '', 0, 10, 1, 'Edo. México', 0, '', 'Calzada CETYS s/n', 'Cetys Universidad', '', '', '', 1, '', 0, '', '', 'Rivera', ''),
(5, 7, 1, '', 'sandra.gmm@davidgomezseguros.com', '', 15, 0, 1, 'Tamaulipas', 89106, '', 'TAMPICO', 'David Eduardo Gomez Salazar', '', 'Sandra <sandra.gmm@davidgomezseguros.com>', '', 1, '', 0, '', '', '', ''),
(6, 8, 1, 'EPIC', '', '', 15, 0, 1, 'Nuevo León', 0, 'Tecnológico', 'Plaza San Sebastian local 12 ', 'Epic', '', '', '', 1, '', 0, '', '', 'Eugenio Garza Sada 276', ''),
(7, 9, 1, 'Oficina Cuernavaca', 'cuernavaca@estudiantesembajadores.com', '', 20, 0, 1, 'Morelos', 0, 'Cuernavaca', '', 'Programas Estudiantiles de Mexico, AC', 'PEM010212KK9', 'cuernavaca@eem.org.mx', '', 1, '', 0, '', '', '', ''),
(8, 9, 1, 'Oficina Zacatecas', 'zacatecas@estudiantesembajadores.com', '', 10, 10, 1, 'Zacatecas', 0, 'Zacatecas', '', 'Programas Estudiantiles de Mexico, AC', 'PEM010212KK9', 'zacatecas@eem.org.mx', '', 1, '', 0, '', '', '', ''),
(9, 9, 1, 'Oficina Merida', 'merida@estudiantesembajadores.com', '', 10, 10, 1, 'Yucatan', 0, 'Mérida', '', 'Programas Estudiantiles de Mexico, AC', 'PEM010212KK9', 'merida@eem.org.mx', '', 1, '', 0, '', '', '', ''),
(10, 9, 1, 'Oficina Leon', 'leon@estudiantesembajadores.com', '', 20, 0, 1, 'Guanajuato', 0, 'León', '', 'Programas Estudiantiles de Mexico, AC', 'PEM010212KK9', 'soporteleon@eem.org.mx', '', 1, '', 0, '', '', '', ''),
(11, 9, 1, 'Oficina Monterrey', 'mnino@misegurointernacional.com', '', 20, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Programas Estudiantiles de Mexico, AC', 'PEM010212KK9', 'mnino@setmex.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(12, 9, 1, 'Franquicia Queretaro', 'queretaro@estudiantesembajadores.com', '', 20, 0, 1, 'Queretaro', 0, 'Queretaro', '', 'Programas Estudiantilies de Mexico, AC', 'PEM010212KK9', 'queretaro@eem.org.mx', '', 1, '', 0, '', '', '', ''),
(13, 9, 1, 'Franquicia  Veracruz', 'veracruz@estudiantesembajadores.com', '229 922 55 68', 10, 10, 1, 'Veracruz', 94299, 'Veracruz ', 'Ave. Ruiz Cortines # 1360 l.5 plaza Tiburón', 'Estudiantes Embajadores', 'PEM010212KK9', 'Esther Zamudio <ezamudio@estudiantesembajadores.com>', '', 1, '', 0, '', '', 'Costa de Oro Boca del Río', ''),
(14, 9, 1, 'Franquicia Metepec', 'metepec@estudiantesembajadores.com', '722 232 8093', 10, 10, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Estudiantes Embajadores', 'PEM010212KK9', 'metepec@estudiantesembajadores.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(15, 9, 1, 'Franquicia Monclova', 'monclova@estudianteseembajadores.com', '866 632 02 05', 10, 10, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Estudiantes Embajadores', 'PEM010212KK9', 'monclova@estudianteseembajadores.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(16, 9, 1, 'Franquicia Puebla', 'puebla@estudianteseembajadores.com', '222 226 53 91', 10, 10, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Estudiantes Embajadores', 'PEM010212KK9', 'puebla@estudianteseembajadores.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(17, 9, 1, 'Franquicia Queretaro', 'queretaro@estudiantesembajadores.com', '442 245 1771', 10, 10, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Estudiantes Embajadores', 'PEM010212KK9', 'queretaro@estudiantesembajadores.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(18, 9, 1, 'Franquicia San Luis Potosi', 'slp@estudiantesembajadores.com', '444 211 95 62', 10, 10, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Estudiantes Embajadores', 'PEM010212KK9', 'slp@estudiantesembajadores.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(19, 9, 1, 'Franquicia Tampico', 'tampico@estudianteseembajadores.com', '833 2323 231', 10, 10, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Estudiantes Embajadores', 'PEM010212KK9', 'adominguez@estudianteseembajadores.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(20, 9, 1, 'Franquicia Torreon', 'torreon@estudiantesembajadores.com', '871 295 3230', 10, 10, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Estudiantes Embajadores', 'PEM010212KK9', 'torreon@estudiantesembajadores.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(21, 9, 1, 'Franquicia Aguascalientes', 'aguascalientes@estudiantesembajadores.com', '449 152 1441', 10, 10, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Estudiantes Embajadores', 'PEM010212KK9', 'aguascalientes@estudiantesembajadores.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(22, 9, 1, 'Franquicia Chihuahua', 'chihuahua@estudiantesembjadores.com', '614 156 7572', 10, 10, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Estudiantes Embajadores', 'PEM010212KK9', 'gduran@estudiantesembjadores.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(23, 9, 1, 'Franquicia Coatzacoalcos', 'coatzacoalcos@estudiantesembajadores.com', '921 2129 150', 10, 10, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Estudiantes Embajadores', 'PEM010212KK9', 'rtorres@estudiantesembajadores.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(24, 9, 1, 'Franquicia Ciudad de México Sur', 'cdmexicosur@estudiantesembajadores.com', '55 70 90 34 76', 10, 10, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Estudiantes Embajadores', 'PEM010212KK9', 'crodriguez@estudiantesembajadores.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(25, 9, 1, 'Franquicia Guadalajara', 'guadalajara@estudianteseembajadores.com', '33 2001 6463', 10, 10, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Estudiantes Embajadores', 'PEM010212KK9', 'guadalajara@estudianteseembajadores.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(26, 9, 1, 'Oficina Ciudad de México', 'mexico@estudiantesembajadores.com', '55 55 555 55 ', 10, 10, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Estudiantes Embajadores', 'PEM010212KK9', 'Ana Novelo Live It Out <mexico@liveitout.com.mx>', '', 1, '', 0, '', '', 'Del Valle', ''),
(27, 10, 1, 'Fernando H. Saenz de Nanclares Ortega', 'fernandoj.saez@euhmeria.com.mx', '', 20, 0, 1, 'Nuevo León', 0, '', '', 'Fernando H. Saez De Nanclares Ortega', '', 'fernandoj.saez@euhmeria.com.mx', '', 1, '', 0, '', '', '', ''),
(28, 14, 1, 'Oficina IDEST', '', '', 0, 10, 1, 'Tamaulipas', 0, '', '', 'Instituto De Estudios Superiores De Tamaulipas', '', '', '', 1, '', 0, '', '', '', ''),
(29, 15, 1, 'Oficina La Salle', '', '', 0, 10, 1, 'Cancun', 0, '', '', 'Universidad La Salle', '', '', '', 1, '', 0, '', '', '', ''),
(30, 17, 1, 'Hector Julio Gomez De La Garza', '', '', 20, 0, 1, 'Nuevo León', 0, 'San Pedro Garza García', 'Sierra Nevada 120 Cruz C. Priv. Sierra N  Villa Montaña', 'Phixius', '', 'f', '', 1, '', 0, '', '', '', ''),
(31, 19, 1, 'Lic. Alfredo Zertuche ', 'plcalf@prodigy.net.mx', '', 15, 0, 1, 'Coahuila ', 66220, '', 'Rio Nadadores # 1013', 'Programas De Lengua Y Cultura', '', 'plcalf@prodigy.net.mx', '', 1, '', 0, '', '', ' Jardines de la Salle', ''),
(32, 20, 1, 'Adolfo Rico Lopez', 'mexicali@eem.org.mx', '52 1686 1389194', 20, 0, 1, 'Nuevo León', 66220, '', 'Lago de los Osos', '', '', 'mexicali@eem.org.mx', '', 1, '', 0, '', '', ' Jardines del Lago', ''),
(33, 20, 1, 'Ana Arias De Buhl', '', '', 0, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', 'Del Valle', ''),
(34, 20, 1, 'Ana Marìa Martinez De Novelo', 'ananovelo@hotmail.com', '01 55 50 49 28 79', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Fuente de Prometeo No. 2  ', 'Setmex S.A. de C.V', 'SET010213LZ8', 'ananovelo@hotmail.com', '', 1, '', 0, '', '', 'Tecamachalco', ''),
(35, 20, 1, 'Denice Castillejos', '', '', 0, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', 'Del Valle', ''),
(36, 20, 1, 'Elias Garza Garza', 'seguroseliasgarza@hotmail.com', '', 15, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'seguroseliasgarza@hotmail.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(37, 20, 1, 'Elizabet Macias', '', '', 0, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'info@setmex.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(38, 20, 1, 'Enrique Saenz Quintana', 'esaenzq@hotmail.com', '', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'esaenzq@hotmail.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(39, 20, 1, 'Eva Maria Flores Garcia', 'evaseguros@gmail.com', '81 14 25 08', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Guiseppe Verdi No. 300 ', 'Setmex S.A. de C.V', 'SET010213LZ8', 'evaseguros@gmail.com', '', 1, '', 0, '', '', 'Colinas de San Jerónimo', ''),
(40, 20, 1, 'Francisco Ruben Oviedo', 'agenciasetej@hotmail.com', '', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'agenciasetej@hotmail.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(41, 20, 1, 'Gabriela Gonzalez', 'cristimadrigal@hotmail.com', '01 444 825 1967', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Monte de Aconcagua No. 440 lomas 2nd. seccion  78210', 'Setmex S.A. de C.V', 'SET010213LZ8', 'cristimadrigal@hotmail.com', '', 1, '', 0, '', '', '', ''),
(42, 20, 1, 'Guillermo Llavona Galvan', 'gllavona@yahoo.com', '811 5312 78 4', 15, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Cerrada Escalane No. 128', 'Setmex S.A. de C.V', 'SET010213LZ8', 'gllavona@yahoo.com', '', 1, '', 0, '', '', 'Cerradas de Cumbres', ''),
(43, 20, 1, 'Guillermo Zuñiga', '', '', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', 'Del Valle', ''),
(44, 20, 1, 'Jose Eli  Garza Carrales', '', '', 0, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', 'Del Valle', ''),
(45, 20, 1, 'Jose Ramons Gutierrez', 'joseramon@gvseguros.com', '', 0, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'joseramon@gvseguros.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(46, 20, 1, 'Juan Pablo Borrego Treviño', '', '82 56 43 33', 0, 0, 1, 'Nuevo León', 66220, '', 'Rosales 220', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', 'Santa Engracia', ''),
(47, 20, 1, 'Maaike Del Villar', 'maaikedv@educacioninternacional.org.mx', '01 55 58 13 19 27', 15, 0, 1, 'Nuevo León', 66220, '', 'Apexoco No. 19 ', 'Setmex S.A. de C.V', 'SET010213LZ8', 'Maaike V. de del Villar [maaikedv@educacioninternacional.org.mx]', '', 1, '', 0, '', '', 'Contadero Cuajimalca', ''),
(48, 20, 1, 'Marcelo Bravo', 'joseramon@gvseguros.com', '', 0, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'joseramon@gvseguros.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(49, 20, 1, 'Marcos Castillo Puente', 'marcos.castillo@live.com.mx', '81230667', 0, 0, 1, 'Nuevo León', 66220, '', 'Anastacio Bustamante 2612', 'Setmex S.A. de C.V', 'SET010213LZ8', 'marcos.castillo@live.com.mx', '', 1, '', 0, '', '', 'Benito Juaréz ', ''),
(50, 20, 1, 'Margarita Romero', '', '', 0, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', 'Del Valle', ''),
(51, 20, 1, 'Maria Adelina Patron', 'guanajuato@hotmail.com', '01 46 46 48 00 39', 0, 0, 1, 'Nuevo León', 66220, '', ' Pánuco 922   ', 'Setmex S.A. de C.V', 'SET010213LZ8', 'guanajuato@hotmail.com', '', 1, '', 0, '', '', 'Bella Vista', ''),
(52, 20, 1, 'Maria Rebeca Alvarez Santana', 'rebecaalvarez01@prodigy.net.mx', '18064215', 15, 0, 1, 'Nuevo León', 66220, '', 'Hacienda de Santa Martha 4418', 'Setmex S.A. de C.V', 'SET010213LZ8', 'REBECAALVAREZ01@PRODIGY.NET.MX', '', 1, '', 0, '', '', 'Pedregal Cumbres', ''),
(53, 20, 1, 'Marina Herrera', '', '01 9999 44 32 09', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', 'Del Valle', ''),
(54, 20, 1, 'Martha Silvia Alvarez', 'eemguaymas@hotmail.com', '01 622 221 15 34', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Pez Espada No. 5  ', 'Setmex S.A. de C.V', 'SET010213LZ8', 'eemguaymas@hotmail.com', '', 1, '', 0, '', '', 'Villa de Miramar', ''),
(55, 20, 1, 'Mauricio Martinez', '', '', 20, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', 'Del Valle', ''),
(56, 20, 1, 'Mireya Solis', 'agenciasasatej@hotmail.com', '01 844 410 22 43', 0, 0, 1, 'Nuevo León', 66220, 'Zona Centro', 'Universidad Autónoma de Coahuila ', 'Setmex S.A. de C.V', 'SET010213LZ8', 'agenciasasatej@hotmail.com', '', 1, '', 0, '', '', 'Hidalgo sur 43', ''),
(57, 20, 1, 'Nora A. Rodriguez De Guzman', 'obregon@eem.org.mx', '', 0, 15, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'obregon@eem.org.mx', '', 1, '', 0, '', '', 'Del Valle', ''),
(58, 20, 1, 'Setmex Mexico', '', '55 5616 5815 ', 0, 0, 1, 'Nuevo León', 66220, '', 'Pedro Henriquez Ureña #540-C 402, ', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', 'Los Reyes Coyoacán', ''),
(59, 20, 1, 'Setmex Queretaro', 'soportequeretaro@eem.org.mx', '01 442 2452659', 10, 0, 1, 'Nuevo León', 66220, '', 'AVE, Fray Luis De León No. 8051 INT. 3B', 'Setmex S.A. de C.V', 'SET010213LZ8', 'soportequeretaro@eem.org.mx', '', 1, '', 0, '', '', '', ''),
(60, 20, 1, 'Raul Fernando Jesus Bustamante Tejada', '', '', 15, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', 'Del Valle', ''),
(61, 20, 1, 'Rodolfo Alanis', '', '', 20, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', 'Del Valle', ''),
(62, 20, 1, 'Setmex Ventas', 'info@setmex.com', '', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'info@setmex.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(63, 20, 1, 'Tity Stewart', '', '', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Caborca', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', '', ''),
(64, 20, 1, 'Web', '', '', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', 'Del Valle', ''),
(65, 20, 1, 'Rocio Farias De Mendez', '', '(351)512 46 91', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', 'Del Valle', ''),
(66, 20, 1, 'Miguel Angel Campos', '', '', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'Info@setmex.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(67, 20, 1, 'UANL', '', '', 0, 10, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'info@setmex.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(68, 20, 1, 'Oficina UNAM', '', '', 0, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'unam', '', 1, '', 0, '', '', 'Del Valle', ''),
(69, 20, 1, 'Universidad Iberoamericana', '', '', 0, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'info@setmex.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(70, 20, 1, 'ITESM', '', '', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'info@setmex.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(71, 20, 1, 'Universidad La Salle', '', '', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'info@setmex.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(72, 20, 1, 'Eladio Dones Rangel', 'eladiodones@yahoo.com.mx', '811 209 24 13', 15, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'eladiodones@yahoo.com.mx', '', 1, '', 0, '', '', 'Del Valle', ''),
(73, 20, 1, 'Universidad de Guanajuato', '', '', 0, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'mnino@setmex.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(74, 20, 1, 'Lorena Rivera Mata', 'rivera_seguros@hotmail.com', '81 1476 2062', 10, 0, 1, 'Nuevo León', 0, 'Monterrey', '', 'Setmex S.A. de C.V', 'SET010213LZ8', 'rivera_seguros@hotmail.com', '', 1, '', 0, '', '', '', ''),
(75, 20, 1, 'Ana Cecilia Hernandez Delgadillo', '', '', 10, 0, 1, 'Zacatecas', 98050, 'Zacatecas', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'achernandez@eem.org.mx', '', 1, '', 0, '', '', 'Del Valle', ''),
(76, 20, 1, 'Setmex Oficina León', '', '', 0, 0, 1, 'Nuevo León', 0, 'León', 'Circunvalación Pte. #1154', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', 'Jardines del Moral', ''),
(77, 20, 1, 'Emilia Elizabeth Cancino', 'milycancino@icloud.com', '', 10, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'milycancino@icloud.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(78, 20, 1, 'Patricio Rodriguez Gomez', '', '', 0, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', '', '', 1, '', 0, '', '', 'Del Valle', ''),
(79, 20, 1, 'Franquicia Torreon', '', '871 11 89543', 10, 0, 1, 'COAHUILA', 66220, '', 'Blvd. Sendero No. 400 ', 'Setmex S.A. de C.V', 'SET010213LZ8', 'raguero@estudiantesembajadores.com', '', 1, '', 0, '', '', '', ''),
(80, 20, 1, 'María Guadalupe Navarro', 'guadalupenavarroyasoc@prodigy.net.mx', '833 215 54 15', 10, 0, 1, 'TAMAULIPAS', 89440, 'Cd. Madero', 'Calle 7 #1010 ', 'Setmex S.A. de C.V', 'SET010213LZ8', 'guadalupenavarroyasoc@prodigy.net.mx', '', 1, '', 0, '', '', 'Jardin 20 de Noviembre', ''),
(81, 20, 1, 'Lorena A. Zubillaga O', 'lzubilla2@hotmail.com', 'cancun', 10, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'lzubilla2@hotmail.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(82, 20, 1, 'Hugo Palafox Terrazas', 'hugopfx78@hotmail.com', '8181331447', 10, 0, 1, 'Nuevo León', 66024, 'San Pedro Garza García', 'Del Valle No. 611', 'Setmex S.A. de C.V', 'SET010213LZ8', 'hugopfx78@hotmail.com', '', 1, '', 0, '', '', 'Las Lomas Sector Jardínes', ''),
(83, 20, 1, 'Rito Torre Quiroga Asesor Profesional de Seguros', 'rito.torres@hotmail.com', '8801704  83574635', 10, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'rito.torres@hotmail.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(84, 20, 1, 'Franquicia Chihuahua', '', '614 156 75 72', 10, 0, 1, 'Chihuahua', 66220, 'Chihuahua', 'Chihuahua', 'Setmex S.A. de C.V', 'SET010213LZ8', 'Graciela Durán <gduran@estudiantesembajadores.com>', '', 1, '', 0, '', '', '', ''),
(85, 20, 1, 'Franquicia Veracruz', '', '229 922 55 68', 10, 0, 1, 'Veracruz', 94299, '', 'Ave. Ruiz Cortinez 1360 local 5 Plaza Tiburón', 'Setmex S.A. de C.V', 'SET010213LZ8', 'Esther Zamudio <ezamudio@estudiantesembajadores.com>', '', 1, '', 0, '', '', 'Costa de Oro Boca del Río', ''),
(86, 20, 1, 'Yolana Martinez Mayar', 'ymartinez@mmasesores.com.mx', '811 911 11 33', 10, 0, 1, 'Nuevo León', 64020, 'Monterrey', 'Zapopan 100', 'Setmex S.A. de C.V', 'SET010213LZ8', 'ymartinez@mmasesores.com.mx', '', 1, '', 0, '', '', 'Mitras Sur', ''),
(87, 20, 1, 'Studio Travel Lufhansa Studio City', 'blanca@studiotravel.om.mx', '81 8004 5160 ', 20, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Moll del Valle, Calzada del Valle No. 400 local N6-06A', 'Setmex S.A. de C.V', 'SET010213LZ8', 'blanca@studiotravel.om.mx', '', 1, '', 0, '', '', 'Del Valle', ''),
(88, 20, 1, 'Logras Consultores', 'lograsconsultores@gmail.com', '999 287 4525 ', 15, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'lograsconsultores@gmail.com', '', 1, '', 0, '', '', 'Del Valle', ''),
(89, 20, 1, 'Arq. Ma. Angeles Pagola', 'himmelasesores@gmail.coM', '8180 92 6936', 10, 0, 1, 'Nuevo León', 66220, '', 'Rio Orinoco # 101 - 3', 'Setmex S.A. de C.V', 'SET010213LZ8', 'himmelasesores@gmail.coM', '', 1, '', 0, '', '', 'Del Valle', ''),
(90, 27, 1, 'Oficina UP', '', '', 0, 10, 1, 'Guadalajara', 0, '', '', 'Universidad Panamericana', '', '', '', 1, '', 0, '', '', '', ''),
(91, 28, 1, 'Alumno Monterrey', 'itesm@itesm.mx', '', 0, 10, 1, 'Nuevo León', 0, 'Monterrey', 'Ave. Eugenio Garza Sada 2501 sur', 'Itesm', 'ITE 430714 KI0 ', 'itesm@itesm.mx', '', 1, '', 0, '', '', 'Tecnológico', ''),
(92, 28, 1, 'Alumno CEM', 'cem@itesm.mx', '', 0, 10, 1, 'Nuevo León', 0, '', 'Ave. Eugenio Garza Sada 2501 sur', 'Itesm', 'ITE 430714 KI0 ', 'cem@itesm.mx', '', 1, '', 0, '', '', 'Tecnológico', ''),
(93, 30, 1, 'Javier Garcia Casanova', '', '', 15, 0, 1, 'Tamaulipas', 0, 'Tampico', '', 'Javier Garcia Casanova', '', '', '', 1, '', 0, '', '', '', ''),
(94, 31, 1, 'Jazmin Ramirez Zapata', 'cesar.vazquez@aseincap.com.mx', '', 15, 0, 1, 'Edo. México', 6030, 'Delegación Cuauhtemoc', 'Madrid 21-314 ', 'Aseincap, Agente De Seguros Y Fianzas, Sa De Cv', '', 'cesar.vazquez@aseincap.com.mx', '', 1, '', 0, '', '', 'Col. Tabacalera', ''),
(95, 32, 1, 'Mercedes Ponce Martínez', 'mercedes.ponce@protegeseguros.com', '', 15, 0, 1, 'Puebla', 66220, '', 'PUEBLA', 'Protege Seguros', '', 'Mercedes Ponce [mercedes.ponce@protegeseguros.com]', '', 1, '', 0, '', '', '', ''),
(96, 36, 1, 'Fernando Quiroz Madrigal', 'fquirozmadrigal@terra.com.mx', '55 5651 2173 ', 20, 0, 1, 'Edo. México', 10400, 'Del. Magdalena Contreras', 'Av. Mexico # 700 Oficina 407 ', 'Fernando Quiroz Madrigal', 'QUMF580628QB3', 'fquirozmadrigal@terra.com.mx', '', 1, '', 0, '', '', 'San Jeronimo Lidic', ''),
(97, 37, 1, 'Agente Capital', 'cobranza@agentecapital.com', '800 987 2201', 17, 0, 1, 'Yucatán', 97127, 'Mérida', 'Calle 27 # 64c', 'G.a.p', 'GAS061204RY4', 'cobranza@agentecapital.com', '', 1, '', 0, '', '', 'Buenavista', ''),
(98, 38, 1, 'Victor Manuel de León Santana', 'seguros@victordeleon.com.mx', '474 742 5025', 20, 0, 1, 'Jalisco', 47440, 'Lagos de Moreno', 'Luis Moreno 520', 'De Leon Asesores En Seguros Y Fianzas', 'LESV660107R17', 'seguros@victordeleon.com.mx', '', 1, '', 0, '', '', 'Centro Lagos de Moreno', ''),
(99, 39, 1, 'Luis Carlos Elizondo Leal', 'lcel1@hotmail.com', '83 87 30 27', 15, 0, 1, 'Nuevo León', 64840, 'Monterrey', 'Luis Elizondo 400 L9', 'Mercurio Viajes Sa De Cv', 'MV1970310B8A', 'lcel1@hotmail.com', '', 1, '', 0, '', '', 'Altavista', ''),
(100, 40, 1, 'LIC. Placido Zapata Tamez', 'placidozapata@yahoo.com', '811044 54 58', 15, 0, 1, 'Nuevo León', 64630, 'Monterrey', 'AV. Puerta del Sol 821', 'Invax', 'INVAX121212', 'placidozapata@yahoo.com', '', 1, '', 0, '', '', 'San Jerónimo', ''),
(101, 42, 1, 'Jose Manuel Ponce de Leon Cubillas', 'jmponcelc@prodigy.net.mx', '99 99 26 01 88 ', 10, 0, 1, 'Yucatán', 97000, 'Mérida', 'Calle 22 # 108 Int. 8  entre 29 Y 31', 'Jose Manuel Ponce De Leon Cubillas ', '', 'jmponcelc@prodigy.net.mx', '', 1, '', 0, '', '', 'México Mérida', ''),
(102, 43, 1, 'Carlos Ignacio Colin Wood', 'ccolin@wood.com.mx', '81 83 46 47 46', 15, 0, 1, 'Nuevo León', 64000, 'Monterrey', 'Treviño # 2125 Pte', 'Wood Corredores De Seguros', '', 'ccolin@wood.com.mx', '', 1, '', 0, '', '', 'Centro de Monterrey', ''),
(103, 44, 1, 'Eduardo Esparza Patiño', 'eduardoesparza@vidatranquila.com', '55 27 49 16', 15, 0, 1, 'Edo. México', 11410, 'Edo. México', 'Lago Hutron # 18', 'Eduardo Esparza Pati;O', '', 'eduardoesparza@vidatranquila.com', '', 1, '', 0, '', '', 'Tacuba', ''),
(104, 45, 1, 'Juan Ricardo Castillo Morales', 'ricardo4786@hotmail.com', '8180293762', 10, 0, 1, 'Nuevo León', 64320, 'Monterrey', 'C. Valdeces 617', 'Juan Ricardo Castillo Morales', 'CAMJ400624', 'ricardo4786@hotmail.com', '', 1, '', 0, '', '', 'Mitras Norte', ''),
(105, 47, 1, 'José Antonio Valero Elizondo', 'ovaleroe@yahoo.com.mx', '81 83 63 30 10', 15, 0, 1, 'Nuevo León', 64630, 'Monterrey', 'Real San José 123', 'Jose Antonio Valero Elizondo', 'VAEA751186626', 'ovaleroe@yahoo.com.mx', '', 1, '', 0, '', '', 'Real de San Jerónimo', ''),
(106, 48, 1, 'Gerardo A. Rocha Cerda', 'ventas@rochay asociados.net', '83470405', 15, 0, 1, 'Nuevo León', 66430, 'Monterrey', 'Lerma 2030', 'Rocha Y Asociados', 'ROCG530413GT3', 'ventas@rochay asociados.net', '', 1, '', 0, '', '', 'Miras Centro', ''),
(107, 49, 1, 'Miguel Angel Ramirez Macias', 'mramirez@phyxius.com.mx', '8111068273', 10, 10, 1, 'Nuevo León', 66350, '', 'LA Republica 112/15', 'Miguel Angel Ramirez Macias', '', 'mramirez@phyxius.com.mx', '', 1, '', 0, '', '', 'Centro', ''),
(108, 51, 1, 'Jessica Karina Martínez Alviar', 'jmartinez@arven.mx', '81 2136 9191  EXT, 4646', 15, 0, 1, 'Nuevo León', 64740, 'Guadalupe', 'Río Lerma 419', 'Arven', '', 'jmartinez@arven.mx', '', 1, '', 0, '', '', '', ''),
(109, 52, 1, 'Sarahi Montes', 'servicios@avanceseguros.com.mx', '(844) 4167788 ', 15, 0, 1, 'Coachuila', 25270, 'SALTILLO', 'Tamaulipas #310', 'Grupo Avance Asesoria En Seguros', 'ROGD760123DPA', 'Sarahi Montes <servicios@avanceseguros.com.mx>', '', 1, '', 0, '', '', 'Republica Ote.', ''),
(110, 53, 1, 'Jose Alonso Cortina Marroquín', 'karlagzzp@gmail.com', '2470 0780 ', 15, 0, 1, 'Nuevo León', 64890, 'Monterrey', 'Caminon de la Rinconada 4648', 'Cortina Asesores', 'COMA610326JC8', 'karlagzzp@gmail.com', '', 1, '', 0, '', '', 'Cortijo del Río', ''),
(111, 54, 1, 'Cristie Esperanza Sandoval Montalbo', 'sandovalsegurosmty@gmail.com', '8118809660', 10, 0, 1, 'Nuevo León', 66220, 'Monterrey', 'Amonio Plaza 2745', 'Cristie Esperanza Sandoval Montalbo', '', 'Sandoval Seguros <sandovalsegurosmty@gmail.com>', '', 1, '', 0, '', '', 'Altavista Sur', ''),
(112, 56, 1, 'José Hector Zamora Cordero', 'hzamora@garanteasesores.com.mx', '81 82 42 70 61  ', 15, 0, 1, 'Nuevo León', 64960, 'Monterrey', 'Plaza Panoramica 102 B', 'Jose Hector Zamora Cordero', 'ZACH631002KQ3', 'hzamora@garanteasesores.com.mx', '', 1, '', 0, '', '', 'Satelite', ''),
(113, 56, 1, 'Jose Luis Prieto Fuentes', '', '', 10, 0, 1, 'Nuevo León', 66245, '', 'Agente Asesor de Seguros Por Cuenta Propia', 'José Luis Prieto Fuentes', '', '', '', 1, '', 0, '', '', '', ''),
(114, 58, 1, 'Alberto Escamilla Flores', 'escamillarodriguez@prodigy.net.mx', '8183336915', 10, 0, 1, 'Nuevo León', 64610, '', 'Terranova # 433', 'Alberto Escamilla Flores', 'EAFA611213KJ7', 'escamillarodriguez@prodigy.net.mx', '', 1, '', 0, '', '', 'Cumbres', ''),
(115, 59, 1, 'Thelma Margarita Gonzalez de la Fuente', 'asistenteadministrativo@phyxius.com.mx', '811 68 60 801', 10, 0, 1, 'Nuevo León', 66220, 'San Pedro Garza García', 'Bizancio 600', 'Thelma Margarita Gonzalez De La Fuente', 'GOFT590127TC6', 'asistenteadministrativo@phyxius.com.mx', '', 1, '', 0, '', '', 'Jard S Agustin Decapolis y Corintio', ''),
(116, 1, 1, 'Operadora Sierra Madre', '', '', 0, 0, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(117, 5, 1, 'CCI Mex', '', '', 0, 0, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(118, 11, 1, 'Flores Diaz Seguros', '', '', 15, 0, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(119, 12, 1, 'Mariana', '', '', 0, 0, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(120, 13, 1, 'Ing. Juan Villarreal Montemayor', '', '', 0, 0, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(121, 16, 1, 'Navarro Y Asociados', '', '', 20, 0, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(122, 18, 1, 'Oficina', '', '', 0, 10, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(123, 21, 1, 'Oficina', '', '', 0, 10, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(124, 22, 1, 'Oficina', '', '', 0, 10, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(125, 23, 1, 'Oficina', '', '', 0, 10, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(126, 24, 1, 'Oficina', '', '', 0, 0, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(127, 25, 1, 'Oficina', '', '', 0, 15, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(128, 26, 1, 'Oficina', '', '', 5, 7, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(129, 31, 1, 'Jazmín Ramírez Zapata', '', '', 15, 0, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(130, 33, 1, 'Victor F. García', '', '', 14, 0, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(131, 34, 1, 'José Hector Zamora', '', '', 10, 0, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(132, 35, 1, 'Seimos Y Asociados', '', '', 0, 0, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(133, 41, 1, 'Emilia Elizabeth Cancino Esponda', '', '', 0, 0, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(134, 46, 1, 'Alberto Escamilla Flores', '', '', 10, 0, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(135, 50, 1, 'Maria Lizeth Guadalupe Araujo Vega', '', '', 0, 0, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(136, 55, 1, 'Ytandehui Sosa Ibarra', '', '', 0, 15, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', ''),
(137, 57, 1, 'Gabriel Olivas Quiróz', '', '', 15, 0, 1, '', 0, '', '', '', '', '', '', 1, '', 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `id` int(11) NOT NULL,
  `id_estado` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `ciudad` varchar(127) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `id_subagente` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `id_tipo_cliente` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `fecha_registro_prospecto` date NOT NULL,
  `fecha_registro_asegurado` date NOT NULL,
  `nombres` varchar(128) NOT NULL,
  `apellidos` varchar(128) NOT NULL,
  `nombre_completo` varchar(255) NOT NULL,
  `correo_electronico` varchar(255) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  `telefono` varchar(32) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `id_fuente_referencia` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `estado` varchar(64) NOT NULL,
  `cp` int(11) NOT NULL,
  `ciudad` text NOT NULL,
  `calle_numero` varchar(511) NOT NULL,
  `razon_social` varchar(255) NOT NULL,
  `rfc` varchar(255) NOT NULL,
  `fac_correo_electronico` varchar(255) NOT NULL,
  `fac_telefono` varchar(32) NOT NULL,
  `fac_id_pais` int(11) NOT NULL,
  `fac_estado` varchar(64) NOT NULL,
  `fac_cp` int(11) NOT NULL,
  `fac_ciudad` text NOT NULL,
  `fac_calle_numero` varchar(511) NOT NULL,
  `colonia` varchar(127) NOT NULL,
  `fac_colonia` varchar(127) NOT NULL,
  `id_cotizacion` int(11) NOT NULL DEFAULT '0',
  `id_cliente` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `id_subagente`, `id_rol`, `id_tipo_cliente`, `activo_inactivo`, `fecha_registro_prospecto`, `fecha_registro_asegurado`, `nombres`, `apellidos`, `nombre_completo`, `correo_electronico`, `contrasena`, `telefono`, `fecha_nacimiento`, `id_fuente_referencia`, `id_pais`, `estado`, `cp`, `ciudad`, `calle_numero`, `razon_social`, `rfc`, `fac_correo_electronico`, `fac_telefono`, `fac_id_pais`, `fac_estado`, `fac_cp`, `fac_ciudad`, `fac_calle_numero`, `colonia`, `fac_colonia`, `id_cotizacion`, `id_cliente`) VALUES
(9, 1, 0, 1, 0, '2018-09-03', '2018-09-04', 'Prueba7', 'Apellidos', 'Prueba7 Apellidos', 'prueba7@a.com', '', '483928492', '2005-06-07', 4, 1, 'Campeche', 64103, 'Monterrey', 'Calle #32131', '', '', '', '', 0, '0', 0, '', '', 'Rómulo Lozano Morales', '', 13, ''),
(11, 1, 0, 2, 0, '2018-09-07', '2018-09-14', 'Prueba21', 'Apellidso', 'Prueba21 Apellidso', 'prueba21@correo.com', '', '48329483204', '1947-08-19', 9, 1, 'Nuevo León', 64104, 'Monterrey', '43242', '', '', '', '', 0, '', 0, '', '', 'San Bernabé III', '', 32, ''),
(12, 1, 0, 2, 0, '2018-09-10', '2018-09-20', 'Prueba22', 'Apellidos', 'Prueba22 Apellidos', 'prueba22@gmail.com', '', '4839248024', '2018-09-19', 4, 1, 'Nuevo León', 64103, 'Monterrey', '', '', '', '', '', 0, '', 0, '', '', 'Fray Servando Teresa de Mier', '', 37, ''),
(13, 1, 0, 2, 0, '2018-09-03', '2018-09-19', 'Prueba8', 'Apellidos', 'Prueba8 Apellidos', 'prueba8@a.com', '', '483928492', '2005-06-07', 4, 1, 'Campeche', 64103, 'Monterrey', 'Calle #32131', '', '', '', '', 0, '0', 0, '', '', 'Rómulo Lozano Morales', '', 30, ''),
(14, 1, 0, 2, 0, '2018-09-03', '2018-09-19', 'Prueba9', 'Apellidos', 'Prueba9 Apellidos', 'prueba9@a.com', '', '483928492', '2005-06-07', 4, 1, 'Campeche', 64103, 'Monterrey', 'Calle #32131', '', '', '', '', 0, '0', 0, '', '', 'Rómulo Lozano Morales', '', 40, ''),
(15, 1, 0, 1, 0, '2018-09-03', '2018-09-04', 'Prueba23', 'Apellidos', 'Prueba23 Apellidos', 'prueba23@a.com', '', '483928492', '2005-06-07', 4, 1, 'Campeche', 64103, 'Monterrey', 'Calle #32131', '', '', '', '', 0, '0', 0, '', '', 'Rómulo Lozano Morales', '', 13, ''),
(16, 1, 0, 1, 0, '2018-09-03', '2018-09-04', 'Prueba24', 'Apellidos', 'Prueba24 Apellidos', 'prueba24@a.com', '', '483928492', '2005-06-07', 4, 1, 'Campeche', 64103, 'Monterrey', 'Calle #32131', '', '', '', '', 0, '0', 0, '', '', 'Rómulo Lozano Morales', '', 13, ''),
(17, 1, 0, 1, 0, '2018-09-03', '2018-09-04', 'Prueba25', 'Apellidos', 'Prueba25 Apellidos', 'prueba25@a.com', '', '483928492', '2005-06-07', 4, 1, 'Campeche', 64103, 'Monterrey', 'Calle #32131', '', '', '', '', 0, '0', 0, '', '', 'Rómulo Lozano Morales', '', 13, ''),
(18, 1, 0, 1, 0, '2018-09-03', '2018-09-04', 'Prueba26', 'Apellidos', 'Prueba26 Apellidos', 'prueba26@a.com', '', '483928492', '2005-06-07', 4, 1, 'Campeche', 64103, 'Monterrey', 'Calle #32131', '', '', '', '', 0, '0', 0, '', '', 'Rómulo Lozano Morales', '', 13, ''),
(19, 1, 0, 1, 0, '2018-09-12', '0000-00-00', '', '', 'null null', '', '', '', '0000-00-00', 0, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', '', 0, ''),
(20, 1, 0, 1, 0, '2018-09-12', '0000-00-00', 'Prueba28', 'Apellidos', 'Prueba28 Apellidos', 'p28@a.com', '', '95438095230', '1997-11-12', 3, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', '', 0, ''),
(21, 1, 0, 1, 0, '2018-09-12', '0000-00-00', 'Prueba29', 'Apellidos', 'Prueba29 Apellidos', 'p29@a.com', '', '95438095230', '1997-11-12', 3, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', '', 0, ''),
(22, 1, 0, 2, 0, '2018-09-12', '2018-09-19', 'Prueba30', 'Apellidos', 'Prueba30 Apellidos', 'p30@a.com', '', '95438095230', '1997-11-12', 3, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', '', 25, ''),
(23, 1, 0, 1, 0, '2018-09-12', '0000-00-00', 'Prueba31', 'Apellidos', 'Prueba31 Apellidos', 'p31@a.com', '', '95438095230', '1997-11-12', 3, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', '', 0, ''),
(24, 1, 0, 1, 0, '2018-09-12', '0000-00-00', 'Prueba32', 'Apellidos', 'Prueba32 Apellidos', 'ecantu@estudiantesembajadores.com', '', '95438095230', '1997-11-12', 3, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', '', 0, ''),
(25, 1, 0, 1, 0, '2018-09-12', '0000-00-00', 'Prueba33', 'Apellidos', 'Prueba33 Apellidos', 'p33@a.com', '', '95438095230', '1997-11-12', 3, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', '', 0, ''),
(26, 1, 0, 1, 0, '2018-09-13', '0000-00-00', '', '', 'null null', '', '', '', '0000-00-00', 0, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', '', 0, ''),
(27, 1, 0, 1, 0, '2018-09-14', '0000-00-00', '', '', 'null null', '', '', '', '0000-00-00', 0, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', '', 0, ''),
(28, 1, 0, 0, 0, '2018-09-17', '0000-00-00', 'Edgar', 'Cantú', 'Edgar Cantú', 'corre@correo.com', '', '874392753', '1998-07-01', 3, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', '', 0, 'CanEdg19980701'),
(29, 1, 0, 2, 0, '2018-09-17', '2018-09-17', 'Edgar', 'Cantú', 'Edgar Cantú', 'a@a.com', '', '843943432', '1945-10-09', 11, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', '', 34, 'CANEDG19451009'),
(30, 1, 0, 2, 0, '2018-09-18', '2018-09-18', 'Prueba72', 'Apellidos', 'Prueba72 Apellidos', 'ap@cor.com', '', '84395803', '1994-07-14', 3, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', '', 36, 'APEPRU19940714');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobertura`
--

CREATE TABLE `cobertura` (
  `id` int(11) NOT NULL,
  `id_tipo_poliza` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `cobertura` int(11) NOT NULL,
  `dias` int(11) NOT NULL,
  `precio` decimal(11,2) NOT NULL,
  `costo` decimal(11,2) NOT NULL,
  `costo_final` decimal(11,2) NOT NULL,
  `codigo` varchar(127) NOT NULL,
  `margen` decimal(11,2) NOT NULL,
  `margen_porcentaje` decimal(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cobertura`
--

INSERT INTO `cobertura` (`id`, `id_tipo_poliza`, `activo_inactivo`, `cobertura`, `dias`, `precio`, `costo`, `costo_final`, `codigo`, `margen`, `margen_porcentaje`) VALUES
(1, 1, 1, 1, 1, '7.50', '2.12', '2.12', 'EViajero 1D1-100', '5.38', '71.73'),
(2, 1, 1, 2, 2, '14.50', '2.12', '4.24', 'EViajero 2D1-100', '10.26', '70.76'),
(3, 1, 1, 3, 3, '21.50', '2.12', '6.36', 'EViajero 3D1-100', '15.14', '70.42'),
(4, 1, 1, 4, 4, '28.50', '2.12', '8.48', 'EViajero 4D1-100', '20.02', '70.25'),
(5, 1, 1, 5, 5, '35.50', '2.12', '10.60', 'EViajero 5D1-100', '24.90', '70.14'),
(6, 1, 1, 6, 6, '42.50', '2.12', '12.72', 'EViajero 6D1-100', '29.78', '70.07'),
(7, 1, 1, 7, 7, '50.00', '2.12', '14.84', 'EViajero 7D1-100', '35.16', '70.32'),
(8, 1, 1, 8, 8, '57.00', '2.12', '16.96', 'EViajero 8D1-100', '40.04', '70.25'),
(9, 1, 1, 9, 9, '64.00', '2.12', '19.08', 'EViajero 9D1-100', '44.92', '70.19'),
(10, 2, 1, 1, 1, '7.50', '2.12', '2.12', 'EViajero 1D2-100', '5.38', '71.73'),
(11, 2, 1, 2, 2, '14.50', '2.12', '4.24', 'EViajero 2D2-100', '10.26', '70.76'),
(12, 2, 1, 3, 3, '21.50', '2.12', '6.36', 'EViajero 3D2-100', '15.14', '70.42'),
(13, 2, 1, 4, 4, '28.50', '2.12', '8.48', 'EViajero 4D2-100', '20.02', '70.25'),
(14, 2, 1, 5, 5, '35.50', '2.12', '10.60', 'EViajero 5D2-100', '24.90', '70.14'),
(15, 2, 1, 6, 6, '42.50', '2.12', '12.72', 'EViajero 2D2-100', '29.78', '70.07'),
(16, 2, 1, 7, 7, '50.00', '2.12', '14.84', 'EViajero 2D2-100', '35.16', '70.32'),
(17, 2, 1, 8, 8, '57.00', '2.12', '16.96', 'EViajero 2D2-100', '40.04', '70.25'),
(18, 2, 1, 9, 9, '64.00', '2.12', '19.08', 'EViajero 9D2-100', '44.92', '70.19'),
(19, 3, 1, 2, 10, '95.00', '2.12', '21.20', 'EViajero 2S1-100', '73.80', '77.68'),
(20, 3, 1, 3, 21, '135.00', '2.12', '44.52', 'EViajero 3S1-100', '90.48', '67.02'),
(21, 3, 1, 4, 28, '168.00', '2.12', '59.36', 'EViajero 4S1-100', '108.64', '64.67'),
(22, 3, 1, 5, 35, '192.00', '2.12', '74.20', 'EViajero 5S1-100', '117.80', '61.35'),
(23, 3, 1, 6, 42, '210.00', '2.12', '89.04', 'EViajero 6S1-100', '120.96', '57.60'),
(24, 3, 1, 7, 49, '230.00', '2.12', '103.88', 'EViajero 7S1-100', '126.12', '54.83'),
(25, 3, 1, 8, 56, '260.00', '2.12', '118.72', 'EViajero 8S1-100', '141.28', '54.34'),
(26, 3, 1, 9, 63, '295.00', '2.12', '133.56', 'EViajero 9S1-100', '161.44', '54.73'),
(27, 4, 1, 2, 10, '95.00', '2.12', '21.20', 'EViajero 2S2-100', '73.80', '77.68'),
(28, 4, 1, 3, 21, '135.00', '2.12', '44.52', 'EViajero 3S2-100', '90.48', '67.02'),
(29, 4, 1, 4, 28, '168.00', '2.12', '59.36', 'EViajero 4S2-100', '108.64', '64.67'),
(30, 4, 1, 5, 35, '192.00', '2.12', '74.20', 'EViajero 5S2-100', '117.80', '61.35'),
(31, 4, 1, 6, 42, '210.00', '2.12', '89.04', 'EViajero 6S2-100', '120.96', '57.60'),
(32, 4, 1, 7, 49, '230.00', '2.12', '103.88', 'EViajero 7S2-100', '126.12', '54.83'),
(33, 4, 1, 8, 56, '260.00', '2.12', '118.72', 'EViajero 8S2-100', '141.28', '54.34'),
(34, 4, 1, 9, 63, '295.00', '2.12', '133.56', 'EViajero 9S2-100', '161.44', '54.73'),
(35, 5, 1, 3, 64, '320.00', '2.12', '135.68', 'EViajero 3M1-100', '184.32', '57.60'),
(36, 5, 1, 4, 120, '410.00', '2.12', '254.40', 'EViajero 4M1-100', '155.60', '37.95'),
(37, 5, 1, 5, 150, '510.00', '2.12', '318.00', 'EViajero 5M1-100', '192.00', '37.65'),
(38, 5, 1, 6, 180, '610.00', '2.12', '381.60', 'EViajero 6M1-100', '228.40', '37.44'),
(39, 5, 1, 7, 210, '710.00', '2.12', '445.20', 'EViajero 7M1-100', '264.80', '37.30'),
(40, 5, 1, 8, 240, '810.00', '2.12', '508.80', 'EViajero 8M1-100', '301.20', '37.19'),
(41, 5, 1, 9, 270, '900.00', '2.12', '572.40', 'EViajero 9M1-100', '327.60', '36.40'),
(42, 5, 1, 10, 300, '985.00', '2.12', '636.00', 'EViajero 10M1-100', '349.00', '35.43'),
(43, 5, 1, 11, 330, '1080.00', '2.12', '699.60', 'EViajero 11M1-100', '380.40', '35.22'),
(44, 5, 1, 12, 360, '1200.00', '2.12', '763.20', 'EViajero 12M1-100', '436.80', '36.40'),
(45, 6, 1, 3, 64, '320.00', '2.12', '135.68', 'EViajero 3M2-100', '184.32', '57.60'),
(46, 6, 1, 4, 120, '410.00', '2.12', '254.40', 'EViajero 4M2-100', '155.60', '37.95'),
(47, 6, 1, 5, 150, '510.00', '2.12', '318.00', 'EViajero 5M2-100', '192.00', '37.65'),
(48, 6, 1, 6, 180, '610.00', '2.12', '381.60', 'EViajero 6M2-100', '228.40', '37.44'),
(49, 6, 1, 7, 210, '710.00', '2.12', '445.20', 'EViajero 7M2-100', '264.80', '37.30'),
(50, 6, 1, 8, 240, '810.00', '2.12', '508.80', 'EViajero 8M2-100', '301.20', '37.19'),
(51, 6, 1, 9, 270, '900.00', '2.12', '572.40', 'EViajero 9M2-100', '327.60', '36.40'),
(52, 6, 1, 10, 300, '985.00', '2.12', '636.00', 'EViajero 10M2-100', '349.00', '35.43'),
(53, 6, 1, 11, 330, '1080.00', '2.12', '699.60', 'EViajero 11M2-100', '380.40', '35.22'),
(54, 6, 1, 12, 360, '1200.00', '2.12', '763.20', 'EViajero 12M2-100', '436.80', '36.40'),
(55, 7, 1, 1, 1, '6.50', '1.93', '1.93', 'EViajero 1D1-100', '4.57', '70.31'),
(56, 7, 1, 2, 2, '13.00', '1.93', '3.86', 'EViajero 2D1-100', '9.14', '70.31'),
(57, 7, 1, 3, 3, '19.50', '1.93', '5.79', 'EViajero 3D1-100', '13.71', '70.31'),
(58, 7, 1, 4, 4, '25.00', '1.93', '7.72', 'EViajero 4D1-100', '17.28', '69.12'),
(59, 7, 1, 5, 5, '32.50', '1.93', '9.65', 'EViajero 5D1-100', '22.85', '70.31'),
(60, 7, 1, 6, 6, '39.00', '1.93', '11.58', 'EViajero 6D1-100', '27.42', '70.31'),
(61, 7, 1, 7, 7, '45.00', '1.93', '13.51', 'EViajero 7D1-100', '31.49', '69.98'),
(62, 7, 1, 8, 8, '52.00', '1.93', '15.44', 'EViajero 8D1-100', '36.56', '70.31'),
(63, 7, 1, 9, 9, '58.50', '1.93', '17.37', 'EViajero 9D1-100', '41.13', '70.31'),
(64, 8, 1, 1, 1, '6.50', '1.93', '1.93', 'EViajero 1D2-100', '4.57', '70.31'),
(65, 8, 1, 2, 2, '13.00', '1.93', '3.86', 'EViajero 2D2-100', '9.14', '70.31'),
(66, 8, 1, 3, 3, '19.50', '1.93', '5.79', 'EViajero 3D2-100', '13.71', '70.31'),
(67, 8, 1, 4, 4, '25.00', '1.93', '7.72', 'EViajero 4D2-100', '17.28', '69.12'),
(68, 8, 1, 5, 5, '32.50', '1.93', '9.65', 'EViajero 5D2-100', '22.85', '70.31'),
(69, 8, 1, 6, 6, '39.00', '1.93', '11.58', 'EViajero 2D2-100', '27.42', '70.31'),
(70, 8, 1, 7, 7, '45.00', '1.93', '13.51', 'EViajero 2D2-100', '31.49', '69.98'),
(71, 8, 1, 8, 8, '52.00', '1.93', '15.44', 'EViajero 2D2-100', '36.56', '70.31'),
(72, 8, 1, 9, 9, '58.50', '1.93', '17.37', 'EViajero 9D2-100', '41.13', '70.31'),
(73, 9, 1, 2, 10, '84.00', '1.93', '19.30', 'EViajero 2S1-100', '64.70', '77.02'),
(74, 9, 1, 3, 21, '115.00', '1.93', '40.53', 'EViajero 3S1-100', '74.47', '64.76'),
(75, 9, 1, 4, 28, '155.00', '1.93', '54.04', 'EViajero 4S1-100', '100.96', '65.14'),
(76, 9, 1, 5, 35, '175.00', '1.93', '67.55', 'EViajero 5S1-100', '107.45', '61.40'),
(77, 9, 1, 6, 42, '195.00', '1.93', '81.06', 'EViajero 6S1-100', '113.94', '58.43'),
(78, 9, 1, 7, 49, '205.00', '1.93', '94.57', 'EViajero 7S1-100', '110.43', '53.87'),
(79, 9, 1, 8, 56, '230.00', '1.93', '108.08', 'EViajero 8S1-100', '121.92', '53.01'),
(80, 9, 1, 9, 63, '260.00', '1.93', '121.59', 'EViajero 9S1-100', '138.41', '53.23'),
(81, 10, 1, 2, 10, '84.00', '1.93', '19.30', 'EViajero 2S2-100', '64.70', '77.02'),
(82, 10, 1, 3, 21, '115.00', '1.93', '40.53', 'EViajero 3S2-100', '74.47', '64.76'),
(83, 10, 1, 4, 28, '155.00', '1.93', '54.04', 'EViajero 4S2-100', '100.96', '65.14'),
(84, 10, 1, 5, 35, '175.00', '1.93', '67.55', 'EViajero 5S2-100', '107.45', '61.40'),
(85, 10, 1, 6, 42, '195.00', '1.93', '81.06', 'EViajero 6S2-100', '113.94', '58.43'),
(86, 10, 1, 7, 49, '205.00', '1.93', '94.57', 'EViajero 7S2-100', '110.43', '53.87'),
(87, 10, 1, 8, 56, '230.00', '1.93', '108.08', 'EViajero 8S2-100', '121.92', '53.01'),
(88, 10, 1, 9, 63, '260.00', '1.93', '121.59', 'EViajero 9S2-100', '138.41', '53.23'),
(89, 11, 1, 3, 64, '280.00', '1.93', '123.52', 'EViajero 3M1-100', '156.48', '55.89'),
(90, 11, 1, 4, 120, '370.00', '1.93', '231.60', 'EViajero 4M1-100', '138.40', '37.41'),
(91, 11, 1, 5, 150, '460.00', '1.93', '289.50', 'EViajero 5M1-100', '170.50', '37.07'),
(92, 11, 1, 6, 180, '550.00', '1.93', '347.40', 'EViajero 6M1-100', '202.60', '36.84'),
(93, 11, 1, 7, 210, '640.00', '1.93', '405.30', 'EViajero 7M1-100', '234.70', '36.67'),
(94, 11, 1, 8, 240, '740.00', '1.93', '463.20', 'EViajero 8M1-100', '276.80', '37.41'),
(95, 11, 1, 9, 270, '830.00', '1.93', '521.10', 'EViajero 9M1-100', '308.90', '37.22'),
(96, 11, 1, 10, 300, '920.00', '1.93', '579.00', 'EViajero 10M1-100', '341.00', '37.07'),
(97, 11, 1, 11, 330, '990.00', '1.93', '636.90', 'EViajero 11M1-100', '353.10', '35.67'),
(98, 11, 1, 12, 360, '1120.00', '1.93', '694.80', 'EViajero 12M1-100', '425.20', '37.96'),
(99, 12, 1, 3, 64, '280.00', '1.93', '123.52', 'EViajero 3M2-100', '156.48', '55.89'),
(100, 12, 1, 4, 120, '370.00', '1.93', '231.60', 'EViajero 4M2-100', '138.40', '37.41'),
(101, 12, 1, 5, 150, '460.00', '1.93', '289.50', 'EViajero 5M2-100', '170.50', '37.07'),
(102, 12, 1, 6, 180, '550.00', '1.93', '347.40', 'EViajero 6M2-100', '202.60', '36.84'),
(103, 12, 1, 7, 210, '640.00', '1.93', '405.30', 'EViajero 7M2-100', '234.70', '36.67'),
(104, 12, 1, 8, 240, '740.00', '1.93', '463.20', 'EViajero 8M2-100', '276.80', '37.41'),
(105, 12, 1, 9, 270, '830.00', '1.93', '521.10', 'EViajero 9M2-100', '308.90', '37.22'),
(106, 12, 1, 10, 300, '920.00', '1.93', '579.00', 'EViajero 10M2-100', '341.00', '37.07'),
(107, 12, 1, 11, 330, '990.00', '1.93', '636.90', 'EViajero 11M2-100', '353.10', '35.67'),
(108, 12, 1, 12, 360, '1120.00', '1.93', '694.80', 'EViajero 12M2-100', '425.20', '37.96'),
(109, 13, 1, 1, 1, '5.70', '1.71', '1.71', 'EViajero 1D1-100', '3.99', '70.00'),
(110, 13, 1, 2, 2, '11.50', '1.71', '3.42', 'EViajero 2D1-100', '8.08', '70.26'),
(111, 13, 1, 3, 3, '17.00', '1.71', '5.13', 'EViajero 3D1-100', '11.87', '69.82'),
(112, 13, 1, 4, 4, '23.00', '1.71', '6.84', 'EViajero 4D1-100', '16.16', '70.26'),
(113, 13, 1, 5, 5, '28.50', '1.71', '8.55', 'EViajero 5D1-100', '19.95', '70.00'),
(114, 13, 1, 6, 6, '34.00', '1.71', '10.26', 'EViajero 6D1-100', '23.74', '69.82'),
(115, 13, 1, 7, 7, '40.00', '1.71', '11.97', 'EViajero 7D1-100', '28.03', '70.08'),
(116, 13, 1, 8, 8, '45.50', '1.71', '13.68', 'EViajero 8D1-100', '31.82', '69.93'),
(117, 13, 1, 9, 9, '52.00', '1.71', '15.39', 'EViajero 9D1-100', '36.61', '70.40'),
(118, 14, 1, 1, 1, '5.70', '1.71', '1.71', 'EViajero 1D2-100', '3.99', '70.00'),
(119, 14, 1, 2, 2, '11.50', '1.71', '3.42', 'EViajero 2D2-100', '8.08', '70.26'),
(120, 14, 1, 3, 3, '17.00', '1.71', '5.13', 'EViajero 3D2-100', '11.87', '69.82'),
(121, 14, 1, 4, 4, '23.00', '1.71', '6.84', 'EViajero 4D2-100', '16.16', '70.26'),
(122, 14, 1, 5, 5, '28.50', '1.71', '8.55', 'EViajero 5D2-100', '19.95', '70.00'),
(123, 14, 1, 6, 6, '34.00', '1.71', '10.26', 'EViajero 2D2-100', '23.74', '69.82'),
(124, 14, 1, 7, 7, '40.00', '1.71', '11.97', 'EViajero 2D2-100', '28.03', '70.08'),
(125, 14, 1, 8, 8, '45.50', '1.71', '13.68', 'EViajero 2D2-100', '31.82', '69.93'),
(126, 14, 1, 9, 9, '52.00', '1.71', '15.39', 'EViajero 9D2-100', '36.61', '70.40'),
(127, 15, 1, 2, 10, '80.00', '1.71', '17.10', 'EViajero 2S1-100', '62.90', '78.63'),
(128, 15, 1, 3, 21, '110.00', '1.71', '35.91', 'EViajero 3S1-100', '74.09', '67.35'),
(129, 15, 1, 4, 28, '140.00', '1.71', '47.88', 'EViajero 4S1-100', '92.12', '65.80'),
(130, 15, 1, 5, 35, '160.00', '1.71', '59.85', 'EViajero 5S1-100', '100.15', '62.59'),
(131, 15, 1, 6, 42, '180.00', '1.71', '71.82', 'EViajero 6S1-100', '108.18', '60.10'),
(132, 15, 1, 7, 49, '195.00', '1.71', '83.79', 'EViajero 7S1-100', '111.21', '57.03'),
(133, 15, 1, 8, 56, '210.00', '1.71', '95.76', 'EViajero 8S1-100', '114.24', '54.40'),
(134, 15, 1, 9, 63, '230.00', '1.71', '107.73', 'EViajero 9S1-100', '122.27', '53.16'),
(135, 16, 1, 2, 10, '80.00', '1.71', '17.10', 'EViajero 2S2-100', '62.90', '78.63'),
(136, 16, 1, 3, 21, '110.00', '1.71', '35.91', 'EViajero 3S2-100', '74.09', '67.35'),
(137, 16, 1, 4, 28, '140.00', '1.71', '47.88', 'EViajero 4S2-100', '92.12', '65.80'),
(138, 16, 1, 5, 35, '160.00', '1.71', '59.85', 'EViajero 5S2-100', '100.15', '62.59'),
(139, 16, 1, 6, 42, '180.00', '1.71', '71.82', 'EViajero 6S2-100', '108.18', '60.10'),
(140, 16, 1, 7, 49, '195.00', '1.71', '83.79', 'EViajero 7S2-100', '111.21', '57.03'),
(141, 16, 1, 8, 56, '210.00', '1.71', '95.76', 'EViajero 8S2-100', '114.24', '54.40'),
(142, 16, 1, 9, 63, '230.00', '1.71', '107.73', 'EViajero 9S2-100', '122.27', '53.16'),
(143, 17, 1, 3, 64, '255.00', '1.71', '109.44', 'EViajero 3M1-100', '145.56', '57.08'),
(144, 17, 1, 4, 120, '340.00', '1.71', '205.20', 'EViajero 4M1-100', '134.80', '39.65'),
(145, 17, 1, 5, 150, '425.00', '1.71', '256.50', 'EViajero 5M1-100', '168.50', '39.65'),
(146, 17, 1, 6, 180, '510.00', '1.71', '307.80', 'EViajero 6M1-100', '202.20', '39.65'),
(147, 17, 1, 7, 210, '600.00', '1.71', '359.10', 'EViajero 7M1-100', '240.90', '40.15'),
(148, 17, 1, 8, 240, '680.00', '1.71', '410.40', 'EViajero 8M1-100', '269.60', '39.65'),
(149, 17, 1, 9, 270, '765.00', '1.71', '461.70', 'EViajero 9M1-100', '303.30', '39.65'),
(150, 17, 1, 10, 300, '850.00', '1.71', '513.00', 'EViajero 10M1-100', '337.00', '39.65'),
(151, 17, 1, 11, 330, '935.00', '1.71', '564.30', 'EViajero 11M1-100', '370.70', '39.65'),
(152, 17, 1, 12, 360, '1020.00', '1.71', '615.60', 'EViajero 12M1-100', '404.40', '39.65'),
(153, 18, 1, 3, 64, '255.00', '1.71', '109.44', 'EViajero 3M2-100', '145.56', '57.08'),
(154, 18, 1, 4, 120, '340.00', '1.71', '205.20', 'EViajero 4M2-100', '134.80', '39.65'),
(155, 18, 1, 5, 150, '425.00', '1.71', '256.50', 'EViajero 5M2-100', '168.50', '39.65'),
(156, 18, 1, 6, 180, '510.00', '1.71', '307.80', 'EViajero 6M2-100', '202.20', '39.65'),
(157, 18, 1, 7, 210, '600.00', '1.71', '359.10', 'EViajero 7M2-100', '240.90', '40.15'),
(158, 18, 1, 8, 240, '680.00', '1.71', '410.40', 'EViajero 8M2-100', '269.60', '39.65'),
(159, 18, 1, 9, 270, '765.00', '1.71', '461.70', 'EViajero 9M2-100', '303.30', '39.65'),
(160, 18, 1, 10, 300, '850.00', '1.71', '513.00', 'EViajero 10M2-100', '337.00', '39.65'),
(161, 18, 1, 11, 330, '935.00', '1.71', '564.30', 'EViajero 11M2-100', '370.70', '39.65'),
(162, 18, 1, 12, 360, '1020.00', '1.71', '615.60', 'EViajero 12M2-100', '404.40', '39.65'),
(163, 19, 1, 1, 1, '100.00', '6.90', '6.90', '7Mayores 70 1S1-100', '93.10', '93.10'),
(164, 19, 1, 2, 14, '185.00', '6.90', '96.60', '7Mayores 70 2S1-100', '88.40', '47.78'),
(165, 19, 1, 3, 21, '270.00', '6.90', '144.90', '7Mayores 70 3S1-100', '125.10', '46.33'),
(166, 19, 1, 4, 28, '355.00', '6.90', '193.20', '7Mayores 70 4S1-100', '161.80', '45.58'),
(167, 19, 1, 5, 35, '440.00', '6.90', '241.50', '7Mayores 70 5S1-100', '198.50', '45.11'),
(168, 19, 1, 6, 42, '525.00', '6.90', '289.80', '7Mayores 70 6S1-100', '235.20', '44.80'),
(169, 19, 1, 7, 49, '610.00', '6.90', '338.10', '7Mayores 70 7S1-100', '271.90', '44.57'),
(170, 19, 1, 8, 56, '695.00', '6.90', '386.40', '7Mayores 70 8S1-100', '308.60', '44.40'),
(171, 19, 1, 9, 63, '780.00', '6.90', '434.70', '7Mayores 70 9S1-100', '345.30', '44.27'),
(172, 20, 1, 1, 1, '100.00', '6.90', '6.90', '7Mayores 70 1S2-100', '93.10', '93.10'),
(173, 20, 1, 2, 14, '185.00', '6.90', '96.60', '7Mayores 70 2S2-100', '88.40', '47.78'),
(174, 20, 1, 3, 21, '270.00', '6.90', '144.90', '7Mayores 70 3S2-100', '125.10', '46.33'),
(175, 20, 1, 4, 28, '355.00', '6.90', '193.20', '7Mayores 70 4S2-100', '161.80', '45.58'),
(176, 20, 1, 5, 35, '440.00', '6.90', '241.50', '7Mayores 70 5S2-100', '198.50', '45.11'),
(177, 20, 1, 6, 42, '525.00', '6.90', '289.80', '7Mayores 70 6S2-100', '235.20', '44.80'),
(178, 20, 1, 7, 49, '610.00', '6.90', '338.10', '7Mayores 70 7S2-100', '271.90', '44.57'),
(179, 20, 1, 8, 56, '695.00', '6.90', '386.40', '7Mayores 70 8S2-100', '308.60', '44.40'),
(180, 20, 1, 9, 63, '780.00', '6.90', '434.70', '7Mayores 70 9S2-100', '345.30', '44.27'),
(181, 21, 1, 3, 64, '950.00', '6.90', '441.60', '7Mayores 70 3M1-100', '508.40', '53.52'),
(182, 21, 1, 4, 120, '1300.00', '6.90', '828.00', '7Mayores 70 4M1-100', '472.00', '36.31'),
(183, 21, 1, 5, 150, '1600.00', '6.90', '1035.00', '7Mayores 70 51M-100', '565.00', '35.31'),
(184, 21, 1, 6, 180, '1900.00', '6.90', '1242.00', '7Mayores 70 6M1-100', '658.00', '34.63'),
(185, 21, 1, 7, 210, '2200.00', '6.90', '1449.00', '7Mayores 70 7M1-100', '751.00', '34.14'),
(186, 21, 1, 8, 240, '2500.00', '6.90', '1656.00', '7Mayores 70 8M1-100', '844.00', '33.76'),
(187, 21, 1, 9, 270, '2800.00', '6.90', '1863.00', '7Mayores 70 9M1-100', '937.00', '33.46'),
(188, 21, 1, 10, 300, '3100.00', '6.90', '2070.00', '7Mayores 70 10M1-100', '1030.00', '33.23'),
(189, 21, 1, 11, 330, '3400.00', '6.90', '2277.00', '7Mayores 70 11M1-100', '1123.00', '33.03'),
(190, 21, 1, 12, 360, '3700.00', '6.90', '2484.00', '7Mayores 70 12M1-100', '1216.00', '32.86'),
(191, 22, 1, 3, 64, '950.00', '6.90', '441.60', '7Mayores 70 3M2-100', '508.40', '53.52'),
(192, 22, 1, 4, 120, '1300.00', '6.90', '828.00', '7Mayores 70 4M2-100', '472.00', '36.31'),
(193, 22, 1, 5, 150, '1600.00', '6.90', '1035.00', '7Mayores 70 5M2-100', '565.00', '35.31'),
(194, 22, 1, 6, 180, '1900.00', '6.90', '1242.00', '7Mayores 70 6M2-100', '658.00', '34.63'),
(195, 22, 1, 7, 210, '2200.00', '6.90', '1449.00', '7Mayores 70 7M2-100', '751.00', '34.14'),
(196, 22, 1, 8, 240, '2500.00', '6.90', '1656.00', '7Mayores 70 8M2-100', '844.00', '33.76'),
(197, 22, 1, 9, 270, '2800.00', '6.90', '1863.00', '7Mayores 70 9M2-100', '937.00', '33.46'),
(198, 22, 1, 10, 300, '3100.00', '6.90', '2070.00', '7Mayores 70 10M2-100', '1030.00', '33.23'),
(199, 22, 1, 11, 330, '3400.00', '6.90', '2277.00', '7Mayores 70 11M2-100', '1123.00', '33.03'),
(200, 22, 1, 12, 360, '3700.00', '6.90', '2484.00', '7Mayores 70 12M2-100', '1216.00', '32.86'),
(201, 23, 1, 1, 1, '65.00', '4.68', '4.68', '6Menores 6 1S1-100', '60.32', '92.80'),
(202, 23, 1, 2, 14, '125.00', '4.68', '65.52', '6Menores 6 2S1-100', '59.48', '47.58'),
(203, 23, 1, 3, 21, '185.00', '4.68', '98.28', '6Menores 6 3S1-100', '86.72', '46.88'),
(204, 23, 1, 4, 28, '245.00', '4.68', '131.04', '6Menores 6 4S1-100', '113.96', '46.51'),
(205, 23, 1, 5, 35, '300.00', '4.68', '163.80', '6Menores 6 5S1-100', '136.20', '45.40'),
(206, 23, 1, 6, 42, '370.00', '4.68', '196.56', '6Menores 6 6S1-100', '173.44', '46.88'),
(207, 23, 1, 7, 49, '425.00', '4.68', '229.32', '6Menores 6 7S1-100', '195.68', '46.04'),
(208, 23, 1, 8, 56, '490.00', '4.68', '262.08', '6Menores 6 8S1-100', '227.92', '46.51'),
(209, 23, 1, 9, 63, '550.00', '4.68', '294.84', '6Menores 6 9S1-100', '255.16', '46.39'),
(210, 24, 1, 1, 1, '65.00', '4.68', '4.68', '6Menores 6 1S2-100', '60.32', '92.80'),
(211, 24, 1, 2, 14, '125.00', '4.68', '65.52', '6Menores 6 2S2-100', '59.48', '47.58'),
(212, 24, 1, 3, 21, '185.00', '4.68', '98.28', '6Menores 6 3S2-100', '86.72', '46.88'),
(213, 24, 1, 4, 28, '245.00', '4.68', '131.04', '6Menores 6 4S2-100', '113.96', '46.51'),
(214, 24, 1, 5, 35, '300.00', '4.68', '163.80', '6Menores 6 5S2-100', '136.20', '45.40'),
(215, 24, 1, 6, 42, '370.00', '4.68', '196.56', '6Menores 6 6S2-100', '173.44', '46.88'),
(216, 24, 1, 7, 49, '425.00', '4.68', '229.32', '6Menores 6 7S2-100', '195.68', '46.04'),
(217, 24, 1, 8, 56, '490.00', '4.68', '262.08', '6Menores 6 8S2-100', '227.92', '46.51'),
(218, 24, 1, 9, 63, '550.00', '4.68', '294.84', '6Menores 6 9S2-100', '255.16', '46.39'),
(219, 25, 1, 3, 64, '600.00', '4.68', '299.52', '6Menores 6 3M1-100', '300.48', '50.08'),
(220, 25, 1, 4, 120, '800.00', '4.68', '561.60', '6Menores 6 4M1-100', '238.40', '29.80'),
(221, 25, 1, 5, 150, '1000.00', '4.68', '702.00', '6Menores 6 5M1-100', '298.00', '29.80'),
(222, 25, 1, 6, 180, '1200.00', '4.68', '842.40', '6Menores 6 6M1-100', '357.60', '29.80'),
(223, 25, 1, 7, 210, '1400.00', '4.68', '982.80', '6Menores 6 7M1-100', '417.20', '29.80'),
(224, 25, 1, 8, 240, '1600.00', '4.68', '1123.20', '6Menores 6 8M1-100', '476.80', '29.80'),
(225, 25, 1, 9, 270, '1800.00', '4.68', '1263.60', '6Menores 6 9M1-100', '536.40', '29.80'),
(226, 25, 1, 10, 300, '2000.00', '4.68', '1404.00', '6Menores 6 10M1-100', '596.00', '29.80'),
(227, 25, 1, 11, 330, '2200.00', '4.68', '1544.40', '6Menores 6 11M1-100', '655.60', '29.80'),
(228, 25, 1, 12, 360, '2400.00', '4.68', '1684.80', '6Menores 6 12M1-100', '715.20', '29.80'),
(229, 26, 1, 3, 64, '600.00', '4.68', '299.52', '6Menores 6 3M2-100', '300.48', '50.08'),
(230, 26, 1, 4, 120, '800.00', '4.68', '561.60', '6Menores 6 4M2-100', '238.40', '29.80'),
(231, 26, 1, 5, 150, '1000.00', '4.68', '702.00', '6Menores 6 5M2-100', '298.00', '29.80'),
(232, 26, 1, 6, 180, '1200.00', '4.68', '842.40', '6Menores 6 6M2-100', '357.60', '29.80'),
(233, 26, 1, 7, 210, '1400.00', '4.68', '982.80', '6Menores 6 7M2-100', '417.20', '29.80'),
(234, 26, 1, 8, 240, '1600.00', '4.68', '1123.20', '6Menores 6 8M2-100', '476.80', '29.80'),
(235, 26, 1, 9, 270, '1800.00', '4.68', '1263.60', '6Menores 6 9M2-100', '536.40', '29.80'),
(236, 26, 1, 10, 300, '2000.00', '4.68', '1404.00', '6Menores 6 10M2-100', '596.00', '29.80'),
(237, 26, 1, 11, 330, '2200.00', '4.68', '1544.40', '6Menores 6 11M2-100', '655.60', '29.80'),
(238, 26, 1, 12, 360, '2400.00', '4.68', '1684.80', '6Menores, 6 12M2-100', '715.20', '29.80');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion`
--

CREATE TABLE `cotizacion` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_subagente` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `id_tipo_seguro` int(11) NOT NULL,
  `origen` int(11) NOT NULL,
  `destino` int(11) NOT NULL,
  `salida` date NOT NULL,
  `regreso` date NOT NULL,
  `codigo_promocion` varchar(127) NOT NULL,
  `id_tipo_poliza` int(11) NOT NULL,
  `numero_dias` int(11) NOT NULL,
  `numero_semanas_meses` int(11) NOT NULL,
  `precioMXN` decimal(10,2) NOT NULL,
  `precioMXN_descuento` decimal(10,2) NOT NULL,
  `precioMXN_ajuste` decimal(10,2) NOT NULL,
  `precioMXN_iva` decimal(10,2) NOT NULL,
  `precioMXN_subtotal` decimal(10,2) NOT NULL,
  `precioMXN_comision` decimal(10,2) NOT NULL,
  `precioMXN_total` decimal(10,2) NOT NULL,
  `precioUSD` decimal(10,2) NOT NULL,
  `precioUSD_descuento` decimal(10,2) NOT NULL,
  `precioUSD_ajuste` decimal(10,2) NOT NULL,
  `precioUSD_iva` decimal(10,2) NOT NULL,
  `precioUSD_subtotal` decimal(10,2) NOT NULL,
  `precioUSD_comision` decimal(10,2) NOT NULL,
  `precioUSD_total` decimal(10,2) NOT NULL,
  `tipo_cambio` decimal(10,2) NOT NULL,
  `descripcion` text NOT NULL,
  `fecha_hecha` date NOT NULL,
  `fecha_asegurado` date NOT NULL,
  `folio` varchar(256) NOT NULL,
  `comentario` text NOT NULL,
  `cargo_administrativo` decimal(10,2) NOT NULL,
  `envio` decimal(10,2) NOT NULL,
  `saldo_pendiente` decimal(10,2) NOT NULL,
  `saldo_liquidado` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cotizacion`
--

INSERT INTO `cotizacion` (`id`, `id_cliente`, `id_subagente`, `activo_inactivo`, `id_tipo_seguro`, `origen`, `destino`, `salida`, `regreso`, `codigo_promocion`, `id_tipo_poliza`, `numero_dias`, `numero_semanas_meses`, `precioMXN`, `precioMXN_descuento`, `precioMXN_ajuste`, `precioMXN_iva`, `precioMXN_subtotal`, `precioMXN_comision`, `precioMXN_total`, `precioUSD`, `precioUSD_descuento`, `precioUSD_ajuste`, `precioUSD_iva`, `precioUSD_subtotal`, `precioUSD_comision`, `precioUSD_total`, `tipo_cambio`, `descripcion`, `fecha_hecha`, `fecha_asegurado`, `folio`, `comentario`, `cargo_administrativo`, `envio`, `saldo_pendiente`, `saldo_liquidado`) VALUES
(19, 16, 1, 1, 1, 1, 5, '2018-09-20', '2019-03-19', '', 18, 181, 7, '8640.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '480.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '18.00', '', '2018-09-06', '0000-00-00', '', '', '0.00', '0.00', '0.00', '0.00'),
(21, 11, 1, 1, 2, 1, 17, '2018-09-14', '2018-11-29', '', 22, 77, 3, '11700.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '650.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '18.00', '', '2018-09-07', '0000-00-00', '', '', '0.00', '0.00', '0.00', '0.00'),
(22, 12, 1, 1, 1, 1, 4, '2018-09-20', '2018-09-29', '', 16, 10, 1, '1224.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '68.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '18.00', '', '2018-09-10', '0000-00-00', '20180919.HAITL3Z2SBC-Setmex.', '', '0.00', '0.00', '0.00', '0.00'),
(23, 20, 1, 1, 1, 1, 159, '2018-11-15', '2019-03-14', '', 12, 120, 4, '6660.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '370.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '18.00', '', '2018-09-12', '0000-00-00', '', '', '0.00', '0.00', '0.00', '0.00'),
(24, 21, 1, 1, 1, 1, 159, '2018-11-15', '2019-03-14', '', 12, 120, 4, '6660.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '370.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '18.00', '', '2018-09-12', '0000-00-00', '', '', '0.00', '0.00', '0.00', '0.00'),
(25, 22, 1, 1, 1, 1, 159, '2018-11-15', '2019-03-14', '', 12, 120, 4, '6660.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '370.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '18.00', '', '2018-09-12', '2018-09-19', '20180919.HAITL3Z2LSC-Setmex.', '', '0.00', '0.00', '0.00', '0.00'),
(26, 23, 1, 1, 1, 1, 159, '2018-11-15', '2019-03-14', '', 12, 120, 4, '6660.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '370.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '18.00', '', '2018-09-12', '0000-00-00', '', '', '0.00', '0.00', '0.00', '0.00'),
(27, 24, 1, 1, 1, 1, 159, '2018-11-15', '2019-03-14', '', 12, 120, 4, '6660.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '370.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '18.00', '', '2018-09-12', '0000-00-00', '', '', '0.00', '0.00', '0.00', '0.00'),
(28, 25, 1, 1, 1, 1, 159, '2018-11-15', '2019-03-14', '', 12, 120, 4, '6660.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '370.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '18.00', '', '2018-09-12', '0000-00-00', '', '', '0.00', '0.00', '0.00', '0.00'),
(29, 13, 1, 1, 2, 1, 5, '2018-09-13', '2018-12-26', '', 22, 105, 4, '24427.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '1300.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '19.00', '', '2018-09-13', '0000-00-00', '', '', '0.00', '0.00', '0.00', '0.00'),
(30, 13, 1, 1, 3, 1, 4, '2018-09-13', '2019-01-22', '', 26, 132, 5, '18790.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '1000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '19.00', '', '2018-09-13', '0000-00-00', '20180919.HAITL3Z2LGC-Setmex-under6.', '', '0.00', '0.00', '0.00', '0.00'),
(32, 11, 1, 1, 3, 1, 5, '2018-09-14', '2018-10-25', '', 24, 42, 6, '7215.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '370.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '20.00', '', '2018-09-14', '0000-00-00', '', '', '0.00', '0.00', '0.00', '0.00'),
(33, 28, 1, 1, 1, 1, 4, '2018-09-17', '2018-09-29', '', 16, 13, 1, '1564.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '20.00', '', '2018-09-17', '0000-00-00', '', '', '0.00', '0.00', '0.00', '0.00'),
(34, 29, 1, 1, 2, 1, 4, '2018-09-17', '2019-07-16', '', 22, 303, 11, '66470.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '3400.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '20.00', '', '2018-09-17', '0000-00-00', '20180917.HAITL3Z2LSC-Setmex-over70.CANEDG19451009', '', '0.00', '0.00', '0.00', '0.00'),
(35, 30, 1, 1, 1, 1, 4, '2018-09-18', '2018-09-30', '', 16, 13, 1, '1564.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '80.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '20.00', '', '2018-09-18', '0000-00-00', '20180918.HAITL3Z2SBC-Setmex.APEPRU19940714', '', '0.00', '0.00', '0.00', '0.00'),
(36, 30, 1, 1, 1, 1, 18, '2018-11-14', '2019-01-29', '', 6, 77, 3, '6256.00', '6256.00', '0.00', '0.00', '6256.00', '0.00', '6256.00', '320.00', '320.00', '0.00', '0.00', '320.00', '0.00', '320.00', '20.00', '', '2018-09-18', '0000-00-00', '20180918.HAITL3Z2LGC-Setmex.APEPRU19940714', 'Comentario', '0.00', '0.00', '0.00', '0.00'),
(37, 12, 1, 1, 2, 1, 4, '2018-09-19', '2018-12-26', '', 22, 99, 4, '25415.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '1300.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '19.55', '', '2018-09-19', '2018-09-20', '20180920.HAITL3Z2LSC-Setmex-over70.', '', '0.00', '0.00', '0.00', '0.00'),
(40, 14, 1, 1, 2, 1, 64, '2018-09-19', '2019-06-26', '', 22, 281, 10, '60605.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '3100.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '19.55', '', '2018-09-19', '0000-00-00', '20180919.HAITL3Z2LSC-Setmex-over70.', '', '0.00', '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cp`
--

CREATE TABLE `cp` (
  `id` int(11) NOT NULL,
  `ciudad` varchar(64) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `cp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE `cuenta` (
  `id` int(11) NOT NULL,
  `cuenta` int(11) NOT NULL,
  `id_forma_pago` int(11) NOT NULL,
  `moneda` varchar(8) NOT NULL,
  `activo_inactivo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direccion`
--

CREATE TABLE `direccion` (
  `id` int(11) NOT NULL,
  `estado` varchar(64) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `ciudad` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distribuidor`
--

CREATE TABLE `distribuidor` (
  `id` int(11) NOT NULL,
  `id_tipo_distribuidor` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `distribuidor` varchar(127) NOT NULL,
  `correo_electronico` varchar(255) NOT NULL,
  `telefono` varchar(32) NOT NULL,
  `comision` int(11) NOT NULL,
  `descuento` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `estado` text NOT NULL,
  `cp` varchar(11) NOT NULL,
  `ciudad` text NOT NULL,
  `calle_numero` varchar(511) NOT NULL,
  `razon_social` varchar(511) NOT NULL,
  `rfc` varchar(255) NOT NULL,
  `fac_correo_electronico` varchar(255) NOT NULL,
  `fac_telefono` varchar(32) NOT NULL,
  `fac_id_pais` int(11) NOT NULL,
  `fac_estado` text NOT NULL,
  `fac_cp` int(11) NOT NULL,
  `fac_ciudad` text NOT NULL,
  `fac_calle_numero` varchar(511) NOT NULL,
  `colonia` varchar(127) NOT NULL,
  `fac_colonia` varchar(127) NOT NULL,
  `contacto` varchar(127) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `distribuidor`
--

INSERT INTO `distribuidor` (`id`, `id_tipo_distribuidor`, `activo_inactivo`, `distribuidor`, `correo_electronico`, `telefono`, `comision`, `descuento`, `id_pais`, `estado`, `cp`, `ciudad`, `calle_numero`, `razon_social`, `rfc`, `fac_correo_electronico`, `fac_telefono`, `fac_id_pais`, `fac_estado`, `fac_cp`, `fac_ciudad`, `fac_calle_numero`, `colonia`, `fac_colonia`, `contacto`) VALUES
(1, 4, 1, 'Operadora Sierra Madre', 'ventas2@operadorasierramadre.com', '', 15, 0, 1, 'Nuevo León', '', 'Monterrey', '', 'Operadora Sierra Madre', '', '', '', 0, '', 0, '', '', '', '', ''),
(2, 1, 1, 'America Seguros', 'americaseguros3@prodigy.net.mx', '01 8999 25 0083', 15, 0, 1, 'Tamaulipas', '', 'Reynosa', '', 'America Seguros', 'RADA6009227K7', '', '', 0, '', 0, '', '', '', '', ''),
(3, 1, 1, 'Asesores Profesionales De Seguros', 'jvillarreal@apasesores.com', ' 8625-1857 ', 15, 0, 1, 'Nuevo León', '', 'San Pedro Garza García', 'Ave. Roble', 'Asesores Profesionales De Seguros', '', '', '', 0, '', 0, '', '', '', '', ''),
(4, 1, 1, 'Carlos A. Muñoz', 'caram1@prodigy.net.mx', '', 15, 0, 1, 'Quintana Roo', '', 'Cancún', 'Pecari 3 16  5M 20 ', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(5, 2, 1, 'CCI Mex', '', '', 15, 0, 1, '', '', '', '', '', ' ', '', '', 0, '', 0, '', '', '', '', ''),
(6, 5, 1, 'Cetys Universidad', '', '', 0, 10, 1, 'México', '21259', '', 'Calzada CETYS', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(7, 1, 1, 'David Eduardo Gomez Salazar', 'sandra.gmm@davidgomezseguros.com', '8331165757', 15, 0, 1, 'Tamaulipas', '89106', 'Tampico', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(8, 2, 1, 'Epic', '', '81-83691614', 15, 0, 1, 'Nuevo León', '', 'Monterrey', 'Plaza San Sebastian', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(9, 3, 1, 'Estudiantes Embajadores', 'info@eem.org.mx', '8183352711', 10, 10, 1, 'Nuevo León', '66220', 'San Pedro Garza García', 'Rio Orinoco ', 'Programas Estudiantiles de Mexico, AC', 'PEM010212KK9', '', '', 0, '', 0, '', '', '', '', ''),
(10, 1, 1, 'Fernando H. Saez De Nanclares Ortega', '', '', 20, 0, 1, 'Nuevo León', '', 'San Pedro Garza García', '', 'Fernando H. Saenz De Nanclares  Ortega', '', '', '', 0, '', 0, '', '', '', '', ''),
(11, 1, 1, 'Flores Diaz Seguros', '', '', 15, 0, 1, '', '', '', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(12, 2, 1, 'Indelpro', '', '', 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(13, 1, 1, 'Ing. Juan Villarreal Montemayor', '', '', 15, 0, 1, 'Nuevo León', '66250', 'Monterrey', 'Ave. Roble ', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(14, 5, 1, 'Instituto De Estudios Superiores De Tamaulipas', '', '833 230 25 50', 0, 10, 1, 'Tamaulipas', '', 'Tampico', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(15, 5, 1, 'Universidad La Salle', '', '', 0, 10, 1, 'Cancún', '', '', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(16, 1, 1, 'Navarro Y Asociados', 'navarroyasoc_cynthia@hotmail.com', '', 20, 0, 1, '', '', '', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(17, 1, 1, 'Phixius', '', '', 20, 0, 1, 'Nuevo León', '', 'San Pedro Garza García', 'Sierra Nevada Cruz C. Priv. Sierra N ', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(18, 5, 1, 'Politecnico Nacional', '', '', 0, 10, 1, '', '', '', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(19, 2, 1, 'Programas De Lengua Y Cultura', 'plcalf@prodigy.net.mx', '52 (866) 633 9090', 15, 0, 1, 'Coahuila ', '25720', 'Monclova', 'Rio Nadadores', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(20, 4, 1, 'Setmex', '', '', 0, 0, 1, 'Nuevo León', '66220', 'San Pedro Garza García', 'Río Orínoco', 'Setmex S.A. de C.V.', '', '', '', 0, '', 0, '', '', '', '', ''),
(21, 5, 1, 'Universidad Autonoma Del Estado De Mexico', '', '', 0, 10, 1, '', '', '', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(22, 5, 1, 'Universidad Autonoma De Nuevo Leon', '', '', 0, 10, 1, '', '', '', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(23, 5, 1, 'Universidad De Monterrey', '', '', 0, 10, 1, '', '', '', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(24, 5, 1, 'Universidad Nacional Autonoma De Mexico', '', '', 0, 10, 1, 'México', '', 'México', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(25, 5, 1, 'Universidad De Guanajuato ', '', '', 0, 15, 1, '', '', '', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(26, 5, 1, 'Universidad Iberoamericana', '', '', 5, 7, 1, '', '', '', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(27, 5, 1, 'Universidad Panamericana', '', '', 0, 10, 1, 'Guadalajara', '', '', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(28, 5, 1, 'ITESM', '', '', 0, 10, 1, 'Nuevo León', '', 'Monterrey', 'Ave. Eugenio Garza Sada Sur', '', 'ITE 430714 KI0 ', '', '', 0, '', 0, '', '', '', '', ''),
(29, 1, 1, 'Gustavo Castillo Perez', '', '', 15, 0, 1, '', '', '', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(30, 1, 1, 'Javier Garcia Casanova', '', '', 15, 0, 1, 'Tamaulipas', '', 'Tampíco', '', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(31, 1, 1, 'Aseincap, Agente De Seguros Y Fianzas', 'cesar.vazquez@aseincap.com.mx', '55 5566 2017', 15, 0, 1, 'México', '06030', 'Delegación Cuauhtemoc', 'Madrid', '', '', '', '', 0, '', 0, '', '', '', '', ''),
(32, 1, 1, 'Protege Seguros', 'mercedes.ponce@protegeseguros.com', '', 15, 0, 1, 'Puebla', '66220', 'Puebla', '', 'Protege Seguros', '', '', '', 0, '', 0, '', '', '', '', ''),
(33, 1, 1, 'Grupo Galesis', 'vgarcia@grupogalesis.com', ' 55 12 53 60 78 EXT. 240 Y 241', 14, 0, 1, '', '', '', '', 'Grupo Galesis', '', '', '', 0, '', 0, '', '', '', '', ''),
(34, 1, 1, 'Garante Asesores', 'hzamora@garanteasesores.com.mx', '81 82 42 7061', 15, 0, 1, '', '', '', '', 'Garante Asesores', '', '', '', 0, '', 0, '', '', '', '', ''),
(35, 1, 1, 'Seimos Y Asociados', 'fflorescantu@hotmail.com', '8117364683', 0, 10, 1, 'Nuevo León', '0', 'Monterrey', '', 'Seimos y Asociados, S.C.', 'xaxx010101000', '', '', 0, '', 0, '', '', '', '', ''),
(36, 1, 1, 'Fernando Quiroz Madrigal', 'fquirozmadrigal@terra.com.mx', '5552483440', 15, 0, 1, 'México', '10400', 'Delegacion Magdalena Contreras', 'Av. Mexico', 'Fernando Quiroz Madrigal', 'QUMF580628QB3', '', '', 0, '', 0, '', '', '', '', ''),
(37, 1, 1, 'G.A.P.', 'cobranza@agentecapital.com', '800 987 22 01', 17, 0, 1, 'Yucatán', '97127', 'Mérida', 'Calle 27', 'G.A.P. Agente de Seguros y Finanzas S.A. de C.V', 'GAS061204RY4', '', '', 0, '', 0, '', '', '', '', ''),
(38, 1, 1, 'De Leon Asesores En Seguros Y Fianzas', 'seguros@victordeleon.com.mx', '474 742 509 25 ', 15, 0, 1, 'Jalisco', '47440', 'Lagos de Moreno', 'Luis Moreno 520 ', 'Victor Manuel de León Santana', 'LESV660107R17', '', '', 0, '', 0, '', '', '', '', ''),
(39, 1, 1, 'Mercurio Viajes', 'lcel1@hotmail.com', '83 87 30 27', 15, 0, 1, 'Nuevo León', '64840', 'Monterrey', 'Luis Elizondo', 'Mercurio Viajes S.A. de C.V.', 'MV1970310B8A', '', '', 0, '', 0, '', '', '', '', ''),
(40, 1, 1, 'Invax', 'placidozapata@yahoo.com', '811 044 54 58', 15, 0, 1, 'Nuevo León', '64630', 'Monterrey', 'AV.Puerta del Sol', 'Lic. Placido E. Zapara Taméz', '', '', '', 0, '', 0, '', '', '', '', ''),
(41, 4, 1, 'Emilia Elizabeth Cancino Esponda', '', '', 10, 0, 1, 'Chiapas', '29230', '', 'Flavio A. Paniagua', 'Emilia Elizabeth Cancino Esponda', '', '', '', 0, '', 0, '', '', '', '', ''),
(42, 1, 1, 'Jose Manuel Ponce De Leon Cubillas ', 'jmponcelc@prodigy.net.mx', '99 91 10 68 91', 10, 0, 1, 'Yucatán', '97000', 'Mérida', 'Calle 22  entre 29 Y 31', 'Jose Manuel Ponce de León Cubillas', '', '', '', 0, '', 0, '', '', '', '', ''),
(43, 1, 1, 'Wood Corredores De Seguros', 'carloscolin@wood.com.mx', '81 83 46 47 46', 15, 0, 1, 'Nuevo León', '64000', 'Monterrey', 'Treviño', 'Carlos Ignacio Colin Wood', '', '', '', 0, '', 0, '', '', '', '', ''),
(44, 1, 1, 'Eduardo Esparza Patíño', 'eduardoesparza@vidatranquila.com.mx', '55 27 49 16', 15, 0, 1, 'México', '11410', 'México', 'Lago Hutron', 'Eduardo Esparza Patino', '', '', '', 0, '', 0, '', '', '', '', ''),
(45, 1, 1, 'Juan Ricardo Castillo Morales', 'ricardo4786@hotmail.com', '818 029 3762', 10, 0, 1, 'Nuevo León', '64320', 'Monterrey', 'C. Valadeces', 'Juan Ricardo Castilllo Morales', 'CAMJ400624', '', '', 0, '', 0, '', '', '', '', ''),
(46, 1, 1, 'Alberto Escamilla Flores', 'escamillarodriguez@prodigy.net.mx', '8183336915', 10, 0, 1, 'Nuevo León', '64610', 'Monterrey', 'Terranova # 433  ', 'Alberto Escamilla Flores', 'EAFA611213KJ7', '', '', 0, '', 0, '', '', '', '', ''),
(47, 1, 1, 'Jose Antonio Valero Elizondo', 'admonantoniovalero@hotmail.com', '81 83633010', 15, 0, 1, 'Nuevo León', '64630', 'Monterrey', 'Real San Jose 123', 'Jose Antonio Valero Elizondo', 'VAEA751186626', '', '', 0, '', 0, '', '', '', '', ''),
(48, 1, 1, 'Rocha Y Asociados', 'ventas@rochayasociados.net', '83470405', 15, 0, 1, 'Nuevo León', '66430', 'Monterrey', 'Lerma2030', 'Rocha y asociados de seguros y finanzas', 'ROCG530413GT3', '', '', 0, '', 0, '', '', '', '', ''),
(49, 1, 1, 'Miguel Angel Ramirez Macias', 'mramirez@phyxius.com.mx', '8111068273', 10, 10, 1, 'Nuevo León', '66350', 'Santa Catarina', 'La Republica', 'Miguel Angel Ramirez Macias', '', '', '', 0, '', 0, '', '', '', '', ''),
(50, 1, 1, 'Maria Lizeth Guadalupe Araujo Vega', 'lizeth@araujoseguros.com', '8115990367', 15, 0, 1, 'Nuevo León', '64790', 'Monterrey', 'Puerto Mazatlan', 'María Lizeth Guadalupe Araujo Vega', 'AAVL8000920415', '', '', 0, '', 0, '', '', '', '', ''),
(51, 1, 1, 'Arven', 'jmartinez@arven.mx', '81 1554  5656', 15, 0, 1, 'Nuevo León', '64740', 'Guadalupe', 'Río Lerma 419', 'Seguros y Finanzas Arven', '', '', '', 0, '', 0, '', '', '', '', ''),
(52, 1, 1, 'Grupo Avance Asesoria En Seguros', 'dianacecylia23@hotmail.com', '844 416 77 88', 15, 0, 1, 'Coahuila ', '25270', 'Saltillo', 'Tamaulipas', 'Diana Cecylia Rodriguez Galindo', 'ROGD760123DPA', '', '', 0, '', 0, '', '', '', '', ''),
(53, 1, 1, 'Cortina Asesores', 'karlagzzp@gmail.com', '2470 0780 ', 15, 0, 1, 'Nuevo León', '64890', 'Monterrey', 'Camino de la Rinconada', 'Jose Alonso Cortina Marroquin', 'COMA610326JC8', '', '', 0, '', 0, '', '', '', '', ''),
(54, 1, 1, 'Cristie Esperanza Sandoval Montalbo', 'sandovalsegurosmty@gmail.com', '8118809660', 10, 0, 1, 'Nuevo León', '66220', 'Monterrey', 'Amonio Plaza', 'Cristi Esperanza Sandoval Montalvo', '', '', '', 0, '', 0, '', '', '', '', ''),
(55, 1, 1, 'Ytandehui Sosa Ibarra', 'ytasosa@gmail.com', '', 10, 0, 1, '', '', '', '', 'Ytandehui Sosa Ibarra', 'YTA', '', '', 0, '', 0, '', '', '', '', ''),
(56, 4, 1, 'José Luis Prieto Fuentes', 'pepeprieto7@hotmail.com', '8181619996', 10, 0, 1, 'Nuevo León', '66245', 'Monterrey', '', 'José Luis Prieto Fuentes', '', '', '', 0, '', 0, '', '', '', '', ''),
(57, 1, 1, 'Olivas Y Asociados Agente De Seguros Y  Fianzas', 'golivas@olivasyasociados.com', '8112578090', 15, 0, 1, 'Nuevo León', '64650', 'Monterrey', 'Blvd. Gustavo  Diaz Ordaz ', 'Olivas y Asociados de Seguros y Finanzas S.A de C.V.', 'OAA1511107V1', '', '', 0, '', 0, '', '', '', '', ''),
(58, 4, 1, 'Agentre De Seguros', 'pepeprieto7@hotmail.com', '', 15, 0, 1, 'Nuevo León', '66220', 'Monterrey', '', 'José Luis Prieto', '', '', '', 0, '', 0, '', '', '', '', ''),
(59, 1, 1, 'Thelma Margarita Gonzalez De La Fuente', 'thelmacastro@hotmail.com', '', 10, 0, 1, 'Nuevo León', '66220', 'San Pedro Garza García', 'Bizancio', 'Thelma Margarita Gonzalez de la Fuente', 'GOFT590127TC6', '', '', 0, '', 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `id` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `estado` varchar(127) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forma_pago`
--

CREATE TABLE `forma_pago` (
  `id` int(11) NOT NULL,
  `forma_pago` int(11) NOT NULL,
  `cargo_administrativo` decimal(10,2) NOT NULL,
  `activo_inactivo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fuente_referencia`
--

CREATE TABLE `fuente_referencia` (
  `id` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `fuente_referencia` varchar(63) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fuente_referencia`
--

INSERT INTO `fuente_referencia` (`id`, `activo_inactivo`, `fuente_referencia`) VALUES
(1, 1, 'Chat del Website'),
(2, 1, 'Correo electrónico'),
(3, 1, 'Escuela'),
(4, 1, 'Facebook'),
(5, 1, 'Ferias'),
(6, 1, 'Ferias MSI'),
(7, 1, 'Google'),
(8, 1, 'Publicidad'),
(9, 1, 'Referido'),
(10, 1, 'Sesión informativa'),
(11, 1, 'Website de MSI'),
(12, 1, 'WhatsApp');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `id` int(11) NOT NULL,
  `id_subagente` int(11) NOT NULL,
  `folio_seguro` int(11) NOT NULL,
  `no_recibo` int(11) NOT NULL,
  `cuenta` int(11) NOT NULL,
  `fecha_pago` date NOT NULL,
  `moneda` varchar(8) NOT NULL,
  `tipo_cambio` decimal(10,2) NOT NULL,
  `cantidad_abonada` decimal(10,2) NOT NULL,
  `forma_pago` int(11) NOT NULL,
  `comentario` text NOT NULL,
  `estado` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `cargo_administrativo` decimal(10,2) NOT NULL,
  `envio` decimal(10,2) NOT NULL,
  `id_cotizacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `pais` varchar(63) NOT NULL,
  `zona` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id`, `activo_inactivo`, `pais`, `zona`) VALUES
(1, 1, 'Mexico', 2),
(2, 1, 'Afganistan', 2),
(3, 1, 'Albania', 2),
(4, 1, 'Alemania', 2),
(5, 1, 'Andorra', 2),
(6, 1, 'Angola', 2),
(7, 1, 'Anguilla', 2),
(8, 1, 'Antartida', 2),
(9, 1, 'Antigua y Barbuda', 2),
(10, 1, 'Antillas Holandesas', 2),
(11, 1, 'Arabia Saudita', 2),
(12, 1, 'Argelia', 2),
(13, 1, 'Argentina', 2),
(14, 1, 'Armenia', 2),
(15, 1, 'Aruba', 2),
(16, 1, 'Asia', 2),
(17, 1, 'Australia', 2),
(18, 1, 'Austria', 2),
(19, 1, 'Azerbaiyan', 2),
(20, 1, 'Bahamas', 2),
(21, 1, 'Bahrein', 2),
(22, 1, 'Bangladesh', 2),
(23, 1, 'Barbados', 2),
(24, 1, 'Belarus', 2),
(25, 1, 'Belgica', 2),
(26, 1, 'Belice', 2),
(27, 1, 'Benin', 2),
(28, 1, 'Bermudas', 2),
(29, 1, 'Bhutan', 2),
(30, 1, 'Bolivia', 2),
(31, 1, 'Bosnia Herzegovina', 2),
(32, 1, 'Botswana', 2),
(33, 1, 'Isla Bouvet', 2),
(34, 1, 'Brasil', 2),
(35, 1, 'Brunei Darussalam', 2),
(36, 1, 'Bulgaria', 2),
(37, 1, 'Burkina Faso', 2),
(38, 1, 'Burundi', 2),
(39, 1, 'Cabo Verde', 2),
(40, 1, 'Islas Caiman', 2),
(41, 1, 'Camboya', 2),
(42, 1, 'Camerún', 2),
(43, 1, 'Canadá', 2),
(44, 1, 'Chad', 2),
(45, 1, 'República Checa', 2),
(46, 1, 'Chile', 2),
(47, 1, 'China', 2),
(48, 1, 'Chipre', 2),
(49, 1, 'Isla Christmas', 2),
(50, 1, 'Isla Cocos', 2),
(51, 1, 'Colombia', 2),
(52, 1, 'Comoros', 2),
(53, 1, 'Congo', 2),
(54, 1, 'Islas Cook', 2),
(55, 1, 'Corea del Norte', 2),
(56, 1, 'Corea del Sur', 2),
(57, 1, 'Costa de Marfil', 2),
(58, 1, 'Costa Rica', 2),
(59, 1, 'Croacia', 2),
(60, 1, 'Cuba', 2),
(61, 1, 'Dinamarca', 2),
(62, 1, 'Djibouti', 2),
(63, 1, 'Commonwealth de Dominica', 2),
(64, 1, 'República Dominicana', 2),
(65, 1, 'Ecuador', 2),
(66, 1, 'Egipto', 2),
(67, 1, 'El Salvador', 2),
(68, 1, 'Emiratos Arabes Unidos', 2),
(69, 1, 'Eritrea', 2),
(70, 1, 'República Eslovaca', 2),
(71, 1, 'Eslovenia', 2),
(72, 1, 'España', 2),
(73, 1, 'Estados Unidos de América', 1),
(74, 1, 'Estonia', 2),
(75, 1, 'Etiopia', 2),
(76, 1, 'Europa', 2),
(77, 1, 'Faroe, Islas', 2),
(78, 1, 'Fiji', 2),
(79, 1, 'Filipinas', 2),
(80, 1, 'Finlandia', 2),
(81, 1, 'Francia', 2),
(82, 1, 'Gabón', 2),
(83, 1, 'Gambia', 2),
(84, 1, 'Georgia', 2),
(85, 1, 'Ghana', 2),
(86, 1, 'Gibraltar', 2),
(87, 1, 'Granada', 2),
(88, 1, 'Grecia', 2),
(89, 1, 'Groenlandia', 2),
(90, 1, 'Guadalupe', 2),
(91, 1, 'Guam', 2),
(92, 1, 'Guatemala', 2),
(93, 1, 'Guayana Francesa', 2),
(94, 1, 'Guinea', 2),
(95, 1, 'Guinea-Bissau', 2),
(96, 1, 'Guyana', 2),
(97, 1, 'Haiti', 2),
(98, 1, 'Islas Heard y McDonald', 2),
(99, 1, 'Holanda', 2),
(100, 1, 'Honduras', 2),
(101, 1, 'Hong Kong', 2),
(102, 1, 'Hungría', 2),
(103, 1, 'India', 2),
(104, 1, 'Indonesia', 2),
(105, 1, 'Irán', 2),
(106, 1, 'Iraq', 2),
(107, 1, 'Irlanda', 2),
(108, 1, 'Islandia', 2),
(109, 1, 'Israel', 2),
(110, 1, 'Italia', 2),
(111, 1, 'Jamaica', 2),
(112, 1, 'Japón', 2),
(113, 1, 'Jordania', 2),
(114, 1, 'Kazajstán', 2),
(115, 1, 'Kenya', 2),
(116, 1, 'Kiribati', 2),
(117, 1, 'Kuwait', 2),
(118, 1, 'Kyrgyzstan', 2),
(119, 1, 'Laos', 2),
(120, 1, 'Latvia', 2),
(121, 1, 'Lesotho', 2),
(122, 1, 'Líbano', 2),
(123, 1, 'Liberia', 2),
(124, 1, 'Libia', 2),
(125, 1, 'Liechtenstein', 2),
(126, 1, 'Lituania', 2),
(127, 1, 'Luxemburgo', 2),
(128, 1, 'Macao', 2),
(129, 1, 'Macedonia', 2),
(130, 1, 'Madagascar', 2),
(131, 1, 'Malasia', 2),
(132, 1, 'Malawi', 2),
(133, 1, 'Maldivas', 2),
(134, 1, 'Mali', 2),
(135, 1, 'Malta', 2),
(136, 1, 'Islas  Malvinas', 2),
(137, 1, 'Marruecos', 2),
(138, 1, 'Islas Marshall', 2),
(139, 1, 'Martinica', 2),
(140, 1, 'Mauricio', 2),
(141, 1, 'Mauritania', 2),
(142, 1, 'Isla Mayotte', 2),
(143, 1, 'Micronesia', 2),
(144, 1, 'Moldova', 2),
(145, 1, 'Mongolia', 2),
(146, 1, 'Monserrat', 2),
(147, 1, 'Mozambique', 2),
(148, 1, 'Myanmar', 2),
(149, 1, 'Namibia', 2),
(150, 1, 'Nauru', 2),
(151, 1, 'Nepal', 2),
(152, 1, 'Nicaragua', 2),
(153, 1, 'Niger', 2),
(154, 1, 'Nigeria', 2),
(155, 1, 'Niue', 2),
(156, 1, 'Isla Norfolk', 2),
(157, 1, 'Noruega', 2),
(158, 1, 'Nueva Caledonia', 2),
(159, 1, 'Nueva Zelanda', 2),
(160, 1, 'Omán', 2),
(161, 1, 'Pakistán', 2),
(162, 1, 'Palau', 2),
(163, 1, 'Panamá', 2),
(164, 1, 'Papúa Nueva Guinea', 2),
(165, 1, 'Paraguay', 2),
(166, 1, 'Perú', 2),
(167, 1, 'Polonia', 2),
(168, 1, 'Portugal', 2),
(169, 1, 'Puerto Rico', 2),
(170, 1, 'Qatar', 2),
(171, 1, 'Reino Unido de la Gran Bretaña', 2),
(172, 1, 'República Central Africana', 2),
(173, 1, 'Departamento Francés', 2),
(174, 1, 'Rumania', 2),
(175, 1, 'Rusia', 2),
(176, 1, 'Rwanda', 2),
(177, 1, 'Islas Salomón', 2),
(178, 1, 'Samoa Americana', 2),
(179, 1, 'Samoa Occidental', 2),
(180, 1, 'San Marino', 2),
(181, 1, 'San Pedro y Miguelon', 2),
(182, 1, 'San Vincente y Granadinas', 2),
(183, 1, 'Isla Santa Helena', 2),
(184, 1, 'Santa Lucía', 2),
(185, 1, 'Senegal', 2),
(186, 1, 'Seychelles', 2),
(187, 1, 'Sierra Leona', 2),
(188, 1, 'Singapur', 2),
(189, 1, 'Siria', 2),
(190, 1, 'Somalia', 2),
(191, 1, 'Sri Lanka', 2),
(192, 1, 'Sudáfrica', 2),
(193, 1, 'Sudán', 2),
(194, 1, 'Suecia', 2),
(195, 1, 'Suiza', 2),
(196, 1, 'Suriname', 2),
(197, 1, 'Swazilandia', 2),
(198, 1, 'Tahití', 2),
(199, 1, 'Tailandia', 2),
(200, 1, 'Taiwán', 2),
(201, 1, 'Tanzania', 2),
(202, 1, 'Tayikistán', 2),
(203, 1, 'Togo', 2),
(204, 1, 'Tokelau', 2),
(205, 1, 'Tonga', 2),
(206, 1, 'Trinidad y Tobago', 2),
(207, 1, 'Islas Turcas y Caicos', 2),
(208, 1, 'Turkmenistán', 2),
(209, 1, 'Turquía', 2),
(210, 1, 'Tuvalu', 2),
(211, 1, 'Ucrania', 2),
(212, 1, 'Uganda', 2),
(213, 1, 'Uruguay', 2),
(214, 1, 'Uzbekistán', 2),
(215, 1, 'Vanuatu', 2),
(216, 1, 'Ciudad del Vaticano', 2),
(217, 1, 'Venezuela', 2),
(218, 1, 'Vietnam', 2),
(219, 1, 'Islas Vírgenes', 2),
(220, 1, ' Islas Vírgenes Brítanicas', 2),
(221, 1, 'Islas Wallis y Futuna', 2),
(222, 1, 'Yemen', 2),
(223, 1, 'Yugoslavia', 2),
(224, 1, 'Zaire', 2),
(225, 1, 'Zambia', 2),
(226, 1, 'Zimbabwe', 2),
(227, 1, 'Sudamerica', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `rol` varchar(63) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `activo_inactivo`, `rol`) VALUES
(1, 1, 'Administrador'),
(2, 1, 'Sub Agente'),
(3, 1, 'Super administrador'),
(4, 1, 'Agente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subagente`
--

CREATE TABLE `subagente` (
  `id` int(11) NOT NULL,
  `id_agente` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `nombres` varchar(127) NOT NULL,
  `apellidos` varchar(127) NOT NULL,
  `nombre_completo` varchar(255) NOT NULL,
  `correo_electronico` varchar(255) NOT NULL,
  `contrasena` varchar(127) NOT NULL,
  `telefono` varchar(32) NOT NULL,
  `comision` int(11) NOT NULL,
  `descuento` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `estado` text NOT NULL,
  `cp` int(11) NOT NULL,
  `ciudad` text NOT NULL,
  `calle_numero` varchar(511) NOT NULL,
  `razon_social` varchar(511) NOT NULL,
  `rfc` varchar(255) NOT NULL,
  `fac_correo_electronico` varchar(255) NOT NULL,
  `fac_telefono` varchar(32) NOT NULL,
  `fac_id_pais` int(11) NOT NULL,
  `fac_estado` text NOT NULL,
  `fac_cp` int(11) NOT NULL,
  `fac_ciudad` text NOT NULL,
  `fac_calle_numero` varchar(511) NOT NULL,
  `colonia` varchar(127) NOT NULL,
  `fac_colonia` varchar(127) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subagente`
--

INSERT INTO `subagente` (`id`, `id_agente`, `id_rol`, `activo_inactivo`, `nombres`, `apellidos`, `nombre_completo`, `correo_electronico`, `contrasena`, `telefono`, `comision`, `descuento`, `id_pais`, `estado`, `cp`, `ciudad`, `calle_numero`, `razon_social`, `rfc`, `fac_correo_electronico`, `fac_telefono`, `fac_id_pais`, `fac_estado`, `fac_cp`, `fac_ciudad`, `fac_calle_numero`, `colonia`, `fac_colonia`) VALUES
(1, 1, 1, 1, 'Edgar', 'Cantú', 'Edgar Cantú', 'ecantu@estudiantesembajadores.com', 'edgar123', '13548291', 0, 0, 1, 'Nuevo León', 64103, 'Monterrey', 'Dirección #12377', 'RS', 'ELRFC', 'fac@correo.com', '545641', 1, 'Nuevo León', 64101, 'Monterrey', 'Calle #Facturacion', 'Fray Servando Teresa de Mier', 'La colonia de fac'),
(2, 1, 2, 0, 'Edgar', 'Cantú', 'Edgar Cantú', 'ecantu@estudiantesembajadores.com', '', '4732842', 2, 4, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', ''),
(3, 3, 1, 1, 'Edgar', 'Cantú', 'Edgar Cantú', 'ecantu3@estudiantesembajadores.com', '', '34242342', 10, 10, 1, 'Nuevo León', 64102, 'Monterrey', 'Calle #3213', 'Razón social', 'elrfc432432', '', '', 1, 'Nuevo León', 64103, 'Monterrey', 'Calle #4334', 'Barrio Acero', 'Rómulo Lozano Morales'),
(4, 1, 3, 1, 'Edgar', 'Cantú', 'Edgar Cantú', 'ecantu@estudiantesembajadores.com', 'edgar123', '13548291', 0, 0, 1, 'Nuevo León', 64000, 'Monterrey', 'Dirección #123', 'RS', 'ELRFC', 'fac@correo.com', '545641', 1, 'Nuevo León', 64101, 'Monterrey', 'Calle #Facturacion', 'La colonia', 'La colonia de fac'),
(5, 1, 2, 0, 'Edgar', 'Cantú', 'Edgar Cantú', 'ecantu@estudiantesembajadores.com', '', '4732842', 2, 4, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', ''),
(6, 3, 1, 1, 'Edgar', 'Cantú', 'Edgar Cantú', 'ecantu3@estudiantesembajadores.com', '', '34242342', 10, 10, 1, 'Nuevo León', 64102, 'Monterrey', 'Calle #3213', 'Razón social', 'elrfc432432', '', '', 1, 'Nuevo León', 64103, 'Monterrey', 'Calle #4334', 'Barrio Acero', 'Rómulo Lozano Morales'),
(7, 117, 2, 1, 'Ejemplo', 'Apellidos', '', 'correo@correo.com', '', '4732894', 1, 1, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', ''),
(8, 1, 2, 1, 'Subagente', 'Subagente', 'Subagente Subagente', 'subagente@correo.com', '', '8493248923', 10, 10, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', ''),
(9, 2, 1, 1, 'Administrador', 'Administrador', 'Administrador Administrador', 'administrador@correo.com', '', '8493248923', 10, 10, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', ''),
(10, 2, 4, 1, 'Agente', 'Agente', 'Agente Agente', 'agente@correo.com', '', '8493248923', 10, 10, 0, '', 0, '', '', '', '', '', '', 0, '', 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_cambio`
--

CREATE TABLE `tipo_cambio` (
  `id` int(11) NOT NULL,
  `tipo_cambio` decimal(10,2) NOT NULL,
  `fecha` date NOT NULL,
  `id_subagente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_cambio`
--

INSERT INTO `tipo_cambio` (`id`, `tipo_cambio`, `fecha`, `id_subagente`) VALUES
(1, '18.74', '2018-09-01', 1),
(2, '17.60', '2018-09-02', 1),
(3, '18.95', '2018-09-03', 1),
(4, '19.05', '2018-09-04', 1),
(5, '0.00', '2018-09-05', 1),
(6, '0.00', '2018-09-06', 1),
(7, '18.70', '2018-09-07', 1),
(8, '18.77', '2018-09-08', 1),
(9, '18.78', '2018-09-09', 1),
(10, '18.79', '2018-09-10', 1),
(11, '18.81', '2018-09-11', 1),
(12, '19.02', '2018-09-12', 1),
(13, '13.03', '2018-09-13', 1),
(14, '18.79', '2018-09-14', 1),
(15, '19.50', '2018-09-15', 1),
(16, '18.54', '2018-09-17', 1),
(17, '17.30', '2018-09-17', 1),
(18, '18.95', '2018-09-17', 1),
(19, '19.55', '2018-09-17', 1),
(20, '18.79', '2018-09-20', 1),
(21, '18.77', '2018-09-20', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_cliente`
--

CREATE TABLE `tipo_cliente` (
  `id` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `tipo_cliente` varchar(63) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_cliente`
--

INSERT INTO `tipo_cliente` (`id`, `activo_inactivo`, `tipo_cliente`) VALUES
(1, 1, 'Prospecto'),
(2, 1, 'Asegurado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_distribuidor`
--

CREATE TABLE `tipo_distribuidor` (
  `id` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `tipo_distribuidor` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_distribuidor`
--

INSERT INTO `tipo_distribuidor` (`id`, `activo_inactivo`, `tipo_distribuidor`) VALUES
(1, 1, 'Agente'),
(2, 1, 'Empresa'),
(3, 1, 'Estudiantes Embajadores'),
(4, 1, 'Mi Seguro Internacional'),
(5, 1, 'Universidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_poliza`
--

CREATE TABLE `tipo_poliza` (
  `id` int(11) NOT NULL,
  `id_tipo_seguro` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `tipo_poliza` varchar(64) NOT NULL,
  `nombre_poliza` varchar(63) NOT NULL,
  `codigo_poliza` varchar(63) NOT NULL,
  `zona` int(11) NOT NULL,
  `deducible` int(11) NOT NULL,
  `dsm` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_poliza`
--

INSERT INTO `tipo_poliza` (`id`, `id_tipo_seguro`, `activo_inactivo`, `tipo_poliza`, `nombre_poliza`, `codigo_poliza`, `zona`, `deducible`, `dsm`) VALUES
(1, 1, 1, 'Oro', 'Oro D1-100', 'HAITL3Z1SGC-Setmex', 1, 100, 'Días'),
(2, 1, 1, 'Oro', 'Oro D2-100', 'HAITL3Z2SGC-Setmex', 2, 100, 'Días'),
(3, 1, 1, 'Oro', 'Oro S1-100', 'HAITL3Z1SGC-Setmex', 1, 100, 'Semanas'),
(4, 1, 1, 'Oro', 'Oro S2-100', 'HAITL3Z2SGC-Setmex', 2, 100, 'Semanas'),
(5, 1, 1, 'Oro', 'Oro M1-100', 'HAITL3Z1LGC-Setmex', 1, 100, 'Meses'),
(6, 1, 1, 'Oro', 'Oro M2-100', 'HAITL3Z2LGC-Setmex', 2, 100, 'Meses'),
(7, 1, 1, 'Plata', 'Plata D1-100', 'HAITL3Z1SSC-Setmex', 1, 100, 'Días'),
(8, 1, 1, 'Plata', 'Plata D2-100', 'HAITL3Z2SSC-Setmex', 2, 100, 'Días'),
(9, 1, 1, 'Plata', 'Plata S1-100', 'HAITL3Z1SSC-Setmex', 1, 100, 'Semanas'),
(10, 1, 1, 'Plata', 'Plata S2-100', 'HAITL3Z2SSC-Setmex', 2, 100, 'Semanas'),
(11, 1, 1, 'Plata', 'Plata M1-100', 'HAITL3Z1LSC-Setmex', 1, 100, 'Meses'),
(12, 1, 1, 'Plata', 'Plata M2-100', 'HAITL3Z2LSC-Setmex', 2, 100, 'Meses'),
(13, 1, 1, 'Bronce', 'Bronce D1-100', 'HAITL3Z1SBC-Setmex', 1, 100, 'Días'),
(14, 1, 1, 'Bronce', 'Bronce D2-100', 'HAITL3Z2SBC-Setmex', 2, 100, 'Días'),
(15, 1, 1, 'Bronce', 'Bronce S1-100', 'HAITL3Z1SBC-Setmex', 1, 100, 'Semanas'),
(16, 1, 1, 'Bronce', 'Bronce S2-100', 'HAITL3Z2SBC-Setmex', 2, 100, 'Semanas'),
(17, 1, 1, 'Bronce', 'Bronce M1-100', 'HAITL3Z1LBC-Setmex', 1, 100, 'Meses'),
(18, 1, 1, 'Bronce', 'Bronce M2-100', 'HAITL3Z2LBC-Setmex', 2, 100, 'Meses'),
(19, 2, 1, 'Plata', 'Plata S1-100-70', 'HAITL3Z1SSC-Setmex-over70', 1, 100, 'Semanas'),
(20, 2, 1, 'Plata', 'Plata S2-100-70', 'HAITL3Z2SSC-Setmex-over70', 2, 100, 'Semanas'),
(21, 2, 1, 'Plata', 'Plata M1-100-70', 'HAITL3Z1LSC-Setmex-over70', 1, 100, 'Meses'),
(22, 2, 1, 'Plata', 'Plata M2-100-70', 'HAITL3Z2LSC-Setmex-over70', 2, 100, 'Meses'),
(23, 3, 1, 'Oro', 'Oro S1-100-U6', 'HAITL3Z1SGC-Setmex-under6', 1, 100, 'Semanas'),
(24, 3, 1, 'Oro', 'Oro S2-100-U6', 'HAITL3Z2SGC-Setmex-under6', 2, 100, 'Semanas'),
(25, 3, 1, 'Oro', 'Oro M1-100-U6', 'HAITL3Z1LGC-Setmex-under6', 1, 100, 'Meses'),
(26, 3, 1, 'Oro', 'Oro M2-100-U6', 'HAITL3Z2LGC-Setmex-under6', 2, 100, 'Meses');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_seguro`
--

CREATE TABLE `tipo_seguro` (
  `id` int(11) NOT NULL,
  `activo_inactivo` int(11) NOT NULL,
  `tipo_seguro` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_seguro`
--

INSERT INTO `tipo_seguro` (`id`, `activo_inactivo`, `tipo_seguro`) VALUES
(1, 1, 'Estudiante/Viajero'),
(2, 1, 'Mayores de 70 años'),
(3, 1, 'Menores de 6 años');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agente`
--
ALTER TABLE `agente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cobertura`
--
ALTER TABLE `cobertura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cp`
--
ALTER TABLE `cp`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `direccion`
--
ALTER TABLE `direccion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `distribuidor`
--
ALTER TABLE `distribuidor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `fuente_referencia`
--
ALTER TABLE `fuente_referencia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `subagente`
--
ALTER TABLE `subagente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_cambio`
--
ALTER TABLE `tipo_cambio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_distribuidor`
--
ALTER TABLE `tipo_distribuidor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_poliza`
--
ALTER TABLE `tipo_poliza`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_seguro`
--
ALTER TABLE `tipo_seguro`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `agente`
--
ALTER TABLE `agente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT de la tabla `cobertura`
--
ALTER TABLE `cobertura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=239;
--
-- AUTO_INCREMENT de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT de la tabla `cp`
--
ALTER TABLE `cp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `direccion`
--
ALTER TABLE `direccion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `distribuidor`
--
ALTER TABLE `distribuidor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `fuente_referencia`
--
ALTER TABLE `fuente_referencia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `pago`
--
ALTER TABLE `pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=228;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `subagente`
--
ALTER TABLE `subagente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `tipo_cambio`
--
ALTER TABLE `tipo_cambio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tipo_distribuidor`
--
ALTER TABLE `tipo_distribuidor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `tipo_poliza`
--
ALTER TABLE `tipo_poliza`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `tipo_seguro`
--
ALTER TABLE `tipo_seguro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
