import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { HTTPService }    from '../../http.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';

@Component({
  selector: 'app-excel-asignar',
  templateUrl: './excel-asignar.component.html',
  styleUrls: ['./excel-asignar.component.css']
})
export class ExcelAsignarComponent implements OnInit {

	constructor(
    public dialogRef: MatDialogRef<ExcelAsignarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService) {}

  cerrar(): void {
    this.dialogRef.close();
  }

  public IdSubagente = 0;

  ngOnInit() {    
    this.Formulario.obtener("Tipo_Distribuidor");
    //data manda ID del cliente
    // console.log(this.data);
    // var id_cliente = this.data;  
    // this.HTTP.datosDe("cliente", [{campo: "id", valor: id_cliente}])
    //   .subscribe(
    //     datos => {
    //       this.DatosCliente = datos[0];
    //     },

    //     error => {
    //       console.log(error);
    //     }
    //   );
  }

  DatosAsignar = {
    id_tipo_distribuidor: null,
    id_distribuidor: null,
    id_agente: null,
    id_subagente: null
  }
  
  cambio(cual){
    if(cual === "Tipo_Distribuidor")
      this.Formulario.obtener("Distribuidor", {Campo: 'id_tipo_distribuidor', Valor: this.DatosAsignar.id_tipo_distribuidor});
    else if(cual === "Distribuidor")
      this.Formulario.obtener("Agente", {Campo: 'id_distribuidor', Valor: this.DatosAsignar.id_distribuidor});
    else if(cual === "Agente")
      this.Formulario.obtener("Subagente", {Campo: 'id_agente', Valor: this.DatosAsignar.id_agente});
  }

  asignarSeguro(){
    // this.DatosCliente.id_subagente = this.DatosAsignar.id_subagente;
    // this.Formulario.guardar("cliente", this.DatosCliente);
    this.dialogRef.close({id_subagente: this.DatosAsignar.id_subagente});
  }

}
