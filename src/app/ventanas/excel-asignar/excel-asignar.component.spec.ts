import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExcelAsignarComponent } from './excel-asignar.component';

describe('ExcelAsignarComponent', () => {
  let component: ExcelAsignarComponent;
  let fixture: ComponentFixture<ExcelAsignarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExcelAsignarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExcelAsignarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
