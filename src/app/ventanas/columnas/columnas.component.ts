import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-columnas',
  templateUrl: './columnas.component.html',
  styleUrls: ['./columnas.component.css']
})
export class ColumnasComponent implements OnInit {
  
  constructor(
	  public dialogRef: MatDialogRef<ColumnasComponent>,
	  @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}
