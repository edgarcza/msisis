import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { HTTPService }    from '../../http.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';

@Component({
  selector: 'app-recuperar-contrasena',
  templateUrl: './recuperar-contrasena.component.html',
  styleUrls: ['./recuperar-contrasena.component.css']
})
export class RecuperarContrasenaComponent implements OnInit {

	constructor(
    public dialogRef: MatDialogRef<RecuperarContrasenaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService) {}

  cerrar(): void {
    this.dialogRef.close();
  }

  public Datos = {
  	correo_electronico: null
  }

  public Mensaje = {
  	hay: false,
  	contenido: null
  }

  recuperarContrasena(){
  	this.HTTP.recuperarContrasena(this.Datos.correo_electronico, 1)
  		.subscribe(
  			datos => {
  				this.Mensaje.hay = true;
  				if(datos.enviado == false)
  					this.Mensaje.contenido = "Error: " + datos.razon;

  				else if(datos.enviado == true)
  					this.Mensaje.contenido = "Se envió un correo a " + datos.correo_electronico;

  				else
  					this.Mensaje.contenido = "Error";
  			},

  			error => {
  				console.log(error);
  			}
  			);
  }

  ngOnInit() {
  	
  }

}
