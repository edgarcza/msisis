import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodigosPromocionComponent } from './codigos-promocion.component';

describe('CodigosPromocionComponent', () => {
  let component: CodigosPromocionComponent;
  let fixture: ComponentFixture<CodigosPromocionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodigosPromocionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodigosPromocionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
