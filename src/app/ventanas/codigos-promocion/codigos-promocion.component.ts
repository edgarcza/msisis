import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { HTTPService }    from '../../http.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';
import {SesionService} from '../../sesion.service'

@Component({
  selector: 'app-codigos-promocion',
  templateUrl: './codigos-promocion.component.html',
  styleUrls: ['./codigos-promocion.component.css']
})
export class CodigosPromocionComponent implements OnInit {

	constructor(
    public dialogRef: MatDialogRef<CodigosPromocionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService,
    public sesion: SesionService) {}

  public DatosCodigo = {
    id: null,
    codigo_promocion: null,
    porcentaje: null,
    id_subagente: null,
    fecha_creacion: null,
  }

  public DatosCodigos = [];
  DatosCantidad = 0;
  public OpcionesCodigos: boolean[] = [];

  ngOnInit() {
    this.tabla();
  }

  cerrar(): void {
    this.dialogRef.close();
  }

  guardarCodigo(datos){
    if(datos.codigo_promocion && datos.porcentaje)
    {
      datos.fecha_creacion = new Date();
      datos.id_subagente = this.sesion.id_datos();
      this.Formulario.guardar("codigo_promocion", datos, null, () => {this.tabla(); this.DatosCodigo.codigo_promocion = null; this.DatosCodigo.porcentaje = null;});
    }
  }

  eliminarCodigo(datos){
    this.Formulario.borrar("codigo_promocion", datos, () => {this.tabla();});
  }

  tabla(pagina = 1, cantidad = 10){
    this.HTTP.datosDe("codigo_promocion", null, null, cantidad, pagina).subscribe(
      (datos) => {
        console.log(datos);
        this.DatosCodigos = datos;    
        this.DatosCantidad = 0;    
        for(var Numero in this.DatosCodigos)
        {
          this.OpcionesCodigos[this.DatosCodigos[Numero]['id']] = false;
          this.DatosCantidad++;
        }
      },

      error => {
        console.log(error);
      }

    );
  }

  opcionCambio(datos){
    if(this.OpcionesCodigos[datos] == false)
      this.OpcionesCodigos[datos] = true;
    else
      this.OpcionesCodigos[datos] = false;
  }

  cambioPagina(event){
		this.tabla(event.pageIndex, event.pageSize);
	}

}
