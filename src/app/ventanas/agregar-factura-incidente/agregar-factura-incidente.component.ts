import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatDatepickerInputEvent, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS} from '../../clases/FechaAdaptador';

import { HTTPService }    from '../../http.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';
import {SesionService} from '../../sesion.service';

import {Archivo} from '../../interfaces/archivo';

@Component({
  selector: 'app-agregar-factura-incidente',
  templateUrl: './agregar-factura-incidente.component.html',
  styleUrls: ['./agregar-factura-incidente.component.css'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class AgregarFacturaIncidenteComponent implements OnInit {

	constructor(
    public dialogRef: MatDialogRef<AgregarFacturaIncidenteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService,
    public sesion: SesionService) {}

  cerrar(): void {
    this.dialogRef.close();
  }

  public DatosFacturaIncidente = {
  	id: null,
  	id_incidente: null,
  	fecha: null,
  	cantidad: null,
  	moneda: null,
  	comentario: null,
  	archivo: null,
  }

  public Archivo = {
    URL_TMP: null,
    Formato: null,
    Archivo: {
      filename: null,
      filetype: null,
      value: null,
    },
    Nombre: null,
  };

  guardarFactura(){
    if(this.data.Moneda && (this.DatosFacturaIncidente.moneda !== this.data.Moneda)){
      this.Formulario.alerta("Utilizar la misma moneda que facturas anteriores", "Cerrar");
      return;
    }
  	this.Formulario.guardar('factura_incidente', this.DatosFacturaIncidente, null, (id) => {
	  	if(this.Archivo.Archivo)
	    	this.Formulario.subirArchivo(this.Archivo.Archivo, "FacturaIncidente", id, () => {this.Formulario.Alerta.open("Factura guardada", "Cerrar"); this.cerrar();});
  	}, false);
  }

  cambioArchivo(event){
  	this.Formulario.onSelectFile(event, this.Archivo, () => {this.DatosFacturaIncidente.archivo = this.Archivo.Formato; console.log(this.Archivo)});
  }


  ngOnInit() {
  	this.DatosFacturaIncidente.id_incidente = this.data.DatosIncidente.id;
  	this.DatosFacturaIncidente.fecha = new Date();
  }

}
