import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarFacturaIncidenteComponent } from './agregar-factura-incidente.component';

describe('AgregarFacturaIncidenteComponent', () => {
  let component: AgregarFacturaIncidenteComponent;
  let fixture: ComponentFixture<AgregarFacturaIncidenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarFacturaIncidenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarFacturaIncidenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
