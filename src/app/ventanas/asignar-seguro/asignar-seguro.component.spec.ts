import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarSeguroComponent } from './asignar-seguro.component';

describe('AsignarSeguroComponent', () => {
  let component: AsignarSeguroComponent;
  let fixture: ComponentFixture<AsignarSeguroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarSeguroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarSeguroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
