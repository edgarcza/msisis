import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { HTTPService }    from '../../http.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';

@Component({
  selector: 'app-asignar-seguro',
  templateUrl: './asignar-seguro.component.html',
  styleUrls: ['./asignar-seguro.component.css']
})
export class AsignarSeguroComponent implements OnInit {

	constructor(
    public dialogRef: MatDialogRef<AsignarSeguroComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService) {}

  cerrar(): void {
    this.dialogRef.close();
  }

  tipoDistribuidor = [
  	{id: 1, nombre: "EE"}
  ];

  distribuidor = [
  	{id: 1, nombre: "Estudiantes Embajadores"}
  ];

  agente = [
  	{id: 1, nombre: "Monterrey"}
  ];

  subAgente = [
  	{id: 1, nombre: "Eder Jiménez"}
  ];

  Datos = {
  	tipoDistribuidor: "",
  	distribuidor: "",
  	agente: "",
  	subAgente: ""
  };

  // constructor() { }

  DatosAsignar = {
    id_tipo_distribuidor: null,
    id_distribuidor: null,
    id_agente: null,
    id_subagente: null
  }

  DatosCliente = { id_subagente: null };

  ngOnInit() {
    this.Formulario.obtener("Tipo_Distribuidor");
    //data manda ID del cliente
    console.log(this.data);
    var id_cliente = this.data;  
    this.HTTP.datosDe("cliente", [{campo: "id", valor: id_cliente}])
      .subscribe(
        datos => {
          this.DatosCliente = datos[0];
        },

        error => {
          console.log(error);
        }
      );
  }

  cambio(cual){
    if(cual === "Tipo_Distribuidor")
      this.Formulario.obtener("Distribuidor", {Campo: 'id_tipo_distribuidor', Valor: this.DatosAsignar.id_tipo_distribuidor});
    else if(cual === "Distribuidor")
      this.Formulario.obtener("Agente", {Campo: 'id_distribuidor', Valor: this.DatosAsignar.id_distribuidor});
    else if(cual === "Agente")
      this.Formulario.obtener("Subagente", {Campo: 'id_agente', Valor: this.DatosAsignar.id_agente});
  }

  asignarSeguro(){
    this.DatosCliente.id_subagente = this.DatosAsignar.id_subagente;
    this.Formulario.guardar("cliente", this.DatosCliente);
  }

}
