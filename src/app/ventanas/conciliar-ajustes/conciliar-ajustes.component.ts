import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatDatepickerInputEvent, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS} from '../../clases/FechaAdaptador';

import { HTTPService }    from '../../http.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';
import {SesionService} from '../../sesion.service'

@Component({
  selector: 'app-conciliar-ajustes',
  templateUrl: './conciliar-ajustes.component.html',
  styleUrls: ['./conciliar-ajustes.component.css'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class ConciliarAjustesComponent implements OnInit {

	constructor(
    public dialogRef: MatDialogRef<ConciliarAjustesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService,
    public sesion: SesionService) {}

  cerrar(datos): void {
    this.dialogRef.close(datos);
  }

  public DatosConciliar = {
    id: null,
    id_subagente: null,
    activo_inactivo: null,
    id_tipo_seguro: null,
    origen: null,
    destino: null,
    salida: null,
    regreso: null,
    codigo_promocion: null,
    id_tipo_poliza: null,
    numero_dias: null,
    numero_semanas_meses: null,
    precioMXN: null,
    precioMXN_descuento: null,
    precioMXN_ajuste: null,
    precioMXN_iva: null,
    precioMXN_subtotal: null,
    precioMXN_comision: null,
    precioMXN_total: null,
    precioUSD: null,
    precioUSD_descuento: null,
    precioUSD_ajuste: null,
    precioUSD_iva: null,
    precioUSD_subtotal: null,
    precioUSD_comision: null,
    precioUSD_total: null,
    tipo_cambio: null,
    descripcion: null,
    fecha_hecha: null,
    fecha_asegurado: null,
    folio: null,
    comentario: null,
    cargo_administrativo: null,
    envio: null,
    saldo_pendiente_MXN: null,
    saldo_pendiente_USD: null,
    saldo_liquidado_MXN: null,
    saldo_liquidado_USD: null,
    id_codigo_promocion: null,
    factura: null,
    factura_numero: null,
    factura_fecha: null,
    factura_monto: null,
    factura_status: null,
    factura_archivo: null,
  }

  public Archivo = {
    URL_TMP: null,
    Formato: null,
    Archivo: {
      filename: null,
      filetype: null,
      value: null,
    },
    Nombre: null,
  };

  cambioArchivo(event){
  	this.Formulario.onSelectFile(event, this.Archivo, () => {
      this.DatosConciliar.factura_archivo = this.Archivo.Formato; console.log(this.Archivo);
      // for(var Num in this.DatosSeleccionados){
      //   this.DatosSeleccionados[Num].factura_archivo = this.Archivo.Formato;
      // }
    });
  }

  guardarConciliar(){
    // this.DatosConciliar.factura_status = 1;
  	// this.Formulario.guardar('cotizacion', this.DatosConciliar, null, (id) => {
	  // 	if(this.Archivo.Archivo)
	  //   	this.Formulario.subirArchivo(this.Archivo.Archivo, "Conciliar", id, () => {this.Formulario.Alerta.open("Conciliar guardado", "Cerrar")});
    // }, false);
    this.DatosConciliar.factura_status = 1;
    this.guardarConciliarR(this.DatosSeleccionados.length - 1);

    // for(var Num in this.DatosSeleccionados){
      
    // }

  }

  guardarConciliarR(num){
    if(num < 0) {
      this.Formulario.Alerta.open("Conciliar guardado", "Cerrar"); 
      return this.cerrar(true)
    };
    this.DatosConciliar.id = this.DatosSeleccionados[num].id;
    this.Formulario.guardar('cotizacion', this.DatosConciliar, null, (id) => {
      if(this.Archivo.Archivo)
        this.Formulario.subirArchivo(this.Archivo.Archivo, "Conciliar", id, () => {
          // this.Formulario.Alerta.open("Conciliar guardado", "Cerrar")
          this.guardarConciliarR(num - 1);
        });
    }, false);
  }

  public DatosSeleccionados = [];

  ngOnInit() {
    
    this.DatosConciliar.factura_fecha = new Date();
    this.DatosConciliar.factura_fecha.setUTCHours(8,0,0,0);
    // console.log(this.DatosConciliar.factura_fecha);
    console.log(this.data);
    if(this.data.Seleccionados <= 0) this.data.DatosSeleccionados.push(this.data.DatosCotizacion);
    // if(this.data.Seleccionados > 0){
    else
      this.DatosSeleccionados = this.data.DatosSeleccionados;
      // console.log("Antes del for1", this.data.DatosSeleccionados, this.data.DatosCotizacion);
      let elAbierto = false;
      for(var Num in this.DatosSeleccionados){
        if(this.DatosSeleccionados[Num].id == this.data.DatosCotizacion.id){
          elAbierto = true;
          // this.DatosSeleccionados.pop();
        }
      }
      if(!elAbierto)
        this.DatosSeleccionados.push(this.data.DatosCotizacion);

      // this.DatosConciliar.factura_monto = 0;
        
      // console.log("Antes del for", this.DatosSeleccionados);
      // for(var Num in this.DatosSeleccionados){
      //   // console.log( Num);
        
      // }
    // }

    if(this.data.DatosCotizacion){
      this.HTTP.datosDe("cotizacion", [{campo: "id", valor: this.data.DatosCotizacion.id}])
        .subscribe(
          datos => {
            console.log(datos);

            this.DatosConciliar = datos[0];
            this.DatosConciliar.factura_fecha = new Date();
            this.DatosConciliar.factura_fecha.setUTCHours(8,0,0,0);

            // if(this.DatosConciliar.factura_fecha === "")
            //   this.DatosConciliar.factura_fecha = new Date();

            // this.DatosConciliar.factura_monto = this.DatosConciliar.precioMXN;
            // this.DatosConciliar.factura_monto = 0;

          },

          error => {
            console.log(error);
          }
        );
    }
    this.DatosConciliar.factura_monto = Number(0);
    this.obtenerDatos(this.DatosSeleccionados.length - 1);
    // if(this.data.DatosSeleccionados && this.data.DatosSeleccionados){
    //   console.log(this.data.DatosSeleccionados);
    // }
  }

  obtenerDatos(num){
    if(num < 0) return this.obtenerMonto();
    this.HTTP.datosDe("cotizacion", [{campo: "id", valor: this.DatosSeleccionados[num].id}])
    .subscribe(
      datos => {
        // console.log(datos);  

        this.DatosSeleccionados[num] = datos[0];

        if(this.DatosSeleccionados[num].factura_fecha === "")            
          this.DatosSeleccionados[num].factura_fecha = new Date();

        this.HTTP.coberturaCotizacion({Cotizacion: this.DatosSeleccionados[num].id}).subscribe(
          datos => {
            console.log(datos);
            console.log(this.DatosConciliar.factura_monto);
            this.DatosConciliar.factura_monto = Number.parseFloat(this.DatosConciliar.factura_monto) + Number.parseFloat(datos['Cobertura'][0].costo_final);
          }
        );

        // console.log(this.DatosSeleccionados, num);

        // // this.DatosSeleccionados.factura
        // this.DatosConciliar.factura_monto += Number(this.DatosSeleccionados[num].precioMXN);
        // console.log(num, this.DatosConciliar.factura_monto, Number(this.DatosSeleccionados[num].precioMXN));
        // this.DatosConciliar.factura_monto = this.Formulario.redondear(this.DatosConciliar.factura_monto, 2);

        this.obtenerDatos(num - 1)

      },

      error => {
        console.log(error);
      }
    );
  }
    
  obtenerMonto()
  {
    // let facturaTotal = 0;
    // for(var num in this.DatosSeleccionados)
    // {
    //   // this.DatosConciliar.factura_monto += Number(this.DatosSeleccionados[num].precioUSD);
    //   // console.log(num, this.DatosConciliar.factura_monto, Number(this.DatosSeleccionados[num].precioUSD));
    //   // this.DatosConciliar.factura_monto = this.Formulario.redondear(this.DatosConciliar.factura_monto, 2);
    //   // console.log(this.DatosConciliar.factura_monto);
    //   facturaTotal += Number(this.DatosSeleccionados[num].precioUSD);
    //   console.log(num, facturaTotal, Number(this.DatosSeleccionados[num].precioUSD));
    //   facturaTotal = this.Formulario.redondear(facturaTotal, 2);
    //   console.log(facturaTotal);
    // }
    // this.DatosConciliar.factura_monto = facturaTotal;
  }

}
