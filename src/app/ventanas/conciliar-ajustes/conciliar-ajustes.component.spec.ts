import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConciliarAjustesComponent } from './conciliar-ajustes.component';

describe('ConciliarAjustesComponent', () => {
  let component: ConciliarAjustesComponent;
  let fixture: ComponentFixture<ConciliarAjustesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConciliarAjustesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConciliarAjustesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
