import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { HTTPService }    from '../../http.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';
import {SesionService} from '../../sesion.service'

@Component({
  selector: 'app-agregar-comentario-incidente',
  templateUrl: './agregar-comentario-incidente.component.html',
  styleUrls: ['./agregar-comentario-incidente.component.css']
})
export class AgregarComentarioIncidenteComponent implements OnInit {

	constructor(
    public dialogRef: MatDialogRef<AgregarComentarioIncidenteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService,
    public sesion: SesionService) {}

  public DatosComentarioIncidente = {
  	id: null,
  	id_incidente: null,
  	fecha_hora: null,
  	comentario: null,
  	archivo: null,
  	id_subagente: null,
  }

  cerrar(): void {
    this.dialogRef.close();
  }

  public Archivo = {
    URL_TMP: null,
    Formato: null,
    Archivo: {
      filename: null,
      filetype: null,
      value: null,
    },
    Nombre: null,
  };

  cambioArchivo(event){
  	this.Formulario.onSelectFile(event, this.Archivo, () => {this.DatosComentarioIncidente.archivo = this.Archivo.Formato; console.log(this.Archivo)});
  }

  guardarComentario(){
  	this.Formulario.guardar('historial_comentarios', this.DatosComentarioIncidente, null, (id) => {
	  	if(this.Archivo.Archivo)
	    	this.Formulario.subirArchivo(this.Archivo.Archivo, "HistorialComentarios", id, () => {this.Formulario.Alerta.open("Comentario guardado", "Cerrar")});
  	}, false);
  }

  ngOnInit() {
  	this.DatosComentarioIncidente.id_incidente = this.data.DatosIncidente.id;
  	this.DatosComentarioIncidente.id_subagente = this.sesion.id();
    this.DatosComentarioIncidente.fecha_hora = new Date();
  }

}
