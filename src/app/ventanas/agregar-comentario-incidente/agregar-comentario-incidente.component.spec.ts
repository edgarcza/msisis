import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarComentarioIncidenteComponent } from './agregar-comentario-incidente.component';

describe('AgregarComentarioIncidenteComponent', () => {
  let component: AgregarComentarioIncidenteComponent;
  let fixture: ComponentFixture<AgregarComentarioIncidenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarComentarioIncidenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarComentarioIncidenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
