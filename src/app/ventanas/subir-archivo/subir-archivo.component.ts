import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { HTTPService }    from '../../http.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';
import {SesionService} from '../../sesion.service'

@Component({
  selector: 'app-subir-archivo',
  templateUrl: './subir-archivo.component.html',
  styleUrls: ['./subir-archivo.component.css']
})
export class SubirArchivoComponent implements OnInit {

	constructor(
    public dialogRef: MatDialogRef<SubirArchivoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService,
    public sesion: SesionService) {}

  cerrar(): void {
    this.dialogRef.close({Subido: false});
  }

  public Archivo = {
    URL_TMP: null,
    Formato: null,
    Archivo: {
      filename: null,
      filetype: null,
      value: null,
    },
    Nombre: null,
  };

  cambioArchivo(event){
  	this.Formulario.onSelectFile(event, this.Archivo, () => {console.log(this.Archivo)});
  }

  guardarArchivo(){
    if(this.Archivo.Archivo)
      this.Formulario.subirArchivo(this.Archivo.Archivo, this.data.Carpeta, this.data.Datos.id, () => {
        this.Formulario.Alerta.open("Archivo guardado", "Cerrar"); 
        this.dialogRef.close({Subido: true});});
  }

  ngOnInit() {
  }

}
