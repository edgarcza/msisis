import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { HTTPService }    from '../../http.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';
import {SesionService} from '../../sesion.service'
import { MatDatepickerInputEvent, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS} from '../../clases/FechaAdaptador';

@Component({
  selector: 'app-comision-ajustes',
  templateUrl: './comision-ajustes.component.html',
  styleUrls: ['./comision-ajustes.component.css'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class ComisionAjustesComponent implements OnInit {

	constructor(
    public dialogRef: MatDialogRef<ComisionAjustesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService,
    public sesion: SesionService) {}

  cerrar(): void {
    this.dialogRef.close();
  }

  public DatosComision = {
    id: null,
    id_subagente: null,
    folio_seguro: null,
    estado: null,
    comision: null,
    subcomision: null,
    impuestos: null,
    ajuste: null,
    comision_final: null,
    estado_factura: null,
    factura: null,
    fecha_pago: null,
    id_forma_pago: null,
    id_cuenta: null,
  };

  public DatosImpuestos = [];
  public SumaImpuestos = 0;

  cambioFormaPago(){
    this.Formulario.obtener("Cuenta", {Campo: 'id_forma_pago', Valor: this.DatosComision.id_forma_pago});
  }

  guardarComision(){
    if(this.DatosComision.factura !== ""){
      this.DatosComision.fecha_pago = new Date();
      this.Formulario.guardar('Comision', this.DatosComision);
    }
    else
      this.Formulario.alerta("Subir factura", "Cerrar");
  }

  cambiarComisionFinal(){
    // this.DatosComision.comision_final = this.DatosComision.comision - this.SumaImpuestos + this.DatosComision.ajuste;
    // this.DatosComision.comision_final = this.DatosComision.subcomision + this.Impuestos.IVA - this.Impuestos.IPC + this.DatosComision.ajuste;
    this.DatosComision.comision_final = this.DatosComision.comision + this.DatosComision.ajuste;
    // this.DatosComision.comision_final = this.DatosComision.comision;
  }

  private Impuestos = {
    IVA: null,
    IPC: null,
  }

  obtenerImpuestos(){    
    this.HTTP.datosDe("impuesto")
    .subscribe(
      datos => {
        console.log(datos);
        this.DatosImpuestos = datos;

        for(var Num in this.DatosImpuestos){
          this.DatosImpuestos[Num].valor = (this.DatosImpuestos[Num].porcentaje / 100) * this.DatosComision.subcomision;
          this.SumaImpuestos += this.DatosImpuestos[Num].valor;
          if(this.DatosImpuestos[Num].nombre_impuesto === "IVA") this.Impuestos.IVA = this.DatosImpuestos[Num].valor;
          if(this.DatosImpuestos[Num].nombre_impuesto === "IPC") this.Impuestos.IPC = this.DatosImpuestos[Num].valor;
        }

      },

      error => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
    this.Formulario.obtener("Forma_Pago");

    //SI ES MODIFICAR...
    if(this.data.DatosComision){
      console.log(this.data);                 
      this.HTTP.datosDe("comision", [{campo: "id", valor: this.data.DatosComision.id}])
        .subscribe(
          datos => {
            console.log(datos);
            this.DatosComision = datos[0];
            this.DatosComision.subcomision = this.Formulario.redondear((this.DatosComision.comision * 1.053), 2);
            this.cambioFormaPago();
            this.obtenerImpuestos();
          },

          error => {
            console.log(error);
          }
        );
    }
    else{
      this.DatosComision.fecha_pago = new Date();
    }

  }

}
