import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComisionAjustesComponent } from './comision-ajustes.component';

describe('ComisionAjustesComponent', () => {
  let component: ComisionAjustesComponent;
  let fixture: ComponentFixture<ComisionAjustesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComisionAjustesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComisionAjustesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
