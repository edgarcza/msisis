import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatDatepickerInputEvent, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS} from '../../clases/FechaAdaptador';

import { HTTPService }    from '../../http.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';
import {SesionService} from '../../sesion.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-agregar-pago',
  templateUrl: './agregar-pago.component.html',
  styleUrls: ['./agregar-pago.component.css'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class AgregarPagoComponent implements OnInit {

	constructor(
    public dialogRef: MatDialogRef<AgregarPagoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService,
    public sesion: SesionService,
    private Link: Router,) {}

  cerrar(): void {
    this.dialogRef.close();
  }

  public DatosPago = {
    id: null,
    id_subagente: null,
    folio_seguro: null,
    no_recibo: null,
    id_cuenta: null,
    fecha_pago: null,
    moneda: null,
    tipo_cambio: null,
    cantidad_abonada_MXN: null,
    cantidad_abonada_USD: null,
    id_forma_pago: null,
    comentario: null,
    estado: null,
    id_cotizacion: null,
    envio: 0,
    archivo: null,
  };

  public DatosExtra = {
  	fecha_tipo_cambio: null,
  	envio: 0,
    cantidad_abonada: null,
  }

  public Archivo = {
    URL_TMP: null,
    Formato: null,
    Archivo: {
      filename: null,
      filetype: null,
      value: null,
    },
    Nombre: null,
  };

  HayArchivo = false;

  ModificarPago = false;

  Tarjeta = false;
  DatosTarjeta = {
    tarjeta: null,
    mes: null,
    anio: null,
    digitos: null,
    titular: null,  
    
    // tarjeta: 1234567891123456,
    // mes: 1,
    // anio: 23,
    // digitos: 123,
    // titular: "El titular",    
  }

  fechaCambio(){
  	this.HTTP.tipoCambio(this.Formulario.aFechaSQL(this.DatosExtra.fecha_tipo_cambio), 'DESC')
    .subscribe(
      datos => {
        console.log(datos);
        if(datos[0].tipo_cambio === "No existe"){
          this.Formulario.Alerta.open("No existe tipo de cambio registrado en el día seleccionado", "Cerrar");
          return;
        }
        this.DatosPago.tipo_cambio = datos[0].tipo_cambio;
      },

      error => {
        console.log(error);
      }
    );
  }

  cambioFormaPago(alTerminar = null){
    // console.log(this.DatosPago.cuenta, this.Formulario.FormDatos['Cuenta'], this.Formulario.formElementoPorId("Cuenta", this.DatosPago.cuenta);
    // this.Formulario.obtener("Forma_Pago", {Campo: 'id', Valor: this.Formulario.FormDatos['Cuenta'][this.Formulario.formElementoPorId("Cuenta", this.DatosPago.cuenta)]['id_forma_pago']});
    // this.Formulario.obtener("Cuenta", {Campo: 'id_forma_pago', Valor: this.Formulario.FormDatos['Forma_Pago'][this.Formulario.formElementoPorId("Forma_Pago", this.DatosPago.forma_pago)]['id']});
    this.Formulario.obtener("Cuenta", {Campo: 'id_forma_pago', Valor: this.DatosPago.id_forma_pago}, alTerminar);
  }

  guardarPago(aruta = false){
    if(this.Formulario.FormDatos['Cuenta'][this.Formulario.formElementoPorId("Cuenta", this.DatosPago.id_cuenta)]['cuenta']  === 'MX'){
      this.DatosPago.cantidad_abonada_MXN = this.DatosExtra.cantidad_abonada;
      this.DatosPago.cantidad_abonada_USD = this.DatosExtra.cantidad_abonada / this.DatosPago.tipo_cambio;
    }
    else if(this.Formulario.FormDatos['Cuenta'][this.Formulario.formElementoPorId("Cuenta", this.DatosPago.id_cuenta)]['cuenta'] === 'USD'){
      this.DatosPago.cantidad_abonada_USD = this.DatosExtra.cantidad_abonada;
      this.DatosPago.cantidad_abonada_MXN = this.DatosExtra.cantidad_abonada * this.DatosPago.tipo_cambio;
    }
    this.DatosPago.estado = 1;
    this.Formulario.guardar("Cliente", this.DatosCliente, null, null, false);
    this.Formulario.guardar('Pago', this.DatosPago, null, 
    (id) => {
      if(this.Archivo.Archivo)
	    	this.Formulario.subirArchivo(this.Archivo.Archivo, "Pagos", id, () => {this.Formulario.Alerta.open("Pago guardado", "Cerrar")});
      this.cerrar();
      if(aruta){
        this.Link.navigate(['/Poliza', this.data.DatosSeguro.id, 'ES']);
      }
    }, false);
  }

  DatosCliente = {
    id: null,
    id_subagente: null,
    id_rol: null,
    id_tipo_cliente: null,
    activo_inactivo: null,
    fecha_registro_prospecto: null,
    universidad: null,
    fecha_registro_asegurado: null,
    nombres: null,
    apellidos: null,
    nombre_completo: null,
    correo_electronico: null,
    contrasena: null,
    telefono: null,
    fecha_nacimiento: null,
    id_fuente_referencia: null,
    id_pais: null,
    estado: null,
    cp: null,
    ciudad: null,
    calle_numero: null,
    razon_social: null,
    rfc: null,
    fac_metodo: null,
    fac_forma: null,
    fac_correo_electronico: null,
    fac_telefono: null,
    fac_id_pais: null,
    fac_estado: null,
    fac_cp: null,
    fac_ciudad: null,
    fac_calle_numero: null,
    colonia: null,
    fac_colonia: null,
    id_cliente: null,
    id_cotizacion: null,
    sexo: null
  };

  ngOnInit() {
    // this.Formulario.obtener("Cuenta");
    this.Formulario.FormDatos['Cuenta'] = null;
    this.Formulario.obtener("Forma_Pago");
    this.DatosPago.id_cotizacion = this.data.DatosSeguro.id;
    this.DatosPago.id_subagente = this.data.DatosSeguro.id_subagente;
    this.DatosPago.fecha_pago = new Date();
    // console.log(this.data.DatosSeguro);
    this.DatosExtra.cantidad_abonada = this.data.DatosSeguro.saldo_pendiente_MXN;

    this.HTTP.datosDe("tipo_cambio", null, null, 1, 1, "DESC").subscribe(
      resultado => {
        this.DatosExtra.fecha_tipo_cambio = resultado[0].fecha;
        this.DatosPago.tipo_cambio = resultado[0].tipo_cambio;
      }, 

      error => {
        console.log(error);
      }
    );

    //SI ES MODIFICAR...
    if(this.data.DatosPago){
      // this.DatosPago = this.data.DatosPago;
      this.ModificarPago = true;
      this.HTTP.datosDe("pago", [{campo: "id", valor: this.data.DatosPago.id}])
        .subscribe(
          datos => {
            console.log(datos);

            this.DatosPago = datos[0];

            this.cambioFormaPago(() => {
              if(this.Formulario.FormDatos['Cuenta'][this.Formulario.formElementoPorId("Cuenta", this.DatosPago.id_cuenta)]['cuenta']  === 'MX'){
                this.DatosExtra.cantidad_abonada = this.DatosPago.cantidad_abonada_MXN;
              }
              else if(this.Formulario.FormDatos['Cuenta'][this.Formulario.formElementoPorId("Cuenta", this.DatosPago.id_cuenta)]['cuenta'] === 'USD'){
                this.DatosExtra.cantidad_abonada = this.DatosPago.cantidad_abonada_USD;
              }
            });

            if(this.DatosPago.archivo)
              this.HayArchivo = true;

          },

          error => {
            console.log(error);
          }
        );
    }
    else{
      // this.DatosPago.no_recibo = this.data.DatosCliente.id;
      let cPagos = this.data.DatosPagos.length + 1;
      // this.DatosPago.no_recibo = this.data.DatosCliente.id.toString() + "-" + cPagos.toString();
      // this.DatosPago.no_recibo = this.data.DatosCliente.id_cliente.toString() + "-" + cPagos.toString();
    }

    this.DatosCliente = this.data.DatosCliente;
    
  }


  cambioArchivo(event){
  	this.Formulario.onSelectFile(event, this.Archivo, () => {this.DatosPago.archivo = this.Archivo.Formato; console.log(this.Archivo)});
  }

  ClickTarjeta()
  {
    if(this.Tarjeta == true)
      this.Tarjeta = false;
    else
      this.Tarjeta = true;
  }

  DatosPagoInformacion = {
    id: null,
    fecha: null,
    hora: null,
    monto: null,
    mensaje: null,
    referencia: null,
    transaccion_id: null,
    aprobacion: null,
  }

  tarjetaPagar(){
    // this.Pagando = true;

    this.HTTP.cotizarTarjeta(this.DatosTarjeta, this.data.DatosSeguro).subscribe(
      respuesta => {
        console.log(respuesta);
        if(respuesta.return.ecommerceVentaResult.codigoRespuesta == 0){ // Aprobada
          this.Formulario.alerta("Pago procesado", "Cerrar");

          this.DatosPagoInformacion.aprobacion = respuesta.return.ecommerceVentaResult.codigoAprobacion;
          this.DatosPagoInformacion.fecha = respuesta.return.ecommerceVentaResult.fechaTransaccion;
          this.DatosPagoInformacion.hora = respuesta.return.ecommerceVentaResult.horaTransaccion;
          this.DatosPagoInformacion.monto = respuesta.return.ecommerceVentaResult.monto;
          this.DatosPagoInformacion.mensaje = respuesta.return.ecommerceVentaResult.procReturnMsg;
          this.DatosPagoInformacion.referencia = respuesta.return.ecommerceVentaResult.referenciaTransaccionBancaria;
          this.DatosPagoInformacion.transaccion_id = respuesta.return.ecommerceVentaResult.transaccionId;

          this.Formulario.guardar("pago_informacion", this.DatosPagoInformacion, null, null, false);


          // // this
          // this.Link.navigate(['Poliza', this.data.DatosSeguro.id, 'ES']);
          // this.Rutas.navigate(['Prospectos', id_cliente, 'Cotizacion', id]);
          // <input type="hidden" id="return" name="return" value="https://misegurointernacional.com/ADMIN/Cotizar/Poliza/{{DatosCotizacion.id}}/ES">
        }
        else
        {
          this.Formulario.alerta("Error al procesar pago", "Cerrar");
        }
        // this.Pagando = false;
      },

      error => {
        console.log(error);
        // this.Pagando = false;
        this.Formulario.alerta("Error al procesar pago", "Cerrar");
      }
    );

  }

}
