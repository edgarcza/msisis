import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {SesionService} from '../../sesion.service';

@Component({
  selector: 'app-correo-cuerpo',
  templateUrl: './correo-cuerpo.component.html',
  styleUrls: ['./correo-cuerpo.component.css']
})
export class CorreoCuerpoComponent implements OnInit {

	constructor(
    public dialogRef: MatDialogRef<CorreoCuerpoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private Sesion: SesionService) {}

  cerrar(): void {
    this.dialogRef.close({Enviar: false});
  }

  public Correo = {
    // Asunto: 'Asunto de prueba',
    // Mensaje: 'Mensaje de prueba',
    // De: null,
    // CC: 'ecantu@estudiantesembajadores.com',
    Asunto: null,
    Mensaje: null,
    De: null,
    CC: null,
    Para: null,
  }

  enviar(){
    this.dialogRef.close({Enviar: true, Datos: this.Correo});
  }

  ngOnInit() {
    this.Sesion.SesionIniciada.subscribe(
      iniciada => {
        if(iniciada)
          this.Correo.De = this.Sesion.correo();
      }
    );
    if(this.data)
      this.Correo = this.data;
    this.Correo.De = this.Sesion.correo();
  }

}
