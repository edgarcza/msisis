import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorreoCuerpoComponent } from './correo-cuerpo.component';

describe('CorreoCuerpoComponent', () => {
  let component: CorreoCuerpoComponent;
  let fixture: ComponentFixture<CorreoCuerpoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorreoCuerpoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorreoCuerpoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
