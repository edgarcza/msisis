import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatDatepickerInputEvent, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS} from '../../clases/FechaAdaptador';

import { HTTPService }    from '../../http.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';
import {SesionService} from '../../sesion.service'
// import {VentanasService} from '../../servicios/ventanas.service'
import {AgregarFacturaIncidenteComponent} from '../agregar-factura-incidente/agregar-factura-incidente.component';
import {AgregarComentarioIncidenteComponent} from '../agregar-comentario-incidente/agregar-comentario-incidente.component';
import {ConfirmarComponent} from '../confirmar/confirmar.component';

import {Incidente} from '../../interfaces';

@Component({
  selector: 'app-agregar-incidente',
  templateUrl: './agregar-incidente.component.html',
  styleUrls: ['./agregar-incidente.component.css'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class AgregarIncidenteComponent implements OnInit {

  public DatosIncidente: Incidente;

	constructor(
    public dialogRef: MatDialogRef<AgregarIncidenteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService,
    public sesion: SesionService,
    public dialog: MatDialog,
    // public DatosIncidente: Incidente
    // public ventana: VentanasService
    ) {
      this.DatosIncidente = {};
    }

  cerrar(): void {
    this.dialogRef.close();
  }

  // public DatosIncidente = {
  // 	id: null,
  // 	fecha_creacion: null,
  // 	status: null,
  // 	fecha_incidente: null,
  // 	id_pais: null,
  // 	ciudad: null,
  // 	fecha_envio: null,
  // 	fecha_autorizacion: null,
  // 	descripcion: null,
  // 	monto_solicitado: null,
  // 	monto_autorizado: null,
  // 	comentarios: null,
  // 	resultado: null,
  // 	id_subagente: null,
  // 	id_cotizacion: null,
  // }

  public DatosExtra = {
    monto_autorizado: null,
    monto_solicitado: null,
  }

  public DatosFacturas = [];
  public DatosHistorialComentarios = [];

  cambioFormulario(){
  	console.log("Cambio formulario");
  	// if(!this.DatosIncidente.id){
  		if(this.DatosIncidente.fecha_incidente && this.DatosIncidente.id_pais && this.DatosIncidente.ciudad && this.DatosIncidente.descripcion){
  			// console.log("guardar");
  			this.guardarIncidente();
  		}
  	// }
  }

  guardarIncidente(cerrar = false){
    this.DatosIncidente.id_subagente = this.sesion.id();
    this.DatosIncidente.fecha_envio = new Date();
    console.log(this.DatosIncidente);
  	this.Formulario.guardar('Incidente', this.DatosIncidente, null, (id) => {
  		this.DatosIncidente.id = id;
  		// if(!this.DatosIncidente.id){ // Era nuevo
  		// 	for(var Numero in this.DatosFacturas){
  		// 		this.DatosFacturas[Numero].id_incidente = id;
  		// 		this.Formulario.guardar('Factura_Incidente', this.DatosFacturas[Numero], null, null, false);
  		// 	}

  		// 	for(var Numero in this.DatosHistorialComentarios){
  		// 		this.DatosHistorialComentarios[Numero].id_incidente = id;
  		// 		this.Formulario.guardar('Historial_Comentarios', this.DatosHistorialComentarios[Numero], null, null, false);
  		// 	}
      // }
      if(cerrar) this.cerrar();
  	}, true);
  }

  agregarFactura(){
    // this.ventana.agregarFacturaIncidente({DatosIncidente: this.DatosIncidente, DatosFacturaIncidente: null}, () => {this.obtenerFacturas();});

    let dialogRef = this.dialog.open(AgregarFacturaIncidenteComponent, {
      width: '70%',
      data: {DatosIncidente: this.DatosIncidente, DatosFacturaIncidente: null, Moneda: this.Moneda}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] AgregarFacturaIncdente cerrado');
      this.obtenerFacturas();
    });


  }

  eliminarFactura(datosFactura){
    this.Confirmar({Mensaje: "¿Eliminar factura?"}, 
      () => {
        // console.log("SÍ");
        this.Formulario.borrar("factura_incidente", datosFactura, () => {this.obtenerFacturas();});
      }, 
      () => {
        // console.log("NO");
      });
  }

  public Moneda = null;

  obtenerFacturas(){
    this.DatosExtra.monto_autorizado = 0;
      this.HTTP.datosDe("factura_incidente", [{campo: "id_incidente", valor: this.DatosIncidente.id}])
        .subscribe(
          datos => {
          	if(!datos) {this.DatosFacturas = []; return;}

            // console.log(datos);

            this.DatosFacturas = datos;
            this.DatosIncidente.monto_solicitado = 0;

            if(this.DatosFacturas[0])
              this.Moneda = this.DatosFacturas[0].moneda;

            // let Moneda = "MXN";
            // if(this.DatosFacturas[0].moneda === "USD")
            //   Moneda = "USD";

            for(var Num in this.DatosFacturas){
              // if(this.DatosFacturas[Num].moneda === Moneda)
                this.DatosIncidente.monto_solicitado = Number(this.DatosIncidente.monto_solicitado) + Number(this.DatosFacturas[Num].cantidad.toString());
              // else{ // Convertir

              // }
            }

            this.guardarIncidente();
          },

          error => {
            console.log(error);
          }
        );
  }

  agregarComentario(){
    // this.ventana.agregarComentarioIncidente({DatosIncidente: this.DatosIncidente, DatosComentarioIncidente: null}, () => {this.obtenerComentarios();});

		let dialogRef = this.dialog.open(AgregarComentarioIncidenteComponent, {
      width: '70%',
      data: {DatosIncidente: this.DatosIncidente, DatosComentarioIncidente: null}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] AgregarComentarioIncdente cerrado');
      this.obtenerComentarios();
    });
  }

  eliminarComentario(datosComentarios){
    this.Confirmar({Mensaje: "¿Eliminar comentario?"}, 
      () => {
        // console.log("SÍ");
        this.Formulario.borrar("historial_comentarios", datosComentarios, () => {this.obtenerComentarios();});
      }, 
      () => {
        // console.log("NO");
      });
  }

  obtenerComentarios(){
      this.HTTP.datosComentarios({campo: "id_incidente", valor: this.DatosIncidente.id})
        .subscribe(
          datos => {
          	if(!datos) {this.DatosHistorialComentarios = []; return;}

            // console.log(datos);

            this.DatosHistorialComentarios = datos.HistorialComentariosCompleto;
          },

          error => {
            console.log(error);
          }
        );
  }

  public ModADMIN = false;
  public Resultados = ['Pendiente', 'Pago total', 'Pago parcial', 'Rechazado'];
  public Modificar = false;
  public Cliente = true;

  ngOnInit() {
    this.Formulario.obtener("Pais");
    
    if(this.sesion.cliente())
      this.Cliente = true;

    //SI ES MODIFICAR...
    if(this.data.DatosIncidente){
      this.Modificar = true;


      if(this.sesion.admin())
        this.ModADMIN = true;

      this.HTTP.datosDe("incidente", [{campo: "id", valor: this.data.DatosIncidente.id}])
        .subscribe(
          datos => {
            // console.log(datos);

            this.DatosIncidente = datos[0];

			      this.obtenerFacturas();
			      this.obtenerComentarios();
          },

          error => {
            console.log(error);
          }
        );

    }

    this.DatosIncidente.id_cotizacion = this.data.DatosSeguro.id;
    this.DatosIncidente.id_subagente = this.sesion.id();
    this.DatosIncidente.fecha_creacion = new Date();

  }



  Confirmar(datos, alCerrarSI = function(){}, alCerrarNO = function(){}){
    // console.log(datos);
    let dialogRef = this.dialog.open(ConfirmarComponent, {
      width: '30%',
      data: datos
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] Confirmar cerrado');
      if(result == true)
        alCerrarSI();
      else
        alCerrarNO();
    });
  }

}
