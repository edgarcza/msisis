import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComisionesAjustesComponent } from './comisiones-ajustes.component';

describe('ComisionesAjustesComponent', () => {
  let component: ComisionesAjustesComponent;
  let fixture: ComponentFixture<ComisionesAjustesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComisionesAjustesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComisionesAjustesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
