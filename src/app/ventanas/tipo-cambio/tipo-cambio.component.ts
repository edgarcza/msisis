import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { HTTPService }    from '../../http.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';
import {SesionService} from '../../sesion.service'

@Component({
  selector: 'app-tipo-cambio',
  templateUrl: './tipo-cambio.component.html',
  styleUrls: ['./tipo-cambio.component.css']
})
export class TipoCambioComponent implements OnInit {

	constructor(
    public dialogRef: MatDialogRef<TipoCambioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService,
    public sesion: SesionService) {}

  cerrar(): void {
    this.dialogRef.close();
  }

  Datos = {
  		tipo_cambio: 19.1,
  		id: 1
  };

  DatosNuevo = {
  		tipo_cambio: null,
  		id: null,
  		fecha: null,
      id_subagente: null,
  };

  guardar(){
  	// this.Datos = this.DatosNuevo;
  	// console.log(this.Datos);
    this.DatosNuevo.id_subagente = this.sesion.id();
		this.Formulario.guardar('tipo_cambio', this.DatosNuevo, null, () => {this.actualizarCambio();});
 //    setTimeout(function(){ this.actualizarCambio(); }, 1000);
 //    setTimeout(()=>{    //<<<---    using ()=> syntax
 //      this.messageSuccess = false;
 // }, 3000);
  	// this.actualizarCambio();
  	this.DatosNuevo.tipo_cambio = null;
  }

  actualizarCambio(){
  	this.HTTP.tipoCambio()
  	.subscribe(
  		datos => {
  			console.log(datos);
  			this.Datos = datos[0];
  		},

  		error => {
  			console.log(error);
  		}
  	);
  }

  public Fecha;
  public HayFecha = false;
  public DatosFecha = [{
    fecha: null,
    tipo_cambio: null
  }];

  fechaCambio(event){
    // console.log(event);

    this.HTTP.tipoCambio(this.Formulario.aFechaSQL(this.Fecha))
    .subscribe(
      datos => {
        console.log(datos);
        if(datos[0].tipo_cambio === "No existe"){
          this.Formulario.Alerta.open("No existe tipo de cambio registrado en el día seleccionado", "Cerrar");
          this.HayFecha = false;
          return;
        }
        this.DatosFecha = datos;
      },

      error => {
        console.log(error);
      }
    );

    this.HayFecha = true;    
  }

  ngOnInit() {
  	// console.log(this.Datos);
  	this.actualizarCambio();

    this.DatosNuevo.fecha = new Date();
    this.DatosNuevo.fecha.setUTCHours(0,0,0,0);

  }

}
