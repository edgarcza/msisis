import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { DatosFormularioService } from '../../servicios/datos-formulario.service';
import { HTTPService } from '../../http.service';
import { DocumentosService } from '../../servicios/documentos.service';

@Component({
  selector: 'app-distribuidores',
  templateUrl: './distribuidores.component.html',
  styleUrls: ['./distribuidores.component.css']
})
export class DistribuidoresComponent implements OnInit {

  constructor(
	  public dialogRef: MatDialogRef<DistribuidoresComponent>,
	  @Inject(MAT_DIALOG_DATA) public data: any,
    public direccion: DatosFormularioService,
    public HTTP: HTTPService,
    public Documento: DocumentosService
    ) {}

  cerrar(): void {
    this.dialogRef.close();
  }

  tipoDistribuidores = [
    {id: 1, nombre: "EE"},
  ]

  distribuidores = [
    {id: 1, nombre: "Estudiantes Embajadores"},
  ]

  paises = [
    {id: 1, nombre: "México"}
  ]

  // estados = [
  //   {id: 1, nombre: "Nuevo León"}
  // ]

  public Datos = {
    id: null,
    id_tipo_distribuidor: null,
    id_distribuidores: null,
    activo_inactivo: null,
    distribuidor: null,
    correo_electronico: null,
    telefono: null,
    comision: null,
    descuento: null,
    id_pais: null,
    estado: null,
    cp: null,
    ciudad: null,
    calle_numero: null,
    razon_social: null,
    rfc: null,
    fac_correo_electronico: null,
    fac_telefono: null,
    fac_id_pais: null,
    fac_estado: null,
    fac_cp: null,
    fac_ciudad: null,
    fac_calle_numero: null,
    colonia: null,
    fac_colonia: null,
    contacto: null,
  }

  public Colonias = [];

  guardar(){
    // console.log(this.Datos);
    this.direccion.guardar('distribuidor', this.Datos, 'correo_electronico');
  }

  ngOnInit() {
    // this.sesion.funcionPrueba(this.Datos);
    this.direccion.obtener("Agente");
    this.direccion.obtener("Distribuidor");
    this.direccion.obtener("Tipo_Distribuidor");
    this.direccion.obtener("Rol");

    if(this.data && this.data.distribuidor_id){
      console.log(this.data);
      // if(!isNaN(this.data)){ // Es número, = id
        // obtener datos de subagente con ID

        this.HTTP.datosDe("distribuidor", [{campo: "id", valor: this.data.distribuidor_id}])
        .subscribe(
          datos => {
            // console.log(datos);

            this.Datos = datos[0];
          },

          error => {
            console.log(error);
          }
        );

      // }
    }
  }

}
