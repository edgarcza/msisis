import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { DatosFormularioService } from '../../servicios/datos-formulario.service';
import { HTTPService } from '../../http.service';
import { DocumentosService } from '../../servicios/documentos.service';

@Component({
  selector: 'app-agentes',
  templateUrl: './agentes.component.html',
  styleUrls: ['./agentes.component.css']
})
export class AgentesComponent implements OnInit {

  constructor(
	  public dialogRef: MatDialogRef<AgentesComponent>,
	  @Inject(MAT_DIALOG_DATA) public data: any,
    public direccion: DatosFormularioService,
    public HTTP: HTTPService,
    public Documento: DocumentosService
    ) {}

  cerrar(): void {
    this.dialogRef.close();
  }

  tipoDistribuidores = [
    {id: 1, nombre: "EE"},
  ]

  distribuidores = [
    {id: 1, nombre: "Estudiantes Embajadores"},
  ]

  paises = [
    {id: 1, nombre: "México"}
  ]

  estados = [
    {id: 1, nombre: "Nuevo León"}
  ]

  public Datos = {
    id: null,
    id_distribuidor: null,
    activo_inactivo: null,
    agente: null,
    correo_electronico: null,
    contrasena: null,
    telefono: null,
    comision: null,
    descuento: null,
    id_pais: null,
    estado: null,
    cp: null,
    ciudad: null,
    calle_numero: null,
    razon_social: null,
    rfc: null,
    fac_correo_electronico: null,
    fac_telefono: null,
    fac_id_pais: null,
    fac_estado: null,
    fac_cp: null,
    fac_ciudad: null,
    fac_calle_numero: null,
    colonia: null,
    fac_colonia: null,
  }

  public Usuario = {
    id: null,
    correo_electronico: null,
    contrasena: null,
    id_rol: null,
    id_datos: null,
    tipo: null,
  }

  // public DatosFacturacion = {
  //   razonSocial: 'Programas Estudiantiles de Mexico AC',
  //   rfc: 'PEMKK01019404',
  //   codigoPostal: "",
  //   colonia: '',
  //   email: 'info@estudiantesembajadores.com',
  //   pais: null,
  //   estado: "",
  //   comision: 10,
  //   descuento: 10,
  //   ciudad: '',
  //   direccion: '',
  //   numInt: null,
  //   numExt: null,
  //   telefono: '83352711',
  // }

  ngOnInit() {
    this.direccion.obtener("Agente");
    this.direccion.obtener("Distribuidor");
    this.direccion.obtener("Rol");

    if(this.data && this.data.agente_id){
      console.log(this.data);
      // if(!isNaN(this.data)){ // Es número, = id
        // obtener datos de agente con ID

        this.HTTP.datosDe("agente", [{campo: "id", valor: this.data.agente_id}])
        .subscribe(
          datos => {
            // console.log(datos);

            this.Datos = datos[0];
          },

          error => {
            console.log(error);
          }
        );

      // }
    }
  }

  // guardar(){
  //   this.direccion.guardar('agente', this.Datos, 'correo_electronico');
  // }

  guardar(){
    this.direccion.guardar('agente', this.Datos, 'correo_electronico', (id) => {
      this.Usuario.correo_electronico = this.Datos.correo_electronico;
      this.Usuario.contrasena = this.Datos.contrasena;
      this.Usuario.id_rol = 4;
      this.Usuario.tipo = 'agente';
      this.Usuario.id_datos = id;
      this.direccion.guardar('usuario', this.Usuario);
      this.Documento.email(this.Datos.correo_electronico, "RegistroUsuario", this.Usuario, () => {this.direccion.alerta("Agente registrado", "Cerrar"); this.cerrar()});
    });
  }

}
