import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';

@Component({
  selector: 'app-confirmar',
  templateUrl: './confirmar.component.html',
  styleUrls: ['./confirmar.component.css']
})
export class ConfirmarComponent implements OnInit {

	constructor(
    public dialogRef: MatDialogRef<ConfirmarComponent>,
    @Inject(MAT_DIALOG_DATA) public Datos: any,
    public Formulario: DatosFormularioService) {}

  cerrar(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}
