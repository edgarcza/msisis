import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { HTTPService }    from '../../http.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';

@Component({
  selector: 'app-recuperar-contrasena2',
  templateUrl: './recuperar-contrasena2.component.html',
  styleUrls: ['./recuperar-contrasena2.component.css']
})
export class RecuperarContrasena2Component implements OnInit {

	constructor(
    public dialogRef: MatDialogRef<RecuperarContrasena2Component>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService) {}

  cerrar(): void {
    this.dialogRef.close();
  }

  public Datos = {
  	contrasena: null,
  	contrasena2: null,
  	id_contrasena: null,
  };

  cambiarContrasena(){
  	this.HTTP.recuperarContrasena(this.Datos.id_contrasena, 2, this.Datos.contrasena)
  		.subscribe(
  			datos => {
  				console.log(datos);
  				if(datos.enviado == true){
  					this.Formulario.Alerta.open("La contraseña fue cambiada", "Cerrar");
      			// this.Rutas.navigate(['Iniciar']);
  				}
  				else
  					this.Formulario.Alerta.open("Error al cambiar la contraseña", "Cerrar");
  			},

  			error => {
  				console.log(error);
  			}
  	);
  }

  ngOnInit() {
  	this.Datos.id_contrasena = this.data.ID;
  }

}
