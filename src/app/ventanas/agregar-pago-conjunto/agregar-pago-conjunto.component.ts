import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatDatepickerInputEvent, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS} from '../../clases/FechaAdaptador';

import { HTTPService }    from '../../http.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';
import {SesionService} from '../../sesion.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-agregar-pago-conjunto',
  templateUrl: './agregar-pago-conjunto.component.html',
  styleUrls: ['./agregar-pago-conjunto.component.css'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class AgregarPagoConjuntoComponent implements OnInit {

  public DatosPago = {
    id: null,
    id_subagente: null,
    folio_seguro: null,
    no_recibo: null,
    id_cuenta: null,
    fecha_pago: null,
    moneda: null,
    tipo_cambio: null,
    cantidad_abonada_MXN: null,
    cantidad_abonada_USD: null,
    id_forma_pago: null,
    comentario: null,
    estado: null,
    id_cotizacion: null,
    envio: 0,
    archivo: null,
  };

  public DatosExtra = {
  	fecha_tipo_cambio: null,
  	envio: 0,
    cantidad_abonada: null,
  }

  public Archivo = {
    URL_TMP: null,
    Formato: null,
    Archivo: {
      filename: null,
      filetype: null,
      value: null,
    },
    Nombre: null,
  };

  HayArchivo = false;
  public Monto: any = {
    montoUSD: 0,
    montoMXN: 0,
  };

  constructor(
    public dialogRef: MatDialogRef<AgregarPagoConjuntoComponent>,
    @Inject(MAT_DIALOG_DATA) public Datos: any,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService,
    public sesion: SesionService,
    private Link: Router,) {}

  cerrar(datos = null): void {
    this.dialogRef.close(datos);
  }

  ngOnInit() {
        
    this.Formulario.obtener("Forma_Pago");
    console.log(this.Datos)
    if(this.Datos.length > 1)
      for(let i = 0; i < this.Datos.length - 1; i++) {
        if(this.Datos[i].id_tipo_poliza != this.Datos[i + 1].id_tipo_poliza) {
          this.Formulario.alerta("Seguros no iguales", "Cerrar");
          this.cerrar();
          return;
        }
      }

    for(let i = 0; i < this.Datos.length; i++) {
      this.Monto.montoUSD += parseInt(this.Datos[i].precioUSD);
      this.Monto.montoMXN += parseInt(this.Datos[i].precioMXN);
      console.log(this.Monto.montoUSD)
    }

    this.DatosExtra.cantidad_abonada = parseInt(this.Monto.montoMXN);

    this.HTTP.datosDe("tipo_cambio", null, null, 1, 1, "DESC").subscribe(
      resultado => {
        this.DatosExtra.fecha_tipo_cambio = resultado[0].fecha;
        this.DatosPago.tipo_cambio = resultado[0].tipo_cambio;
      }, 

      error => {
        console.log(error);
      }
      );
  }

  guardarPago() {
    console.log(this.DatosPago);
    if(this.Formulario.FormDatos['Cuenta'][this.Formulario.formElementoPorId("Cuenta", this.DatosPago.id_cuenta)]['cuenta']  === 'MX'){
      this.DatosPago.cantidad_abonada_MXN = this.DatosExtra.cantidad_abonada;
      this.DatosPago.cantidad_abonada_USD = this.DatosExtra.cantidad_abonada / this.DatosPago.tipo_cambio;
    }
    else if(this.Formulario.FormDatos['Cuenta'][this.Formulario.formElementoPorId("Cuenta", this.DatosPago.id_cuenta)]['cuenta'] === 'USD'){
      this.DatosPago.cantidad_abonada_USD = this.DatosExtra.cantidad_abonada;
      this.DatosPago.cantidad_abonada_MXN = this.DatosExtra.cantidad_abonada * this.DatosPago.tipo_cambio;
    }
    for(let i = 0; i < this.Datos.length; i++) {
      this.DatosPago.id_cotizacion = this.Datos[i].id;
      this.Formulario.guardar('Pago', this.DatosPago, null, 
        (id) => {
          if(this.Archivo.Archivo)
            this.Formulario.subirArchivo(this.Archivo.Archivo, "Pagos", id, () => {});
          if(i == (this.Datos.length - 1)) {
            this.Formulario.alerta("Pagos guardados", "Cerrar");
            this.cerrar();
          }
        }, false);
    }
  }

  fechaCambio(){
  	this.HTTP.tipoCambio(this.Formulario.aFechaSQL(this.DatosExtra.fecha_tipo_cambio), 'DESC')
    .subscribe(
      datos => {
        console.log(datos);
        if(datos[0].tipo_cambio === "No existe"){
          this.Formulario.Alerta.open("No existe tipo de cambio registrado en el día seleccionado", "Cerrar");
          return;
        }
        this.DatosPago.tipo_cambio = datos[0].tipo_cambio;
      },

      error => {
        console.log(error);
      }
    );
  }
  
  cambioArchivo(event){
  	this.Formulario.onSelectFile(event, this.Archivo, () => {this.DatosPago.archivo = this.Archivo.Formato; console.log(this.Archivo)});
  }

  cambioFormaPago(alTerminar = null){
    this.Formulario.obtener("Cuenta", {Campo: 'id_forma_pago', Valor: this.DatosPago.id_forma_pago}, alTerminar);
  }

  cambioCuenta() {
    if(this.DatosPago.id_cuenta > 6) {
      this.DatosExtra.cantidad_abonada = this.Monto.montoUSD;
    }
    else {
      this.DatosExtra.cantidad_abonada = this.Monto.montoMXN;
    }
  }

}
