import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarPagoConjuntoComponent } from './agregar-pago-conjunto.component';

describe('AgregarPagoConjuntoComponent', () => {
  let component: AgregarPagoConjuntoComponent;
  let fixture: ComponentFixture<AgregarPagoConjuntoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarPagoConjuntoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarPagoConjuntoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
