import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { DatosFormularioService } from '../../servicios/datos-formulario.service';
import { HTTPService } from '../../http.service';
import { DocumentosService } from '../../servicios/documentos.service';

@Component({
  selector: 'app-sub-agentes',
  templateUrl: './sub-agentes.component.html',
  styleUrls: ['./sub-agentes.component.css']
})
export class SubAgentesComponent implements OnInit {

  constructor(
	  public dialogRef: MatDialogRef<SubAgentesComponent>,
	  @Inject(MAT_DIALOG_DATA) public data: any,
    public direccion: DatosFormularioService,
    public HTTP: HTTPService,
    public Documento: DocumentosService) {}

  cerrar(): void {
    this.dialogRef.close();
  }

  public Datos = {
    id: null,
    id_agente: null,
    id_rol: null,
    activo_inactivo: 1,
    contrasena: null,
    nombres: null,
    apellidos: null,
    nombre_completo: null,
    correo_electronico: null,
    telefono: null,
    comision: null,
    descuento: null,
    id_pais: null,
    estado: null,
    cp: null,
    ciudad: null,
    calle_numero: null,
    razon_social: null,
    rfc: null,
    fac_correo_electronico: null,
    fac_telefono: null,
    fac_id_pais: null,
    fac_estado: null,
    fac_cp: null,
    fac_ciudad: null,
    fac_calle_numero: null,
    colonia: null,
    fac_colonia: null,
    id_distribuidor: null,
  };

  public DatosExtra = {
    id_distribuidor: null,
  }

  public Usuario = {
    id: null,
    correo_electronico: null,
    contrasena: null,
    id_rol: null,
    id_datos: null,
    tipo: null,
  }

  public BloquearAgente = true;

  guardar(){
    this.direccion.guardar('subagente', this.Datos, 'correo_electronico', (id) => {
      this.Usuario.correo_electronico = this.Datos.correo_electronico;
      this.Usuario.contrasena = this.Datos.contrasena;
      this.Usuario.id_rol = 2;
      this.Usuario.tipo = 'subagente';
      this.Usuario.id_datos = id;
      this.direccion.guardar('usuario', this.Usuario);
      this.Documento.email(this.Datos.correo_electronico, "RegistroUsuario", this.Usuario, () => {this.direccion.alerta("Subagente registrado", "Cerrar"); this.cerrar()});
    });
  }

  ngOnInit() {
    // this.direccion.obtenerRoles();
    // this.direccion.obtenerDistribuidores();
    // this.direccion.obtenerAgentes();
    this.direccion.obtener("Agente");
    this.direccion.obtener("Distribuidor");
    this.direccion.obtener("Rol");

    if(this.data && this.data.subagente_id){
      console.log(this.data);
      this.DatosExtra.id_distribuidor = this.data.distribuidor_id;
      // if(!isNaN(this.data)){ // Es número, = id
        // obtener datos de subagente con ID

        this.HTTP.datosDe("subagente", [{campo: "id", valor: this.data.subagente_id}])
        .subscribe(
          datos => {
            // console.log(datos);

            this.Datos = datos[0];
          },

          error => {
            console.log(error);
          }
        );

      // }
    }

  }

  cambioDistribuidor(){
    // console.log(this.Datos.id_distribuidor);
    this.direccion.obtener("Agente", {Campo: 'id_distribuidor', Valor: this.DatosExtra.id_distribuidor});
    this.BloquearAgente = false;
  }

}
