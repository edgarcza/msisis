import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubAgentesComponent } from './sub-agentes.component';

describe('SubAgentesComponent', () => {
  let component: SubAgentesComponent;
  let fixture: ComponentFixture<SubAgentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubAgentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubAgentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
