import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDatepickerInputEvent, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS} from '../clases/FechaAdaptador';
import { VentanasService } from '../servicios/ventanas.service';
// import {Router} from "@angular/router";

import { DatosFormularioService } from '../servicios/datos-formulario.service';
import { HTTPService } from '../http.service'
import { SesionService } from '../sesion.service'

@Component({
  selector: 'app-cotizacion',
  templateUrl: './cotizacion.component.html',
  styleUrls: ['./cotizacion.component.css'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class CotizacionComponent implements OnInit {

  constructor(
    private ruta: ActivatedRoute,
    public direccion: DatosFormularioService,
    private HTTP: HTTPService,
    public Sesion: SesionService,
    public Rutas: Router,
    private Ventana: VentanasService) {
  }

  // tipoSeguro = [
  // {"tipo":"Universitario", "id": 1},
  // {"tipo":"Otro", "id": 2}
  // ];
  
  // origen  = [
  // {"pais":"México", "id": 1}
  // ];
  
  // destino  = [
  // {"pais":"España", "id": 1}
  // ];
  
  // tipoPoliza  = [
  // {"tipoPoliza":"Plata", "id": 1}
  // ];
  
  // referencia  = [
  // {"fuente":"Facebook", "id": 1}
  // ];

  tipoCliente: string;

  public ID_Cotizacion;
  public esNuevo = true;
  public BloquearTipoPoliza = true;
  public Nuevo = true;
  public codigoPromocion = null;

  public DatosCotizacion = {
    id: null,
    id_cliente: null,
    id_subagente: null,
    activo_inactivo: 1,
    id_tipo_seguro: null,
    origen: 1,
    destino: null,
    salida: null,
    regreso: null,
    codigo_promocion: null,
    id_tipo_poliza: null,
    numero_dias: null,
    numero_semanas_meses: null,
    precioMXN: null,
    precioMXN_descuento: null,
    precioMXN_ajuste: null,
    precioMXN_iva: null,
    precioMXN_subtotal: null,
    precioMXN_comision: null,
    precioMXN_total: null,
    precioUSD: null,
    precioUSD_descuento: null,
    precioUSD_ajuste: null,
    precioUSD_iva: null,
    precioUSD_subtotal: null,
    precioUSD_comision: null,
    precioUSD_total: null,
    tipo_cambio: null,
    descripcion: null,
    fecha_hecha: null,
    fecha_asegurado: null,
    folio: null,
    comentario: null,
    cargo_administrativo: null,
    envio: null,
    saldo_pendiente_MXN: null,
    saldo_pendiente_USD: null,
    saldo_liquidado_MXN: null,
    saldo_liquidado_USD: null,
    id_codigo_promocion: null,
    factura: null,
    factura_numero: null,
    factura_fecha: null,
    factura_monto: null,
    factura_status: null,
    factura_archivo: null,
    origen_cp: null,
    origen_ciudad: null,
    destino_cp: null,
    destino_ciudad: null,
    sexo: null,
    descuento: null,
    comision:null,
  };

  public DatosCliente: any = {
    id: null,
    nombres: null,
    apellidos: null,
    nombre_completo: null,
    correo_electronico: null,
    telefono: null,
    fecha_nacimiento: null,
    sexo: null,
    id_fuente_referencia: null,
    fecha_registro_prospecto: new Date(),
    id_tipo_cliente: 1,
    id_subagente: this.Sesion.id_datos(),
    id_cliente: null,
    id_codigo_promocion: null,
    // id_pais: null
  }

  // Total = {
  //   "MXN": 0,
  //   "USD": 0,
  //   "Cambio": 0
  // }
  YaGuardado = true;
  guardar(tabla, datos, restriccion, imprimir = false){
    if(!this.DatosCliente.id){
      this.DatosCliente.nombre_completo = this.DatosCliente.nombres + " " + this.DatosCliente.apellidos;
      this.DatosCliente.id_subagente = this.Sesion.id_datos();
      this.DatosCliente.fecha_registro_prospecto = new Date();
      this.DatosCliente.id_pais = this.DatosCotizacion.origen;
      // this.DatosCliente.id_cliente = this.DatosCliente.apellidos.substring(0, 3).toUpperCase() + this.DatosCliente.nombres.substring(0, 3).toUpperCase() + 
      //     this.DatosCliente.fecha_nacimiento.getFullYear() + ("0" + (this.DatosCliente.fecha_nacimiento.getMonth() + 1)).slice(-2) + 
      //     ("0" + this.DatosCliente.fecha_nacimiento.getDate()).slice(-2);
    }
    // console.log(this.direccion.guardar(tabla, datos, restriccion));
    // console.log(this.DatosCliente);
    
    if(this.DatosCliente.id){ // Ya existe cliente
      this.guardarCotizacion(this.DatosCliente.id);
      return 0;
    }

    if(this.YaGuardado)
      restriccion = null;

    this.HTTP.guardar(tabla, datos, restriccion).subscribe(
      resultado => {
        if(resultado.guardado){
          this.guardarCotizacion(resultado.id, imprimir);
          this.YaGuardado = true;
        }
        else{
          this.direccion.alerta(resultado.error, 'Cerrar');
        }
      },

      error => {
        console.log(error.error);
      });

      // if(imprimir) { 
        // http://localhost:4200/Cotizacion/Ver/303/Imprimir
        // this.Link.navigate(['Cotizacion', this.ID, 'Cotizaciones'])
      // }
  }

  guardarCotizacion(id_cliente, imprimir = false){
    this.DatosCotizacion.id_cliente = id_cliente;
    // this.DatosCotizacion.id_subagente = this.Sesion.id();
    this.DatosCotizacion.id_subagente = this.Sesion.id_datos();

    if(this.Sesion.Info.id_distribuidor != this.DescuentoEleccion) {
      if(this.DescuentoEleccion == 9) {
        this.DatosCotizacion.descuento = 10; this.DatosCotizacion.comision = 10;
      }
      else if(this.DescuentoEleccion == 20) {
        this.DatosCotizacion.descuento = 0; this.DatosCotizacion.comision = 20;
      }
    }

    this.direccion.guardar("cotizacion", this.DatosCotizacion, null, (id) => {
      // this.Rutas.navigate(['Prospectos', id_cliente]);
      // this.Rutas.navigate(['Cotizacion', 'Confirmar', id]);
      if(!imprimir) {
        this.Ventana.confirmarCotizacion({ID: id}, 
          (resultado) => {
            if(resultado){
              // this.tabla();
              // this.Formulario.alerta("Asegurado", "Cerar");
              this.Rutas.navigate(["Asegurados", this.DatosCliente.id, "Pagos", id]);
            }
        })
        // this.Rutas.navigate(['Prospectos', id_cliente, 'Cotizacion', id]);
      }
      else
      // http://localhost:4200/Cotizacion/Ver/303/Imprimir
        // this.Rutas.navigate(['Cotizacion', 'Ver', id.id, 'Imprimir'])
        window.open('/Cotizacion/Ver/'+id+'/Imprimir', '_blank');
      
      // var getUrl = window.location;
      // var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";
      // window.location.href = baseUrl + 'Prospectos/' + id_cliente;

      // alert("fjdskfsdf");
      // console.log("otiertol");
    }, false);
  }

  public Nombre = "";
  public Nombres;
  public ReferenciaSTR = "";
  public DatosClientesFiltro = [];
  public filtradoClientes;

  clienteExistente(texto){
    // console.log(texto, this.Nombres);
    if(texto && texto.length > 2){
      this.HTTP.buscar("cliente", {nombre_completo: texto, nombres: texto, apellidos: texto, correo_electronico: texto}, "or", 100).subscribe(
        resultado => {
          if(resultado){
            this.DatosClientesFiltro = resultado;
            this.DatosCliente = resultado[0];
            this.ReferenciaSTR = this.direccion.formValorPorId("Fuente_Referencia", this.DatosCliente.id_fuente_referencia);

            const valorFiltro = texto.toLowerCase();
            this.filtradoClientes = this.DatosClientesFiltro.filter(valor => valor['nombre_completo'].toLowerCase().indexOf(valorFiltro) === 0);

            this.verificarListo();
          }
          else{
            for(var Clave in this.DatosCliente) {
               this.DatosCliente[Clave] = null;
            }
            this.ReferenciaSTR = null;
            this.verificarListo();
          }

        },

        error => {
          console.log(error);
          this.verificarListo();
        }
      );
      
    }
    // else if(texto.length > 2){
    // }

  }

  // clienteExistente(texto){
  //   // console.log(texto);
  //   if(texto.length > 3){
  //     // this.direccion.buscar("cliente", {nombre_completo: texto, nombres: texto, apellidos: texto, correo_electronico: texto}, "or", 1, this.DatosCliente);
  //     // this.HTTP.buscar("cliente", {nombre_completo: texto, nombres: texto, apellidos: texto, correo_electronico: texto}, "or", 1).subscribe(
  //       resultado => {
  //         // console.log("Buscar", resultado);
  //         if(resultado){
  //           this.DatosCliente = resultado[0];
  //           this.ReferenciaSTR = this.direccion.formValorPorId("Fuente_Referencia", this.DatosCliente.id_fuente_referencia);
  //         }
  //         else{
  //           for(var Clave in this.DatosCliente) {
  //              this.DatosCliente[Clave] = null;
  //           }
  //           this.ReferenciaSTR = null;
  //         }
  //         // console.log(variable);
  //       },

  //       error => {
  //         console.log(error);
  //       }
  //     );
  //   }
  // }

  public DatosCPFiltro;
  public filtradoCodigosPromocion = [];
  public Precios = {USD: null, MXN: null};

  buscarPromocion(enter: any = false){
    // console.log(this.codigoPromocion);
    if(this.codigoPromocion.length > 2){
      this.HTTP.buscar("codigo_promocion", {codigo_promocion: this.codigoPromocion}, "and", 100).subscribe(
        resultado => {
          if(resultado){
            this.DatosCPFiltro = resultado;

            if(enter){
            this.DatosCliente.id_codigo_promocion = resultado[0].id;
            this.DatosCotizacion.precioMXN = this.Precios.MXN * (1 - (resultado[0].porcentaje / 100));
            this.DatosCotizacion.precioUSD = this.Precios.USD * (1 - (resultado[0].porcentaje / 100));
            }

            const valorFiltro = this.codigoPromocion.toLowerCase();
            this.filtradoCodigosPromocion = this.DatosCPFiltro.filter(valor => valor['codigo_promocion'].toLowerCase().indexOf(valorFiltro) === 0);
          }
          else{
            // console.log("sin r");
          }

        },

        error => {
          console.log(error);
        }
      );
    }
    // else if(texto.length > 2){
    // }

  }

  cambioTipoSeguro(){
    // this.direccion.obtener("Tipo_Poliza", {Campo: 'id_tipo_seguro', Valor: this.DatosCotizacion.id_tipo_seguro});
    // this.BloquearTipoPoliza = false;
  }

  Fecha = new Date(2018,10,10);
  Diferencia = {
    Segundos: null,
    Minutos: null,
    Horas: null,
    Dias: null,
    Semanas: null,
  };

  fechaCambio(type: string, event: MatDatepickerInputEvent<Date>){
    // console.log((<HTMLInputElement>event.targetElement).placeholder);
    if((<HTMLInputElement>event.targetElement).placeholder === "Salida" || (<HTMLInputElement>event.targetElement).placeholder === "Regreso")
    {      
      this.manana = new Date(this.DatosCotizacion.salida.getTime() + 12 * 60 * 60 * 1000);
      this.manana.setUTCHours(8,0,0,0);
      if(this.DatosCotizacion.regreso.getTime() <= this.DatosCotizacion.salida.getTime())
        this.DatosCotizacion.regreso = this.manana;

      
      this.asignarDiferencia();
      // console.log(this.Diferencia);
      // console.log(this.DatosCotizacion.salida.getTime());
      // console.log(this.DatosCotizacion.regreso.getTime());
      if(this.DatosCotizacion.origen && this.DatosCotizacion.destino)
      {
        this.obtenerTiposPoliza();
      }

      this.DatosCotizacion.id_tipo_poliza = null;
      this.DatosCotizacion.precioMXN = 0;
      this.DatosCotizacion.precioUSD = 0;

    }
    // console.log(this.DatosCotizacion.regreso);
  }

  cambioNuevo(){
    this.direccion.limpiar(this.DatosCliente);
  }

  asignarDiferencia(){
    // Calcudora de días
    this.Diferencia = {
      Segundos: 
        ((this.DatosCotizacion.regreso.getTime() - this.DatosCotizacion.salida.getTime())/1000),
      Minutos: 
        (((this.DatosCotizacion.regreso.getTime() - this.DatosCotizacion.salida.getTime())/1000) / 60),
      Horas: 
        ((((this.DatosCotizacion.regreso.getTime() - this.DatosCotizacion.salida.getTime())/1000) / 60) / 60),
      Dias: 
        ((((((this.DatosCotizacion.regreso.getTime() - this.DatosCotizacion.salida.getTime())/1000) / 60) / 60) / 24) + 1),
      Semanas:
        ((((((this.DatosCotizacion.regreso.getTime() - this.DatosCotizacion.salida.getTime())/1000) / 60) / 60) / 24) / 7)
    }
  }

  // Tiempo = null;
  obtenerTiposPoliza(){
    if(this.DatosCotizacion.origen && this.DatosCotizacion.destino && this.DatosCotizacion.salida && this.DatosCotizacion.regreso && this.DatosCotizacion.id_tipo_seguro)
    {
      // console.log("Días: "+this.Diferencia.Dias +
      //   "\nZona origen: "+this.direccion.FormDatos['Pais'][this.direccion.formElementoPorId("Pais", this.DatosCotizacion.origen)]['zona']+
      //   "\nZona destino: "+this.direccion.FormDatos['Pais'][this.direccion.formElementoPorId("Pais", this.DatosCotizacion.destino)]['zona']+
      //   "\nTipo seguro: "+this.DatosCotizacion.id_tipo_seguro);
      this.BloquearTipoPoliza = false;
      this.direccion.coberturaPoliza(this.DatosCotizacion.id_tipo_seguro, 
        this.Diferencia.Dias, this.direccion.FormDatos['Pais'][this.direccion.formElementoPorId("Pais", this.DatosCotizacion.origen)]['zona'],
        this.direccion.FormDatos['Pais'][this.direccion.formElementoPorId("Pais", this.DatosCotizacion.destino)]['zona'],
        this.DatosCotizacion, () => {
          for(var N in this.direccion.FormDatos['Tipo_Poliza']){
            if(this.direccion.FormDatos['Tipo_Poliza'][N].tipo_poliza == "Oro")
              this.direccion.FormDatos['Tipo_Poliza'][N].tipo_poliza = "Oro ilimitado";
            else if(this.direccion.FormDatos['Tipo_Poliza'][N].tipo_poliza == "Plata")
              this.direccion.FormDatos['Tipo_Poliza'][N].tipo_poliza = "Plata EUR/USD 250,000";
            else if(this.direccion.FormDatos['Tipo_Poliza'][N].tipo_poliza == "Bronce")
              this.direccion.FormDatos['Tipo_Poliza'][N].tipo_poliza = "Bronce EUR/USD 50,000";
          }
        });

        if(Math.ceil(this.Diferencia.Dias) < 10)
        {
          this.VerDatosDatos.tiempo = Math.ceil(this.Diferencia.Dias) + " días";
        }
        else if(Math.ceil(this.Diferencia.Dias) >= 10 && Math.ceil(this.Diferencia.Dias) < 64)
        {
          this.VerDatosDatos.tiempo = Math.ceil(Math.ceil(this.Diferencia.Dias) / 7) + " semanas / ";
          // this.VerDatosDatos.tiempo += (Math.ceil(Math.ceil(this.Diferencia.Dias) % 7) > 0) ? ", " + Math.ceil(Math.ceil(this.Diferencia.Dias) % 7) + " días" : "";
          this.VerDatosDatos.tiempo += Math.ceil(this.Diferencia.Dias) + " días";
        }
        else if(Math.ceil(this.Diferencia.Dias) >= 64)
        {
          this.VerDatosDatos.tiempo = Math.ceil(Math.ceil(this.Diferencia.Dias) / 30) + " meses / ";
          // this.VerDatosDatos.tiempo += (Math.ceil(Math.ceil(this.Diferencia.Dias) % 30) > 0) ? ", " + Math.ceil(Math.ceil(this.Diferencia.Dias) % 30) + " días" : "";
          this.VerDatosDatos.tiempo += Math.ceil(this.Diferencia.Dias) + " días";
        }
    }

    if(this.DatosCotizacion.id_tipo_seguro == 1)
      this.CoberturaLink = "http://misegurointernacional.com/Poliza.pdf";
    else if(this.DatosCotizacion.id_tipo_seguro == 2)
      this.CoberturaLink = "http://misegurointernacional.com/Poliza.pdf";
    else if(this.DatosCotizacion.id_tipo_seguro == 3)
      this.CoberturaLink = "http://misegurointernacional.com/Poliza.pdf";
  }

  CoberturaLink = null;
  cambioTipoPoliza(){
    this.DatosCotizacion.precioUSD = this.direccion.FormDatos['Tipo_Poliza'][this.direccion.formElementoPorId("Tipo_Poliza", this.DatosCotizacion.id_tipo_poliza)]['precio'];
    this.DatosCotizacion.precioMXN = this.direccion.redondear(this.DatosCotizacion.precioUSD * this.DatosCotizacion.tipo_cambio, 2);

    this.Precios.MXN = this.DatosCotizacion.precioMXN;
    this.Precios.USD = this.DatosCotizacion.precioUSD;

    this.verificarListo();
  }

  asignarTipoCambio(){
    this.HTTP.tipoCambio()
    .subscribe(
      datos => {
        console.log(datos);
        this.DatosCotizacion.tipo_cambio = datos[0].tipo_cambio;
      },

      error => {
        console.log(error);
      }
    );
  }

  origenCP(){
    try{
      if(this.DatosCotizacion.origen_cp.toString().length > 3){
        this.HTTP.cp_buscarDatos(this.DatosCotizacion.origen_cp)
        .subscribe(
          datos => {
            console.log(datos);
            this.DatosCotizacion.origen_ciudad = datos.municipio;
            // Datos[pre + 'estado'] = datos.estado;
            // this.Colonias = datos.colonias;
          },

          error => {
            console.log(error);
          }
        );
      }
    }
    catch(error){
      // console.log(error);
    }
  }

  public manana = null;
  public hoy = null;
  public DescuentoEleccion = 20;
  ngOnInit() {
    // this.Datos.nombreCompleto = this.Datos.nombre + " " + this.Datos.apellidos;

    this.DescuentoEleccion = this.Sesion.Info.id_distribuidor;

    if(this.DescuentoEleccion == null)
      this.Sesion.SesionIniciada.subscribe(
        iniciada => {
          this.DescuentoEleccion = this.Sesion.Info.id_distribuidor;
          console.log(this.DescuentoEleccion);
        }
      )

    this.direccion.obtener("Pais");
    this.direccion.obtener("Tipo_Seguro");
    this.direccion.obtener("Tipo_Poliza");
    this.direccion.obtener("Fuente_Referencia");

    this.DatosCotizacion.salida = new Date(new Date().getTime() + 12 * 60 * 60 * 1000);
    this.DatosCotizacion.salida.setUTCHours(6,0,0,0);
    this.DatosCotizacion.regreso = new Date(new Date().getTime() + 36 * 60 * 60 * 1000);
    this.DatosCotizacion.regreso.setUTCHours(6,0,0,0);

    this.hoy = new Date(new Date().getTime() + 12 * 60 * 60 * 1000);
    this.hoy.setUTCHours(8,0,0,0);
    this.manana = new Date(new Date().getTime() + 36 * 60 * 60 * 1000);
    this.manana.setUTCHours(8,0,0,0);

    this.DatosCotizacion.fecha_hecha = new Date();
    this.DatosCotizacion.fecha_hecha.setUTCHours(8,0,0,0);

    this.asignarDiferencia();
    this.asignarTipoCambio();

    // setInterval(() => console.log(this.DatosCotizacion.numero_dias), 3000);

    // this.ruta.params.subscribe(params => {
    //   this.ID_Cotizacion = +params['id'];  
    //   console.log(this.ID_Cotizacion);
    //   if(!isNaN(this.ID_Cotizacion)){
    //     this.esNuevo = false;
    //     this.Datos.nuevo = this.esNuevo;
    //   }
    // });

  }

  Listo = false;
  verificarListo(){
    if(this.DatosCotizacion.precioMXN && this.DatosCotizacion.precioUSD 
      && this.DatosCliente.nombres && this.DatosCliente.apellidos && this.DatosCliente.correo_electronico)
      //  && this.DatosCliente.telefono)
      this.Listo = true;
    else
      this.Listo = false;
  }


  VerDatos = false;
  // VerDatos = true;
  VerDatosDatos = {
    tipo_seguro: '- - - - -',
    origen: '- - - - -',
    destino: '- - - - -',
    salida: null,
    regreso: null,
    tipo_poliza: '- - - - -',
    codigo: '- - - - -',
    tiempo: '- - - - -',
  };
  stepperCambio(e){
    // console.log(e);
    if(e.selectedIndex == 1){
      this.VerDatosDatos.tipo_seguro = this.direccion.FormDatos['Tipo_Seguro'][this.direccion.formElementoPorId("Tipo_Seguro", this.DatosCotizacion.id_tipo_seguro)]['tipo_seguro'];
      this.VerDatosDatos.origen = this.direccion.FormDatos['Pais'][this.direccion.formElementoPorId("Pais", this.DatosCotizacion.origen)]['pais'];
      this.VerDatosDatos.destino = this.direccion.FormDatos['Pais'][this.direccion.formElementoPorId("Pais", this.DatosCotizacion.destino)]['pais'];
      
    let Fecha = new Date(this.DatosCotizacion.salida);
    // this.VerDatosDatos.salida = this.Formulario.aFechaSQL(this.DatosSeguro.salida);
    this.VerDatosDatos.salida = `${Fecha.getDate()}/${Fecha.getMonth() + 1}/${Fecha.getFullYear()}`;
    Fecha = new Date(this.DatosCotizacion.regreso);
    // this.VerDatosDatos.regreso = this.Formulario.aFechaSQL(this.DatosSeguro.regreso);
    this.VerDatosDatos.regreso = `${Fecha.getDate()}/${Fecha.getMonth() + 1}/${Fecha.getFullYear()}`;

      this.VerDatosDatos.tipo_poliza = this.direccion.FormDatos['Tipo_Poliza'][this.direccion.formElementoPorId("Tipo_Poliza", this.DatosCotizacion.id_tipo_poliza)]['tipo_poliza'];
      this.VerDatosDatos.codigo = this.DatosCotizacion.codigo_promocion;

      this.VerDatos = true;
    }
    else
      this.VerDatos = false;
  }

}
