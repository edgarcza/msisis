import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PDFDocumentProxy } from 'ng2-pdf-viewer';
import { PDFAnnotationData } from 'pdfjs-dist';
import {HTTPService} from '../http.service';
import {Tipo_Poliza, Cliente} from '../interfaces';
import { Router, ActivatedRoute } from '@angular/router';
import {DocumentosService} from '../servicios/documentos.service';
import {BarraProgresoService} from '../servicios/barra-progreso.service';
import {VentanasService} from '../servicios/ventanas.service';

// declare var pdfform:any;
// declare var transform:any;

@Component({
  selector: 'app-poliza',
  templateUrl: './poliza.component.html',
  styleUrls: ['./poliza.component.css']
})
export class PolizaComponent implements OnInit {

  constructor(public HTTP: HTTPService,
    private Ruta: ActivatedRoute,
    private Link: Router,
    private Documento: DocumentosService,
    private Ventana: VentanasService,
    private Progreso: BarraProgresoService) { }

  private Fecha = new Date();
  public DatosPoliza: Tipo_Poliza = {};
  public DatosCliente: Cliente = {};
  public Datos = {
    id: null,
    folio2: null,
    id_cliente: null,
    cliente_nombres: null,
    cliente_apellidos: null,
    pais_origen: null,
    id_subagente: null,
    activo_inactivo: null,
    id_tipo_seguro: null,
    origen: null,
    destino: null,
    salida: null,
    regreso: null,
    codigo_promocion: null,
    id_tipo_poliza: null,
    numero_dias: null,
    numero_semanas_meses: null,
    precioMXN: null,
    precioMXN_descuento: null,
    precioMXN_ajuste: null,
    precioMXN_iva: null,
    precioMXN_subtotal: null,
    precioMXN_comision: null,
    precioMXN_total: null,
    precioUSD: null,
    precioUSD_descuento: null,
    precioUSD_ajuste: null,
    precioUSD_iva: null,
    precioUSD_subtotal: null,
    precioUSD_comision: null,
    precioUSD_total: null,
    tipo_cambio: null,
    descripcion: null,
    fecha_hecha: null,
    fecha_asegurado: null,
    folio: null,
    comentario: null,
    cargo_administrativo: null,
    envio: null,
    saldo_pendiente_MXN: null,
    saldo_pendiente_USD: null,
    saldo_liquidado_MXN: null,
    saldo_liquidado_USD: null,
    id_codigo_promocion: null,
    factura: null,
    cliente_idp: null,
    factura_numero: null,
    factura_fecha: null,
    factura_monto: null,
    factura_status: null,
    factura_archivo: null,
    origen_cp: null,
    origen_ciudad: null,
    destino_cp: null,
    destino_ciudad: null,
    id_rol: null,
    id_tipo_cliente: null,
    fecha_registro_prospecto: null,
    fecha_registro_asegurado: null,
    nombres: null,
    apellidos: null,
    nombre_completo: null,
    correo_electronico: null,
    contrasena: null,
    telefono: null,
    fecha_nacimiento: null,
    id_fuente_referencia: null,
    id_pais: null,
    estado: null,
    cp: null,
    ciudad: null,
    calle_numero: null,
    razon_social: null,
    rfc: null,
    fac_correo_electronico: null,
    fac_telefono: null,
    fac_id_pais: null,
    fac_estado: null,
    fac_cp: null,
    fac_ciudad: null,
    fac_calle_numero: null,
    colonia: null,
    fac_colonia: null,
    id_cotizacion: null,
    sexo: null,
    tipo_poliza: null,
    nombre_poliza: null,
    codigo_poliza: null,
    zona: null,
    deducible: null,
    dsm: null,
    vigencia_inicio: null,
    vigencia_final: null,
    tipo_poliza_ing: null,
    a_codigo: null,
    edad_min: null,
    edad_max: null,
    gastos_medicos: null,
    eleccion_medico: null,
    dental_urgente: null,
    dental_accidente: null,
    psiquiatrica: null,
    evacuacion: null,
    repatriacion: null,
    fisioterapia: null,
    rayosx: null,
    ayuda_medica: null,
    mri: null,
    muerte: null,
    demembramiento: null,
    rescate: null,
    viaje_familiar: null,
    pasaje_aereo: null,
    robo_prioridad: null,
    relojes: null,
    responsabilidad_civil: null,
    terceros: null,
    cancelacion_viaje: null,
    seguro_tarifa_aerea: null,
    territorio: null,
    retraso: null,
    cliente_nombre: null,
    cliente_nacimiento: null,
    pais_destino: null,
    cliente_correo: null
  };

  public Textos = { };
  public TextosCarta = { };
  public TextosEspanol = { };
  public TextosFrances = { };
  public TextosAleman = { };
  public TextosIngles = { };
  public TextosCartaEspanol = { };
  public TextosCartaAleman = { };


  fecha_DiaSemana(fecha: Date = null){
    if(!fecha) fecha = this.Fecha;
    else fecha = new Date(fecha);
    if(fecha.getDay() == 0)
      return "Domingo";
    else if(fecha.getDay() == 1)
      return "Lunes";
    else if(fecha.getDay() == 2)
      return "Martes";
    else if(fecha.getDay() == 3)
      return "Miércoles";
    else if(fecha.getDay() == 4)
      return "Jueves";
    else if(fecha.getDay() == 5)
      return "Viernes";
    else if(fecha.getDay() == 6)
      return "Sábado";
  }

  fecha_DiaMes(fecha: Date = null){
    if(!fecha) fecha = this.Fecha;
    else fecha = new Date(new Date(fecha).getTime() + (1000 * 60 * 60 * 24));
    // else fecha = new Date(fecha);
    // return fecha.getDate() + 1;
    return fecha.getDate();
  }

  fecha_Mes(fecha: Date = null){
    if(!fecha) fecha = this.Fecha;
    // else fecha = new Date(fecha);
    else fecha = new Date(new Date(fecha).getTime() + (1000 * 60 * 60 * 24));
    if(fecha.getMonth() == 0)
      return "Enero";
    else if(fecha.getMonth() == 1)
      return "Febrero";
    else if(fecha.getMonth() == 2)
      return "Marzo";
    else if(fecha.getMonth() == 3)
      return "Abril";
    else if(fecha.getMonth() == 4)
      return "Mayo";
    else if(fecha.getMonth() == 5)
      return "Junio";
    else if(fecha.getMonth() == 6)
      return "Julio";
    else if(fecha.getMonth() == 7)
      return "Agosto";
    else if(fecha.getMonth() == 8)
      return "Septiembre";
    else if(fecha.getMonth() == 9)
      return "Octubre";
    else if(fecha.getMonth() == 10)
      return "Noviembre";
    else if(fecha.getMonth() == 11)
      return "Diciembre";
  }

  fecha_AAAA(fecha: Date = null){
    if(!fecha) fecha = this.Fecha;
    // else fecha = new Date(fecha); 
    else fecha = new Date(new Date(fecha).getTime() + (1000 * 60 * 60 * 24));
    return fecha.getFullYear();
  }

  DSM(dsm){

  }

  public Idioma = null;
  private ID_COT = null;
  ngOnInit() {
    // this.guardarPDF();
    this.Ruta.params.subscribe(
      params => {
        var cotizacion = +params['id'];
        var idioma = params['var'];
        this.Idioma = idioma;
        this.datosPoliza(cotizacion);
        this.ID_COT = cotizacion;
      }
    );
  }

  datosPoliza(id_cotizacion = null){
    if(!id_cotizacion) id_cotizacion = this.ID_COT;
    this.HTTP.datosPoliza(id_cotizacion).subscribe(
      resultado => {
        this.Datos = resultado.Poliza[0];
        // console.log(resultado);
        console.log(this.Datos);
        this.iniciarVariables();
        this.HTTP.datosDe("cliente", [{campo: 'id', valor: this.Datos.id_cliente}]).subscribe(
          (resultado) =>{
            console.log(resultado);
            this.DatosCliente = resultado[0];
          }
        )
        // if(this.Datos.destino == 4)
        //   this.Link.navigate(['Poliza', id_cotizacion, 'AL'])
      },

      error => {
        console.log(error);
      }
    )
  }

  idioma(id){
      this.Link.navigate(['Poliza', this.ID_COT, id]);
  }

  iniciarVariables(){

    // const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

    // module.exports = {
    //   plugins: [
    //     new BundleAnalyzerPlugin()
    //   ]
    // }


    this.TextosEspanol = {
      'A quien corresponda': "A quien corresponda",
      'Presente': "Presente",
      "Fecha": "Monterrey, N.L. a " + this.fecha_DiaSemana() + ", " + this.fecha_DiaMes() + " de " + this.fecha_Mes() + " de " + this.fecha_AAAA(),
      // "ID": "ID: " + this.Datos.cliente_idp,
      "ID": "ID: " + this.Datos.folio2,
      'Introduccion': "Por medio de la presente, certifico que "+this.Datos.cliente_nombre+". Fecha de Nacimiento: "+this.fecha_DiaMes(this.Datos.cliente_nacimiento)+" de "+this.fecha_Mes(this.Datos.cliente_nacimiento)+" de "+this.fecha_AAAA(this.Datos.cliente_nacimiento)+" adquirió un seguro\
                      internacional válido en todo el mundo excepto en su pais de residencia oficial, por el siguiente periodo de tiempo: "+((this.Datos.numero_dias > 10) ? this.Datos.numero_semanas_meses : this.Datos.numero_dias)+" "+this.Datos.dsm+" empezando el\
                      "+this.fecha_DiaMes(this.Datos.salida)+" de "+this.fecha_Mes(this.Datos.salida)+" de "+this.fecha_AAAA(this.Datos.salida)+" \
                      hasta "+this.fecha_DiaMes(this.Datos.regreso)+" de "+this.fecha_Mes(this.Datos.regreso)+" de "+this.fecha_AAAA(this.Datos.regreso)+" durante su estancia en "+this.Datos.pais_destino,
      'Numero poliza es': "El número de póliza es: ",
      'Numero poliza': this.Datos.a_codigo + this.Datos.codigo_poliza,
      'Caracteristicas': "Características y detalle del Seguro Internacional CareMed",
      'Seguro y accidente': "Seguro de salud y accidente",
      'Gastos medicos': "Gastos médicos en accidente o enfermedad",
      'Gastos medicos R': this.Datos.gastos_medicos,
      'Eleccion medico': "Eleccion del médico",
      'Eleccion medico R': this.Datos.eleccion_medico,
      'Tratamiento dental urgente': "Tratamiento Dental urgente",
      'Tratamiento dental urgente R': this.Datos.dental_urgente,
      'Tratamiento dental en caso de accidente': "Tratamiento Dental en caso de accidente",
      'Tratamiento dental en caso de accidente R': this.Datos.dental_accidente,
      'Evaluacion psiquiatrica': "Evaluación Psiquiátrica",
      'Evaluacion psiquiatrica R': this.Datos.psiquiatrica,
      'Evacuacion medica': "Evacuación Médica",
      'Evacuacion medica R': this.Datos.evacuacion,
      'Repatriacion': "Repatriación de restos",
      'Repatriacion R': this.Datos.repatriacion,
      'Fisioterapia': "Fisioterapia",
      'Fisioterapia R': this.Datos.fisioterapia,
      'Rayos X': "Rayos X y laboratorios",
      'Rayos X R': this.Datos.rayosx,
      'Seguro de accidentes': "Seguro de accidentes de viajes",
      'Muerte': "Muerte",
      'Muerte R': this.Datos.muerte,
      'Desmembramiento': "Desmembramiento/incapacidad",
      'Desmembramiento R': this.Datos.demembramiento,
      'Costo por rescate': "Costo por rescate",
      'Costo por rescate R': this.Datos.rescate,
      'Seguro de asistencia': "Seguro de asistencia",
      'Gastos de viaje': "Gastos de viaje de un familiar al lugar del asegurado",
      'Gastos de viaje R': this.Datos.viaje_familiar,
      'Pasaje aereo 1': "Pasaje aéreo de regreso al país de origen, caso de",
      'Pasaje aereo 2': "muerte de padres o hermanos (pólizas > 3 meses)",
      'Pasaje aereo R': this.Datos.pasaje_aereo,
      'Seguro de equipajes': "Seguro de equipajes",
      'Hurto': "Hurto/Robo/Daño de propeidad personal",
      'Hurto R': this.Datos.robo_prioridad,
      'Relojes': "Relojes, Joyería",
      'Relojes R': this.Datos.relojes,
      'Retraso': "Retraso de equipaje",
      'Retraso R': this.Datos.retraso,
      'Responsabilidad civil': "Responsabilidad civil y daño a terceros",
      'Responsabilidad': "Responsabilidad civil",
      'Responsabilidad R': this.Datos.responsabilidad_civil,
      'Dano a terceros': "Daño a terceros o porpiedad privada / ajena",
      'Dano a terceros R': this.Datos.deducible,
      'Es responsabilidad 1': "Es responsabilidad del asegurado saber los beneficios, términos, condiciones, limitaciones y exclusionesde la póliza que está adquiriendo",
      'Es responsabilidad 2': "Para obtener una copia electrónica y mayor información de la misma se recomienda consultar la página de internet:\
                              http://www.misegurointernacional.com/sblock/www/web.php donde podrá consultar la póliza en detalle.",
      'Deducible': "Deducible",
      'Deducible R': this.Datos.deducible,
      'Compania de seguros': "Compañía de seguros",
      'Compania de seguros R': "Chubb Seguros México S.A",
      'Centro USA': "Centro de Emergencia USA/Canadá",
      'Centro USA R': "+ 1 855 327 1411 gratuito (Stamford, CT, USA.)",
      'Centro mundial': "Centro de Emergencia Mundial",
      'Centro mundial R': "+ (312) 935 1703 (Mundial, llamada a cobro revertido posible)",
      'Correo': "Correo Electrónico",
      'Correo R': "medassist-usa@axa-assistance.us",
      'Mas informacion': "Si necesita más información, por favor contacte nuestra oficina.",
      'Sinceramente': "Sinceramente",
      'Direccion': "Rio Orinoco # 101 -3 Col. del Valle San Pedro Garza García, NL 66220",
      'Telefono': "Tel: 81 8335 2711 FAX: 81 8335 2701 www.misegurointernacional.com info@misegurointernacional.com",
    };

    this.TextosIngles = {
      'A quien corresponda': "To Whom It May",
      'Presente': "Concern:",
      "Fecha": "Monterrey, N.L. a " + this.fecha_DiaSemana() + ", " + this.fecha_DiaMes() + " de " + this.fecha_Mes() + " de " + this.fecha_AAAA(),
      // "ID": "ID: " + this.Datos.cliente_idp,
      "ID": "ID: " + this.Datos.folio2,
      'Introduccion': "This is to certify that the insured "+this.Datos.cliente_nombre+" ("+this.fecha_DiaMes(this.Datos.cliente_nacimiento)+" de "+this.fecha_Mes(this.Datos.cliente_nacimiento)+" de "+this.fecha_AAAA(this.Datos.cliente_nacimiento)+") has acquired\
                      an international insurance valid worldwide for "+((this.Datos.numero_dias > 10) ? this.Datos.numero_semanas_meses : this.Datos.numero_dias)+" starting at\
                      "+this.fecha_DiaMes(this.Datos.salida)+" de "+this.fecha_Mes(this.Datos.salida)+" de "+this.fecha_AAAA(this.Datos.salida)+" \
                      until "+this.fecha_DiaMes(this.Datos.regreso)+" de "+this.fecha_Mes(this.Datos.regreso)+" de "+this.fecha_AAAA(this.Datos.regreso)+" during his/her stay in "+this.Datos.pais_destino,
      'Numero poliza es': "Under the policy number: ",
      'Numero poliza': this.Datos.a_codigo + this.Datos.codigo_poliza,
      'Caracteristicas': "Below there is detail information of the coverage.",
      'Seguro y accidente': "Travel Health Insurance (HA)",
      'Gastos medicos': "Medical exponses in case of illiness or acc",
      'Gastos medicos R': this.Datos.gastos_medicos,
      'Eleccion medico': "Choice of physician",
      'Eleccion medico R': "Any or suggested by the emergency center",
      'Tratamiento dental urgente': "Emergency dental care",
      'Tratamiento dental urgente R': this.Datos.dental_urgente,
      'Tratamiento dental en caso de accidente': "Dental in case of accident",
      'Tratamiento dental en caso de accidente R': this.Datos.dental_accidente,
      'Evaluacion psiquiatrica': "Psychiatric evaluation",
      'Evaluacion psiquiatrica R': this.Datos.psiquiatrica,
      'Evacuacion medica': "Medical Evacuation",
      'Evacuacion medica R': this.Datos.evacuacion,
      'Repatriacion': "Repatriation of remains",
      'Repatriacion R': this.Datos.repatriacion,
      'Fisioterapia': "Physioteraphy",
      'Fisioterapia R': this.Datos.fisioterapia,
      'Rayos X': "X-Ray and Lab test",
      'Rayos X R': this.Datos.rayosx,
      'Seguro de accidentes': "Travel Accident Insurance (I)",
      'Muerte': "Death",
      'Muerte R': this.Datos.muerte,
      'Desmembramiento': "Disability/Dismembrement",
      'Desmembramiento R': this.Datos.demembramiento,
      'Costo por rescate': "Rescue charges",
      'Costo por rescate R': this.Datos.rescate,
      'Seguro de asistencia': "Travel Assistence (T)",
      'Gastos de viaje': "Family member to sickbed of ill insured",
      'Gastos de viaje R': this.Datos.viaje_familiar,
      'Pasaje aereo 1': "Return-airfare to home country in case of",
      'Pasaje aereo 2': "death of relative (policy > than 3 months)",
      'Pasaje aereo R': this.Datos.pasaje_aereo,
      'Seguro de equipajes': "Travel Luggage Insurance (L)",
      'Hurto': "Theft/damage of personal property",
      'Hurto R': this.Datos.robo_prioridad,
      'Relojes': "Whatches, jewerly, etc",
      'Relojes R': this.Datos.relojes,
      'Retraso': "Luggage Delay",
      'Retraso R': this.Datos.retraso,
      'Responsabilidad civil': "Travel Third Party Liability Insurance (3)",
      'Responsabilidad': "Personal Liability",
      'Responsabilidad R': this.Datos.responsabilidad_civil,
      'Dano a terceros': "Damage to Property",
      'Dano a terceros R': this.Datos.deducible,
      'Es responsabilidad 1': "It is the injured responsability to know the termns, benefits, conditions and limitations of the policy he/she is acquiring.",
      'Es responsabilidad 2': "To download an electronic copy and more information about the insurance we recommend to access:\
                              http://www.misegurointernacional.com/sblock/www/web.php where a full polocy detail coverage is available.",
      'Deducible': "Deductible",
      'Deducible R': this.Datos.deducible,
      'Compania de seguros': "Insurance Company",
      'Compania de seguros R': "Chubb Seguros México S.A",
      'Centro USA': "Emergency Number USA/Canada",
      'Centro USA R': "+ 1 855 327 1411 gratuito (Stamford, CT, USA.)",
      'Centro mundial': "Emergency Number Worlwide",
      'Centro mundial R': "+ (312) 935 1703 (Mundial, llamada a cobro revertido posible)",
      'Correo': "Email",
      'Correo R': "medassist-usa@axa-assistance.us",
      'Mas informacion': "If you need further assitance, please do not hestitae to contact our office.",
      'Sinceramente': "Sincerely,",
      'Direccion': "Rio Orinoco # 101 -3 Col. del Valle San Pedro Garza García, NL 66220",
      'Telefono': "Tel: 81 8335 2711 FAX: 81 8335 2701 www.misegurointernacional.com info@misegurointernacional.com",
    };
    
    this.TextosAleman = {
      'A quien corresponda': "Sehr geehrte Damen",
      'Presente': "und Herren,",
      "Fecha": "Monterrey, N.L. a " + this.fecha_DiaSemana() + ", " + this.fecha_DiaMes() + " de " + this.fecha_Mes() + " de " + this.fecha_AAAA(),
      // "ID": "ID: " + this.Datos.cliente_idp,
      "ID": "ID: " + this.Datos.folio2,
      'Introduccion': "Hiermit wird bestätigt, dass "+this.Datos.cliente_nombre+" geb. am "+this.fecha_DiaMes(this.Datos.cliente_nacimiento)+" de "+this.fecha_Mes(this.Datos.cliente_nacimiento)+" de "+this.fecha_AAAA(this.Datos.cliente_nacimiento)
                      +", für die Dauer seines Aufenthalts in Deutschland von "+this.fecha_DiaMes(this.Datos.salida)+" de "+this.fecha_Mes(this.Datos.salida)+" de "+this.fecha_AAAA(this.Datos.salida)+" bis "+this.fecha_DiaMes(this.Datos.regreso)+" de "+this.fecha_Mes(this.Datos.regreso)+" de "+this.fecha_AAAA(this.Datos.regreso)
                      +" eine "+((this.Datos.numero_dias > 10) ? this.Datos.numero_semanas_meses : this.Datos.numero_dias)+" weltweit gültige internationale Versicherung erworben hat.",
      'Numero poliza es': "Seine Versicherungsnummer lautet: ",
      'Numero poliza': this.Datos.a_codigo + this.Datos.codigo_poliza,
      'Caracteristicas': "Detaillierte Information zur Kostendeckung durch die internationale Versicherung CareMed",
      'Seguro y accidente': "Reisekrankenversicherung*",
      'Gastos medicos': "Arztkosten bei Verletzungen und akuter Erkrankung",
      'Gastos medicos R': this.Datos.gastos_medicos,
      'Eleccion medico': "Wahl des Arztes/Krankenhauses ",
      'Eleccion medico R': "Nach eigener Wahl oder nach Empfehlung der Notfallzentrale",
      'Tratamiento dental urgente': "Notärztliche Zahnbehandlung",
      'Tratamiento dental urgente R': this.Datos.dental_urgente,
      'Tratamiento dental en caso de accidente': "Zahnbehandlung nach Unfällen",
      'Tratamiento dental en caso de accidente R': this.Datos.dental_accidente,
      'Evaluacion psiquiatrica': "Psychiatrische Evaluierung",
      'Evaluacion psiquiatrica R': this.Datos.psiquiatrica,
      'Evacuacion medica': "Medizinischer Rücktransport",
      'Evacuacion medica R': this.Datos.evacuacion,
      'Repatriacion': "Rückführung im Todesfall",
      'Repatriacion R': this.Datos.repatriacion,
      'Fisioterapia': "Röntgen und Labor fur Diagnosezwecke",
      'Fisioterapia R': this.Datos.fisioterapia,
      'Rayos X': "-",
      'Rayos X R': this.Datos.rayosx,
      'Seguro de accidentes': "Reiseunfallversicherung",
      'Muerte': "Todesfall",
      'Muerte R': this.Datos.muerte,
      'Desmembramiento': "Vollinvalidität",
      'Desmembramiento R': this.Datos.demembramiento,
      'Costo por rescate': "Such- und Bergungskosten",
      'Costo por rescate R': this.Datos.rescate,
      'Seguro de asistencia': "Reise Assistance",
      'Gastos de viaje': "Familienmitglieder zum Krankenbett des Versicherten",
      'Gastos de viaje R': this.Datos.viaje_familiar,
      'Pasaje aereo 1': "Rückflug ins Heimatland bei Tod der Eltern oder",
      'Pasaje aereo 2': "Geschwister (verfügbar nur für Langzeitreisende 3 Monate +)	",
      'Pasaje aereo R': this.Datos.pasaje_aereo,
      'Seguro de equipajes': "Reisegepäckversicherung",
      'Hurto': "Diebstahl/Schaden an persönlichem Eigentum",
      'Hurto R': this.Datos.robo_prioridad,
      'Relojes': "Uhren, Schmuck, etc.",
      'Relojes R': this.Datos.relojes,
      'Retraso': "Verspätung von aufgegebenem Gepäck",
      'Retraso R': this.Datos.retraso,
      'Responsabilidad civil': "Haftung und Schäden an Dritten",
      'Responsabilidad': "Personenschäden",
      'Responsabilidad R': this.Datos.responsabilidad_civil,
      'Dano a terceros': "Sachschäden",
      'Dano a terceros R': this.Datos.deducible,
      'Responsabilidad 2': "Gesamt f. Personen- u. Sachschäden max.",
      'Responsabilidad 2 R': this.Datos.responsabilidad_civil,
      'Es responsabilidad 1': "*Auf die medizinischen Versicherungsleistungen sind die Definitionen, Begrenzungen und Bedingungen des Versicherungsvertrages anwendbar.",
      'Es responsabilidad 2': "",
      'Deducible': "Zu erbringender Selbstbetrag:",
      'Deducible R': this.Datos.deducible,
      'Compania de seguros': "Versicherungsunternehmen",
      'Compania de seguros R': "Chubb Seguros México S.A",
      'Centro USA': "Notfallnummer USA/CANADA:",
      'Centro USA R': "+ 1 855 327 1411 gratuito (Stamford, CT, USA.)",
      'Centro mundial': "Notfallnummer Worlwide:",
      'Centro mundial R': "+ (312) 935 1703 (Mundial, llamada a cobro revertido posible)",
      'Correo': "Email",
      'Correo R': "medassist-usa@axa-assistance.us",
      'Mas informacion': "Sollten Sie weitere Fragen haben, stehen wir Ihnen jederzeit gern zur Verfügung.",
      'Sinceramente': "Mit freundlichen Grüssen",
      'Direccion': "Rio Orinoco # 101 -3 Col. del Valle San Pedro Garza García, NL 66220",
      'Telefono': "Tel: 81 8335 2711 FAX: 81 8335 2701 www.misegurointernacional.com info@misegurointernacional.com",
    };
    
    this.TextosFrances = {
      'A quien corresponda': "A qui de droit:",
      'Presente': "",
      "Fecha": "Monterrey, N.L. a " + this.fecha_DiaSemana() + ", " + this.fecha_DiaMes() + " de " + this.fecha_Mes() + " de " + this.fecha_AAAA(),
      // "ID": "ID: " + this.Datos.cliente_idp,
      "ID": "ID: " + this.Datos.folio2,
      'Introduccion': "La presénte est pour certifier que "+this.Datos.cliente_nombre+", date de naissance:"+this.fecha_DiaMes(this.Datos.cliente_nacimiento)+" de "+this.fecha_Mes(this.Datos.cliente_nacimiento)+" de "+this.fecha_AAAA(this.Datos.cliente_nacimiento)+",  j'achète d a une assurance internationale, valable á travers le \
                      monde, pour la période suivante: "+((this.Datos.numero_dias > 10) ? this.Datos.numero_semanas_meses : this.Datos.numero_dias)+" á partir du \
                      "+this.fecha_DiaMes(this.Datos.salida)+" de "+this.fecha_Mes(this.Datos.salida)+" de "+this.fecha_AAAA(this.Datos.salida)+" \
                      jusqu' "+this.fecha_DiaMes(this.Datos.salida)+" de "+this.fecha_Mes(this.Datos.salida)+" de "+this.fecha_AAAA(this.Datos.salida)+" "+"\
                      pour son séjour en "+this.fecha_DiaMes(this.Datos.regreso)+" de "+this.fecha_Mes(this.Datos.regreso)+" de "+this.fecha_AAAA(this.Datos.regreso),
      'Numero poliza es': "Le numéro de police est: ",
      'Numero poliza': this.Datos.a_codigo + this.Datos.codigo_poliza,
      'Caracteristicas': "",
      'Seguro y accidente': "Assurance Santé de Voyage",
      'Gastos medicos': "Médicin/Hópital/Médicaments",
      'Gastos medicos R': this.Datos.gastos_medicos,
      'Eleccion medico': "Choix du médecin ou hopital",
      'Eleccion medico R': "Quelconque",
      'Tratamiento dental urgente': "Soins dentaires d´urgence",
      'Tratamiento dental urgente R': this.Datos.dental_urgente,
      'Tratamiento dental en caso de accidente': "Soins dentaires apres accident",
      'Tratamiento dental en caso de accidente R': this.Datos.dental_accidente,
      'Evaluacion psiquiatrica': "Bilan psychiatrique",
      'Evaluacion psiquiatrica R': this.Datos.psiquiatrica,
      'Evacuacion medica': "Repatriement sanitaire",
      'Evacuacion medica R': this.Datos.evacuacion,
      'Repatriacion': "Rapatriement en cas de deces",
      'Repatriacion R': this.Datos.repatriacion,
      'Gastos medicos 2': "Dépenses médicales apres accident",
      'Gastos medicos 2 R': this.Datos.gastos_medicos,
      'Fisioterapia': "Physiothérapie",
      'Fisioterapia R': this.Datos.fisioterapia,
      'Rayos X': "Examens diagnostic et radiologiques",
      'Rayos X R': this.Datos.rayosx,
      'Seguro de accidentes': "Assurance Accident de Voyage",
      'Muerte': "Mort",
      'Muerte R': this.Datos.muerte,
      'Desmembramiento': "Invalidité/ Démembrement",
      'Desmembramiento R': this.Datos.demembramiento,
      'Costo por rescate': "Frais de recherché et de sauvatage",
      'Costo por rescate R': this.Datos.rescate,
      'Seguro de asistencia': "Assistance Voyage",
      'Gastos de viaje': "Membres de la famille au chevet de l´assure",
      'Gastos de viaje R': this.Datos.viaje_familiar,
      'Pasaje aereo 1': "Repatriement dans le pays d´origen après le cedes",
      'Pasaje aereo 2': "des parents, des freres et/ou souers; (police > 3 mois)",
      'Seguro de equipajes': "Assurance Equipage de Voyage",
      'Hurto': "Vol/dommages sur biens personnels",
      'Hurto R': this.Datos.robo_prioridad,
      'Relojes': "Montres, bijoux, etc.",
      'Relojes R': this.Datos.relojes,
      'Retraso': "Voyage retard",
      'Retraso R': this.Datos.retraso,
      'Responsabilidad civil': "Responsabilité et dommages aux tiers",
      'Responsabilidad': "Responsabilité",
      'Responsabilidad R': this.Datos.responsabilidad_civil,
      'Dano a terceros': "Les dommages matériels à des tiers ou privé / extérieur",
      'Dano a terceros R': this.Datos.deducible,
      'Es responsabilidad 1': "Le montant alloué et de responsabilité combinée et les dommages aux tiers ne peut être supérieur à la somme de RC selon le type d'assurance",
      'Es responsabilidad 2': "*Les prestations medicales ci-dessus sont soumises aux conditions, limitations et exclusions du present contrat d´assurance.",
      'Deducible': "Deductible",
      'Deducible R': this.Datos.deducible,
      'Compania de seguros': "D´assurances",
      'Compania de seguros R': "Chubb Seguros México S.A",
      'Centro USA': "Nombre de secours USA/CANADA",
      'Centro USA R': "+ 1 855 327 1411 gratuito (Stamford, CT, USA.)",
      'Centro mundial': "Nombre de secours Worlwide",
      'Centro mundial R': "+ (312) 935 1703 (Mundial, llamada a cobro revertido posible)",
      'Correo': "Email",
      'Correo R': "medassist-usa@axa-assistance.us",
      'Mas informacion': "Veuillez contacter notre bureau pour tout renseignement supplémentaire.",
      'Sinceramente': "Attentivement",
      'Direccion': "Rio Orinoco # 101 -3 Col. del Valle San Pedro Garza García, NL 66220",
      'Telefono': "Tel: 81 8335 2711 FAX: 81 8335 2701 www.misegurointernacional.com info@misegurointernacional.com",
    };



    
    this.TextosCartaEspanol = {
      'A quien': "A quien corresponda",
      'Presente': "Presente",
      'Embajada': "Embajada de España",
      "Fecha": "Monterrey, N.L. a " + this.fecha_DiaSemana() + ", " + this.fecha_DiaMes() + " de " + this.fecha_Mes() + " de " + this.fecha_AAAA(),
      // "ID": "ID: " + this.Datos.cliente_idp,
      "ID": "ID: " + this.Datos.folio2,
      'Por medio': "Por medio de la presente se certifica que "+this.Datos.cliente_nombre+" con fecha de Nacimiento: "+this.fecha_DiaMes(this.Datos.cliente_nacimiento)+" de "+this.fecha_Mes(this.Datos.cliente_nacimiento)+" de "+this.fecha_AAAA(this.Datos.cliente_nacimiento)+" ha adquirido\
                      un seguro internacional durante su estancia en España",
      'Como anexo': "Como anexo a la carta de descripción de la cobertura.",
      'Se menciona': "Se menciona dicho extracto de la cobertura de la póliza del seguro CareMed con respecto a la evacuación sanitaria y repatriación al país en la página 20 del detalle.",
      'Tratamiento': "Tratamiento médico en el país de residencia permanente:",
      'Carta': "Si no es de aguda necesidad obtener servicios médicos caros o tratamiento de necesidad médica de manera inmediata y si los gastos relativos al tratamiento en el país de residencia sobrepasan los gastos contraídos para la realización del transporte del asegurado a su lugar de residencia y si la condición de salud del asegurado permite tal transporte, el asegurador tendrá el derecho de decidir por el transporte del asegurado a su lugar de residencia permanente a costas del asegurador con fines de llevar a cabo el tratamiento allí. Los gastos médicos de tal tratamiento en el país de residencia no serán remunerados por el asegurador. Los relatos médicos sobre las condiciones de salud del asegurado formarán la base de tal decisión. Si el asegurador se decide por transportar al asegurado a su lugar de residencia permanente y si el asegurado insistiera, no obstante, en someterse a tratamiento en el país anfitrión, los gastos del tratamiento serán la responsabilidad exclusiva del asegurado. En este caso, el asegurador sólo reembolsará la suma que habría surgido en relación al transporte al lugar de residencia permanente. El asegurador le reembolsa esta cantidad al asegurado directamente. El asegurado tiene que decidirse en el plazo de 72 horas tras haber recibido notificación por parte del asegurador con respecto a su decisión sobre el transporte. ",
      'Sinceramente': "Atentamente  ",
      'Direccion': "Rio Orinoco # 101 -3 Col. del Valle San Pedro Garza García, NL 66220",
      'Telefono': "Tel: 81 8335 2711 FAX: 81 8335 2701 www.misegurointernacional.com info@misegurointernacional.com",
    };

    this.TextosCartaAleman = {
      'A quien': "A quien corresponda",
      'Presente': "Presente",
      'Embajada': "Embajada de Alemania",
      "Fecha": "Monterrey, N.L. a " + this.fecha_DiaSemana() + ", " + this.fecha_DiaMes() + " de " + this.fecha_Mes() + " de " + this.fecha_AAAA(),
      // "ID": "ID: " + this.Datos.cliente_idp,
      "ID": "ID: " + this.Datos.folio2,
      'Por medio': "Por medio de la presente se certifica que "+this.Datos.cliente_nombre+" con fecha de Nacimiento: "+this.fecha_DiaMes(this.Datos.cliente_nacimiento)+" de "+this.fecha_Mes(this.Datos.cliente_nacimiento)+" de "+this.fecha_AAAA(this.Datos.cliente_nacimiento)+" ha adquirido\
                      un seguro internacional durante su estancia en Alemania",
      'Como anexo': "Como anexo a la carta de descripción de la cobertura.",
      'Se menciona': "Se menciona dicho extracto de la cobertura de la póliza del seguro CareMed con respecto a la evacuación sanitaria y repatriación al país en la página 20 del detalle.",
      'Tratamiento': "Tratamiento médico en el país de residencia permanente:",
      'Carta': "Si no es de aguda necesidad obtener servicios médicos caros o tratamiento de necesidad médica de manera inmediata y si los gastos relativos al tratamiento en el país de residencia sobrepasan los gastos contraídos para la realización del transporte del asegurado a su lugar de residencia y si la condición de salud del asegurado permite tal transporte, el asegurador tendrá el derecho de decidir por el transporte del asegurado a su lugar de residencia permanente a costas del asegurador con fines de llevar a cabo el tratamiento allí. Los gastos médicos de tal tratamiento en el país de residencia no serán remunerados por el asegurador. Los relatos médicos sobre las condiciones de salud del asegurado formarán la base de tal decisión. Si el asegurador se decide por transportar al asegurado a su lugar de residencia permanente y si el asegurado insistiera, no obstante, en someterse a tratamiento en el país anfitrión, los gastos del tratamiento serán la responsabilidad exclusiva del asegurado. En este caso, el asegurador sólo reembolsará la suma que habría surgido en relación al transporte al lugar de residencia permanente. El asegurador le reembolsa esta cantidad al asegurado directamente. El asegurado tiene que decidirse en el plazo de 72 horas tras haber recibido notificación por parte del asegurador con respecto a su decisión sobre el transporte. ",
      'Sinceramente': "Atentamente  ",
      'Direccion': "Rio Orinoco # 101 -3 Col. del Valle San Pedro Garza García, NL 66220",
      'Telefono': "Tel: 81 8335 2711 FAX: 81 8335 2701 www.misegurointernacional.com info@misegurointernacional.com",
    };

    
    if(this.Idioma==="ES"){
      this.Textos = this.TextosEspanol;
      this.TextosCarta = this.TextosCartaEspanol;
    }
    else if(this.Idioma==="EN")
      this.Textos = this.TextosIngles;
    else if(this.Idioma==="FR")
      this.Textos = this.TextosFrances;
    else if(this.Idioma==="AL"){
      this.Textos = this.TextosAleman;
      this.TextosCarta = this.TextosCartaAleman;
    }
  }

  Cargando = {
    Imprimir: false,
    Email: false,
    Descargar: false,
  }

  imprimir(){
    // this.Documento.print(document.getElementById('area').innerHTML, this.Estilos);
    this.Cargando.Imprimir = true;
    this.Progreso.CargandoGlobal = true;
    // this.Documento.pdf(document.getElementById('area'), true, () => {this.Progreso.CargandoGlobal = false; this.Cargando.Imprimir = false;});
    this.imprimirRec(document.getElementsByClassName("area"), document.getElementsByClassName("area").length - 1);
  }
  imprimirRec(docs, i){
    if(i < 0) return;
    this.Documento.pdf(docs[i], true, () => {
      if(i == 0)
        {this.Progreso.CargandoGlobal = false; this.Cargando.Imprimir = false;}
      this.imprimirRec(docs, i - 1);
    });
  }

  @ViewChild('area') contenido: ElementRef;

  descargar(){
    this.Cargando.Descargar = true;
    this.Progreso.CargandoGlobal = true;

    let Docs = document.getElementsByClassName("area");
    // let Docs = document.getElementsByClassName("AREAG");
    this.descargarRec(Docs, Docs.length - 1, 0);
    // for(var i in Docs){
      
    // }
    // this.Documento.pdf(document.getElementById('area'), false, (link) => {
    //   this.Progreso.CargandoGlobal = false; this.Cargando.Descargar = false; this.LINKIMG = link;
    // });
  }
  descargarRec(docs, l, i){
    if(i > l) return;
    // this.Documento.pdf(docs[i], false, (link) => {
    //   if(i == 0)
    //     {this.Progreso.CargandoGlobal = false; this.Cargando.Descargar = false; this.LINKIMG = link;}
    //   this.descargarRec(docs, i - 1);

    // });
    let listo = false;
    if(i == l) listo = true;
    this.Documento.pdfMAS(docs[i], false, (link) => {
      if(i == l)
        {this.Progreso.CargandoGlobal = false; this.Cargando.Descargar = false; this.LINKIMG = link;}
      this.descargarRec(docs, l, i + 1);

    }, true, listo);
  }

  LINKIMG = "asd";
  correo(){
    this.Ventana.correoCuerpo({
      Mensaje: this.DatosCliente.nombre_completo + ",\nMuchas gracias por elegir a Setmex como la empresa que te dará la protección \n\
        internacional durante tu experiencia de estudiante en el extranjero. Estamos \n\
        convencidos que será la mejor experiencia de tu vida y que pondrás el nombre \n\
        de México en alto durante tu viaje.  \n\
        \n\
        Setmex estará todo el tiempo contigo para que cualquier evento médico o de \n\
        enfermedad no tengas problema. Cuenta con nosotros para que te podamos \n\
        orientar y ayudar en cualquier momento. Te anexamos tu póliza en pdf la cual \n\
        podrás tenerla en tu correo electrónico e imprimir. Estas cartas que \n\
        enviamos sirven para que las puedas presentar en la embajada o directamente \n\
        en tu universidad como comprobante de tu seguro. Si requieres algún otro \n\
        documento con todo gusto lo podemos ver.  \n\
        \n\
        Así mismo te recuerdo que puedes consultar las condiciones de la póliza \n\
        general (la cual se anexa también) y en la página de internet puedes \n\
        consultar mas teléfonos e información que se te pueda solicitar, así que lo \n\
        puedes ver en: www.misegurointernacional.com o nos puedes enviar un correo electrónico a \n\
        info@misegurointernacional.com \n\
        Te deseamos la mejor de las suertes en tu viaje.  \n\
        \n\
        Gracias por tu confianza.  \n\
        \n\
        Seguros Setmex       \n\
      ",
      Para: this.Datos.cliente_correo,
    }, (Resultado) => {
      this.Cargando.Email = true;
      this.Progreso.CargandoGlobal = true;
      // this.correoRec(document.getElementsByClassName("area"), document.getElementsByClassName("area").length - 1, 0, Resultado);
      this.correoRec(document.getElementsByClassName("area"), document.getElementsByClassName("area").length - 1, 0, Resultado);
      // console.log(document.getElementsByClassName("area"));
      // let docs = document.getElementsByClassName("area");
      // this.Documento.pdf(document.getElementsByClassName("area"), false, (link, base, pdf, pdfbase) => {
      //   // console.log(this.Datos.cliente_correo, "Poliza",  base, pdf, pdfbase);
      //   this.LINKIMG = pdf;
      //   this.Documento.email(this.Datos.cliente_correo, "Poliza", 
      //       {
      //       Archivo: base, 
      //       Asunto: Resultado.Datos.Asunto, 
      //       Mensaje: Resultado.Datos.Mensaje, 
      //       De: Resultado.Datos.De, 
      //       CC: Resultado.Datos.CC
      //       }, 
      //   () => {
      //     this.Cargando.Email = false;
      //     this.Progreso.CargandoGlobal = false;
      //     // this.Formulario.alerta("Correo enviado", "Cerrar");
      //   });

      // }, false);    
      
    }, () => {

    })
  }
  correoRec(docs, l, i, correo){
    if(i > l) return;

    let listo = false;
    if(i == l) listo = true;
    this.Documento.pdfMAS(docs[i], false, (link, base, pdf, pdfFD) => {
      if(i == l)
      {
        // this.Documento.email(this.Datos.cliente_correo, "Poliza", 
        this.Documento.email(correo.Datos.Para, "Poliza", 
        {
          Archivo: pdf, 
          Asunto: correo.Datos.Asunto, 
          Mensaje: correo.Datos.Mensaje, 
          De: correo.Datos.De, 
          CC: correo.Datos.CC
        }, () => { 
          this.Progreso.CargandoGlobal = false; this.Cargando.Email = false; this.LINKIMG = link;
          console.log(pdf);
          console.log(pdfFD);
        })
      
      }
      this.correoRec(docs, l, i + 1, correo);

    }, true, listo, true);

    // this.Documento.pdf(docs[i], false, (link, base, pdf, pdfbase, baseC) => {
    //   // this.LINKIMG = pdf;
    //   // this.Documento.email(this.Datos.cliente_correo, "Poliza", {Archivo: base}, () => {
    //   //   if(i == 0){
    //   //     this.Cargando.Email = false;
    //   //     this.Progreso.CargandoGlobal = false;
    //   //   }
    //   //   this.correoRec(docs, i - 1, correo);
    //   // });
    //   console.log(base);
    //   // console.log(baseC);
    //   this.Documento.email(this.Datos.cliente_correo, "Poliza", 
    //       {
    //         // Archivo: baseC, 
    //         Archivo: base, 
    //         Asunto: correo.Datos.Asunto, 
    //         Mensaje: correo.Datos.Mensaje, 
    //         De: correo.Datos.De, 
    //         CC: correo.Datos.CC
    //       }, 
    //     () => {
    //       if(i == l){
    //         this.Cargando.Email = false;
    //         this.Progreso.CargandoGlobal = false;
    //         // this.Formulario.alerta("Correo enviado", "Cerrar");
    //       }
    //       this.correoRec(docs, l, i + 1, correo);
    //   });

    // }, false); 
  }

}
