import { Component, OnInit, EventEmitter, Input, Output,
  ComponentFactoryResolver, Type,
  ViewChild,
  ViewContainerRef } from '@angular/core';

import { SesionService } from '../sesion.service';
import { VentanasService } from '../servicios/ventanas.service';
import { DatosFormularioService } from '../servicios/datos-formulario.service';
import { FiltroDatos } from '../interfaces';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-datos-filtro',
  templateUrl: './datos-filtro.component.html',
  styleUrls: ['./datos-filtro.component.css']
})
export class DatosFiltroComponent implements OnInit {

  constructor(
      public sesion: SesionService,
      public ventana: VentanasService,
      public Formulario: DatosFormularioService,
      private ARoute: ActivatedRoute,
      private Router: Router
  	) { }

  @Output() clickColumnas = new EventEmitter();
  @Output() keyupFiltro = new EventEmitter();
  @Output() clickEliminar = new EventEmitter();
  @Output() changeSeleccionar = new EventEmitter();
  @Output() clickExcel = new EventEmitter();
  @Output() clickExcelIngles = new EventEmitter();
  @Output() aplicarFiltro = new EventEmitter();
  
  @Input('columnas') Columnas;
  @Input('seleccionados') Seleccionados;
  @Input('agregar') Agregar;
  @Input('excelIngles') Ingles = false;
  @Input('subirExcel') SubirExcel;

  public filtroSelect;
  public efiltroTexto;
 
  columnas() {
    this.clickColumnas.emit('nada');
  }

  filtroAplicar(borrar = false){
    var DatosEnviar = [];
    var Enlaces = ['..', ' a ', ' hasta ', ' .. ', ' A '];
    // console.log(this.FiltroDatos);
    for(var i in this.FiltroParams) {      
      for(var j in this.FiltroDatos) {
        if(this.FiltroDatos[j].Nombre === i) {
          this.FiltroDatos[j].Seleccionado = true;
          this.FiltroDatos[j].Valor = this.FiltroParams[i];
        }
      }
    }

    for(var N in this.FiltroDatos){
      if(this.FiltroDatos[N].Seleccionado && this.FiltroDatos[N].Valor){         
        // let Bet = this.FiltroDatos[N].Valor.indexOf("..");
        let Bet, BetLgth;
        for(var i in Enlaces){
          Bet = this.FiltroDatos[N].Valor.indexOf(Enlaces[i]);
          BetLgth = Enlaces[i].length;
          if(Bet != -1)
            break;
        }
        if(Bet != -1){
          let Valor = this.FiltroDatos[N].Valor.substring(0, Bet);
          let Valor2 = this.FiltroDatos[N].Valor.substring(Bet + BetLgth, this.FiltroDatos[N].Valor.length);
          var FiltroNuevo = JSON.parse(JSON.stringify(this.FiltroDatos[N]));
          // console.log("1", this.FiltroDatos[N].Valor);
          FiltroNuevo.Valor = Valor;
          FiltroNuevo.Valor2 = Valor2;
          // console.log("2", this.FiltroDatos[N].Valor);
          FiltroNuevo.Operador = 'BETWEEN';
          DatosEnviar[N] = JSON.parse(JSON.stringify(FiltroNuevo));
          console.log(N, DatosEnviar[N], FiltroNuevo);
        }
        else
          DatosEnviar[N] = this.FiltroDatos[N];

        
        if(this.FiltroDatos[N].Campo == "cotizacion.liquidado"){
          var FiltroNuevo = JSON.parse(JSON.stringify(this.FiltroDatos[N]));
          FiltroNuevo.Valor = (this.FiltroDatos[N].Valor === "si") ? "1" : "0";
          DatosEnviar[N] = JSON.parse(JSON.stringify(FiltroNuevo));
        }
        else if(this.FiltroDatos[N].Campo == "cotizacion.estado_t"){
          var FiltroNuevo = JSON.parse(JSON.stringify(this.FiltroDatos[N]));
          if(this.FiltroDatos[N].Valor.toUpperCase() === "NEW")
           FiltroNuevo.Valor = "0";
          else if(this.FiltroDatos[N].Valor.toUpperCase() === "EXT")
           FiltroNuevo.Valor = "1";
          else if(this.FiltroDatos[N].Valor.toUpperCase() === "CORR")
           FiltroNuevo.Valor = "3";
          else if(this.FiltroDatos[N].Valor.toUpperCase() === "CAN")
           FiltroNuevo.Valor = "2";
          DatosEnviar[N] = JSON.parse(JSON.stringify(FiltroNuevo));
        }
      }
    }
    console.log(DatosEnviar);
    if(borrar == true)
    {
      this.aplicarFiltro.emit(borrar);
      this.filtroSelect = null;
      for(var N in this.FiltroDatos){
        this.FiltroDatos[N].Seleccionado = false;
      }
      this.Filtrando = false;
    }
    else if(DatosEnviar.length > 0)
      this.aplicarFiltro.emit(DatosEnviar);
  }

  filtroTexto(campo, texto: string){
    // this.keyupFiltro.emit(texto);
    // if(texto.length > 1){
    //   this.filtroAplicar();
    // }
    // this.filtroSeleccionar();
    let Params: any = {};
    for(var i in this.FiltroDatos) {
      // if(this.FiltroDatos[i].Campo === campo) {
      if(this.FiltroDatos[i].Seleccionado) {
        // Params[]
        Params[this.FiltroDatos[i].Nombre] = this.FiltroDatos[i].Valor;
      }
    }
    console.log(Params);
    this.Router.navigate([], 
      {queryParams: Params});
  }

  Filtrando = false;

  filtroSeleccionar(valor){
    // console.log(valor);
    console.log(this.filtroSelect);   
    let Params: any = {};

    let Selects = 0;
    for(var N in this.FiltroDatos){
      var i = valor.indexOf(this.FiltroDatos[N].Campo)
      if(i >= 0){
        this.FiltroDatos[N].Seleccionado = true;
        this.Filtrando = true;
        Params[this.FiltroDatos[N].Campo] = this.FiltroDatos[N].Valor;
        Selects++;
      }
      else
        this.FiltroDatos[N].Seleccionado = false;
    }

    if(Selects == 0)
      this.Filtrando = false;
    // this.changeSeleccionar.emit(valor);

    // console.log(Params);
    // this.Router.navigate([], 
    //   {queryParams: Params});
  }

  filtroEliminar(){
    this.clickEliminar.emit();
  }

  filtroExcel(){
    this.clickExcel.emit();
  }

  filtroExcelIngles(){
    this.clickExcelIngles.emit();
  }

  /** Verificar si hay uno seleccionado */
  haySeleccionado(){
    if(this.Seleccionados > 0)
      return true;
    return false;
  }

  agregarFiltro(){

  }

  public FiltroDatos = [];

  ngOnInit() {
    console.log(this.SubirExcel);
    if(this.Columnas){
      // console.log(this.Columnas);
      // this.FiltroDatos = new function () {
      //     this[a] = {
      //         greetings: b,
      //         data: c
      //     };
      // };
        // this.FiltroDatos[0].nombre = this.Columnas[0].nombre;
        // this.FiltroDatos[0].tabla = this.Columnas[0].tabla;
        // this.FiltroDatos[0].seleccionado = false;
        // this.FiltroDatos[0].valor = null;
      for(var N in this.Columnas){
        this.FiltroDatos[N] = {};
        this.FiltroDatos[N].Nombre = this.Columnas[N].nombre;
        this.FiltroDatos[N].Campo = this.Columnas[N].tabla;
        this.FiltroDatos[N].Seleccionado = false;
        this.FiltroDatos[N].Valor = null;
        this.FiltroDatos[N].Valor2 = null;
        this.FiltroDatos[N].Operador = null;

        if(this.FiltroDatos[N].Campo.search("fecha") >= 0)
          this.FiltroDatos[N].Fecha = true;
        else
          this.FiltroDatos[N].Fecha = false;
      }
      // console.log(this.FiltroDatos);
    }

    // console.log(this.ARoute.snapshot.queryParams);
    this.ARoute.queryParams.subscribe((params) => {
      console.log(params);
      this.FiltroParams = params;
      this.filtroAplicar();
    })

  }  

  public FiltroParams: any = {};;


  public Archivo = {
    URL_TMP: null,
    Formato: null,
    Archivo: {
      filename: null,
      filetype: null,
      value: null,
    },
    Nombre: null,
  };

  cambioArchivo(event){
  	this.Formulario.onSelectFile(event, this.Archivo, () => {

      this.ventana.sxlsAsignar(null, (datos) => {
        console.log(datos);
        console.log(this.Archivo);
        // this.Formulario.subirExcel(this.Archivo.Archivo, this.SubirExcel, {Subagente: this.sesion.id_datos()}, () => {
          this.Formulario.subirExcel(this.Archivo.Archivo, this.SubirExcel, {Subagente: datos.id_subagente}, () => {
          // this.Formulario.Alerta.open("Excel guardado", "Cerrar");
        });
      })

    });
  }

  subirExcel()
  {
    console.log("Excel");
  }


}
