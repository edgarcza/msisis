export interface Archivo {
	URL_TMP: string;
	Formato: string;
	Archivo: {
		filename: string;
		filetype: string;
		value: string;
	};
}
