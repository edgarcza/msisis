import { Component, OnInit } from '@angular/core';
import {SesionService} from '../sesion.service';
import {HTTPService} from '../http.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  constructor(
    public Sesion : SesionService,
    private HTTP: HTTPService
  ) { }

  ngOnInit() {
  }

  Busqueda = null;
  DatosBusqueda = [];

  busqueda(event){
    // console.log(event);
    console.log(this.Busqueda);
    if(this.Busqueda.length > 2){
      // console.log({campo: "nombre_completo", valor: "%"+this.Busqueda+"%", operador: 'LIKE'},
      // {campo: "correo_electronico", valor: "%"+this.Busqueda+"%", or: true, operador: 'LIKE'})
      this.HTTP.datosDe("cliente", [
        {campo: "nombre_completo", valor: this.Busqueda, operador: 'LIKE'},
        {campo: "correo_electronico", valor: this.Busqueda, or: true, operador: 'LIKE'}
        // {campo: "correo_electronico", valor: this.Busqueda}
      ]).subscribe(
        datos => {
          console.log(datos);
          if(!datos) this.DatosBusqueda = [];
          else{
            this.DatosBusqueda = datos
            for(var N in this.DatosBusqueda){
              if(this.DatosBusqueda[N].id_tipo_cliente == 1)
                this.DatosBusqueda[N].link = "Prospectos";
              else
                this.DatosBusqueda[N].link = "Asegurados";
            }
          }
        },
        
        error => {
          console.log(error);
        }
      )
    }
    else
      this.DatosBusqueda = [];
  }

}
