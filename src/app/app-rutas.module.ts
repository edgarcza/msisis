import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent }      from './login/login.component';
import { ClientesProspectosComponent }      from './clientes-prospectos/clientes-prospectos.component';
import { ClientesAseguradosComponent }      from './clientes-asegurados/clientes-asegurados.component';
import { CotizacionComponent }      from './cotizacion/cotizacion.component';
import { NoEncontradaComponent }      from './no-encontrada/no-encontrada.component';
import { InicioComponent }      from './inicio/inicio.component';
import { CotizacionConfirmacionComponent }      from './cotizacion-confirmacion/cotizacion-confirmacion.component';
import { ConfiguracionComponent }      from './configuracion/configuracion/configuracion.component';
import { PerfilComponent }      from './configuracion/perfil/perfil.component';
import { ProspectosComponent }      from './prospectos/prospectos.component';
import { AseguradosComponent }      from './asegurados/asegurados.component';
// import { UsuariosComponent }      from './usuarios/usuarios.component';
import { GeneralComponent }      from './usuarios/general/general.component';
import { SubAgentesComponent }      from './ventanas/sub-agentes/sub-agentes.component';
import { RecuperarComponent }      from './recuperar/recuperar.component';
import { RegistrarComponent }      from './registrar/registrar.component';
import { PagosPrincipalComponent }      from './administracion/pagos-principal/pagos-principal.component';
import { ComisionesComponent }      from './administracion/comisiones/comisiones.component';
import { IncidenciasComponent }      from './incidencias/incidencias.component';
import { ComisionesSubagenteComponent }      from './comisiones-subagente/comisiones-subagente.component';
import { ConciliarComponent }      from './conciliar/conciliar.component';
import {PolizaComponent} from './poliza/poliza.component';
import { MargenPrincipalComponent }      from './administracion/margen/margen-principal/margen-principal.component';
import { CotizacionVerComponent }      from './cotizacion-ver/cotizacion-ver.component';
import { PreciosComponent }      from './configuracion/precios/precios.component';

const routes: Routes = [
  { path: '', redirectTo: '/Iniciar', pathMatch: 'full' },
  { path: 'Iniciar', component: LoginComponent },
  { path: 'iniciar', component: LoginComponent },
  { path: 'Iniciar/Recuperar/:id', component: LoginComponent },
  { path: 'Clientes/Prospectos', component: ClientesProspectosComponent },
  { path: 'Clientes/Asegurados', component: ClientesAseguradosComponent },
  { path: 'Comisiones', component: ComisionesSubagenteComponent },
  { path: 'Administracion/Pagos', component: PagosPrincipalComponent },
  { path: 'Administracion/Comisiones', component: ComisionesComponent },
  { path: 'Administracion/Conciliar', component: ConciliarComponent },
  { path: 'Administracion/Margen', component: MargenPrincipalComponent },
  { path: 'Incidencias', component: IncidenciasComponent },
  { path: 'Cotizacion', component: CotizacionComponent },
  { path: 'Cotizacion/:id', component: CotizacionComponent },
  { path: 'Cotizacion/Confirmar/:id', component: CotizacionConfirmacionComponent },
  { path: 'Cotizacion/Ver/:id', component: CotizacionVerComponent },
  { path: 'Cotizacion/Ver/:id/:accion', component: CotizacionVerComponent },
  { path: 'Inicio', component: InicioComponent },
  { path: 'Configuracion/Configuracion', component: ConfiguracionComponent },
  { path: 'Configuracion/Perfil', component: PerfilComponent },
  { path: 'Configuracion/Precios', component: PreciosComponent },
  { path: 'Prospectos/:id', component: ProspectosComponent },
  { path: 'Prospectos/:id/:tab', component: ProspectosComponent },
  { path: 'Prospectos/:id/:tab/:cot', component: ProspectosComponent },
  { path: 'Asegurados/:id', component: AseguradosComponent },
  { path: 'Asegurados/:id/:tab', component: AseguradosComponent },
  { path: 'Asegurados/:id/:tab/:var', component: AseguradosComponent },
  { path: 'Asegurados/:id', component: AseguradosComponent },
  { path: 'Cliente/:id', component: AseguradosComponent },
  // { path: 'Configuracion/Usuarios', component: UsuariosComponent },
  // { path: 'Usuarios', component: UsuariosComponent },
  { path: 'Configuracion/Usuarios', component: GeneralComponent },
  { path: 'Usuarios', component: GeneralComponent },
  { path: 'Recuperar/:id', component: RecuperarComponent },
  { path: 'Registrar/:id', component: RegistrarComponent },
  { path: 'Poliza/:id/:var', component: PolizaComponent },
  
  { path: 'Pruebas/Subagentes', component: SubAgentesComponent },
  {path: '404', component: NoEncontradaComponent},
 	{path: '**', redirectTo: '/404'}
];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRutasModule {}