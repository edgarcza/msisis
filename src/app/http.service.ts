import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HTTPService {

	public SesionID;

  constructor(
  	public HTTP: HttpClient) { }

  // private url: string = 'http://localhost/miseguro/http.php';
  // private url: string = 'https://misegurointernacional.com/ADMIN/PHP/http.php';
  private url: string = 'https://admin.misegurointernacional.com/php/http.php';

  
  private urlCodigoPostal = 'https://api-codigos-postales.herokuapp.com/v2/codigo_postal/';
  private urlPaises = 'https://restcountries.eu/rest/v2/all?fields=name';
	private headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');	

  haySesion(sesionId): Observable<any>{
  	return this.HTTP.post(this.url, this.cJSON("haySesion", sesionId), {headers: this.headers});
	}
	
  verificarSesion(sid = null): Observable<any>{
  	return this.HTTP.post(this.url, this.cJSON("verificarSesion", sid), {headers: this.headers});
  }

  iniciarSesion(correo_electronico): Observable<any>{
  	let json = this.cJSON("iniciarSesion", correo_electronico);
  	return this.HTTP.post(this.url, json, {headers: this.headers});
  }

  cerrarSesion(sesionId): Observable<any>{
  	let json = this.cJSON("cerrarSesion", sesionId);
  	return this.HTTP.post(this.url, json, {headers: this.headers});
  }

  buscarUsuario(usuario, iniciarSesion = false): Observable<any>{
		let json = this.cJSON("buscarUsuario", usuario, iniciarSesion);
		return this.HTTP.post(this.url, json, {headers: this.headers});
	}

  buscarUsuario2(datos): Observable<any>{
		let json = this.cJSON("buscarUsuario2", datos);
		return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	datosUsuario(dato, caso = "normal"){
		let json = this.cJSON("datosUsuario", dato, caso);
		return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	datosDe(tabla, where: any = null, campos: any = '*', cantidad = 100, pagina = null, orden = null): Observable<any>{
		// console.log("[HTTPService] SesionID: " + this.SesionID);
		let Datos = {
			Tabla: tabla,
			SesionID: localStorage.getItem("sesionId"),
			Campos: campos,
			Cantidad: cantidad,
			Where: where,
			Pagina: pagina,
			Orden: orden
		};
		let json = this.cJSON("datosDe", Datos);
		return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	coberturaCotizacion(datos){
		let json = this.cJSON("coberturaCotizacion", datos);
		return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	cotizarTarjeta(datosTarjeta, datosCot): Observable<any>{
		let json = this.cJSON("cotizarTarjeta", {Tarjeta: datosTarjeta, Cotizacion: datosCot});
	  return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	datosPagos(where: any = null): Observable<any>{
		// console.log("[HTTPService] SesionID: " + this.SesionID);
		let Datos = {
			Where: where
		};
		let json = this.cJSON("datosPagos", Datos);
		return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	datosIncidentes(where: any = null): Observable<any>{
		// console.log("[HTTPService] SesionID: " + this.SesionID);
		let Datos = {
			Where: where
		};
		let json = this.cJSON("datosIncidentes", Datos);
		return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	datosComentarios(where: any = null): Observable<any>{
		let Datos = {
			Where: where
		};
		let json = this.cJSON("datosComentarios", Datos);
		return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	datosRel(tabla, where): Observable<any>{
		let Datos = {
			Tabla: tabla,
			Where: where
		};
		let json = this.cJSON("datosRel", Datos);
		return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	tipoCambio(deFecha: any = null, orden: any = null): Observable<any>{
		let json = this.cJSON("tipoCambio", {Fecha: deFecha, Orden: orden});
		return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	paginar(caso, pagina, cantidad, filtro = null, excel = false, whereOb = null, excelIngles = false): Observable<any>{
		return this.HTTP.post(
			this.url, 
			this.cJSON(
				caso,
				{
					Pagina: pagina,
					Cantidad: cantidad,
					Filtro: filtro,
					Excel: excel,
					ExcelIngles: excelIngles,
					Where: whereOb
				}
			),
			{
				headers: this.headers
			}
		)
	}

	pagUsuarios(pagina, cantidad, filtro: any = null, excel: any = false): Observable<any>{
		let json = this.cJSON("pagUsuarios", {Pagina: pagina, Cantidad: cantidad, Filtro: filtro, Excel: excel});
		return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	aseguradosExtra(whereOb: any = null): Observable<any>{
		let json = this.cJSON("aseguradosExtra", {Where: whereOb});
		return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	pagCotizaciones(id_cliente): Observable<any>{
		let json = this.cJSON("pagCotizaciones", {ID_Cliente: id_cliente});
		return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	cotizacionConfirmar(id_cotizacion): Observable<any>{
		let json = this.cJSON("cotizacionConfirmar", {ID_Cotizacion: id_cotizacion});
		return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	guardar(tabla, datos, restricciones): Observable<any>{
		// console.log(tabla);
		// console.log(datos);
		let json = this.cJSON("guardar", tabla, datos, restricciones);
	  return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	borrar(tabla, datos): Observable<any>{
		let json = this.cJSON("borrar", tabla, datos);
	  return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	borrarCond(tabla, condiciones): Observable<any>{
		let json = this.cJSON("borrarCond", {Tabla: tabla, Condiciones: condiciones});
	  return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	buscar(tabla, campos, condicion, filas): Observable<any>{
		// console.log(tabla);
		// console.log(datos);
		let Datos = {
			Tabla: tabla,
			Campos: campos,
			Condicion: condicion,
			Filas: filas
		};
		let json = this.cJSON("buscar", Datos);
	  return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	email(para, caso, datos): Observable<any>{
		
		let json = this.cJSON("email", {Caso: caso, Para: para, Datos: datos});
	//   return this.HTTP.post(this.url, json, {headers: this.headers});
	  return this.HTTP.post(this.url, json, {headers: new HttpHeaders().set('Content-Type','undefined')});
	}

	mail(para, contenido): Observable<any>{
		let json = this.cJSON("email", {Para: para, Contenido: contenido});
	  return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	documento(caso, datos): Observable<any>{
		let json = this.cJSON("documento", {Caso: caso, Datos: datos});
	  return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	recuperarContrasena(para, paso, nuevacon: any = null): Observable<any>{
		let json = this.cJSON("recuperarContrasena", {Paso: paso, Para: para, NuevaCon: nuevacon});
	  	return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	distribuidor(dado, id): Observable<any>{
		let json = this.cJSON("distribuidor", {Dado: dado, ID: id});
	  return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	subirArchivo(file, carpeta, nombre): Observable<any>{
		// let formData = new FormData();
		// formData.append("archivo", file, file.name);
		console.log(file);
		let json = this.cJSON("subirArchivo", {Archivo: file, Carpeta: carpeta, Nombre: nombre});
	  return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	subirExcel(file, tabla, datos = null): Observable<any>{
		console.log(file);
		let json = this.cJSON("subirExcel", {Archivo: file, Tabla: tabla, Datos: datos});
	  return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	generarPDF(): Observable<any>{
		let json = this.cJSON("generarPDF", {});
	  return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	datosPoliza(id): Observable<any>{
		let json = this.cJSON("datosPoliza", {Cotizacion: id});
	  return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	aseguradoAsignarSeguro(id): Observable<any>{
		let json = this.cJSON("aseguradoAsignarSeguro", {Cliente: id});
	  return this.HTTP.post(this.url, json, {headers: this.headers});
	}


	// DIRECCIONES

	cp_buscarDatos(Codigo): Observable<any>{
		let json = this.cJSON("cp_buscarDatos", Codigo);
		return this.HTTP.get(this.urlCodigoPostal + Codigo, {headers: this.headers});
	}

	dir_Paises(): Observable<any>{
		// let json = this.cJSON("dir_Paises");
		// return this.HTTP.get(this.urlPaises, {headers: this.headers});
  	let json = this.cJSON("dir_Paises");
  	return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	coberturaPoliza(tipoSeguro, dias, zonaOrigen, zonaDestino): Observable<any>{
		let Datos = {
			TipoSeguro: tipoSeguro,
			Dias: dias,
			ZonaOrigen: zonaOrigen,
			ZonaDestino: zonaDestino
		};
  	let json = this.cJSON("coberturaPoliza", Datos);
  	return this.HTTP.post(this.url, json, {headers: this.headers});
	}

	dformulario(Caso, valor: any): Observable<any>{
	  	let json = this.cJSON("dFormulario", Caso, valor);
	  	return this.HTTP.post(this.url, json, {headers: this.headers});
	}


	cJSON(jCaso, jDatos: any = '', jExtra: any = '', jExtra2: any = ''){
		let fJSON = {
			Caso: jCaso,
			Datos: jDatos,
			Extra: jExtra,
			Extra2: jExtra2
		}
		return JSON.stringify(fJSON);
	}


  
}
