import { Component, OnInit } from '@angular/core';
import * as Tabla from '../../tablas'
import {Tablas} from '../../clases/tablas'

@Component({
  selector: 'app-comisiones-subagente-pagado',
  templateUrl: './pagado.component.html',
  styleUrls: ['./pagado.component.css']
})
export class ComisionesSubagentePagadoComponent extends Tablas implements OnInit {

	constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }

  ngOnInit(){

    this.agregarColumnas(
      [
        {id: 'fecha_asegurado',    	  nombre: 'Fecha de venta',      		ocultar: false,    tabla: 'cotizacion.fecha_asegurado'},
        {id: 'cliente',            		nombre: 'Cliente',    						ocultar: false,    tabla: 'cliente.nombre_completo'},
        {id: 'precioMXN',    					nombre: 'Precio (MXN)',      		  ocultar: false,    tabla: 'cotizacion.precioMXN'},
        {id: 'comision',    					nombre: 'Comisión',      				  ocultar: false,    tabla: 'comision.precioMXN'},
        {id: 'impuestos',    					nombre: 'Impuestos',      				ocultar: false,    tabla: 'comision.impuestos'},
        {id: 'comision_final',    	  nombre: 'Comisión final',      	  ocultar: false,    tabla: 'comision.precioMXN'},
        // {id: 'factura',    					  nombre: 'Factura',      				  ocultar: false,    tabla: 'comision.factura'},
      ]
    );

    this.agregarColIds(
      ['select', 'fecha_asegurado', 'cliente', 'precioMXN', 
      'comision', 'impuestos', 'comision_final', 'factura']
    );

    this.tablaConfig("pagADMComisionesPagado", "ComisionesPagados", "comisión", "comision", [{Campo: "subagente.id", Valor: this.sesion.id_datos(), Operador: "="}]);

    this.tabla();

  }

}
