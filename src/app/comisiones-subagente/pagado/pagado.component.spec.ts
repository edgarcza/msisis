import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComisionesSubagente } from './pagado.component';

describe('ComisionesSubagente', () => {
  let component: ComisionesSubagente;
  let fixture: ComponentFixture<ComisionesSubagente>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComisionesSubagente ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComisionesSubagente);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
