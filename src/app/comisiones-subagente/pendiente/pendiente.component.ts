import { Component, OnInit } from '@angular/core';
import * as Tabla from '../../tablas'
import {Tablas} from '../../clases/tablas'


@Component({
  selector: 'app-comisiones-subagente-pendiente',
  templateUrl: './pendiente.component.html',
  styleUrls: ['./pendiente.component.css']
})
export class ComisionesSubagentePendienteComponent extends Tablas implements OnInit {

	constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }
  private DatosImpuestos;

  ngOnInit(){

    this._sesion.SesionIniciada.subscribe(
      resultado => {
        if(resultado){
          
        }
      }
    )
    this.agregarColumnas(
      [
        {id: 'fecha_asegurado',    	  nombre: 'Fecha de venta',      		ocultar: false,    tabla: 'cotizacion.fecha_asegurado'},
        {id: 'cliente',            		nombre: 'Cliente',    						ocultar: false,    tabla: 'cliente.nombre_completo'},
        {id: 'precioMXN',    					nombre: 'Precio (MXN)',      		  ocultar: false,    tabla: 'cotizacion.precioMXN'},
        {id: 'comision',    					nombre: 'Comisión',      				  ocultar: false,    tabla: 'comision.precioMXN'},
        {id: 'impuestos1',    				nombre: 'IVA',      				ocultar: false,    tabla: 'comision.impuestos'},
        {id: 'impuestos2',    				nombre: 'IPC',      				ocultar: false,    tabla: 'comision.impuestos'},
        {id: 'comision_final',    	  nombre: 'Comisión final',      	  ocultar: false,    tabla: 'comision.precioMXN'},
        // {id: 'factura',    					  nombre: 'Factura',      				  ocultar: false,    tabla: 'comision.factura'},
      ]
    );

    this.agregarColIds(
      ['select', 'fecha_asegurado', 'cliente', 'precioMXN', 
      'comision', 'impuestos1', 'impuestos2', 'comision_final', 'factura', 'acciones']
    );

    this.tablaConfig("pagADMComisionesPendiente", "ComisionesPendientes", "comisión", "comision", [{Campo: "subagente.id", Valor: this.sesion.id_datos(), Operador: "="}]);

    this.tablaTerminar = () => {
      this._HTTP.datosDe("impuesto")
      .subscribe(
        datos => {
            this.DatosImpuestos = datos;
          // console.log(this.DatosCliente);
          // console.log(this.DatosCotizacion);

          for(var N in this.Datos){
            // this.Datos[N][6] = this.Datos[N][5];
            // for(var i in this.DatosImpuestos){
            //   this.Datos[N][this.DatosImpuestos[i].nombre_impuesto] = this._Formulario.redondear(this.Datos[N].precioMXN * (this.DatosImpuestos[0].porcentaje / 100), 2);
            // }
            this.Datos[N]['impuestos1'] = this._Formulario.redondear(this.Datos[N].precioMXN * (this.DatosImpuestos[0].porcentaje / 100), 2);
            this.Datos[N]['impuestos2'] = this._Formulario.redondear(this.Datos[N].precioMXN * (this.DatosImpuestos[1].porcentaje / 100), 2);
          }
          console.log(this.Datos);
          this.dataSource.data = this.Datos;
        },

        error => {
          console.log(error);
        }
      );
    }

    this.tabla();

  }

  subirFactura(datos){
    this.Ventana.subirArchivo({Datos: datos, Carpeta: 'FacturasComisiones'}, 
    () => {
      this._HTTP.datosDe("comision", [{campo: "id", valor: datos.id}]).subscribe(
        (resultado) => {
          let Comision = resultado[0];
          Comision.factura = 1;
          this.Formulario.guardar("comision", Comision, null, () => {this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize)});
        },

        (error) => {
          console.log(error);
        }
      )
    },
    () => {

    })
  }

}
