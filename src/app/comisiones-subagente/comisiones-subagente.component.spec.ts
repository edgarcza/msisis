import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComisionesSubagenteComponent } from './comisiones-subagente.component';

describe('ComisionesSubagenteComponent', () => {
  let component: ComisionesSubagenteComponent;
  let fixture: ComponentFixture<ComisionesSubagenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComisionesSubagenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComisionesSubagenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
