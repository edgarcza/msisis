import { Component, OnInit } from '@angular/core';

import { SesionService } from './sesion.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(
  	public Sesion: SesionService
  	) { }

  ngOnInit() {
  	this.Sesion.verificar();
	}


}
