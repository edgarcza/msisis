import { Injectable, EventEmitter  } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {Router} from "@angular/router";
import { HTTPService }    from './http.service';

import { Subagente }    from './clases/clases';

import {MatSnackBar} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SesionService {

  constructor(
    private router: Router,
    private HTTP: HTTPService,
    public Alerta: MatSnackBar
    ) { }

  public SesionIniciada: EventEmitter<boolean> = new EventEmitter();
  // SesionIniciada = new BehaviorSubject<boolean>(false);

  private iniciada = false;
  private Datos = {
    ID: -1,
    Correo: null,
    Nombre: null,
    Rol: null,
    ID_DATOS: null,
  }
  public Info = {
    id_distribuidor: null,
  };
  // private iniciada = true;
  // public Rol = "Asesor";
  // public Rol = "Administrador";
  public Rol = "";

  iniciar2(sesionId: string){
    localStorage.setItem("sid", sesionId);
    this.HTTP.SesionID = sesionId;
    this.verificar();
  }

  iniciar(correo_electronico){
    this.HTTP.iniciarSesion(correo_electronico).subscribe(
      resultado => {
        // console.log(resultado)
        if(resultado.iniciada){
          localStorage.setItem("sid", resultado.sid);
          localStorage.setItem("uid", resultado.id);
          this.verificar(true);
        }
      },

      error => {
        console.log(error);
      }
    )
  }

  sesionId(){
    return localStorage.getItem("sid");
  }

  cerrar(){
    // return localStorage.removeItem("nombre");
    this.HTTP.cerrarSesion(localStorage.getItem("sid"))
      .subscribe(
        resultado => {
          if(resultado.cerrada){
            this.iniciada = false;
            this.verificar(); 
            this.Alerta.open('Sesión cerrada', 'Cerrar');      
          }
        },

        error => {
          console.log(error);
        }
      );
    localStorage.clear();
    // this.httpService.cerrarSesion();

    return this.router.navigate(['/Iniciar']);
  }

  existe(){
    return this.iniciada;
  }

  verificar(iniciarSesion = false){
    if(!localStorage.getItem("sid"))
      return this.iniciada = false;
    this.HTTP.verificarSesion(localStorage.getItem("sid")).subscribe(
      resultado => {
        // console.log(resultado);
        if(resultado.iniciada){
          this.iniciada = true;
          this.Datos.ID = resultado.id;
          this.Datos.Correo = resultado.correo_electronico;
          this.Datos.Rol = resultado.rol;
          // this.Datos.Nombre = resultado.rol;
          this.Datos.ID_DATOS = resultado.id_datos;
          this.Info = resultado.datos;
          console.log(this.Info)

          if(iniciarSesion){
            if(this.cliente())
              this.router.navigate(['/Cliente', this.Datos.ID_DATOS]);
            else
              this.router.navigate(['/Inicio']);
          }

          this.SesionIniciada.next(true);

        }
        else{
          this.iniciada = false;
          this.router.navigate(['/Iniciar']);
        }
        this.verificada();
      },
      
      error => {
        console.log(error);
        this.iniciada = false;
                this.verificada();
      }
    )

    // if(!localStorage.getItem("sesionId"))
    //   this.iniciada = false;
    // else{
    //   this.HTTP.haySesion(localStorage.getItem("sesionId"))
    //     .subscribe(
    //       resultado => { 
    //           // console.log("HTTP HaySesion:",resultado);
    //         if(resultado.existe){
    //           // console.log("HTTP resultado.existe");
    //           this.iniciada = true;
    //           // this.HTTP.SesionID = localStorage.getItem("sesionId");
    //           // console.log(this.HTTP.SesionID);
    //           this.Datos.ID = resultado.id;
    //           this.Datos.Correo = resultado.correo;
    //           this.Datos.Rol = resultado.rol;

    //           localStorage.setItem("uid", resultado.id);

    //           // COMENTAR PARA PRUEBAS
    //           this.router.navigate(['/Inicio']);


    //           // console.log(this.Datos);
    // // let Usuario = new Subagente(this.Datos.ID);
    //         }
    //         else{
    //           this.iniciada = false;
    //         }
    //         this.verificada();
    //       },

    //       error => {
    //         console.log(error);
    //         this.iniciada = false;
    //         this.verificada();
    //       }

    //     );
    // }
  }

  verificada(){
    // console.log("[Sesión] Verificada. \nIniciada: " + this.iniciada + "\nID: " + this.Datos.ID + 
    // "\nRol: " + this.Datos.Rol + "\nCE: " + this.Datos.Correo + "\nCU: " + this.Datos.ID_DATOS);    
  }

  id(): number{
    if(this.Datos.ID >= 0)
      return this.Datos.ID;
    else
      return parseInt(localStorage.getItem("uid"));
  }

  rol(){
    return this.Datos.Rol;
  }

  id_datos(){
    // console.log(this.Datos);
    // if(!this.Datos.ID_DATOS) return setTimeout(() => {this.id_datos();}, 1000);
    return this.Datos.ID_DATOS;
  }

  correo(){
    if(!this.Datos.Correo) return setTimeout(() => {this.correo();}, 1000);
    return this.Datos.Correo;
  }

  // idCliente(){

  // }

  subagente(){
  	if(this.Datos.Rol === "SubAgente" || this.Datos.Rol === "Administrador" || this.Datos.Rol === "Super administrador" || this.Datos.Rol === "Gerente")
      return true;
    return false;
  }

  gerente(){
  	if(this.Datos.Rol === "Administrador" || this.Datos.Rol === "Super administrador" || this.Datos.Rol === "Gerente")
      return true;
    return false;
  }

  admin(){  	
    // console.log(this.Datos.Rol);
  	if(this.Datos.Rol === "Administrador" || this.Datos.Rol === "Super administrador")
      return true;	
    return false;
  }

  superAdmin(){  
  	if(this.Datos.Rol === "Administrador" || this.Datos.Rol === "Super administrador")
  	// if(this.Datos.Rol === "Super administrador")
      return true;	
    return false;
  }

  cliente(){
    // console.log(this.Datos.Rol);
    // if(!this.Datos.Rol) return setTimeout(() => {this.cliente();}, 1000);
  	if(this.Datos.Rol === "Cliente")
      return true;	
    return false;
  }

  // funcionPrueba(Datos){
  //   Datos.codigoPostal = 66666;
  // }

}
