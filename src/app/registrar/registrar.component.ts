import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { HTTPService }    from '../http.service';
import { DatosFormularioService } from '../servicios/datos-formulario.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {

  constructor(
    private Ruta: ActivatedRoute,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService,
    public Rutas: Router) {}

  public Datos = {
    contrasena: null,
  	contrasena2: null,
    id: null,
  };
  DatosCliente = {
    contrasena: null,
  };
  Contra = null;

  cambiarContrasena(){
  	this.HTTP.datosDe("usuario", [{campo: 'contrasena', valor: this.Contra}])
  		.subscribe(
  			datos => {
          console.log(datos);
    
          datos[0].contrasena = this.Datos.contrasena;

          this.Formulario.guardar('usuario', datos[0], null, () => {
              this.Formulario.Alerta.open("La contraseña fue creada", "Cerrar");
              this.Rutas.navigate(['Iniciar']);
          });
          // this.HTTP.datosDe("cliente", [{campo: 'id', valor: datos[0].id_datos}])
          // .subscribe(
          //   datosCli => {
          //     this.DatosCliente = datosCli[0];
          //   }
          // );
  			},

  			error => {
  				console.log(error);
  			}
  			);
  }

  ngOnInit() {

    this.Ruta.params.subscribe(
      params => {
        this.Contra = params['id'];
        // console.log(this.Datos);
      }
    );

  }

}
