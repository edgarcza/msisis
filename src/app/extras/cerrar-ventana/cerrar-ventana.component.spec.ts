import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CerrarVentanaComponent } from './cerrar-ventana.component';

describe('CerrarVentanaComponent', () => {
  let component: CerrarVentanaComponent;
  let fixture: ComponentFixture<CerrarVentanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CerrarVentanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CerrarVentanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
