import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-regresar',
  templateUrl: './regresar.component.html',
  styleUrls: ['./regresar.component.css']
})
export class RegresarComponent implements OnInit {

  constructor(private Sitio: Location) { }

  atras() {
      this.Sitio.back();
  }

  ngOnInit() {
  }

}
