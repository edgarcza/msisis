import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-carta-poliza',
  templateUrl: './carta-poliza.component.html',
  styleUrls: ['./carta-poliza.component.css']
})
export class CartaPolizaComponent implements OnInit {

  @Input('textos') Textos;

  constructor() { }

  ngOnInit() {
  }

}
