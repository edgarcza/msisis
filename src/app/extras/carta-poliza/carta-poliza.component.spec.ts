import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartaPolizaComponent } from './carta-poliza.component';

describe('CartaPolizaComponent', () => {
  let component: CartaPolizaComponent;
  let fixture: ComponentFixture<CartaPolizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartaPolizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartaPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
