import { Component, OnInit, OnChanges, Input } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { MatDatepickerInputEvent, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS} from '../../clases/FechaAdaptador';

import {DatosFormularioService} from '../../servicios/datos-formulario.service';
// import {DatosService} from '../../datos.service';

@Component({
  selector: 'app-form-direccion',
  templateUrl: './form-direccion.component.html',
  styleUrls: ['./form-direccion.component.css'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})


export class FormDireccionComponent implements OnInit {

  @Input("datos") Datos;
	@Input("facturacion") Facturacion;

  public pre = "";

  controlColonia = new FormControl();
  controlPais = new FormControl();

  constructor(
    public datosDireccion: DatosFormularioService) { }

  ngOnInit() {
  	// this.datosDireccion.filtradoColonias = this.filtrado(this.controlColonia, "Colonias");
  	// this.datosDireccion.filtradoPaises = this.filtrado(this.controlPais, "Paises");

    // this.datosDireccion.obtenerPaises();
  	this.datosDireccion.obtener("Pais");

    // if(this.Facturacion)
    //   this.pre = "fac_";

  }

  
  // ngOnChanges(changes: SimpleChanges) {   
    // console.log(changes['Facturacion']);
    // if(changes['Facturacion'])
    //   if(changes['Facturacion'].currentValue)
    //     this.pre = "fac_";
    //   console.log("this.pre = " + this.pre);
  // }

  private filtrado(control, caso){
  	// if(caso === "Colonias")
	  	return control.valueChanges
	      .pipe(
	        startWith(''),
	        map(value => this._filter(value, caso))
	      );
	  // return 0;
  }

  private _filter(value: any, caso): string[] {
    const filterValue = value.toLowerCase();
    if(caso === "Colonias")
    	return this.datosDireccion.Colonias.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    else if(caso === "Paises")
    	return this.datosDireccion.Paises.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    return ["asd"];
  }

  codCambio(pre = ""){
    // console.log(this.Datos.colonia);
    if(this.Datos[pre + 'colonia']){
      const valorFiltro = this.Datos[pre + 'colonia'].toLowerCase();
      this.datosDireccion.filtradoColonias = this.datosDireccion.Colonias.filter(valor => valor.toLowerCase().indexOf(valorFiltro) === 0);
      // console.log(this.datosDireccion.filtradoColonias);
    }
  }




}
