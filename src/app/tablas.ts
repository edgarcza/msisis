export {SelectionModel} from '@angular/cdk/collections';
export {MatTableDataSource, PageEvent, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

export { HTTPService }    from './http.service';
export {SesionService} from './sesion.service'
export {VentanasService} from './servicios/ventanas.service';
export { DatosFormularioService } from './servicios/datos-formulario.service';
export {ExcelService} from './servicios/excel.service';