import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.component.html',
  styleUrls: ['./configuracion.component.css']
})
export class ConfiguracionComponent implements OnInit {

	public Datos = {
		"usuario": "",
		"pw": "",
		"email": "",
		"nombreCompleto": ""
	};

	public Oficinas = [
		{"oficina":"Oficina1", "id":1},
		{"oficina":"Oficina2", "id":2},
		{"oficina":"Oficina3", "id":3},
	];

	public Empresas = [
		{"empresa":"Empresa1", "id":1},
		{"empresa":"Empresa2", "id":2},
		{"empresa":"Empresa3", "id":3},
	];

	public Roles = [
		{"rol":"Asesor", "id":1},
		{"rol":"Administrador", "id":2},
		{"rol":"Super administrador", "id":3},
	];

  constructor() { }

  ngOnInit() {
  }

}
