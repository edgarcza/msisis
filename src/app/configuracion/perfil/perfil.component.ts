import { Component, OnInit } from '@angular/core';

import {HTTPService} from '../../http.service';
import {SesionService} from '../../sesion.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';

import {Archivo} from '../../interfaces/Archivo';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  constructor(
    private HTTP: HTTPService,
    private Sesion: SesionService,
    public Formulario: DatosFormularioService,) { }

    public Datos = {
      id: null,
      id_agente: null,
      id_rol: null,
      activo_inactivo: null,
      nombres: null,
      apellidos: null,
      nombre_completo: null,
      correo_electronico: null,
      contrasena: null,
      contrasena_link: null,
      contrasena_tiempo: null,
      telefono: null,
      comision: null,
      descuento: null,
      id_pais: null,
      estado: null,
      cp: null,
      ciudad: null,
      calle_numero: null,
      razon_social: null,
      rfc: null,
      fac_correo_electronico: null,
      fac_telefono: null,
      fac_id_pais: null,
      fac_estado: null,
      fac_cp: null,
      fac_ciudad: null,
      fac_calle_numero: null,
      colonia: null,
      fac_colonia: null,
      perfil_formato: null    
  }

  // public DatosFacturacion = {
  //   razonSocial: 'Programas Estudiantiles de Mexico AC',
  //   rfc: 'PEMKK01019404',
  //   codigoPostal: "",
  //   colonia: '',
  //   email: 'info@estudiantesembajadores.com',
  //   pais: null,
  //   estado: "",
  //   comision: 10,
  //   descuento: 10,
  //   ciudad: '',
  //   direccion: '',
  //   numInt: null,
  //   numExt: null,
  //   telefono: '83352711'
  // }

  // public Archivo = null;
  public Archivo = {
    URL_TMP: null,
    Formato: null,
    Archivo: {
      filename: null,
      filetype: null,
      value: null,
    },
    Nombre: null,
  };

	url = '../assets/img/perfil-sin.jpg';
  // onSelectFile(event) {
  //   if (event.target.files && event.target.files[0]) {
  //     let file = event.target.files[0];
  //     var reader = new FileReader();

  //     reader.readAsDataURL(event.target.files[0]);

  //     reader.onload = (event) => {
  //       this.url = reader.result;

  //       this.Datos.perfil_formato = file.name.split(".").pop();

  //       this.Archivo = {
  //         filename: file.name,
  //         filetype: file.type,
  //         value: reader.result.split(',')[1]
  //       }

  //     }
  //   }
  // }

  cambioArchivo(event){
    this.Formulario.onSelectFile(event, this.Archivo, () => {this.Datos.perfil_formato = this.Archivo.Formato; console.log(this.Archivo)});
  }

  guardarPerfil(){
    this.Formulario.guardar('subagente', this.Datos);
    this.Formulario.subirArchivo(this.Archivo.Archivo, "Perfil", this.Datos.id);
  }

  ngOnInit() {

    if(this.Sesion.id_datos()) return this.perfil();

    this.Sesion.SesionIniciada.subscribe(sesion => {
      console.log(sesion);
      if(sesion){
        this.perfil();
      }
    })

    
  }
  
  perfil(){
    let tabla = "subagente";
    if(this.Sesion.rol() === "Cliente") tabla = "cliente";
    this.HTTP.datosDe(tabla, [{campo: 'id', valor: this.Sesion.id_datos()}])
      .subscribe(
        datos => {
          console.log(datos);
          this.Datos = datos[0];
          if(this.Datos.perfil_formato && this.Datos.perfil_formato !== "")
            this.Archivo.URL_TMP = 'http://misegurointernacional.com/MSISIS/archivos/Perfil/'+this.Datos.id+'.'+this.Datos.perfil_formato;
          // console.log(this.Datos);
        },

        error => {
          console.log(error);
        }
      );
  }

}
