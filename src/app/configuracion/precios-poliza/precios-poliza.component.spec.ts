import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreciosPolizaComponent } from './precios-poliza.component';

describe('PreciosPolizaComponent', () => {
  let component: PreciosPolizaComponent;
  let fixture: ComponentFixture<PreciosPolizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreciosPolizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreciosPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
