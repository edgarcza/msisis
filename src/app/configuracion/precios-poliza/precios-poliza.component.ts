import { Component, OnInit } from '@angular/core';
import { HTTPService } from '../../http.service';
import { BarraProgresoService } from '../../servicios/barra-progreso.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';

@Component({
  selector: 'app-precios-poliza',
  templateUrl: './precios-poliza.component.html',
  styleUrls: ['./precios-poliza.component.css']
})
export class PreciosPolizaComponent implements OnInit {

  Valor = null;
  DatosPolizas = [];

  constructor(
    public Formulario: DatosFormularioService,
    public HTTP: HTTPService,
    public Progreso: BarraProgresoService,
  ) { }

  ngOnInit() {
    this.Formulario.obtener("Tipo_Seguro");

    this.HTTP.datosDe("tipo_poliza").subscribe(
      (datos) => {
        console.log(datos);
        this.DatosPolizas = datos;        
        for(var Numero in this.DatosPolizas)
          this.OpcionesPolizas[this.DatosPolizas[Numero]['id']] = false;
      },

      error => {
        console.log(error);
      }

    );

  }

  public OpcionesPolizas: boolean[] = [];
  opcionCambio(datos){
    if(this.OpcionesPolizas[datos] == false)
      this.OpcionesPolizas[datos] = true;
    else
      this.OpcionesPolizas[datos] = false;
  }

  guardarPoliza(datos){
    this.Formulario.guardar("tipo_poliza", datos);
  }

  eliminarPoliza(datos){
    this.Formulario.borrar("tipo_poliza", datos);
  }

  Guardando = false;
  guardarlos(){
    // for(var N in this.DatosPolizas){
    this.Guardando = true;
    this.guardar(this.DatosPolizas[0], 0, this.DatosPolizas.length);
    // }
  }

  guardar(esto, idx, len){
    if(idx >= len) {
      this.Guardando = false;
      return this.Formulario.alerta("Guardados", "Cerrar");
    }
    this.Formulario.guardar("tipo_poliza", esto, null, () => {
      this.guardar(this.DatosPolizas[idx], idx + 1, len);
    });
  }

}
