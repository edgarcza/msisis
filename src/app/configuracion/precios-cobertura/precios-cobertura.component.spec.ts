import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreciosCoberturaComponent } from './precios-cobertura.component';

describe('PreciosCoberturaComponent', () => {
  let component: PreciosCoberturaComponent;
  let fixture: ComponentFixture<PreciosCoberturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreciosCoberturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreciosCoberturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
