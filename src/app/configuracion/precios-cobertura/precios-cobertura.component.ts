import { Component, OnInit } from '@angular/core';
import { HTTPService } from '../../http.service';
import { BarraProgresoService } from '../../servicios/barra-progreso.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';

@Component({
  selector: 'app-precios-cobertura',
  templateUrl: './precios-cobertura.component.html',
  styleUrls: ['./precios-cobertura.component.css']
})
export class PreciosCoberturaComponent implements OnInit {

  Valor = null;
  DatosCoberturas = [];
  DatosCantidad = 0;

  constructor(
    public Formulario: DatosFormularioService,
    public HTTP: HTTPService,
    public Progreso: BarraProgresoService,
  ) { }

  ngOnInit() {
    // this.Formulario.obtener("Tipo_Seguro");

    this.tabla();

  }

  tabla(pag = 1, cantidad = 10)
  {
    this.HTTP.datosDe("cobertura", null, null, cantidad, pag).subscribe(
      (datos) => {
        console.log(datos);
        this.DatosCoberturas = datos;        
        for(var Numero in this.DatosCoberturas)
        {
          this.OpcionesCoberturas[this.DatosCoberturas[Numero]['id']] = false;
          this.DatosCantidad++;
        }
      },

      error => {
        console.log(error);
      }

    );
  }

  public OpcionesCoberturas: boolean[] = [];
  opcionCambio(datos){
    if(this.OpcionesCoberturas[datos] == false)
      this.OpcionesCoberturas[datos] = true;
    else
      this.OpcionesCoberturas[datos] = false;
  }

  guardarCobertura(datos){
    this.Formulario.guardar("cobertura", datos);
  }

  eliminarCobertura(datos){
    this.Formulario.borrar("cobertura", datos);
  }

  cambioPagina(event){
		this.tabla(event.pageIndex, event.pageSize);
	}

  Guardando = false;
  guardarlos(){
    // for(var N in this.DatosPolizas){
    this.Guardando = true;
    this.guardar(this.DatosCoberturas[0], 0, this.DatosCoberturas.length);
    // }
  }

  guardar(esto, idx, len){
    if(idx >= len) {
      this.Guardando = false;
      return this.Formulario.alerta("Guardados", "Cerrar");
    }
    this.Formulario.guardar("cobertura", esto, null, () => {
      this.guardar(this.DatosCoberturas[idx], idx + 1, len);
    });
  }

}
