import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidenciasPendienteComponent } from './pendiente.component';

describe('IncidenciasPendienteComponent', () => {
  let component: IncidenciasPendienteComponent;
  let fixture: ComponentFixture<IncidenciasPendienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidenciasPendienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidenciasPendienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
