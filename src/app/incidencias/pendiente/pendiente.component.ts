import { Component, OnInit } from '@angular/core';
import * as Tabla from '../../tablas'
import {Tablas} from '../../clases/tablas'

@Component({
  selector: 'app-incidencias-pendiente',
  templateUrl: './pendiente.component.html',
  styleUrls: ['./pendiente.component.css']
})
export class IncidenciasPendienteComponent extends Tablas implements OnInit {

  constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }

  ngOnInit() {
    this.agregarColumnas(
      [
        {id: 'nombre_completo',    		nombre: 'Asegurado ',            	ocultar: false, tabla: 'cliente.nombre_completo'},
        {id: 'fecha_creacion',    		nombre: 'Fecha de registro',     	ocultar: false, tabla: 'incidente.fecha_creacion'},
        {id: 'fecha_incidente',      	nombre: 'Fecha de incidente',   	ocultar: false, tabla: 'incidente.fecha_incidente'},
        {id: 'monto_solicitado',    	nombre: 'Cantidad solicitada',   	ocultar: false, tabla: 'incidente.monto_solicitado'}, 
        {id: 'descripcion',         	nombre: 'Descripción',           	ocultar: false, tabla: 'incidente.descripcion'}, 
        // {id: 'fecha_envio',          	nombre: 'Fecha de envío',       	ocultar: false, tabla: 'incidente.fecha_envio'}, 
        // {id: 'status',            		nombre: 'Status',    							ocultar: false, tabla: 'incidente.status'},
        // {id: 'fecha_autorizacion',		nombre: 'Fecha de pago',         	ocultar: false, tabla: 'incidente.fecha_autorizacion'}, 
        // {id: 'monto_pagado',         	nombre: 'Cantidad pagada',       	ocultar: false, tabla: 'incidente.monto_pagado'}, 
        // {id: 'resultado',   					nombre: 'Resultado',  						ocultar: false, tabla: 'incidente.resultado'}, 
        // {id: 'tipo_distribuidor',    	nombre: 'Tipo de distribuidor',   ocultar: false, tabla: 'tipo_distribuidor.tipo_distribuidor'}, 
        // {id: 'distribuidor',         	nombre: 'Distribuidor',           ocultar: false, tabla: 'distribuidor.distribuidor'}, 
        // {id: 'agente',               	nombre: 'Agente',                 ocultar: false, tabla: 'agente.agente'}, 
        // {id: 'subagente',   					nombre: 'Sub Agente',             ocultar: false, tabla: 'sub_agente.nombre_completo'},
      ]
    );

    this.agregarColIds(
      ['select', 'nombre_completo', 'fecha_creacion', 'fecha_incidente',
        'monto_solicitado', 'descripcion',
        //  'monto_pagado', 'resultado', 'tipo_distribuidor', 'distribuidor', 
        // 'agente', 'subagente', 'acciones'
        'acciones'
      ]
    );

    this.tablaConfig("pagIncidencias", "IncidenciasPendientes", "incidencia", "incidente", [{Campo: "incidente.resultado", Valor: '', Operador: "="}]);

    this.tabla();
  }

  modificarIncidente(datos){
    this._Ventana.agregarIncidente({DatosSeguro: {id: datos.cotizacion_id}, DatosIncidente: datos});
  }

}
