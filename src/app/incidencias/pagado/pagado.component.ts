import { Component, OnInit } from '@angular/core';
import * as Tabla from '../../tablas'
import {Tablas} from '../../clases/tablas'

@Component({
  selector: 'app-incidencias-pagado',
  templateUrl: './pagado.component.html',
  styleUrls: ['./pagado.component.css']
})
export class IncidenciasPagadoComponent extends Tablas implements OnInit {

  constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }

  ngOnInit() {
    this.agregarColumnas(
      [
        {id: 'nombre_completo',    		nombre: 'Asegurado ',            	ocultar: false, tabla: 'cliente.nombre_completo'},
        {id: 'fecha_creacion',    		nombre: 'Fecha de registro',     	ocultar: false, tabla: 'incidente.fecha_creacion'},
        {id: 'fecha_incidente',      	nombre: 'Fecha de incidente',   	ocultar: false, tabla: 'incidente.fecha_incidente'},
        {id: 'monto_solicitado',    	nombre: 'Cantidad solicitada',   	ocultar: false, tabla: 'incidente.monto_solicitado'}, 
        {id: 'descripcion',         	nombre: 'Descripción',           	ocultar: false, tabla: 'incidente.descripcion'}, 
        {id: 'fecha_autorizacion',		nombre: 'Fecha de pago',         	ocultar: false, tabla: 'incidente.fecha_autorizacion'}, 
        {id: 'monto_pagado',         	nombre: 'Cantidad pagada',       	ocultar: false, tabla: 'incidente.monto_pagado'}, 
        {id: 'resultado',   					nombre: 'Resultado',  						ocultar: false, tabla: 'incidente.resultado'}, 
      ]
    );

    this.agregarColIds(
      ['select', 'nombre_completo', 'fecha_creacion', 'fecha_incidente',
        'monto_solicitado', 'descripcion', 'fecha_autorizacion', 'monto_pagado', 'resultado', 'acciones'
      ]
    );

    this.tablaConfig("pagIncidencias", "IncidenciasPendientes", "incidencia", "incidente", [{Campo: "NOT incidente.resultado", Valor: '', Operador: "="}]);

    this.tabla();
  }

  modificarIncidente(datos){
    this._Ventana.agregarIncidente({DatosSeguro: {id: datos.cotizacion_id}, DatosIncidente: datos});
  }

}
