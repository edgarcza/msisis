import { Component, OnInit, EventEmitter, Input, Output, OnChanges, SimpleChanges } from '@angular/core';
import {PageEvent} from '@angular/material';

@Component({
  selector: 'app-datos-pagina',
  templateUrl: './datos-pagina.component.html',
  styleUrls: ['./datos-pagina.component.css']
})
export class DatosPaginaComponent implements OnInit {

  @Output() changePaginas = new EventEmitter();

	@Input('total') Total;

    // MatPaginator Inputs
  public length = this.Total;
  // public length = 155;
  public pageSize = 50;
  public pageSizeOptions: number[] = [10, 25, 50, 100, length];
  public pageIndex = 0;

  // MatPaginator Output
  public pageEvent: PageEvent;

  cambioPagina(event){
    this.changePaginas.emit(event);
    // console.log(event);
    // console.log(this.Total.length);
  }


  constructor() { }

  ngOnInit() {
  	this.length = this.Total
  }

  ngOnChanges(changes: SimpleChanges) {  
    this.length = changes['Total'].currentValue;
    this.pageSizeOptions = [10, 25, 50, 100, this.length];
  }

}
