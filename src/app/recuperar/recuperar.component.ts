import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { HTTPService }    from '../http.service';
import { DatosFormularioService } from '../servicios/datos-formulario.service';

@Component({
  selector: 'app-recuperar',
  templateUrl: './recuperar.component.html',
  styleUrls: ['./recuperar.component.css']
})
export class RecuperarComponent implements OnInit {

	constructor(
    private Ruta: ActivatedRoute,
    public Formulario: DatosFormularioService,
    private HTTP: HTTPService,
    public Rutas: Router) {}

  public Datos = {
  	contrasena: null,
  	contrasena2: null,
  	id_contrasena: null,
  };

  cambiarContrasena(){
  	this.HTTP.recuperarContrasena(this.Datos.id_contrasena, 2, this.Datos.contrasena)
  		.subscribe(
  			datos => {
  				console.log(datos);
  				if(datos.enviado == 1){
  					this.Formulario.Alerta.open("La contraseña fue cambiada", "Cerrar");
      			this.Rutas.navigate(['Iniciar']);
  				}
  				else if(datos.enviado == 2){
  					this.Formulario.Alerta.open("El enlace ya no es válido. Hacer prodecimiento nuevamente", "Cerrar");
      			this.Rutas.navigate(['Iniciar']);
  				}
  				else
  					this.Formulario.Alerta.open("Error al cambiar la contraseña", "Cerrar");
  			},

  			error => {
  				console.log(error);
  			}
  			);
  }

  ngOnInit() {

    this.Ruta.params.subscribe(
      params => {
        this.Datos.id_contrasena = params['id'];
        // console.log(this.Datos);
      }
    );

  }

}
