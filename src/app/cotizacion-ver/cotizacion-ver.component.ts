import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { HTTPService }    from '../http.service';
import { DocumentosService }    from '../servicios/documentos.service';
import { BarraProgresoService }    from '../servicios/barra-progreso.service';
import { VentanasService }    from '../servicios/ventanas.service';

@Component({
  selector: 'app-cotizacion-ver',
  templateUrl: './cotizacion-ver.component.html',
  styleUrls: ['./cotizacion-ver.component.css']
})
export class CotizacionVerComponent implements OnInit {

  constructor(
    private Ruta: ActivatedRoute,
    private Link: Router,
    private HTTP: HTTPService,
    public Progreso: BarraProgresoService,
    private Documento: DocumentosService,
    private Ventana: VentanasService,
    ) { }

  Datos = {
    cliente_correo_electronico: null,
    subagente_nombre_completo: null,
    agente: null,
    tipo_seguro: null,
    pais_origen: null,
    pais_destino: null,
    salida: null,
    precioMXN: null,
    precioUSD: null,
    tipo_cambio: null,
    cliente_nombre_completo: null,
    subagente_correo_electronico: null,
    fecha_hecha: null,
    tipo_poliza: null,
    regreso: null,
    id_cliente: null,
  };
  private ID;

  ngOnInit() {

    this.Ruta.params.subscribe(
      params => {
        var idCot = +params['id'];  
        var accion = params['accion'];  
        // this.ID = idCot;
        // console.log(this.Datos.id);
        this.HTTP.cotizacionConfirmar(idCot)
        .subscribe(
          datos => {
            console.log(datos);
            this.Datos = datos.Cotizacion[0];
            this.ID = this.Datos.id_cliente;
            setTimeout(() => {
              if(accion){
                if(accion === "Imprimir"){
                  this.imprimir(true)
                }
                else if(accion === "Descargar"){
                  this.descargar(true)
                }
                else if(accion === "Correo"){
                  this.correo(true)
                }
              }
            }, 2000);
            
          },
          error => {
            console.log(error);
          }
        );

      }
    );

  }

  imprimir(regresar = false){
    this.Progreso.CargandoGlobal = true;
    this.Documento.pdf(document.getElementsByClassName("area")[0], true, () => {
        // this.Cargando = false;
        this.Progreso.CargandoGlobal = false;
        if(regresar == true){
          // this.Link.navigate(['Prospectos', this.ID, 'Cotizaciones'])
        }
    });
  }

  descargar(regresar = false){
    this.Progreso.CargandoGlobal = true;
    this.Documento.pdf(document.getElementsByClassName("area")[0], false, () => {
        // this.Cargando = false;
        this.Progreso.CargandoGlobal = false;
        if(regresar == true){
          this.Link.navigate(['Prospectos', this.ID, 'Cotizaciones'])
        }
    });
  }

  correo(regresar = false){
    this.Ventana.correoCuerpo(this.Datos, 
      (Resultado) => {
        this.Progreso.CargandoGlobal = true;
        this.Documento.pdf(document.getElementsByClassName("area")[0], false, (link, base, pdf, pdfbase) => {
          // this.LINKIMG = pdf;
          console.log(Resultado);
          console.log(base);
          this.Documento.email(this.Datos.cliente_correo_electronico, "CotizacionVer", 
          {
            Archivo: base, 
            // Archivo: pdfbase, 
            // Archivo: 'hfdhdsfhdioshioewuroinfdksngjskdhgkshgj64hfjhieuhruwrhugfjdsbiurewcbkbcalfporiuoietrighjknnmcvnxbutriyteopturpfvnmnbvjkf', 
            Asunto: Resultado.Datos.Asunto, 
            Mensaje: Resultado.Datos.Mensaje, 
            De: Resultado.Datos.De, 
            CC: Resultado.Datos.CC
          }, 
          () => {
              // this.Cargando = false;
              this.Progreso.CargandoGlobal = false;
              if(regresar == true){
                this.Link.navigate(['Prospectos', this.ID, 'Cotizaciones'])
              }
          });
        }, false); 
      },
      
      () => {

      })    
  }

}
