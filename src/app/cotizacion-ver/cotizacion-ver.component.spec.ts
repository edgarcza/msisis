import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizacionVerComponent } from './cotizacion-ver.component';

describe('CotizacionVerComponent', () => {
  let component: CotizacionVerComponent;
  let fixture: ComponentFixture<CotizacionVerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizacionVerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizacionVerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
