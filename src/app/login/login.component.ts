import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {MatSnackBar} from '@angular/material';

import { HTTPService }    from '../http.service';
import { SesionService }    from '../sesion.service';
import { VentanasService }    from '../servicios/ventanas.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private Ruta: ActivatedRoute,
  	public Alerta: MatSnackBar,
    public HTTP: HTTPService,
    public Sesion: SesionService,
    public Ventana: VentanasService
  	) { }

  public Datos = {"Usuario":"", "PW":""};
  public Cargando = false;

  iniciarSesion(){
    this.Cargando = true;

    this.HTTP.buscarUsuario2(this.Datos).subscribe(
      resultado => {
        // console.log(resultado);
        if(resultado.existe){
          if(resultado.activo_inactivo == 1)
            this.Sesion.iniciar(resultado.correo_electronico);
            else
              this.Alerta.open('Cuenta desactivada', 'Cerrar'); 
            this.Cargando = false;
        }
        else{
          this.Alerta.open('Datos incorrectos', 'Cerrar'); 
          this.Cargando = false;
        }
      }, 

      error => {
        console.log(error);
        this.Cargando = false;
      }
    );

    // this.HTTP.buscarUsuario(this.Datos, true)
    //   .subscribe(
    //     resultado => { 
    //       console.log(resultado);
    //       if(resultado.sesion === "iniciada")
    //       {
    //         // this.Sesion.verificar();
    //         this.Sesion.iniciar(resultado.sesion_id);
    //         this.Alerta.open('Sesión iniciada', 'Cerrar'); 
    //       }
    //       else if(resultado.sesion === "desactivado")
    //       {
    //         // this.Sesion.verificar();
    //         // this.Sesion.iniciar(resultado.sesion_id);
    //         this.Alerta.open('No', 'Cerrar'); 
    //       }
    //       else
    //       {
    //         this.Alerta.open('Datos incorrectos', 'Cerrar'); 
    //       }
    //       // if(inicio.iniciado == 1)
    //       // {
    //       //   this.not.open('Iniciado', 'Cerrar'); 
    //       //   this.sesion.iniciar(inicio.usuariof);
    //       //   // this.router.navigate(['/inicio']);
            
    //       //   this.router.navigateByUrl('/iniciar', {skipLocationChange: true}).then(()=>
    //       //   this.router.navigate(["inicio"]));
    //       // }
    //       this.Cargando = false;
    //     },

    //     error => { 
    //       console.log(error);
    //       this.Alerta.open('ERROR', 'Cerrar'); 
    //       this.Cargando = false; 
    //     }
    //  );
  }

  ngOnInit() {
    // console.log(this.Ruta.snapshot);
    // // this.Alerta.open("dsdsad", "Cerrar");
    // this.Ruta.params.subscribe(
    //   params => {
    //     console.log(this.Ruta);
    //     console.log(params);
    //     if(params['id'])
    //       this.Ventana.recuperarContrasena2({ID: params['id']});
    //     // console.log(this.Datos);
    //   },
    //   asd => {console.log(asd);}
    // );
  }


}
