import * as Tabla from '../tablas'
import { isArray } from 'util';

export class Tablas {
	constructor(
			private HTTP: Tabla.HTTPService,
			public sesion: Tabla.SesionService,
			public dialog: Tabla.MatDialog,
			public Ventana: Tabla.VentanasService,
			public Formulario: Tabla.DatosFormularioService,
			public ServcioExcel: Tabla.ExcelService
	) { }

	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	// - - -- - - - - -- - - -  SECCIÓN FILTROS
	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -

	public Filtros = {
		Campo: null,
		Valor: null,
		pageIndex: 0,
		pageSize: 50,
		Condiciones: null,
	};

	public Excel = false;
	public ExcelIngles = false;
	private CasoTabla = '';
	private ExcelNombre = 'Excel';
	private WhereOB = null;
	private Nombre = "";
	private Tabla = "";
	public Opciones: boolean[] = [];

	filtrar(){
		// if(this.Filtros.Campo && this.Filtros.Valor){
		// 	if(this.Filtros.Valor.length > 1)
		// 		this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize, this.Filtros);
		// 	else
		// 		this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize);
		// }
		// else{
		// 		this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize);
		// }
		this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize, this.Filtros);
		// var s = this.Filtros.Valor;
		// console.log(this.Datos.filter(e => console.log(e));
	}

	filtroAplicar(datos){
		console.log(datos);
		this.Filtros.Condiciones = datos;
		console.log(this.Filtros.Condiciones);
		if(datos == true)
			this.Filtros.Condiciones = null;
		console.log(this.Filtros.Condiciones);
		this.filtrar();
	}

	filtroTexto(texto: string){
		console.log(texto);
		this.Filtros.Valor = texto;
		this.filtrar();
	}

	filtroEliminar(texto: string){
		// console.log(texto);
		if(this.selection.selected){
			for(var DatosEn in this.selection.selected){

				this.Ventana.Confirmar({Mensaje: "¿Eliminar "+this.Nombre+" "+this.selection.selected[DatosEn].id+"?"}, 
					() => {
						this.Formulario.borrar(this.Tabla, this.selection.selected[DatosEn], () => {
							this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize);});
					}, 
					() => { // console.log("NO");
						});
				// this.Formulario.borrar("pago", this.selection.selected[DatosEn]);
			}
		}
	}

	filtroSeleccionar(valor){
		// console.log(valor);
		this.Filtros.Campo = valor;
		this.filtrar();
	}

	filtroExcel(valor){
		// console.log(valor);
		this.Excel = true;
		this.filtrar();
	}

	filtroExcelIngles(valor){
		// console.log(valor);
		this.ExcelIngles = true;
		this.filtrar();
	}

	columnas(texto: string){
		this.Ventana.columnas(this.Columnas);
	}

	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	// - - -- - - - - -- - - -  SECCIÓN PÁGINA
	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -

	cambioPagina(event){
		console.log(event);
		this.Filtros.pageIndex = event.pageIndex;
		this.Filtros.pageSize = event.pageSize;
		this.tabla(event.pageIndex, event.pageSize);
	}


	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	// - - -- - - - - -- - - -  SECCIÓN TABLA
	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	// - - - - - - - - - - - -- -  --  --  - - - - - - - - -
	
	public Columnas = [];
	public displayedColumns = [];

	agregarColumnas(arreglo){
		this.Columnas = arreglo;
		// console.log(this.Columnas, arreglo)
	}

	agregarColIds(arreglo){
		this.displayedColumns = arreglo;
		// console.log(this.displayedColumns)
	}

	// public Columnas = [
	//   {id: 'fecha_asegurado',    	  nombre: 'Fecha de venta',      		ocultar: false,    tabla: 'cotizacion.fecha_asegurado'},
	//   {id: 'subagente',   					nombre: 'Sub Agente',             ocultar: false,    tabla: 'subagente.nombre_completo'},
	//   {id: 'cliente',            		nombre: 'Cliente',    						ocultar: false,    tabla: 'cliente.nombre_completo'},
	//   {id: 'agente',               	nombre: 'Agente',                 ocultar: false,    tabla: 'agente.agente'}, 
	//   {id: 'distribuidor',         	nombre: 'Distribuidor',           ocultar: false,    tabla: 'distribuidor.distribuidor'}, 
	//   {id: 'tipo_distribuidor',    	nombre: 'Tipo de distribuidor',   ocultar: false,    tabla: 'tipo_distribuidor.tipo_distribuidor'}, 
	//   {id: 'precioMXN',    					nombre: 'Precio (MXN)',      		  ocultar: false,    tabla: 'cotizacion.precioMXN'},
	//   {id: 'comision',    					nombre: 'Comisión',      				  ocultar: false,    tabla: 'comision.precioMXN'},
	//   {id: 'impuestos',    					nombre: 'Impuestos',      				ocultar: false,    tabla: 'comision.impuestos'},
	//   {id: 'comision_final',    	  nombre: 'Comisión final',      	  ocultar: false,    tabla: 'comision.precioMXN'},
	//   // {id: 'factura',    					  nombre: 'Factura',      				  ocultar: false,    tabla: 'comision.factura'},
	// ];
	// displayedColumns: string[] = ['select', 'fecha_asegurado', 'subagente', 'cliente', 
	//   'agente', 'distribuidor', 'tipo_distribuidor', 'precioMXN', 'comision', 'impuestos', 'comision_final', 'factura', 'acciones'];

	public Datos = [];
	public DatosExcel = [];
	public DatosCantidad = 0;

	// ngOnInit() {
	//   // this.tabla();
	// }

	tablaTerminar = null;

	tablaConfig(casoTabla, excelNombre, nombre = "", tabla = "", whereOb = null){
		this.CasoTabla = casoTabla;
		this.ExcelNombre = excelNombre;
		this.WhereOB = whereOb;
		this.Nombre = nombre;
		this.Tabla = tabla;
	}

	tabla(pagina = 0, cantidad = 50, filtro: any = this.Filtros){
		// console.trace();
		console.log(filtro);
		// this.Formulario.Progreso.Cargando = true;
		// this.HTTP[this.CasoTabla](pagina, cantidad, filtro, this.Excel, this.WhereOB)
		this.HTTP.paginar(this.CasoTabla, pagina, cantidad, filtro, this.Excel, this.WhereOB, this.ExcelIngles)
			.subscribe(
				datos => {
					if(!datos) return;
					this.selection.clear();
					if(this.Excel)
					{
						// exportar
						if(this.tablaTerminar)
							this.DatosExcel = this.tablaTerminar(datos.Filas);
						else
							this.DatosExcel = datos.Filas;
						console.log(this.DatosExcel);
						this.ServcioExcel.exportAsExcelFile(this.DatosExcel, this.ExcelNombre);
						// this.ServcioExcel.exportAsExcelFile([{a: "dsad"}], this.ExcelNombre);
						this.Excel = false;
						return 0;
					}
					else if(this.ExcelIngles)
					{
						// exportar
						if(this.tablaTerminar)
							this.DatosExcel = this.tablaTerminar(datos.Filas);
						else
							this.DatosExcel = datos.Filas;
						console.log(this.DatosExcel);
						this.ServcioExcel.exportAsExcelFile(this.DatosExcel, this.ExcelNombre);
						// this.ServcioExcel.exportAsExcelFile([{a: "dsad"}], this.ExcelNombre);
						this.Excel = false;
						return 0;
					}
					else
					{
						// console.log("else");
						this.Datos = datos.Filas;
						for(var i in datos.Filas){
							this.Opciones[datos.Filas[i].id] = false;
						}
						this.dataSource.data = datos.Filas;
						this.DatosCantidad = datos.Total;
						if(this.tablaTerminar)
							this.tablaTerminar(datos.Filas);
					}
					console.log(datos);
				},
				error => {
					console.log(error);
				}
			);
	}

	dataSource = new Tabla.MatTableDataSource(this.Datos);
	selection = new Tabla.SelectionModel(true, []);

	/** Whether the number of selected elements matches the total number of rows. */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		this.isAllSelected() ?
				this.selection.clear() :
				this.dataSource.data.forEach(row => this.selection.select(row));
	}

	/** Actualizar número de casillas seleccionadas */
	public Seleccionados = 0;
	actualizarSeleccion(){
		console.log(this.selection.selected);
		this.Seleccionados = this.selection.selected.length;
	}

	opcionCambio(id){
		// console.log(id);
		
		for(var N in this.Opciones){
			// if(this.Opciones[N] == this.Opciones[id]){
			// 	if(this.Opciones[id] == true)
			// 		this.Opciones[id] = false;
			// 	else
			// 		this.Opciones[id] = true;
			// }
			// else{
			// 	this.Opciones[N] = false;
			// }
			// if(this.Opciones[N] != this.Opciones[id]){
			// 	this.Opciones[N] = false;
			// }
			if(this.Opciones[N] != this.Opciones[id])
				this.Opciones[N] = false;
		}
		if(this.Opciones[id] == true){
			this.Opciones[id] = false;
		}
		else{
			this.Opciones[id] = true;
		}
	}
}
