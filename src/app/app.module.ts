import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AppRutasModule } from './/app-rutas.module';
import { LoginComponent } from './login/login.component';

import { ExcelService } from './servicios/excel.service';

import { PdfViewerModule } from 'ng2-pdf-viewer';

import {
  MatInputModule,
  MatButtonModule,
  MatListModule,
  MatProgressBarModule,
  MatSnackBarModule,
  MatDividerModule,
  MatSelectModule,
  MatTableModule,
  MatCheckboxModule,
  MatPaginatorModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatRadioModule,
  MatTabsModule,
  MatExpansionModule,
  MatDialogModule,
  MatAutocompleteModule,
  MatProgressSpinnerModule,
  MatPaginatorIntl,
  MatStepperModule,
} from '@angular/material';

import { MenuComponent } from './menu/menu.component';
import { ClientesProspectosComponent } from './clientes-prospectos/clientes-prospectos.component';
import { ClientesAseguradosComponent } from './clientes-asegurados/clientes-asegurados.component';
import { NoEncontradaComponent } from './no-encontrada/no-encontrada.component';
import { PieComponent } from './pie/pie.component';
import { DatosFiltroComponent } from './datos-filtro/datos-filtro.component';
import { CotizacionComponent } from './cotizacion/cotizacion.component';
import { InicioComponent } from './inicio/inicio.component';
import { CotizacionConfirmacionComponent } from './cotizacion-confirmacion/cotizacion-confirmacion.component';
import { ConfiguracionComponent } from './configuracion/configuracion/configuracion.component';
import { PerfilComponent } from './configuracion/perfil/perfil.component';
import { ProspectosComponent } from './prospectos/prospectos.component';
import { AseguradosComponent } from './asegurados/asegurados.component';
import { DatosPaginaComponent } from './datos-pagina/datos-pagina.component';
import { CerrarVentanaComponent } from './extras/cerrar-ventana/cerrar-ventana.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { RecuperarComponent } from './recuperar/recuperar.component';

import { AsignarSeguroComponent } from './ventanas/asignar-seguro/asignar-seguro.component';
import { ColumnasComponent } from './ventanas/columnas/columnas.component';
import { DistribuidoresComponent } from './ventanas/distribuidores/distribuidores.component';
import { AgentesComponent } from './ventanas/agentes/agentes.component';
import { SubAgentesComponent } from './ventanas/sub-agentes/sub-agentes.component';
import { FacturacionComponent } from './extras/facturacion/facturacion.component';
import { FormDireccionComponent } from './extras/form-direccion/form-direccion.component';
import { TipoCambioComponent } from './ventanas/tipo-cambio/tipo-cambio.component';
import { RegresarComponent } from './extras/regresar/regresar.component';
import { AgregarPagoComponent } from './ventanas/agregar-pago/agregar-pago.component';
import { ConfirmarComponent } from './ventanas/confirmar/confirmar.component';
import { RecuperarContrasenaComponent } from './ventanas/recuperar-contrasena/recuperar-contrasena.component';
import { AgregarIncidenteComponent } from './ventanas/agregar-incidente/agregar-incidente.component';
import { AgregarFacturaIncidenteComponent } from './ventanas/agregar-factura-incidente/agregar-factura-incidente.component';
import { AgregarComentarioIncidenteComponent } from './ventanas/agregar-comentario-incidente/agregar-comentario-incidente.component';
import { PagosComponent } from './administracion/pagos/pagos.component';
import { RecuperarContrasena2Component } from './ventanas/recuperar-contrasena2/recuperar-contrasena2.component';
import { ComisionesComponent } from './administracion/comisiones/comisiones.component';
import { IncidenciasComponent } from './incidencias/incidencias.component';
import { IncidenciasPendienteComponent } from './incidencias/pendiente/pendiente.component';
import { IncidenciasPagadoComponent } from './incidencias/pagado/pagado.component';
import { PendienteComponent } from './administracion/comisiones/pendiente/pendiente.component';
import { PagadoComponent } from './administracion/comisiones/pagado/pagado.component';
import { ComisionAjustesComponent } from './ventanas/comision-ajustes/comision-ajustes.component';
import { SubirArchivoComponent } from './ventanas/subir-archivo/subir-archivo.component';
import { ComisionesSubagenteComponent } from './comisiones-subagente/comisiones-subagente.component';
import { ComisionesSubagentePendienteComponent } from './comisiones-subagente/pendiente/pendiente.component';
import { ComisionesSubagentePagadoComponent } from './comisiones-subagente/pagado/pagado.component';
import { ConciliarPendienteComponent } from './conciliar/conciliar-pendiente/conciliar-pendiente.component';
import { ConciliarAprobadoComponent } from './conciliar/conciliar-aprobado/conciliar-aprobado.component';
import { ConciliarAjustesComponent } from './ventanas/conciliar-ajustes/conciliar-ajustes.component';
import { ConciliarComponent } from './conciliar/conciliar.component';
import { PolizaComponent } from './poliza/poliza.component';
import { CartaPolizaComponent } from './extras/carta-poliza/carta-poliza.component';
import { MargenComponent } from './administracion/margen/margen.component';
import { CorreoCuerpoComponent } from './ventanas/correo-cuerpo/correo-cuerpo.component';
import { CotizacionVerComponent } from './cotizacion-ver/cotizacion-ver.component';
import { getSpnPaginatorIntl } from './paginador';
import { AcreditadosComponent } from './administracion/pagos/acreditados/acreditados.component';
import { PagosPrincipalComponent } from './administracion/pagos-principal/pagos-principal.component';
import { MargenPrincipalComponent } from './administracion/margen/margen-principal/margen-principal.component';
import { MargenIngresosComponent } from './administracion/margen/margen-ingresos/margen-ingresos.component';
import { MargenEgresosComponent } from './administracion/margen/margen-egresos/margen-egresos.component';
import { PreciosComponent } from './configuracion/precios/precios.component';
import { CodigosPromocionComponent } from './ventanas/codigos-promocion/codigos-promocion.component';
import { PreciosPolizaComponent } from './configuracion/precios-poliza/precios-poliza.component';
import { PreciosCoberturaComponent } from './configuracion/precios-cobertura/precios-cobertura.component';

import { TiposDistribuidorComponent } from './usuarios/tipos-distribuidor/tipos-distribuidor.component';
import { AgentesUsuariosComponent } from './usuarios/agentes/agentes.component';
import { DistribuidoresUsuariosComponent } from './usuarios/distribuidores/distribuidores.component';
import { GeneralComponent } from './usuarios/general/general.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { ComisionesAjustesComponent } from './ventanas/comisiones-ajustes/comisiones-ajustes.component';
import { ExcelAsignarComponent } from './ventanas/excel-asignar/excel-asignar.component';
import { AgregarPagoConjuntoComponent } from './ventanas/agregar-pago-conjunto/agregar-pago-conjunto.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuComponent,
    ClientesProspectosComponent,
    NoEncontradaComponent,
    PieComponent,
    DatosFiltroComponent,
    ClientesAseguradosComponent,
    CotizacionComponent,
    InicioComponent,
    CotizacionConfirmacionComponent,
    ConfiguracionComponent,
    PerfilComponent,
    ProspectosComponent,
    AseguradosComponent,
    DatosPaginaComponent,
    AsignarSeguroComponent,
    CerrarVentanaComponent,
    UsuariosComponent,
    ColumnasComponent,
    DistribuidoresComponent,
    AgentesComponent,
    SubAgentesComponent,
    FacturacionComponent,
    FormDireccionComponent,
    TipoCambioComponent,
    RegresarComponent,
    AgregarPagoComponent,
    ConfirmarComponent,
    RecuperarContrasenaComponent,
    RecuperarComponent,
    AgregarIncidenteComponent,
    AgregarFacturaIncidenteComponent,
    AgregarComentarioIncidenteComponent,
    PagosComponent,
    RecuperarContrasena2Component,
    ComisionesComponent,
    IncidenciasComponent,
    IncidenciasPendienteComponent,
    IncidenciasPagadoComponent,
    PendienteComponent,
    PagadoComponent,
    ComisionAjustesComponent,
    SubirArchivoComponent,
    ComisionesSubagenteComponent,
    ComisionesSubagentePendienteComponent,
    ComisionesSubagentePagadoComponent,
    ConciliarPendienteComponent,
    ConciliarAprobadoComponent,
    ConciliarAjustesComponent,
    ConciliarComponent,
    PolizaComponent,
    CartaPolizaComponent,
    MargenComponent,
    CorreoCuerpoComponent,
    CotizacionVerComponent,
    AcreditadosComponent,
    PagosPrincipalComponent,
    MargenPrincipalComponent,
    MargenIngresosComponent,
    MargenEgresosComponent,
    PreciosComponent,
    CodigosPromocionComponent,
    PreciosPolizaComponent,
    PreciosCoberturaComponent,
    TiposDistribuidorComponent,
    AgentesUsuariosComponent,
    DistribuidoresUsuariosComponent,
    GeneralComponent,
    RegistrarComponent,
    ComisionesAjustesComponent,
    ExcelAsignarComponent,
    AgregarPagoConjuntoComponent
  ],
  entryComponents: [
    AsignarSeguroComponent,
    ColumnasComponent,
    DistribuidoresComponent,
    AgentesComponent,
    SubAgentesComponent,
    TipoCambioComponent,
    AgregarPagoComponent,
    ConfirmarComponent,
    RecuperarContrasenaComponent,
    AgregarIncidenteComponent,
    AgregarFacturaIncidenteComponent,
    AgregarComentarioIncidenteComponent,
    ComisionAjustesComponent,
    SubirArchivoComponent,
    ConciliarAjustesComponent,
    CorreoCuerpoComponent,
    CotizacionConfirmacionComponent,
    CodigosPromocionComponent,
    ExcelAsignarComponent,
    AgregarPagoConjuntoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRutasModule,
    MatInputModule,
    MatButtonModule,
    MatListModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatDividerModule,
    MatSelectModule,
    MatTableModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatTabsModule,
    MatExpansionModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    MatStepperModule,
    PdfViewerModule
  ],
  providers: [
    ExcelService,
    { provide: MatPaginatorIntl, useValue: getSpnPaginatorIntl() }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
