import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagosPrincipalComponent } from './pagos-principal.component';

describe('PagosPrincipalComponent', () => {
  let component: PagosPrincipalComponent;
  let fixture: ComponentFixture<PagosPrincipalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagosPrincipalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagosPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
