import {SelectionModel} from '@angular/cdk/collections';
import { Component, OnInit, Inject } from '@angular/core';
import {MatTableDataSource, PageEvent, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { HTTPService }    from '../../http.service';
import {SesionService} from '../../sesion.service'
import {VentanasService} from '../../servicios/ventanas.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';

@Component({
  selector: 'app-comisiones',
  templateUrl: './comisiones.component.html',
  styleUrls: ['./comisiones.component.css']
})
export class ComisionesComponent implements OnInit {

  constructor(
  	private HTTP: HTTPService,
    public sesion: SesionService,
    public ventana: VentanasService,
    public Formulario: DatosFormularioService,
  	) { }

    ngOnInit(){

    }

}
