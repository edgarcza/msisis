import { Component, OnInit } from '@angular/core';
import * as Tabla from '../../../tablas'
import {Tablas} from '../../../clases/tablas'

@Component({
  selector: 'app-comision-pagado',
  templateUrl: './pagado.component.html',
  styleUrls: ['./pagado.component.css']
})
export class PagadoComponent extends Tablas implements OnInit {

	constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }

    ngOnInit(){

      this.agregarColumnas(
        // [
        //   {id: 'fecha_asegurado',    	  nombre: 'Fecha de venta',      		ocultar: false,    tabla: 'cotizacion.fecha_asegurado'},
        //   {id: 'subagente',   					nombre: 'Sub Agente',             ocultar: false,    tabla: 'subagente.nombre_completo'},
        //   {id: 'agente',               	nombre: 'Agente',                 ocultar: false,    tabla: 'agente.agente'}, 
        //   {id: 'distribuidor',         	nombre: 'Distribuidor',           ocultar: false,    tabla: 'distribuidor.distribuidor'}, 
        //   {id: 'tipo_distribuidor',    	nombre: 'Tipo de distribuidor',   ocultar: false,    tabla: 'tipo_distribuidor.tipo_distribuidor'}, 
        //   {id: 'cliente',            		nombre: 'Cliente',    						ocultar: false,    tabla: 'cliente.nombre_completo'},
        //   {id: 'precioMXN',    					nombre: 'Precio (MXN)',      		  ocultar: false,    tabla: 'cotizacion.precioMXN'},
        //   {id: 'comision',    					nombre: 'Comisión',      				  ocultar: false,    tabla: 'comision.precioMXN'},
        //   {id: 'impuestos',    					nombre: 'Impuestos',      				ocultar: false,    tabla: 'comision.impuestos'},
        //   {id: 'comision_final',    	  nombre: 'Comisión final',      	  ocultar: false,    tabla: 'comision.precioMXN'},
        //   // {id: 'factura',    					  nombre: 'Factura',      				  ocultar: false,    tabla: 'cotizacion.precioMXN'},
        // ]
        [
          {id: 'fecha_asegurado',    	  nombre: 'Fecha de registro',      		ocultar: true,    tabla: 'cotizacion.fecha_asegurado'},
          {id: 'estado_t',    	        nombre: 'Transacción',      		            ocultar: false,    tabla: 'cotizacion.estado_t'},
          {id: 'nombres',            		nombre: 'Nombre',    						  ocultar: false,    tabla: 'cliente.nombres'},
          {id: 'apellidos',             nombre: 'Apellidos',    				  ocultar: false,    tabla: 'cliente.apellidos'},
          {id: 'fecha_nacimiento',      nombre: 'Fecha de nacimiento',    ocultar: false,    tabla: 'cliente.fecha_nacimiento'},
          {id: 'pais_origen',           nombre: 'País de origen',    		  ocultar: false,    tabla: 'pais_origen.pais'},
          {id: 'pais_origen_ab',        nombre: 'AB',    				  ocultar: false,    tabla: 'pais_origen.abreviacion'},
          {id: 'salida',            		nombre: 'Salida',    				  ocultar: false,    tabla: 'cotizacion.salida'},
          {id: 'pais_destino',          nombre: 'Destino',    				  ocultar: false,    tabla: 'pais_destino.pais'},
          {id: 'pais_destino_ab',       nombre: 'AB',    				  ocultar: false,    tabla: 'pais_destino.abreviacion'},
          {id: 'regreso',            		nombre: 'Regreso',    				  ocultar: false,    tabla: 'cotizacion.regreso'},
          {id: 'dsm',            		    nombre: 'Días',    				  ocultar: false,    tabla: 'tipo_poliza.dsm'},
          {id: 'precioUSD',             nombre: 'Precio USD',    				  ocultar: false,    tabla: 'cotizacion.precioUSD'},
          {id: 'precioUSD_descuentoSA',   nombre: 'USD descuento',    		  ocultar: false,    tabla: 'cotizacion.precioUSD_descuentoSA'},
          {id: 'precioUSD_comision',    nombre: 'USD comisión',    			  ocultar: false,    tabla: 'cotizacion.precioUSD_comision'},
          {id: 'precioUSD_total',       nombre: 'USD total',    				  ocultar: false,    tabla: 'cotizacion.precioUSD_total'},
          {id: 'precioMXN',             nombre: 'Precio MXN',    				  ocultar: false,    tabla: 'cotizacion.precioMXN'},
          {id: 'precioMXN_descuentoSA',   nombre: 'MXN descuento',    		  ocultar: false,    tabla: 'cotizacion.precioMXN_descuentoSA'},
          {id: 'precioMXN_comision',    nombre: 'MXN comisión',    			  ocultar: false,    tabla: 'cotizacion.precioMXN_comision'},
          {id: 'precioMXN_total',       nombre: 'MXN total',    				  ocultar: false,    tabla: 'cotizacion.precioMXN_total'},
          {id: 'subagente',   					nombre: 'Sub Agente',             ocultar: true,    tabla: 'subagente.nombre_completo'},
          {id: 'agente',               	nombre: 'Agente',                 ocultar: false,    tabla: 'agente.agente'}, 
          {id: 'distribuidor',         	nombre: 'Distribuidor',           ocultar: true,    tabla: 'distribuidor.distribuidor'}, 
          {id: 'tipo_distribuidor',    	nombre: 'Tipo de distribuidor',   ocultar: true,    tabla: 'tipo_distribuidor.tipo_distribuidor'}, 
          // {id: 'precioMXN',    					nombre: 'Precio (MXN)',      		  ocultar: false,    tabla: 'cotizacion.precioMXN'},
          // {id: 'comision',    					nombre: 'Comisión',      				  ocultar: false,    tabla: 'comision.precioMXN'},
          // {id: 'impuestos',    					nombre: 'Impuestos',      				ocultar: false,    tabla: 'comision.impuestos'},
          // {id: 'comision_final',    	  nombre: 'Comisión final',      	  ocultar: false,    tabla: 'comision.precioMXN'},
          // {id: 'factura',    					  nombre: 'Factura',      				  ocultar: false,    tabla: 'cotizacion.precioMXN'},
        ]
      );
  
      this.agregarColIds(
        // ['select', 'fecha_asegurado', 'subagente', 'cliente', 'subagente', 
        // 'agente', 'distribuidor', 'tipo_distribuidor', 'cliente', 'precioMXN', 'comision', 'impuestos', 'comision_final', 'factura', 'acciones']
        ['select', 
        'fecha_asegurado',
        'estado_t',
        'nombres',
        'apellidos',
        'fecha_nacimiento',
        'pais_origen',
        'pais_origen_ab',
        'salida',
        'pais_destino',
        'pais_destino_ab',
        'regreso',
        'dsm',
        'precioUSD',
        'precioUSD_descuentoSA',
        'precioUSD_comision',
        'precioUSD_total',
        'precioMXN',
        'precioMXN_descuentoSA',
        'precioMXN_comision',
        'precioMXN_total',
        'subagente', 
        'agente', 
        'distribuidor', 
        'tipo_distribuidor',
        'acciones']
      );
      this.tablaConfig("pagADMComisionesPagado", "Comisiones-Pagadas", "comision", "comision");

      this.tablaTerminar = (datos) => {
        if(this.ExcelIngles) return datos;
        // var Datos = this.dataSource.data;
        var Datos = datos;
        var Sumas = [], Descs = [];
        for(var i in Datos){
          for(var j in Datos[i]){
            if(!Sumas[j]) Sumas[j] = 0;
            if(!isNaN(Datos[i][j])){
              Sumas[j] += this._Formulario.redondear(parseFloat(Datos[i][j]), 2);
            }
            else{
              Sumas[j] = "- - - -";
            }
          }
        }
        for(var i in Sumas){
          if(!isNaN(Sumas[i])){
            Descs[i] = "Total: ";
            Sumas[i] = "$" + this._Formulario.redondear(Sumas[i], 2);
          }
          else{
            Descs[i] = " ";
          }
        }
        Datos.push(Descs);
        Datos.push(Sumas);
        if(!this.Excel && !this.ExcelIngles)
          this.dataSource.data = Datos;
        return Datos;
      }

  
      this.tabla();
  
    }

  ajustesComision(datos){
    this._Ventana.ajustesComision({DatosComision: datos}, () => {
      this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize);
    });
  }

  subirFactura(datos){
    this._Ventana.subirArchivo({Datos: datos, Carpeta: 'FacturasComisiones'}, 
    () => {
      this._HTTP.datosDe("comision", [{campo: "id", valor: datos.id}]).subscribe(
        (resultado) => {
          let Comision = resultado[0];
          Comision.factura = 1;
          this.Formulario.guardar("comision", Comision, null, () => {this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize)});
        },

        (error) => {
          console.log(error);
        }
      )
    },
    () => {

    })
  }

}
