import { Component, OnInit } from '@angular/core';
import * as Tabla from '../../tablas'
import {Tablas} from '../../clases/tablas'

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.css']
})
export class PagosComponent extends Tablas implements OnInit {

  constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }

  ngOnInit(){

    this.agregarColumnas(
      [
        {id: 'no_recibo',    					nombre: 'No. recibo',      				ocultar: false, tabla: 'pago.no_recibo'},
        {id: 'fecha_pago',            nombre: 'Fecha de pago',         	ocultar: false, tabla: 'pago.fecha_pago'},
        {id: 'cliente',            		nombre: 'Cliente',    						ocultar: false, tabla: 'cliente.nombre_completo'},
        {id: 'cuenta',               	nombre: 'Cuenta',        					ocultar: false, tabla: 'cuenta.cuenta'}, 
        {id: 'forma_pago',           	nombre: 'Forma de pago',         	ocultar: false, tabla: 'forma_pago.forma_pago'}, 
        {id: 'cantidad_abonada_MXN',	nombre: 'Monto (MXN)',           	ocultar: false, tabla: 'pago.cantidad_abonada_MXN'}, 
        {id: 'cantidad_abonada_USD',	nombre: 'Monto (USD)',           	ocultar: false, tabla: 'pago.cantidad_abonada_USD'}, 
        {id: 'tipo_cambio',          	nombre: 'TC',         						ocultar: false, tabla: 'pago.tipo_cambio'}, 
        {id: 'saldo_pendiente_MXN',   nombre: 'Saldo pendiente (MXN)',  ocultar: false, tabla: 'cotizacion.saldo_pendiente_MXN'}, 
        {id: 'tipo_distribuidor',    	nombre: 'Tipo de distribuidor',   ocultar: false, tabla: 'tipo_distribuidor.tipo_distribuidor'}, 
        {id: 'distribuidor',         	nombre: 'Distribuidor',           ocultar: false, tabla: 'distribuidor.distribuidor'}, 
        {id: 'agente',               	nombre: 'Agente',                 ocultar: false, tabla: 'agente.agente'}, 
        {id: 'subagente',   					nombre: 'Sub Agente',             ocultar: false, tabla: 'sub_agente.nombre_completo'},
      ]
    );

    this.agregarColIds(
      ['select', 'no_recibo', 'fecha_pago', 'cliente', 
        'cuenta', 'forma_pago', 'cantidad_abonada_MXN', 'cantidad_abonada_USD', 'tipo_cambio', 'saldo_pendiente_MXN', 'tipo_distribuidor', 'distribuidor', 
        'agente', 'subagente', 'archivo', 'acciones']
    );

    this.tablaConfig("pagADMPagos", "Pagos", "pago", "pago", [{Campo: "pago.estado", Valor: "0", Operador: "="}]);

    this.tabla();

  }

  acreditar(pago){
    let ls = this.selection.selected.length;
    let seleccion = this.selection.selected;
    let texto = "¿Acreditar pago?";
    if(ls > 1)
      texto = "¿Acreditar pagos seleccionados?";
    if(ls == 0){
      // console.log(pago);
      // seleccion.concat(pago);
      seleccion[0] = (pago);
    }
    console.log(seleccion);
    this._Ventana.Confirmar({Mensaje: texto}, 
    () => {
      for(var c = 0; c < ls; c++)
      {
        let DatosPago = {
          id: seleccion[c].id,
          estado: 1,
        };
        this._Formulario.guardar("pago", DatosPago);
      }
      this.tabla();
      window.location.reload();
    }, 
    () => {

    })
  }

  eliminar(id){
    this._Ventana.Confirmar({Mensaje: "¿Eliminar pago?"}, 
    () => {
      let DatosPago = {
        id: id
      };
      this._Formulario.borrar("pago", DatosPago);
      this.tabla();
    }, 
    () => {

    })
  }

}
