import { Component, OnInit } from '@angular/core';
import * as Tabla from '../../../tablas'
import {Tablas} from '../../../clases/tablas'

@Component({
  selector: 'app-acreditados',
  templateUrl: './acreditados.component.html',
  styleUrls: ['./acreditados.component.css']
})
export class AcreditadosComponent extends Tablas implements OnInit {

  constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }

  ngOnInit(){

    this.agregarColumnas(
      [
        {id: 'no_recibo',    					nombre: 'No. recibo',      				ocultar: false, tabla: 'pago.no_recibo'},
        {id: 'fecha_pago',            nombre: 'Fecha de pago',         	ocultar: false, tabla: 'pago.fecha_pago'},
        {id: 'cliente',            		nombre: 'Cliente',    						ocultar: false, tabla: 'cliente.nombre_completo'},
        {id: 'cuenta',               	nombre: 'Cuenta',        					ocultar: false, tabla: 'cuenta.cuenta'}, 
        {id: 'forma_pago',           	nombre: 'Forma de pago',         	ocultar: false, tabla: 'forma_pago.forma_pago'}, 
        {id: 'cantidad_abonada_MXN',	nombre: 'Monto (MXN)',           	ocultar: false, tabla: 'pago.cantidad_abonada_MXN'}, 
        {id: 'cantidad_abonada_USD',	nombre: 'Monto (USD)',           	ocultar: false, tabla: 'pago.cantidad_abonada_USD'}, 
        {id: 'tipo_cambio',          	nombre: 'TC',         						ocultar: false, tabla: 'pago.tipo_cambio'}, 
        {id: 'saldo_pendiente_MXN',   nombre: 'Saldo pendiente (MXN)',  ocultar: false, tabla: 'cotizacion.saldo_pendiente_MXN'}, 
        {id: 'tipo_distribuidor',    	nombre: 'Tipo de distribuidor',   ocultar: false, tabla: 'tipo_distribuidor.tipo_distribuidor'}, 
        {id: 'distribuidor',         	nombre: 'Distribuidor',           ocultar: false, tabla: 'distribuidor.distribuidor'}, 
        {id: 'agente',               	nombre: 'Agente',                 ocultar: false, tabla: 'agente.agente'}, 
        {id: 'subagente',   					nombre: 'Sub Agente',             ocultar: false, tabla: 'sub_agente.nombre_completo'},
      ]
    );

    this.agregarColIds(
      ['select', 'no_recibo', 'fecha_pago', 'cliente', 
        'cuenta', 'forma_pago', 'cantidad_abonada_MXN', 'cantidad_abonada_USD', 'tipo_cambio', 'saldo_pendiente_MXN', 'tipo_distribuidor', 'distribuidor', 
        'agente', 'subagente', 'archivo', 'acciones']
    );

    this.tablaConfig("pagADMPagos", "Pagos", "pago", "pago", [{Campo: "pago.estado", Valor: "1", Operador: "="}]);

    this.tabla();

  }

}
