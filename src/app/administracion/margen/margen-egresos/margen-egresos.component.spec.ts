import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MargenEgresosComponent } from './margen-egresos.component';

describe('MargenEgresosComponent', () => {
  let component: MargenEgresosComponent;
  let fixture: ComponentFixture<MargenEgresosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MargenEgresosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MargenEgresosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
