import { Component, OnInit } from '@angular/core';
import * as Tabla from '../../../tablas'
import {Tablas} from '../../../clases/tablas'

@Component({
  selector: 'app-margen-egresos',
  templateUrl: './margen-egresos.component.html',
  styleUrls: ['./margen-egresos.component.css']
})
export class MargenEgresosComponent extends Tablas implements OnInit {


	constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }

    ngOnInit(){

      this.agregarColumnas(
        [
          {id: 'fecha_asegurado',    	  nombre: 'Fecha de venta',      		ocultar: false,    tabla: 'cotizacion.fecha_asegurado'},
          {id: 'subagente',   					nombre: 'Sub Agente',             ocultar: false,    tabla: 'subagente.nombre_completo'},
          {id: 'agente',               	nombre: 'Agente',                 ocultar: false,    tabla: 'agente.agente'}, 
          {id: 'distribuidor',         	nombre: 'Distribuidor',           ocultar: false,    tabla: 'distribuidor.distribuidor'}, 
          {id: 'tipo_distribuidor',    	nombre: 'Tipo de distribuidor',   ocultar: false,    tabla: 'tipo_distribuidor.tipo_distribuidor'}, 
          {id: 'cliente',            		nombre: 'Cliente',    						ocultar: false,    tabla: 'cliente.nombre_completo'},
          {id: 'precioMXN',    					nombre: 'Precio (MXN)',      		  ocultar: false,    tabla: 'cotizacion.precioMXN'},
          {id: 'comision',    					nombre: 'Comisión',      				  ocultar: false,    tabla: 'comision.precioMXN'},
          {id: 'impuestos',    					nombre: 'Impuestos',      				ocultar: false,    tabla: 'comision.impuestos'},
          {id: 'comision_final',    	  nombre: 'Comisión final',      	  ocultar: false,    tabla: 'comision.precioMXN'},
          // {id: 'factura',    					  nombre: 'Factura',      				  ocultar: false,    tabla: 'cotizacion.precioMXN'},
        ]
      );
  
      this.agregarColIds(
        ['select', 'fecha_asegurado', 'subagente', 'cliente', 'subagente', 
        'agente', 'distribuidor', 'tipo_distribuidor', 'cliente', 'precioMXN', 'comision', 'impuestos', 'comision_final', 'factura']
      );
      this.tablaConfig("pagADMComisionesPagado", "Comisiones-Pagadas", "comision", "comision");

      this.tablaTerminar = (datos) => {
        // var Datos = this.dataSource.data;
        var Datos = datos;
        var Sumas = [], Descs = [];
        for(var i in Datos){
          for(var j in Datos[i]){
            if(!Sumas[j]) Sumas[j] = 0;
            if(!isNaN(Datos[i][j])){
              Sumas[j] += this._Formulario.redondear(parseFloat(Datos[i][j]), 2);
            }
            else{
              Sumas[j] = "- - - -";
            }
          }
        }
        for(var i in Sumas){
          if(!isNaN(Sumas[i])){
            Descs[i] = "Total: ";
            Sumas[i] = "$" + Sumas[i];
          }
          else{
            Descs[i] = " ";
          }
        }
        Datos.push(Descs);
        Datos.push(Sumas);
        if(!this.Excel)
          this.dataSource.data = Datos;
        return Datos;
      }
  
      this.tabla();
  
    }

  ajustesComision(datos){
    this._Ventana.ajustesComision({DatosComision: datos}, () => {
      this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize);
    });
  }

  subirFactura(datos){
    this._Ventana.subirArchivo({Datos: datos, Carpeta: 'FacturasComisiones'}, 
    () => {
      this._HTTP.datosDe("comision", [{campo: "id", valor: datos.id}]).subscribe(
        (resultado) => {
          let Comision = resultado[0];
          Comision.factura = 1;
          this.Formulario.guardar("comision", Comision, null, () => {this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize)});
        },

        (error) => {
          console.log(error);
        }
      )
    },
    () => {

    })
  }

}
