import { Component, OnInit } from '@angular/core';
import * as Tabla from '../../tablas'
import {Tablas} from '../../clases/tablas'

@Component({
  selector: 'app-margen',
  templateUrl: './margen.component.html',
  styleUrls: ['./margen.component.css']
})
export class MargenComponent extends Tablas implements OnInit {

	constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }

  // public FiltrosAgregar = false;

  ngOnInit(){

    this.agregarColumnas(
      [
        {id: 'fecha_asegurado',    	  nombre: 'Fecha de registro',      ocultar: true,    tabla: 'cotizacion.fecha_asegurado'},
        {id: 'id_distribuidores',    	nombre: 'Código',              	  ocultar: true,     tabla: 'distribuidor.id_distribuidores'},
        {id: 'codigo',    	          nombre: 'Póliza código',      	  ocultar: true,     tabla: 'tipo_poliza.a_codigo'},
        {id: 'nombres',            		nombre: 'Nombres',    						ocultar: false,    tabla: 'cliente.nombres'},
        {id: 'apellidos',             nombre: 'Apellidos',    				  ocultar: false,    tabla: 'cliente.apellidos'},
        {id: 'fecha_nacimiento',      nombre: 'Fecha de nacimiento',    ocultar: false,    tabla: 'cliente.fecha_nacimiento'},
        {id: 'sexo',                  nombre: 'Sexo',    				        ocultar: false,    tabla: 'cliente.sexo'},
        // {id: 'correo_electronico',    nombre: 'Coreo electrónico',      ocultar: false,    tabla: 'cliente.correo_electronico'},
        {id: 'correo_electronico',    nombre: 'Coreo electrónico',      ocultar: false,    tabla: 'subagente.correo_electronico'},
        {id: 'salida',    		  			nombre: 'Salida',      		        ocultar: false,    tabla: 'cotizacion.salida'},
        {id: 'regreso',    		  			nombre: 'Regreso',      		      ocultar: false,    tabla: 'cotizacion.regreso'},
        {id: 'pais_origen',    		    nombre: 'Origen',      		        ocultar: false,    tabla: 'cotizacion.pais_origen'},
        {id: 'cp',          		      nombre: 'CP Origen',    				  ocultar: false,    tabla: 'cliente.cp'},
        {id: 'pais_destino',    		  nombre: 'País destino',  		      ocultar: false,    tabla: 'cotizacion.pais_destino'},
        {id: 'destino_ciudad',        nombre: 'Ciudad Destino',      	  ocultar: false,    tabla: 'cotizacion.destino_ciudad'},
        {id: 'destino_cp',      			nombre: 'CP Destino',      		    ocultar: false,    tabla: 'cotizacion.destino_cp'},
        {id: 'zona',      			      nombre: 'Zona',      		    ocultar: false,    tabla: 'pais_destino.zona'},
        {id: 'dias',    		  			  nombre: 'DMS',      		          ocultar: false,    tabla: 'cobertura.dias'},
        {id: 'subagente',    	        nombre: 'Subagente',      	      ocultar: false,     tabla: 'subagente.nombre_completo'},
        {id: 'agente',    	          nombre: 'Agente',      	          ocultar: false,     tabla: 'agente.agente'},
        {id: 'distribuidor',    	    nombre: 'Distribuidor',      	    ocultar: false,     tabla: 'distribuidor.distribuidor'},
        {id: 'fuente_referencia',     nombre: 'Fuente de referencia',   ocultar: true,     tabla: 'fuente_referencia.fuente_referencia'},
        {id: 'tt',    		  			    nombre: 'Transacción',            ocultar: false,    tabla: 'tt'},
        // {id: 'cliente',            		nombre: 'Cliente',    						ocultar: false,    tabla: 'cliente.nombre_completo'},
        // {id: 'fecha_asegurado',    	  nombre: 'Fecha de registro',      ocultar: false,    tabla: 'cotizacion.fecha_asegurado'},
        // {id: 'apellidos',             nombre: 'Apellidos',    				  ocultar: false,    tabla: 'cliente.apellidos'},
        // {id: 'fecha_asegurado',    	  nombre: 'Fecha de registro',      ocultar: false,    tabla: 'cotizacion.fecha_asegurado'},
        // {id: 'precioUSD',    					nombre: 'Precio USD',      		    ocultar: false,    tabla: 'cotizacion.precioUSD'},
        // {id: 'precioUSD_descuento',   nombre: 'Descuento USD',      	  ocultar: false,    tabla: 'cotizacion.precioUSD_descuento'},
        // {id: 'precioUSD_ajuste',      nombre: 'Ajuste USD',      		    ocultar: false,    tabla: 'cotizacion.precioUSD_ajuste'},
        // {id: 'precioUSD_iva',    		  nombre: 'IVA USD',      		      ocultar: false,    tabla: 'cotizacion.precioUSD_iva'},
        // {id: 'precioUSD_subtotal',    nombre: 'Subtotal USD',      		  ocultar: false,    tabla: 'cotizacion.precioUSD_subtotal'},
        // {id: 'precioUSD_comision',    nombre: 'Comision USD',      		  ocultar: false,    tabla: 'cotizacion.precioUSD_comision'},
        // {id: 'precioUSD_total',    	  nombre: 'Total USD',      		    ocultar: false,    tabla: 'cotizacion.precioUSD_total'},
        // {id: 'precioMXN_total',    	  nombre: 'Total MXN',      		    ocultar: false,    tabla: 'cotizacion.precioMXN_total'},
        // {id: 'costo',    					    nombre: 'Costo por día',          ocultar: false,    tabla: 'cobertura.costo'},
        // {id: 'dias',    					    nombre: 'Número de días',      	  ocultar: false,    tabla: 'cobertura.dias'},
        // {id: 'costo_final',    			  nombre: 'Costo total',      		  ocultar: false,    tabla: 'cobertura.costo_final'},
        // {id: 'margen_usd',    	      nombre: 'Margen USD',      	      ocultar: false,    tabla: 'margen_usd'},
        // {id: 'subagente',    	        nombre: 'Subagente',      	      ocultar: true,     tabla: 'subagente.nombre_completo'},
        // {id: 'agente',    	          nombre: 'Agente',      	          ocultar: true,     tabla: 'agente.agente'},
        // {id: 'distribuidor',    	    nombre: 'Distribuidor',      	    ocultar: true,     tabla: 'distribuidor.distribuidor'},
        // {id: 'id_distribuidores',    	nombre: 'Distribuidores',      	  ocultar: true,     tabla: 'distribuidor.id_distribuidores'},
        // {id: 'tipo_distribuidor',     nombre: 'Tipo distribuidor',      ocultar: true,     tabla: 'tipo_distribuidor.tipo_distribuidor'},
        // {id: 'tipo_seguro',    	      nombre: 'Tipo seguro',      	    ocultar: true,     tabla: 'tipo_seguro.tipo_seguro'},
        // {id: 'tipo_poliza',    	      nombre: 'Tipo poliza',      	    ocultar: true,     tabla: 'tipo_poliza.tipo_poliza'},
        // {id: 'codigo',    	          nombre: 'Código',      	          ocultar: true,     tabla: 'tipo_poliza.a_codigo'},
        // {id: 'factura',    					  nombre: 'Factura',      				  ocultar: false,    tabla: 'comision.factura'},
      ]
    );

    this.agregarColIds(
      // ['select', 'fecha_asegurado', 'nombres', 'apellidos', 'precioUSD', 'precioUSD_descuento', 'precioUSD_ajuste', 'precioUSD_iva', 'precioUSD_subtotal', 'precioUSD_comision',
      // 'precioUSD_total', 'precioMXN_total', 'costo', 'dias', 'costo_final', 'margen_usd', 'subagente', 'agente', 'distribuidor', 'id_distribuidores', 'tipo_distribuidor',
      // 'tipo_seguro', 'tipo_poliza', 'codigo']
      ['select', 'fecha_asegurado', 'id_distribuidores', 'codigo', 'nombres', 'apellidos', 'fecha_nacimiento', 'sexo', 'correo_electronico', 'salida', 'regreso',
      'pais_origen', 'cp', 'pais_destino', 'destino_ciudad', 'destino_cp', 'dias', 'zona', 'subagente', 'agente', 'distribuidor', 'fuente_referencia', 'tt']
    );

    // this.tablaConfig("pagADMComisionesPendiente", "ComisionesPendientes", "comisión", "comision", {Campo: "subagente.id", Valor: this.sesion.id()});
    // this.tablaConfig("pagMargen", "ComisionesPendientes", "comisión", "comision", {Campo: "subagente.id", Valor: this.sesion.id()});
    this.tablaConfig("pagMargen", "Margen", "margen", "margen");//, {Campo: "subagente.id", Valor: this.sesion.id()});
    // this.tablaConfig("pagMargen", "Margen", "margen", "margen", [{Campo: "cliente.nombre_completo", Valor: "Edgar"}, {Campo: "precioUSD", Valor: "3"}]);

    this.tablaTerminar = (datos) => {
      if(this.ExcelIngles) return datos;
      // var Datos = this.dataSource.data;
      var Datos = datos;
      var Sumas = [], Descs = [];
      for(var i in Datos){
        for(var j in Datos[i]){
          if(!Sumas[j]) Sumas[j] = 0;
          if(!isNaN(Datos[i][j])){
            Sumas[j] += this._Formulario.redondear(parseFloat(Datos[i][j]), 2);
          }
          else{
            Sumas[j] = "- - - -";
          }
        }
      }
      for(var i in Sumas){
        if(!isNaN(Sumas[i])){
          Descs[i] = "Total: ";
          Sumas[i] = this._Formulario.redondear(Sumas[i], 2);
          Sumas[i] = "$" + Sumas[i];
          console.log(Sumas[i]);
        }
        else{
          Descs[i] = " ";
        }
      }
      Datos.push(Descs);
      Datos.push(Sumas);
      if(!this.Excel && !this.ExcelIngles)
        this.dataSource.data = Datos;
      return Datos;
    }

    this.tabla();

  }
}
