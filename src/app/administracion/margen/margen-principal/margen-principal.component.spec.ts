import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MargenPrincipalComponent } from './margen-principal.component';

describe('MargenPrincipalComponent', () => {
  let component: MargenPrincipalComponent;
  let fixture: ComponentFixture<MargenPrincipalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MargenPrincipalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MargenPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
