import { Component, OnInit } from '@angular/core';
import * as Tabla from '../../../tablas'
import {Tablas} from '../../../clases/tablas'

@Component({
  selector: 'app-margen-ingresos',
  templateUrl: './margen-ingresos.component.html',
  styleUrls: ['./margen-ingresos.component.css']
})
export class MargenIngresosComponent extends Tablas implements OnInit {

  constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }

  ngOnInit(){

    this.agregarColumnas(
      [
        {id: 'no_recibo',    					nombre: 'No. recibo',      				ocultar: false, tabla: 'pago.no_recibo'},
        {id: 'fecha_pago',            nombre: 'Fecha de pago',         	ocultar: false, tabla: 'pago.fecha_pago'},
        {id: 'cliente',            		nombre: 'Cliente',    						ocultar: false, tabla: 'cliente.nombre_completo'},
        {id: 'cuenta',               	nombre: 'Cuenta',        					ocultar: false, tabla: 'cuenta.cuenta'}, 
        {id: 'forma_pago',           	nombre: 'Forma de pago',         	ocultar: false, tabla: 'forma_pago.forma_pago'}, 
        {id: 'cantidad_abonada_MXN',	nombre: 'Monto (MXN)',           	ocultar: false, tabla: 'pago.cantidad_abonada_MXN'}, 
        {id: 'tipo_cambio',          	nombre: 'TC',         						ocultar: false, tabla: 'pago.tipo_cambio'}, 
        {id: 'saldo_pendiente_MXN',   nombre: 'Saldo pendiente (MXN)',  ocultar: false, tabla: 'cotizacion.saldo_pendiente_MXN'}, 
        {id: 'tipo_distribuidor',    	nombre: 'Tipo de distribuidor',   ocultar: false, tabla: 'tipo_distribuidor.tipo_distribuidor'}, 
        {id: 'distribuidor',         	nombre: 'Distribuidor',           ocultar: false, tabla: 'distribuidor.distribuidor'}, 
        {id: 'agente',               	nombre: 'Agente',                 ocultar: false, tabla: 'agente.agente'}, 
        {id: 'subagente',   					nombre: 'Sub Agente',             ocultar: false, tabla: 'sub_agente.nombre_completo'},
      ]
    );

    this.agregarColIds(
      ['select', 'no_recibo', 'fecha_pago', 'cliente', 
        'cuenta', 'forma_pago', 'cantidad_abonada_MXN', 'tipo_cambio', 'saldo_pendiente_MXN', 'tipo_distribuidor', 'distribuidor', 
        'agente', 'subagente']
    );

    // this.tablaConfig("pagADMPagos", "Pagos", "pago", "pago", [{Campo: "pago.estado", Valor: "1", Operador: "="}]);
    // this.tablaConfig("pagADMPagos", "Pagos", "pago", "pago", [{Campo: "pago.estado", Valor: "0", Operador: "="}]);
    this.tablaConfig("pagADMPagos", "Pagos", "pago", "pago");

    this.tablaTerminar = (datos) => {
      // var Datos = this.dataSource.data;
      var Datos = datos;
      var Sumas = [], Descs = [];
      for(var i in Datos){
        for(var j in Datos[i]){
          if(!Sumas[j]) Sumas[j] = 0;
          if(!isNaN(Datos[i][j])){
            Sumas[j] += this._Formulario.redondear(parseFloat(Datos[i][j]), 2);
          }
          else{
            Sumas[j] = "- - - -";
          }
        }
      }
      for(var i in Sumas){
        if(!isNaN(Sumas[i])){
          Descs[i] = "Total: ";
          Sumas[i] = "$" + Sumas[i];
        }
        else{
          Descs[i] = " ";
        }
      }
      Datos.push(Descs);
      Datos.push(Sumas);
      if(!this.Excel)
        this.dataSource.data = Datos;
      return Datos;
    }

    this.tabla();

  }

}
