import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MargenIngresosComponent } from './margen-ingresos.component';

describe('MargenIngresosComponent', () => {
  let component: MargenIngresosComponent;
  let fixture: ComponentFixture<MargenIngresosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MargenIngresosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MargenIngresosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
