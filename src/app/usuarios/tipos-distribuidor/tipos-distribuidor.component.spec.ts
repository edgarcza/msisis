import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiposDistribuidorComponent } from './tipos-distribuidor.component';

describe('TiposDistribuidorComponent', () => {
  let component: TiposDistribuidorComponent;
  let fixture: ComponentFixture<TiposDistribuidorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiposDistribuidorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiposDistribuidorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
