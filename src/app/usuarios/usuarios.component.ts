import { Component, OnInit } from '@angular/core';
import * as Tabla from '../tablas'
import {Tablas} from '../clases/tablas'

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent extends Tablas implements OnInit {

	constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }

  ngOnInit(){

    this.agregarColumnas([
      {id: 'nombre_completo',     nombre: 'Sub Agente',             ocultar: false, tabla: 'subagente.nombre_completo'},
      {id: 'agente',              nombre: 'Agente',                 ocultar: false, tabla: 'agente.agente'},
      {id: 'distribuidor',        nombre: 'Distribuidor',           ocultar: false, tabla: 'distribuidor.distribuidor'}, 
      {id: 'tipo_distribuidor',   nombre: 'Tipo de distribuidor',   ocultar: false, tabla: 'tipo_distribuidor.tipo_distribuidor'}, 
      {id: 'rol',                 nombre: 'Rol',                    ocultar: false, tabla: 'rol.rol'}, 
      {id: 'correo_electronico',  nombre: 'Correo electrónico',     ocultar: false, tabla: 'subagente.correo_electronico'}, 
    ]);
    
    this.agregarColIds(
      ['select', 'nombre_completo', 'agente', 'distribuidor', 'tipo_distribuidor', 'rol', 'correo_electronico', 'activo_inactivo', 'acciones']
    );
  
    this.tablaConfig("pagUsuarios", "Usuarios", "usuarios", "usuarios");
    
  
    this.tabla();	

  }

  // EDITAR SUBAGENTE
  editarSubagente(datos){
    this.Ventana.subAgentes(datos, () => {this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize);});
  }


  // DESACTIVAR USUARIO
  desactivarUsuario(datosUsuario){
    console.log(datosUsuario);
    this.Ventana.Confirmar({Mensaje: "¿Desactivar usuario?"}, 
      () => {
        this.activarDesactivarUsuario(datosUsuario.subagente_id, 0);
      }, 
      () => { // console.log("NO");
       });
  }

  // ACTIVAR USUARIO
  activarUsuario(datosUsuario){
    console.log(datosUsuario);
    this.Ventana.Confirmar({Mensaje: "¿Activar usuario?"}, 
      () => {
        this.activarDesactivarUsuario(datosUsuario.subagente_id, 1);
      }, 
      () => { // console.log("NO");
       });
  }

  activarDesactivarUsuario(subagenteID, activo_inactivo){
        console.log(subagenteID, activo_inactivo);
    this._HTTP.datosDe('subagente', [{campo: "id", valor: subagenteID}])
    .subscribe(
      datos => {
        console.log(datos);
        let DatosSubAgente = datos[0];
        DatosSubAgente.activo_inactivo = activo_inactivo;

        this.Formulario.guardar("subagente", DatosSubAgente, null, () => {
          console.log("Actualizado");
          this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize);
        });

      },

      error => {
        console.log(error);
      }
    );
  }

}
