import { Component, OnInit } from '@angular/core';
import * as Tabla from '../../tablas'
import {Tablas} from '../../clases/tablas'

@Component({
  selector: 'app-agentes-usuarios',
  templateUrl: './agentes.component.html',
  styleUrls: ['./agentes.component.css']
})
export class AgentesUsuariosComponent extends Tablas implements OnInit {

  constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }

  ngOnInit() {
    this.agregarColumnas(
      [
        {id: 'agente',    	  nombre: 'Agente',      		ocultar: false,    tabla: 'agente.agente'},
        {id: 'distribuidor',   					nombre: 'Distribuidor',             ocultar: false,    tabla: 'distribuidor.distribuidor'},
        {id: 'tipo_distribuidor',               	nombre: 'Tipo distribuidor',                 ocultar: false,    tabla: 'tipo_distribuidor.tipo_distribuidor'}, 
        {id: 'correo_electronico',         	nombre: 'Correo electrónico',           ocultar: false,    tabla: 'agente.correo_electronico'}, 
        // {id: 'factura',    					  nombre: 'Factura',      				  ocultar: false,    tabla: 'cotizacion.precioMXN'},
      ]
    );

    this.agregarColIds(
      ['select', 'agente', 'distribuidor', 'tipo_distribuidor', 'correo_electronico']
    );
    this.tablaConfig("pagAgentes", "Agentes", "agentes", "agentes");

    this.tabla();
  }

}
