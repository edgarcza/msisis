import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentesUsuariosComponent } from './agentes.component';

describe('AgentesUsuariosComponent', () => {
  let component: AgentesUsuariosComponent;
  let fixture: ComponentFixture<AgentesUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentesUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentesUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
