import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistribuidoresUsuariosComponent } from './distribuidores.component';

describe('DistribuidoresUsuariosComponent', () => {
  let component: DistribuidoresUsuariosComponent;
  let fixture: ComponentFixture<DistribuidoresUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistribuidoresUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistribuidoresUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
