import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesAseguradosComponent } from './clientes-asegurados.component';

describe('ClientesAseguradosComponent', () => {
  let component: ClientesAseguradosComponent;
  let fixture: ComponentFixture<ClientesAseguradosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesAseguradosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesAseguradosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
