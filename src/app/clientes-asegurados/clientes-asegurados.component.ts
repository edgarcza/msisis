import { Component, OnInit } from '@angular/core';
// import { SesionService } from '../sesion.service';
import * as Tabla from '../tablas'
import {Tablas} from '../clases/tablas'


@Component({
  selector: 'app-clientes-asegurados',
  templateUrl: './clientes-asegurados.component.html',
  styleUrls: ['./clientes-asegurados.component.css']
})
export class ClientesAseguradosComponent extends Tablas implements OnInit {

	constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }

  ngOnInit(){

    this.agregarColumnas(
      [
        {id: 'estado_t',                    nombre: 'Transacción',            ocultar: false, tabla: 'cotizacion.estado_t'},
        {id: 'fecha_registro_asegurado',    nombre: 'Fecha de registro',      ocultar: false, tabla: 'cliente.fecha_registro_asegurado'},
        {id: 'nombre_completo',             nombre: 'Cliente',                ocultar: false, tabla: 'cliente.nombre_completo'},
        {id: 'fecha_nacimiento',            nombre: 'Fecha de nacimiento',    ocultar: true, tabla: 'cliente.fecha_nacimiento'},
        {id: 'salida',                      nombre: 'Fecha de inicio',        ocultar: false, tabla: 'cotizacion.salida'}, 
        {id: 'regreso',                     nombre: 'Regreso',                ocultar: false, tabla: 'cotizacion.regreso'}, 
        {id: 'pais_destino',                nombre: 'Destino',                ocultar: false, tabla: 'cotizacion_destino.pais'}, 
        {id: 'tipo_poliza',                 nombre: 'Tipo de póliza',         ocultar: false, tabla: 'tipo_poliza.tipo_poliza'}, 
        {id: 'tipo_distribuidor',           nombre: 'Tipo de distribuidor',   ocultar: false, tabla: 'tipo_distribuidor.tipo_distribuidor'}, 
        {id: 'distribuidor',                nombre: 'Distribuidor',           ocultar: false, tabla: 'distribuidor.distribuidor'}, 
        {id: 'agente',                      nombre: 'Agente',                 ocultar: false, tabla: 'agente.agente'}, 
        {id: 'subagente_nombre_completo',   nombre: 'Sub Agente',             ocultar: false, tabla: 'sub_agente.nombre_completo'}, 
        {id: 'fuente_referencia',           nombre: 'Fuente de referencia',   ocultar: true, tabla: 'fuente_referencia.fuente_referencia'}, 
        {id: 'liquidado',                   nombre: 'Liquidado',              ocultar: true, tabla: 'cotizacion.liquidado'}, 
        {id: 'saldo_pendiente_USD',         nombre: 'Saldo pendiente (USD)',  ocultar: false, tabla: 'cotizacion.saldo_pendiente_USD'}, 
        {id: 'saldo_pendiente_MXN',         nombre: 'Saldo pendiente (MXN)',  ocultar: false, tabla: 'cotizacion.saldo_pendiente_MXN'}, 
        {id: 'precioUSD',                   nombre: 'Precio (USD)',           ocultar: false, tabla: 'cotizacion.precioUSD'}, 
        {id: 'precioMXN',                   nombre: 'Precio (MXN)',           ocultar: false, tabla: 'cotizacion.precioMXN'}, 
      ]
    );

    this.agregarColIds(
      ['select', 'estado_t', 'fecha_registro_asegurado', 'nombre_completo', 'fecha_nacimiento', 
        'salida', 'regreso', 'pais_destino', 'tipo_poliza', 'tipo_distribuidor', 'distribuidor', 
        'agente', 'subagente_nombre_completo', 'fuente_referencia', 
        'precioUSD', 'precioMXN', 'saldo_pendiente_USD', 'saldo_pendiente_MXN', 'liquidadoI', 'liquidado', 'acciones']
    );

    if(this.sesion.subagente())
    this.tablaConfig("pagAsegurados", "Asegurados", "Asegurados", "cliente", [{Campo: "cliente.id_subagente", Valor: this.sesion.id_datos(), Operador: "="}]);
    if(this.sesion.gerente())
      this.tablaConfig("pagAsegurados", "Asegurados", "Asegurados", "cliente", [{Campo: "subagente.id_agente", Valor: this.sesion.id_datos(), Operador: "="}]);
    if(this.sesion.admin())
      this.tablaConfig("pagAsegurados", "Asegurados", "Asegurados", "cliente");

    this.sesion.SesionIniciada.subscribe(
      iniciada => {
        console.log(iniciada);
        if(iniciada)
        {
          if(this.sesion.subagente())
            this.tablaConfig("pagAsegurados", "Asegurados", "Asegurados", "cliente", [{Campo: "cliente.id_subagente", Valor: this.sesion.id_datos(), Operador: "="}]);
          if(this.sesion.gerente())
            this.tablaConfig("pagAsegurados", "Asegurados", "Asegurados", "cliente", [{Campo: "subagente.id_agente", Valor: this.sesion.id_datos(), Operador: "="}]);
          if(this.sesion.admin())
            this.tablaConfig("pagAsegurados", "Asegurados", "Asegurados", "cliente");
          this.tabla();
        }
      }
    )
    


      

    this.tablaTerminar = () => {
      for(var i in this.Datos){
        // if(this.Datos[i].liquidado == 0)
        //   this.Datos[i].liquidado = '<i class="fas fa-times" style="font-size: 16pt;"></i>';
        // else
        //   this.Datos[i].liquidado = '<i class="fas fa-check" style="font-size: 16pt;"></i>';
        
        // if(this.Datos[i].saldo_pendiente_USD == 0)
        //   this.Datos[i].saldo_pendiente_USD = '<i class="fas fa-times" style="font-size: 16pt;"></i>';
        // else
        //   this.Datos[i].saldo_pendiente_USD = '<i class="fas fa-check" style="font-size: 16pt;"></i>';
      }
      this.dataSource.data = this.Datos;
    }
    this.tabla();

    // this._sesion.SesionIniciada.subscribe()
    // {
    //   (iniciada) => {
    //     if(iniciada)
    //     {
    //       this.tabla();
    //     }
    //   }
    // }

  }

  pagoGrupo(datos) {
    var mandar = [];
    console.log(this.selection.selected);
    if(this.selection.selected.length < 1) {
      mandar[0] = datos;
    }
    else {
      // mandar.push(datos);
      mandar = this.selection.selected;
    }

    this._Ventana.agregarPagoConjunto(mandar, (r) => {
      console.log(r)
    })
  }

}

