import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizacionConfirmacionComponent } from './cotizacion-confirmacion.component';

describe('CotizacionConfirmacionComponent', () => {
  let component: CotizacionConfirmacionComponent;
  let fixture: ComponentFixture<CotizacionConfirmacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizacionConfirmacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizacionConfirmacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
