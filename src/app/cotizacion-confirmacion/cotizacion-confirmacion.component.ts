import { Component, OnInit, Inject } from '@angular/core';
import { BasicosService } from '../basicos.service';
import { Router, ActivatedRoute } from '@angular/router';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';

import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS} from '../clases/FechaAdaptador';

import { HTTPService }    from '../http.service';
import { DatosFormularioService } from '../servicios/datos-formulario.service';
import { DocumentosService } from '../servicios/documentos.service';
// import { VentanasService } from '../servicios/ventanas.service';
import { BarraProgresoService } from '../servicios/barra-progreso.service';
import { SesionService } from '../sesion.service';

@Component({
  selector: 'app-cotizacion-confirmacion',
  templateUrl: './cotizacion-confirmacion.component.html',
  styleUrls: ['./cotizacion-confirmacion.component.css'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class CotizacionConfirmacionComponent implements OnInit {

	// public Datos = {
	// 	"nombre":"Nombres",
	// 	"apellidos":"Apellidos",
	// 	"email":"mail@mail.com",
	// 	"cotizado":"Nombre",
	// 	"cotizadoEmail":"mail2@mail.com",
	// 	"agente":"MSI-Corporativo",
	// 	"fecha":"31/07/2018",

	// }

  constructor(
    public Basicos: BasicosService,
    private Ruta: ActivatedRoute,
    private HTTP: HTTPService,
    public Formulario: DatosFormularioService,
    public Documento: DocumentosService,
    public Progreso: BarraProgresoService,
    // public Ventana: VentanasService,
    public Rutas: Router,
    public Sesion: SesionService,
    public dialogRef: MatDialogRef<CotizacionConfirmacionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,) { }

    cerrar(hecho = false): void {
      this.dialogRef.close(hecho);
    }

  public Datos = {
    id: null,
    fecha_hecha: new Date(),
    cliente_correo_electronico: null,
    salida: new Date(),
    regreso: new Date(),
    codigo_poliza: null,
    a_codigo: null,
    cliente_nombre_completo: null,
    subagente_nombre_completo: null,
    agente: null,
    tipo_seguro: null,
    tipo_poliza: null,
    pais_origen: null,
    pais_destino: null,
    descripcion: null,
    notas: null,
    precioMXN: null,
    precioUSD: null,
    tipo_cambio: null,
    subagente_correo_electronico: null,
  };

  public DatosCliente = { 
    id: null,
    id_tipo_cliente: null,
    id_cotizacion: null,
    fecha_registro_asegurado: null,
    id_cliente: null,
    contrasena: null,
    correo_electronico: null,
    fecha_nacimiento: null,
    sexo: null,
    apellidos: null,
    nombres: null,
    ciudad: null,
    cp: null,
    universidad:null,
    estado: null,
    id_pais: null,
    colonia: null,
    calle_numero: null,
    id_subagente: null,
  };
  public DatosCotizacion = { 
    id: null,
    id_cliente: null,
    id_subagente: null,
    activo_inactivo: null,
    id_tipo_seguro: null,
    origen: null,
    destino: null,
    salida: null,
    regreso: null,
    codigo_promocion: null,
    id_tipo_poliza: null,
    numero_dias: null,
    numero_semanas_meses: null,
    precioMXN: null,
    precioMXN_descuento: null,
    precioMXN_ajuste: null,
    precioMXN_iva: null,
    precioMXN_subtotal: null,
    precioMXN_comision: null,
    precioMXN_total: null,
    precioUSD: null,
    precioUSD_descuento: null,
    precioUSD_ajuste: null,
    precioUSD_iva: null,
    precioUSD_subtotal: null,
    precioUSD_comision: null,
    precioUSD_total: null,
    tipo_cambio: null,
    descripcion: null,
    fecha_hecha: null,
    fecha_asegurado: null,
    folio: null,
    folio2: null,
    comentario: null,
    cargo_administrativo: null,
    envio: null,
    saldo_pendiente_MXN: null,
    saldo_pendiente_USD: null,
    saldo_liquidado_MXN: null,
    saldo_liquidado_USD: null,
    id_codigo_promocion: null,
    factura: null,
    factura_numero: null,
    factura_fecha: null,
    factura_monto: null,
    factura_status: null,
    factura_archivo: null,
    origen_cp: null,
    origen_ciudad: null,
    destino_cp: null,
    destino_ciudad: null,
    liquidado: null,
    comision: null,
    descuento: null,
  };

  public DatosSubagente = {
    comision: null,
    descuento: null,
  }

  public DatosComision = {
    id: null,
    id_subagente: null,
    folio_seguro: null,
    estado: null,
    comision: null,
    impuestos: null,
    ajuste: null,
    comision_final: null,
    estado_factura: null,
    factura: null,
    fecha_pago: null,
    forma_pago: null,
  }

  public DatosImpuestos = [];

  emailCargando = false;

  public Usuario = {
    id: null,
    correo_electronico: null,
    contrasena: null,
    id_rol: null,
    id_datos: null,
  }

  DatosExtra = {
    salida: null,
    regreso: null,
    fecha_nacimiento: null,
  }

  Total = {
    "MXN": 2800.50,
    "USD": 150.00,
    "Cambio": 18.67
  }

  Agente = false;

  Id_Cotizacion = -1;

  ngOnInit() {
  	// this.Datos.nombreCompleto = this.Datos.nombre + " " + this.Datos.apellidos;
  	// console.log(this.Datos);
    this.Formulario.obtener("Tipo_Poliza");
    this.Formulario.obtener("Tipo_Seguro");
    this.Formulario.obtener("Pais");


    // OBTENER ID Y DATOS COTIZACION
    // this.Ruta.params.subscribe(
    //   params => {
    //     var idCot = +params['id'];  
    //     // console.log(this.Datos.id);
    //     this.Id_Cotizacion = idCot;
    //     this.HTTP.cotizacionConfirmar(idCot)
    //     .subscribe(
    //       datos => {
    //         console.log(datos);
    //         this.Datos = datos.Cotizacion[0];
    //         this.llenarDatos("cliente", datos.Cotizacion[0].id_cliente);
    //         this.llenarDatos("cotizacion", datos.Cotizacion[0].id);
    //         this.llenarDatos("subagente", datos.Cotizacion[0].id_subagente);
    //         this.obtenerAsignarSeguro(datos.Cotizacion[0].id_cliente);
    //         this.llenarDatos("impuesto");
    //       },
    //       error => {
    //         console.log(error);
    //       }
    //     );
    //     // this.HTTP.datosDe("cotizacion", [{campo: 'id', valor: idCot}]).subscribe(
    //     //   datos => {
    //     //     this.DatosCotizacion 
    //     //   },

    //     //   error => {
    //     //     console.log(error);
    //     //   }
    //     // );

    //   }
    // );

    if(!this.Sesion.admin())
      this.Agente = true;

    this.HTTP.cotizacionConfirmar(this.data.ID)
      .subscribe(
        datos => {
          console.log(datos);
          this.Datos = datos.Cotizacion[0];
          this.llenarDatos("cliente", datos.Cotizacion[0].id_cliente);
          this.llenarDatos("cotizacion", datos.Cotizacion[0].id);
          this.llenarDatos("subagente", datos.Cotizacion[0].id_subagente);
          this.obtenerAsignarSeguro(datos.Cotizacion[0].id_cliente);
          this.llenarDatos("impuesto");
        },
        error => {
          console.log(error);
        }
      );

      this.DatosCotizacion.destino_cp = 66220;

  }
  
  DatosAsignar = {
    tipo_distribuidor_id: null,
    distribuidor_id: null,
    agente_id: null,
    subagente_id: null,

  };
  obtenerAsignarSeguro(id_cliente){
    // this.Formulario.obtener("Tipo_Distribuidor");
    // this.Formulario.obtener("Distribuidor");
    // this.Formulario.obtener("Agente");
    // this.Formulario.obtener("Subagente");

    // this.Formulario.obtener("Tipo_Distribuidor", {Campo: 'id_tipo_distribuidor', Valor: this.DatosAsignar.tipo_distribuidor_id});
    // this.Formulario.obtener("Distribuidor", {Campo: 'id_tipo_distribuidor', Valor: this.DatosAsignar.tipo_distribuidor_id});
    // this.Formulario.obtener("Agente", {Campo: 'id_distribuidor', Valor: this.DatosAsignar.distribuidor_id});
    // this.Formulario.obtener("Subagente", {Campo: 'id_subagente', Valor: this.DatosCliente.id_subagente});


    this.HTTP.aseguradoAsignarSeguro(id_cliente).subscribe(
      (datos) => {
        this.DatosAsignar = datos.Seguro[0];
        console.log(this.DatosAsignar)
        // console.log(datos)>
        this.Formulario.obtener("Tipo_Distribuidor");
        // this.Formulario.obtener("Tipo_Distribuidor", {Campo: 'id', Valor: this.DatosAsignar.tipo_distribuidor_id});
        this.Formulario.obtener("Distribuidor", {Campo: 'id_tipo_distribuidor', Valor: this.DatosAsignar.tipo_distribuidor_id});
        this.Formulario.obtener("Agente", {Campo: 'id_distribuidor', Valor: this.DatosAsignar.distribuidor_id});
        this.Formulario.obtener("Subagente", {Campo: 'id_agente', Valor: this.DatosAsignar.agente_id});
      },

      (error) => {
        console.log(error);
      }
    );
  }

  cambioAsignarSeguro(cual){
    if(cual === "Tipo_Distribuidor")
      this.Formulario.obtener("Distribuidor", {Campo: 'id_tipo_distribuidor', Valor: this.DatosAsignar.tipo_distribuidor_id});
    else if(cual === "Distribuidor")
      this.Formulario.obtener("Agente", {Campo: 'id_distribuidor', Valor: this.DatosAsignar.distribuidor_id});
    else if(cual === "Agente")
      this.Formulario.obtener("Subagente", {Campo: 'id_agente', Valor: this.DatosAsignar.agente_id});
  }
  AsignarGuardar(){
    // let Distr = {
    //   id: this.DatosAsignar.distribuidor_id,
    //   id_distribuidores: this.DatosAsignar.id_distribuidores,
    // }
    this.DatosCliente.id_subagente = this.DatosAsignar.subagente_id;
    this.DatosCotizacion.id_subagente = this.DatosAsignar.subagente_id;
    // this.Formulario.guardar("distribuidor", Distr);
    // this.Formulario.guardar("cliente", this.DatosCliente);
    this.Formulario.guardar("cotizacion", this.DatosCotizacion);
    // window.location.reload();
  }

  PrimerAsegurar = false;
  SinDatos = false;
  asegurar(){
    this.AsignarGuardar();
    console.log(this.DatosCotizacion, this.DatosCliente);
    if(this.SinDatos || (
      // this.DatosCotizacion.origen_cp !== "" && this.DatosCotizacion.origen_ciudad !== "" && 
      this.DatosCotizacion.destino_cp !== "" 
    // && this.DatosCotizacion.destino_ciudad !== "" && this.DatosCliente.sexo !== "" && this.DatosCliente.fecha_nacimiento !== ""
    && this.DatosCliente.sexo !== "" && this.DatosCliente.fecha_nacimiento !== ""
    && this.DatosCliente.universidad !== "" && 
    this.DatosCliente.cp !== "" && this.DatosCliente.ciudad !== "" && this.DatosCliente.estado !== "" 
    && this.DatosCliente.id_pais !== "" && this.DatosCliente.colonia !== "" && this.DatosCliente.calle_numero !== ""
    )) {

      this.DatosCliente.fecha_nacimiento = new Date(this.DatosCliente.fecha_nacimiento);
      this.DatosCliente.fecha_nacimiento.setUTCHours(8,0,0,0);
      var id_cliente = this.DatosCliente.id;
      this.DatosCliente.id_tipo_cliente = 2; // ASEGURADO
      // this.DatosCliente.cp = this.DatosCotizacion.origen_cp; // ASEGURADO
      // this.DatosCliente.ciudad = this.DatosCotizacion.origen_ciudad; // ASEGURADO
      this.DatosCliente.fecha_registro_asegurado = new Date(); // HOY
      this.DatosCliente.id_cotizacion = this.Datos.id; //ID cotizacion
      this.DatosCliente.id_cliente = this.DatosCliente.apellidos.substring(0, 3).toUpperCase() + this.DatosCliente.nombres.substring(0, 3).toUpperCase() + 
            this.DatosCliente.fecha_nacimiento.getFullYear() + ("0" + (this.DatosCliente.fecha_nacimiento.getMonth() + 1)).slice(-2) + 
            ("0" + this.DatosCliente.fecha_nacimiento.getDate()).slice(-2);
      // this.DatosCliente.contrasena = new Date().getMilliseconds();
      // this.DatosCotizacion.
      this.Formulario.guardar("cliente", this.DatosCliente, null, (id) => {
        this.guardarCotizacion();

        this.Usuario.correo_electronico = this.DatosCliente.correo_electronico;
        this.Usuario.contrasena = new Date().getMilliseconds();
        this.Usuario.id_rol = 5;
        this.Usuario.id_datos = id;
        this.Formulario.guardar('usuario', this.Usuario, 'correo_electronico');

        this.Documento.email(this.DatosCliente.correo_electronico, "RegistroUsuario", this.Usuario, null);

        // this.Rutas.navigate(['Asegurados', id_cliente]);
        // var getUrl = window.location;
        // var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";
        // window.location.href = baseUrl + 'Asegurados/' + id_cliente;
      });
    }
    else{
      this.PrimerAsegurar = true;
      this.SinDatos = true;
      // this.Formulario.alerta("Llenar todos los campos", "Cerrar");
    }
  }

  guardarComision(){
    if(this.DatosCotizacion.comision != 0 || this.DatosCotizacion.descuento != 0) {
      this.DatosComision.comision = (this.DatosCotizacion.comision / 100) * this.DatosCotizacion.precioMXN;
    }
    else
      this.DatosComision.comision = (this.DatosSubagente.comision / 100) * this.DatosCotizacion.precioMXN;
    console.log(this.DatosComision.comision);
    
    this.DatosComision.impuestos = 0;
    for(var Num in this.DatosImpuestos){
      this.DatosComision.impuestos += (this.DatosImpuestos[Num].porcentaje / 100) * this.DatosComision.comision;
    }
    this.DatosComision.comision_final = this.DatosComision.comision - this.DatosComision.impuestos;
    this.DatosComision.folio_seguro = this.DatosCotizacion.folio;
    this.DatosComision.id_subagente = this.DatosCotizacion.id_subagente;
    this.Formulario.guardar("comision", this.DatosComision, null, 
      () => {
        // this.Rutas.navigate(['Asegurados', this.DatosCliente.id]);
        this.cerrar(true);
        this.Rutas.navigate(["Asegurados", this.DatosCliente.id, "Pagos", this.DatosCotizacion.id]);
      })
  }

  guardarCotizacion(){
    this.DatosCotizacion.origen_cp = this.DatosCliente.cp;
    this.DatosCotizacion.origen_ciudad = this.DatosCliente.ciudad;
    this.DatosCotizacion.folio = 
      this.DatosCliente.fecha_registro_asegurado.getFullYear() + ("0" + (this.DatosCliente.fecha_registro_asegurado.getMonth() + 1)).slice(-2) + ("0" + this.DatosCliente.fecha_registro_asegurado.getDate()).slice(-2) + "." +
      this.Datos.a_codigo + this.Datos.codigo_poliza + "." + this.DatosCliente.id_cliente;
    this.DatosCotizacion.fecha_asegurado = this.DatosCliente.fecha_registro_asegurado;
    this.DatosCotizacion.folio2 = 
      this.DatosCliente.fecha_registro_asegurado.getFullYear() + ("0" + (this.DatosCliente.fecha_registro_asegurado.getMonth() + 1)).slice(-2) + ("0" + this.DatosCliente.fecha_registro_asegurado.getDate()).slice(-2) + "." +
      this.Datos.a_codigo + this.Datos.codigo_poliza;
    this.DatosCotizacion.fecha_asegurado = this.DatosCliente.fecha_registro_asegurado;
    this.Formulario.guardar("cotizacion", this.DatosCotizacion, null, (id) => {
      this.DatosCotizacion.id = id;
      this.Formulario.borrarCond("cotizacion", [{Campo: "id_cliente", Valor: this.DatosCliente.id}, {Campo: "folio", Valor: ""}], () => {
        // this.Rutas.navigate(['Asegurados', this.DatosCliente.id]);
        this.guardarComision();
      });
    });
  }

  guardar(){
    // this.DatosCliente.fecha_nacimiento.setUTCHours(8,0,0,0);
    // this.DatosCliente.fecha_nacimiento.setMinutes((this.DatosCliente.fecha_nacimiento.getTimezoneOffset() * -1));
    console.log(this.DatosCliente.fecha_nacimiento);
    this.DatosCotizacion.origen_cp = this.DatosCliente.cp;
    this.DatosCotizacion.origen_ciudad = this.DatosCliente.ciudad;
    this.Formulario.guardar("cotizacion", this.DatosCotizacion, null, () => {
      
    });
  }

  email(){
    this.emailCargando = true; 
    this.Documento.email(this.Datos.cliente_correo_electronico, "Cotizacion", {id_cotizacion: this.Id_Cotizacion}, () => {this.emailCargando = false;});
  }

  documento(imprimir = false){
    this.emailCargando = true; 
    this.Documento.documento("Cotizacion", {id_cotizacion: this.DatosCotizacion.id}, imprimir, () => {this.emailCargando = false;});
  }

  // FechaNacimiento;
  llenarDatos(tabla, id = null){
    let where = null;
    if(id)
      where = [{campo: "id", valor: id}];
    this.HTTP.datosDe(tabla, where)
      .subscribe(
        datos => {
          if(tabla === "cliente") {
            this.DatosCliente = datos[0];
            this.DatosExtra.fecha_nacimiento = new Date(new Date(this.DatosCliente.fecha_nacimiento).getTime() + (24 * 60 * 60 * 1000));
          }
          else if(tabla === "cotizacion"){
            this.DatosCotizacion = datos[0];            
            this.DatosCotizacion.destino_cp = 66220;
            this.DatosExtra.salida = new Date(new Date(this.DatosCotizacion.salida).getTime() + 1000 * 60 * 60 * 24);
            this.DatosExtra.regreso = new Date(new Date(this.DatosCotizacion.regreso).getTime() + 1000 * 60 * 60 * 24);
          }
          else if(tabla === "subagente")
            this.DatosSubagente = datos[0];
          else if(tabla === "impuesto")
            this.DatosImpuestos = datos;
          console.log(datos);
          // console.log(this.DatosCotizacion);
        },

        error => {
          console.log(error);
        }
      );
  }

  Cargando = false;

  imprimir(){
    this.Progreso.CargandoGlobal = true;
    this.Documento.pdf(document.getElementsByClassName("area")[0], true, () => {
        // this.Cargando = false;
        this.Progreso.CargandoGlobal = false;
    });
  }

  descargar(){
    this.Progreso.CargandoGlobal = true;
    this.Documento.pdf(document.getElementsByClassName("area")[0], false, () => {
        // this.Cargando = false;
        this.Progreso.CargandoGlobal = false;
    });
  }

  correo(){
    // this.Ventana.correoCuerpo(this.Datos, 
    //   (Resultado) => {
    //     this.Progreso.CargandoGlobal = true;
    //     this.Documento.pdf(document.getElementsByClassName("area")[0], false, (link, base, pdf, pdfbase) => {
    //       // this.LINKIMG = pdf;
    //       this.Documento.email(this.DatosCliente.correo_electronico, "CotizacionConfirmacion", 
    //       {Archivo: base, Asunto: Resultado.Datos.Asunto, Mensaje: Resultado.Datos.Mensaje}, 
    //       () => {
    //           // this.Cargando = false;
    //           this.Progreso.CargandoGlobal = false;
    //       });
    //     }, false); 
    //   },
      
    //   () => {

    //   })    
  }

  Diferencia = {
    Segundos: null,
    Minutos: null,
    Horas: null,
    Dias: null,
    Semanas: null,
  };
  obtenerTiposPoliza(){
    if(this.DatosCotizacion.origen && this.DatosCotizacion.destino && this.DatosCotizacion.salida && this.DatosCotizacion.regreso && this.DatosCotizacion.id_tipo_seguro)
    {
      console.log("Días: "+this.Diferencia.Dias +
        "\nZona origen: "+this.Formulario.FormDatos['Pais'][this.Formulario.formElementoPorId("Pais", this.DatosCotizacion.origen)]['zona']+
        "\nZona destino: "+this.Formulario.FormDatos['Pais'][this.Formulario.formElementoPorId("Pais", this.DatosCotizacion.destino)]['zona']+
        "\nTipo seguro: "+this.DatosCotizacion.id_tipo_seguro);
      // this.BloquearTipoPoliza = false;
      this.Formulario.coberturaPoliza(this.DatosCotizacion.id_tipo_seguro, 
        this.Diferencia.Dias, this.Formulario.FormDatos['Pais'][this.Formulario.formElementoPorId("Pais", this.DatosCotizacion.origen)]['zona'],
        this.Formulario.FormDatos['Pais'][this.Formulario.formElementoPorId("Pais", this.DatosCotizacion.destino)]['zona'],
        this.DatosCotizacion);
    }
  }

  fechaCambio(type: string, event: MatDatepickerInputEvent<Date>){
    console.log((<HTMLInputElement>event.targetElement).placeholder);
    if((<HTMLInputElement>event.targetElement).placeholder === "Salida" || (<HTMLInputElement>event.targetElement).placeholder === "Regreso")
    {
      this.asignarDiferencia();
      console.log(this.Diferencia);
      // console.log(this.DatosCotizacion.salida.getTime());
      // console.log(this.DatosCotizacion.regreso.getTime());
      if(this.DatosCotizacion.origen && this.DatosCotizacion.destino)
      {
        this.obtenerTiposPoliza();
      }

    }

    
    // console.log(this.DatosCotizacion.regreso);
  }

  asignarDiferencia(){
    // Calcudora de días
    this.Diferencia = {
      Segundos: 
        ((this.DatosCotizacion.regreso.getTime() - this.DatosCotizacion.salida.getTime())/1000),
      Minutos: 
        (((this.DatosCotizacion.regreso.getTime() - this.DatosCotizacion.salida.getTime())/1000) / 60),
      Horas: 
        ((((this.DatosCotizacion.regreso.getTime() - this.DatosCotizacion.salida.getTime())/1000) / 60) / 60),
      Dias: 
        ((((((this.DatosCotizacion.regreso.getTime() - this.DatosCotizacion.salida.getTime())/1000) / 60) / 60) / 24) + 1),
      Semanas:
        ((((((this.DatosCotizacion.regreso.getTime() - this.DatosCotizacion.salida.getTime())/1000) / 60) / 60) / 24) / 7)
    }
  }

  cambioTipoPoliza(){
    this.DatosCotizacion.precioUSD = this.Formulario.FormDatos['Tipo_Poliza'][this.Formulario.formElementoPorId("Tipo_Poliza", this.DatosCotizacion.id_tipo_poliza)]['precio'];
    this.DatosCotizacion.precioMXN = this.Formulario.redondear(this.DatosCotizacion.precioUSD * this.DatosCotizacion.tipo_cambio, 2);

    // this.Precios.MXN = this.DatosCotizacion.precioMXN;
    // this.Precios.USD = this.DatosCotizacion.precioUSD;
  }

  origenCP(){
    try{
      if(this.DatosCotizacion.origen_cp.toString().length > 3){
        this.HTTP.cp_buscarDatos(this.DatosCotizacion.origen_cp)
        .subscribe(
          datos => {
            console.log(datos);
            this.DatosCotizacion.origen_ciudad = datos.municipio;
            // Datos[pre + 'estado'] = datos.estado;
            // this.Colonias = datos.colonias;
          },

          error => {
            console.log(error);
          }
        );
      }
    }
    catch(error){
      // console.log(error);
    }
  }

}
