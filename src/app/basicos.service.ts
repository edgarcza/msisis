import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BasicosService {

  constructor() { }

  fechaNormal(fecha:Date){
  	var fechareal = new Date(fecha);
  	return fecha.getDay() + "/" + fecha.getMonth() + "/" + fecha.getFullYear();
  }

}
