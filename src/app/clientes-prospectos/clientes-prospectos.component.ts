import { Component, OnInit } from '@angular/core';
import * as Tabla from '../tablas'
import {Tablas} from '../clases/tablas'

@Component({
  selector: 'app-clientes-prospectos',
  templateUrl: './clientes-prospectos.component.html',
  styleUrls: ['./clientes-prospectos.component.css']
})
export class ClientesProspectosComponent extends Tablas implements OnInit {

  constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }
  
  ngOnInit() {
    this.agregarColumnas(
      [
        {id: 'fecha_registro_prospecto',  nombre: 'Fecha de registro',      ocultar: false,   tabla: 'cliente.fecha_registro_prospecto'},
        {id: 'nombre_completo',           nombre: 'Cliente',                ocultar: false,   tabla: 'cliente.nombre_completo'},
        {id: 'fecha_nacimiento',          nombre: 'Fecha de nacimiento',    ocultar: false,   tabla: 'cliente.fecha_nacimiento'}, 
        {id: 'correo_electronico',        nombre: 'Correo electrónico',     ocultar: false,   tabla: 'cliente.correo_electronico'}, 
        {id: 'telefono',                  nombre: 'Telefono',               ocultar: false,   tabla: 'cliente.telefono'}, 
        {id: 'tipo_distribuidor',         nombre: 'Tipo de distribuidor',   ocultar: false,   tabla: 'tipo_distribuidor.tipo_distribuidor'}, 
        {id: 'distribuidor',              nombre: 'Distribuidor',           ocultar: false,   tabla: 'distribuidor.distribuidor'}, 
        {id: 'agente',                    nombre: 'Agente',                 ocultar: false,   tabla: 'agente.agente'}, 
        {id: 'sub_agente',                nombre: 'Subagente',              ocultar: false,   tabla: 'sub_agente.sub_agente'}, 
        {id: 'fuente_referencia',         nombre: 'Referencia',             ocultar: false,   tabla: 'fuente_referencia.fuente_referencia'}, 
      ]
    );

    this.agregarColIds(
      ['select', 'fecha_registro_prospecto', 'nombre_completo', 'fecha_nacimiento', 'correo_electronico', 
    'telefono', 'tipo_distribuidor', 'distribuidor', 'agente', 'sub_agente', 'fuente_referencia', 'acciones']
    ); 

    if(this._sesion.subagente())
      this.tablaConfig("pagProspectos", "Prospectos", "Prospectos", "cliente", [{Campo: "cliente.id_subagente", Valor: this._sesion.id_datos(), Operador: "="}]);
    if(this._sesion.gerente())
      this.tablaConfig("pagProspectos", "Prospectos", "Prospectos", "cliente", [{Campo: "subagente.id_agente", Valor: this._sesion.id_datos(), Operador: "="}]);
    if(this._sesion.admin())
      this.tablaConfig("pagProspectos", "Prospectos", "Prospectos", "cliente");

  	this.tabla();
  }

}
