import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesProspectosComponent } from './clientes-prospectos.component';

describe('ClientesProspectosComponent', () => {
  let component: ClientesProspectosComponent;
  let fixture: ComponentFixture<ClientesProspectosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesProspectosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesProspectosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
