import { TestBed, inject } from '@angular/core/testing';

import { BasicosService } from './basicos.service';

describe('BasicosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BasicosService]
    });
  });

  it('should be created', inject([BasicosService], (service: BasicosService) => {
    expect(service).toBeTruthy();
  }));
});
