import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, NavigationStart, NavigationCancel, NavigationError } from '@angular/router';

import {SesionService} from '../sesion.service'
import { VentanasService } from '../servicios/ventanas.service';
import { BarraProgresoService } from '../servicios/barra-progreso.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

	Datos = [];

  constructor(
  	  public sesion: SesionService,
      public ventana: VentanasService,
      public Progreso: BarraProgresoService,
      private router: Router
  	) {

      router.events.subscribe((event) => {
          // see also 
            this.Progreso.CargandoGlobal = true;

          if(event instanceof NavigationStart) {
            this.Progreso.CargandoGlobal = true;
            // console.log("Start");
          }

          if(event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
            this.Progreso.CargandoGlobal = false;
            // console.log("End");
          }
          // NavigationEnd
          // NavigationCancel
          // NavigationError
          // RoutesRecognized
      });

    }

  ngOnInit() {
  }

}
