import { Component, OnInit } from '@angular/core';
import * as Tabla from '../../tablas'
import {Tablas} from '../../clases/tablas'

@Component({
  selector: 'app-conciliar-aprobado',
  templateUrl: './conciliar-aprobado.component.html',
  styleUrls: ['./conciliar-aprobado.component.css']
})
export class ConciliarAprobadoComponent extends Tablas implements OnInit {

	constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }

  ngOnInit(){

    this.agregarColumnas(
      [
        {id: 'estado_t',   nombre: 'Transacción',     ocultar: false,    tabla: 'cotizacion.estado_t'},
        {id: 'fecha_asegurado',   nombre: 'Fecha de registro',     ocultar: false,    tabla: 'cotizacion.fecha_asegurado'},
        {id: 'nombre_completo',           nombre: 'Cliente',    		    ocultar: false,    tabla: 'cliente.nombre_completo'},
        {id: 'fecha_nacimiento',  nombre: 'Nacimiento',         ocultar: false,    tabla: 'cliente.fecha_nacimiento'},
        {id: 'pais_origen',       nombre: 'País de origen',     ocultar: false,    tabla: 'pais_origen.pais'},
        {id: 'ciudad',            nombre: 'Ciudad de origen',   ocultar: false,    tabla: 'cliente.ciudad'},
        {id: 'cp',            		nombre: 'C.P. origen',    	  ocultar: false,    tabla: 'cliente.cp'},
        {id: 'pais_destino',      nombre: 'País de destino',    ocultar: false,    tabla: 'pais_destino.pais'},
        {id: 'salida',            nombre: 'Inicio',    				  ocultar: false,    tabla: 'cotizacion.salida'},
        {id: 'regreso',    			  nombre: 'Regreso',      		  ocultar: false,    tabla: 'cotizacion.regreso'},
        {id: 'numero_dias',    	  nombre: 'Días',      		      ocultar: false,    tabla: 'cotizacion.numero_dias'},
        {id: 'tipo_poliza',    	  nombre: 'Tipo de póliza',     ocultar: false,    tabla: 'tipo_poliza.tipo_poliza'},
        {id: 'zona',    					nombre: 'Zona',      				  ocultar: false,    tabla: 'pais_destino.zona'},
        {id: 'precioMXN',    		  nombre: 'Precio MXN',      	  ocultar: false,     tabla: 'cotizacion.precioMXN'},
        {id: 'precioUSD',    			nombre: 'Precio USD',      	  ocultar: false,     tabla: 'cotizacion.precioUSD'},
        {id: 'factura_numero',    nombre: 'Número factura',     ocultar: false,    tabla: 'cotizacion.factura_numero'},
        {id: 'factura_fecha',     nombre: 'Fecha factura',      ocultar: false,    tabla: 'cotizacion.factura_fecha'},
        {id: 'factura_monto',     nombre: 'Monto factura',      ocultar: false,    tabla: 'cotizacion.factura_monto'},
        // {id: 'factura',    					  nombre: 'Factura',      				  ocultar: false,    tabla: 'comision.factura'},
      ]
    );

    this.agregarColIds(
      ['select', 'estado_t', 'fecha_asegurado', 'nombre_completo', 'fecha_nacimiento', 'pais_origen', 'ciudad', 
      'cp', 'pais_destino', 'salida', 'regreso', 'numero_dias', 'tipo_poliza', 'zona', 'precioMXN', 'precioUSD',
      'factura_numero', 'factura_fecha', 'factura_monto', 'factura_archivo']
    );

    this.tablaConfig("pagConciliar", "ConciliarPendientes", "cotización", "cotizacion", [{Campo: "cotizacion.factura_status", Valor: 1}]);

    this.tabla();

  }

}
