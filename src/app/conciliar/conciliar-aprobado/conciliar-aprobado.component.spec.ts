import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConciliarAprobadoComponent } from './conciliar-aprobado.component';

describe('ConciliarAprobadoComponent', () => {
  let component: ConciliarAprobadoComponent;
  let fixture: ComponentFixture<ConciliarAprobadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConciliarAprobadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConciliarAprobadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
