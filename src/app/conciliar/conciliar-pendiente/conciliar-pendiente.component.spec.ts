import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConciliarPendienteComponent } from './conciliar-pendiente.component';

describe('ConciliarPendienteComponent', () => {
  let component: ConciliarPendienteComponent;
  let fixture: ComponentFixture<ConciliarPendienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConciliarPendienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConciliarPendienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
