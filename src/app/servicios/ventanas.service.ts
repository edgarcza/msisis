import { Component, OnInit, Inject, Injectable } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import {AsignarSeguroComponent} from '../ventanas/asignar-seguro/asignar-seguro.component';
import {ColumnasComponent} from '../ventanas/columnas/columnas.component';
import {DistribuidoresComponent} from '../ventanas/distribuidores/distribuidores.component';
import {AgentesComponent} from '../ventanas/agentes/agentes.component';
import {SubAgentesComponent} from '../ventanas/sub-agentes/sub-agentes.component';
import {TipoCambioComponent} from '../ventanas/tipo-cambio/tipo-cambio.component';
import {AgregarPagoComponent} from '../ventanas/agregar-pago/agregar-pago.component';
import {AgregarPagoConjuntoComponent} from '../ventanas/agregar-pago-conjunto/agregar-pago-conjunto.component';
import {ConfirmarComponent} from '../ventanas/confirmar/confirmar.component';
import {RecuperarContrasenaComponent} from '../ventanas/recuperar-contrasena/recuperar-contrasena.component';
import {RecuperarContrasena2Component} from '../ventanas/recuperar-contrasena2/recuperar-contrasena2.component';
import {AgregarIncidenteComponent} from '../ventanas/agregar-incidente/agregar-incidente.component';
import {AgregarFacturaIncidenteComponent} from '../ventanas/agregar-factura-incidente/agregar-factura-incidente.component';
import {AgregarComentarioIncidenteComponent} from '../ventanas/agregar-comentario-incidente/agregar-comentario-incidente.component';
import {ComisionAjustesComponent} from '../ventanas/comision-ajustes/comision-ajustes.component';
import {ConciliarAjustesComponent} from '../ventanas/conciliar-ajustes/conciliar-ajustes.component';
import {SubirArchivoComponent} from '../ventanas/subir-archivo/subir-archivo.component';
import {CorreoCuerpoComponent} from '../ventanas/correo-cuerpo/correo-cuerpo.component';
import {CotizacionConfirmacionComponent} from '../cotizacion-confirmacion/cotizacion-confirmacion.component';
import { CodigosPromocionComponent } from '../ventanas/codigos-promocion/codigos-promocion.component';
import { ExcelAsignarComponent } from '../ventanas/excel-asignar/excel-asignar.component';

@Injectable({
  providedIn: 'root'
})
export class VentanasService {

  constructor(
    public dialog: MatDialog,
    ) { }

  dialogRef;

  asignarSeguro(datos){
    console.log(datos);
    this.dialogRef = this.dialog.open(AsignarSeguroComponent, {
      width: '50%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] AsignarSeguro cerrado');
    });
  }

  columnas(datos){
    console.log(datos);
    this.dialogRef = this.dialog.open(ColumnasComponent, {
      width: '30%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] Columnas cerrado');
    });
  }

  distribuidores(datos){
    console.log(datos);
    this.dialogRef = this.dialog.open(DistribuidoresComponent, {
      width: '99%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] Distribuidores cerrado');
    });
  }

  agentes(datos){
    console.log(datos);
    this.dialogRef = this.dialog.open(AgentesComponent, {
      width: '99%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] Agentes cerrado');
    });
  }

  subAgentes(datos, alCerrar = function(){}){
    console.log(datos);
    this.dialogRef = this.dialog.open(SubAgentesComponent, {
      width: '99%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] SubAgentes cerrado');
      alCerrar();
    });
  }

  tipoCambio(datos){
    console.log(datos);
    this.dialogRef = this.dialog.open(TipoCambioComponent, {
      width: '50%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] TipoCambio cerrado');
    });
  }

  agregarPago(datos, alCerrar = function(){}){
    // console.log(datos);
    this.dialogRef = this.dialog.open(AgregarPagoComponent, {
      width: '70%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] AgregarPago cerrado');
      alCerrar();
    });
  }

  agregarPagoConjunto(datos, alCerrar = function(args){}){
    // console.log(datos);
    this.dialogRef = this.dialog.open(AgregarPagoConjuntoComponent, {
      width: '60%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] AgregarPagoConjunto cerrado');
      alCerrar(result);
    });
  }

  Confirmar(datos, alCerrarSI = function(){}, alCerrarNO = function(){}){
    // console.log(datos);
    this.dialogRef = this.dialog.open(ConfirmarComponent, {
      width: '30%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] Confirmar cerrado');
      if(result == true)
        alCerrarSI();
      else
        alCerrarNO();
    });
  }

  recuperarContrasena(datos, alCerrar = function(){}){
    // console.log(datos);
    this.dialogRef = this.dialog.open(RecuperarContrasenaComponent, {
      width: '40%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] RecuperarContrasena cerrado');
      alCerrar();
    });
  }

  recuperarContrasena2(datos, alCerrar = function(){}){
    // console.log(datos);
    this.dialogRef = this.dialog.open(RecuperarContrasena2Component, {
      width: '40%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] RecuperarContrasena2 cerrado');
      alCerrar();
    });
  }

  agregarIncidente(datos, alCerrar = function(){}){
    // console.log(datos);
    this.dialogRef = this.dialog.open(AgregarIncidenteComponent, {
      width: '70%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] AgregarIncdente cerrado');
      alCerrar();
    });
  }

  agregarFacturaIncidente(datos, alCerrar = function(){}){
    // console.log(datos);
    this.dialogRef = this.dialog.open(AgregarFacturaIncidenteComponent, {
      width: '70%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] AgregarFacturaIncdente cerrado');
      alCerrar();
    });
  }

  agregarComentarioIncidente(datos, alCerrar = function(){}){
    // console.log(datos);
    this.dialogRef = this.dialog.open(AgregarComentarioIncidenteComponent, {
      width: '70%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] AgregarComentarioIncdente cerrado');
      alCerrar();
    });
  }

  ajustesComision(datos, alCerrar = function(){}){
    // console.log(datos);
    this.dialogRef = this.dialog.open(ComisionAjustesComponent, {
      width: '30%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] ComisionAjustes cerrado');
      alCerrar();
    });
  }

  subirArchivo(datos, alCerrarSUBIDO = function(){}, alCerrarNOSUBIDO = function(){}){
    // console.log(datos);
    this.dialogRef = this.dialog.open(SubirArchivoComponent, {
      width: '40%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] SubirArchivo cerrado');
      if(result && result.Subido == true)
        alCerrarSUBIDO();
      else
        alCerrarNOSUBIDO();
    });
  }

  ajustesConciliar(datos, alCerrar = null){
    // console.log(datos);
    this.dialogRef = this.dialog.open(ConciliarAjustesComponent, {
      width: '30%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] ConciliarAjustes cerrado');
      alCerrar(result);
    });
  }

  correoCuerpo(datos, alCerrarSI = null, alCerrarNO = null){
    // console.log(datos);
    this.dialogRef = this.dialog.open(CorreoCuerpoComponent, {
      width: '70%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] Correo cerrado');
      if(result.Enviar == true)
        alCerrarSI(result);
      else
        alCerrarNO();
    });
  }

  confirmarCotizacion(datos, alCerrar = null){
    // console.log(datos);
    this.dialogRef = this.dialog.open(CotizacionConfirmacionComponent, {
      width: '90%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] Confirmar cerrado');
        alCerrar(result);
    });
  }

  codigosPromociones(datos){
    console.log(datos);
    this.dialogRef = this.dialog.open(CodigosPromocionComponent, {
      width: '40%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] CodigosPromocion cerrado');
    });
  }

  sxlsAsignar(datos, alCerrar = null){
    // console.log(datos);
    this.dialogRef = this.dialog.open(ExcelAsignarComponent, {
      width: '50%',
      data: datos
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('[Ventana] Confirmar cerrado');
        alCerrar(result);
    });
  }


}
