import { TestBed, inject } from '@angular/core/testing';

import { DatosFormularioService } from './direccion.service';

describe('DatosFormularioService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DatosFormularioService]
    });
  });

  it('should be created', inject([DatosFormularioService], (service: DatosFormularioService) => {
    expect(service).toBeTruthy();
  }));
});
