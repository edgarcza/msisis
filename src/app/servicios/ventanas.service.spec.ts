import { TestBed, inject } from '@angular/core/testing';

import { VentanasService } from './ventanas.service';

describe('VentanasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VentanasService]
    });
  });

  it('should be created', inject([VentanasService], (service: VentanasService) => {
    expect(service).toBeTruthy();
  }));
});
