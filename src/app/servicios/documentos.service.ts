import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material';

import { HTTPService }    from '../http.service';

import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';  


@Injectable({
  providedIn: 'root'
})
export class DocumentosService {

  constructor(
    private HTTP: HTTPService,
  	public Alerta: MatSnackBar,
    ) { }

  imprimir(titulo: any = 'Titulo'){
  	let printContents, popupWin;
    printContents = document.getElementById('area-impresion').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>${titulo}</title>
          // <link rel="stylesheet" type="text/css" href="../../style.css">
          <style>
          	@import url('../../assets/css/bootstrap.min.css');
						@import url('https://fonts.googleapis.com/css?family=Roboto:100,400,700,900');
          	@import url('../../app/cotizacion-confirmacion/cotizacion-confirmacion.component.css');
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  print(contenido, estilo){
  	let printContents, popupWin;
    printContents = contenido;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <style>${estilo}</style>
      ${printContents}
      `
    );
    popupWin.document.close();
  }

  pdf(contenido, imprimir = false, alTerminar = null, guardar = true){
    // var contenido 
    html2canvas(contenido).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      // var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      // var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png');
      // console.log(contentDataURL.split(',')[1]);
      // console.log(contentDataURL);
      let pdf = new jsPDF('p', 'mm', 'letter'); // A4 size page of PDF  
      // var position = 0;
      // contentDataURL.
      pdf.addImage(contentDataURL, 'PNG', 2, 2, imgWidth, imgHeight) ;
      let pdffile = "dsad,fdsff";
      if(guardar){
        if(imprimir)
          pdf.autoPrint();
        pdf.save('PDF.pdf'); // Generated PDF   
      }
      else{
        pdffile = pdf.output('datauristring');
      }
      let baseC = this.lzw_encode(contentDataURL.split(',')[1]);
      if(alTerminar)
        alTerminar(contentDataURL, contentDataURL.split(',')[1], pdffile, pdffile.split(',')[1], baseC);
    });  

  }

  PDFMAS = null;
  pdfMAS(contenido, imprimir = false, alTerminar = null, guardar = true, listo = false, email = null){
    var contenido 
    html2canvas(contenido).then(canvas => {  
      var imgWidth = 208;   
      var imgHeight = canvas.height * imgWidth / canvas.width;  
  
      // const contentDataURL = canvas.toDataURL('image/png');
      // const contentDataURL = canvas.toDataURL('image/jpeg', 0.8);
      const contentDataURL = canvas.toDataURL('image/jpeg', 1);

      if(!this.PDFMAS)
        // this.PDFMAS = new jsPDF('p', 'mm', 'letter', 1000);
        this.PDFMAS = new jsPDF('p', 'mm', 'letter', 1);

      // this.PDFMAS.addImage(contentDataURL, 'PNG', 2, 2, imgWidth, imgHeight, undefined, 'SLOW');
      this.PDFMAS.addImage(contentDataURL, 'JPEG', 2, 2, imgWidth, imgHeight, undefined, 'SLOW');

      // let pdffile = "dsadfdsff";
      let pdffile;
      if(listo && guardar){
        if(email){          
          var blobPDF = this.dataURLtoBlob(this.PDFMAS.output('datauristring'));

          // let file = this.PDFMAS.output('blob');
          // pdffile = new FormData();     // To carry on your data  
          // pdffile.append('FILE', file);
          // console.log(pdffile, file); 

          // pdffile = this.PDFMAS.output('datauristring');
          pdffile = this.PDFMAS.output('datauri');
          // pdffile = this.PDFMAS.output('blob');
          var FD = new FormData();
          // FD.append("PDF", pdffile);
          // pdffile = this.dataURLtoBlob(this.PDFMAS.output('datauri'));
          // pdffile = this.PDFMAS.output('datauristring').replace(/^[^,]*,/, '');
        }
        else{
          if(imprimir)
            this.PDFMAS.autoPrint();
          this.PDFMAS.save('PDF.pdf'); // Generated PDF
        }
        // this.PDFMAS.
        this.PDFMAS = null;
      }

      if(this.PDFMAS)
        this.PDFMAS.addPage();
      // else{
      //   pdffile = this.PDFMAS.output('datauristring');
      // }
      if(alTerminar)
        // alTerminar(contentDataURL, contentDataURL.split(',')[1], pdffile, pdffile.split(',')[1]);
        alTerminar(contentDataURL, contentDataURL.split(',')[1], pdffile, FD);
    });  

  }

  dataURLtoBlob(dataurl) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], {type:mime});
  }

  documento(caso, datos, imprimir = false, alTerminar = null){
    this.HTTP.documento(caso, datos).subscribe(
      datos => {
        console.log(datos);
        const PDF = new jsPDF('p', 'pt', 'letter');
        // PDF.text("Texto", 10, 10);
        if(imprimir)
          PDF.autoPrint();

        PDF.fromHTML(datos.HTML, 10, 10, {
          
        }, () => {PDF.save('PDF.pdf');});
        // PDF.output("dataurlnewwindow");
        // PDF.save("PDF.pdf");

        if(alTerminar)
          alTerminar();
      },

      error => {

      }
    );
  }

  email(para, caso, datos, alTerminar = null){
  	// let contenido = "<html>"
  	    // contenido += "    <head>"
        // contenido += "      <style>"
      //   contenido += "      	@import url('../../assets/css/bootstrap.min.css');"
		    // contenido += "				@import url('https://fonts.googleapis.com/css?family=Roboto:100,400,700,900');"
      //   contenido += "      	@import url('../../app/cotizacion-confirmacion/cotizacion-confirmacion.component.css');"
        // contenido += "      </style>";
        // contenido += "    </head>";
        // contenido += "		<body>"+document.getElementById('area-impresion').innerHTML+"</body>";
        // contenido += "</html>";

        // let contenido = document.getElementById('area-impresion').innerHTML;

  	this.HTTP.email(para, caso, datos)
  		.subscribe(
  			datos => {
  				console.log(datos);

          if(datos.Enviado)
            this.Alerta.open("Correo enviado", 'Cerrar');
          else            
            this.Alerta.open("Correo no enviado", 'Cerrar');
          
          if(alTerminar)
            alTerminar(true);
  			},

  			error => {
  				console.log("Error mail", error);
          if(alTerminar)
            alTerminar(false);

  			}
    	);
    // window.open("mailto:example@domain.com?subject=Test&body=Test");
    // window.open("mailto:example@domain.com?subject=Test&body=Test&attachments='" + datos.Archivo + "'");
    
  }

  mail(para, contenido, alTerminar = null){
  	this.HTTP.mail(para, contenido)
  		.subscribe(
  			datos => {
  				console.log(datos);

          if(datos.Enviado)
            this.Alerta.open("Correo enviado", 'Cerrar');
          else            
            this.Alerta.open("Correo no enviado", 'Cerrar');
          
          if(alTerminar)
            alTerminar();
  			},

  			error => {
  				console.log("Error mail", error);

  			}
  		);
  }

  lzw_encode(s) {
    // var dict = {};
    // var data = (s + "").split("");
    // var out = [];
    // var currChar;
    // var phrase = data[0];
    // var code = 256;
    // for (var i=1; i<data.length; i++) {
    //     currChar=data[i];
    //     if (dict[phrase + currChar] != null) {
    //         phrase += currChar;
    //     }
    //     else {
    //         out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0));
    //         dict[phrase + currChar] = code;
    //         code++;
    //         phrase=currChar;
    //     }
    // }
    // out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0));
    // for (var i=0; i<out.length; i++) {
    //     out[i] = String.fromCharCode(out[i]);
    // }
    // return out.join("");

    "use strict";
    // Build the dictionary.
    var i,
        dictionary = {},
        c,
        wc,
        w = "",
        result = [],
        dictSize = 256;
    for (i = 0; i < 256; i += 1) {
        dictionary[String.fromCharCode(i)] = i;
    }

    for (i = 0; i < s.length; i += 1) {
        c = s.charAt(i);
        wc = w + c;
        //Do not use dictionary[wc] because javascript arrays 
        //will return values for array['pop'], array['push'] etc
      // if (dictionary[wc]) {
        if (dictionary.hasOwnProperty(wc)) {
            w = wc;
        } else {
            result.push(dictionary[w]);
            // Add wc to the dictionary.
            dictionary[wc] = dictSize++;
            w = String(c);
        }
    }

    // Output the code for w.
    if (w !== "") {
        result.push(dictionary[w]);
    }
    return result;
  }
  

}
