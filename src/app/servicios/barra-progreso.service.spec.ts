import { TestBed, inject } from '@angular/core/testing';

import { BarraProgresoService } from './barra-progreso.service';

describe('BarraProgresoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BarraProgresoService]
    });
  });

  it('should be created', inject([BarraProgresoService], (service: BarraProgresoService) => {
    expect(service).toBeTruthy();
  }));
});
