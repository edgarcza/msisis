import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {Router} from "@angular/router";

import { HTTPService } from '../http.service'
import { BarraProgresoService } from './barra-progreso.service';
import {MatSnackBar} from '@angular/material';

import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DatosFormularioService {

  constructor(
  	private HTTP: HTTPService,
  	public Alerta: MatSnackBar,
    private Router: Router,
    public Progreso: BarraProgresoService) { }

  // COLONIAS

  public Colonias = [''];
  public filtradoColonias = [""];

  //PAÍSES  
  public Paises = [""];
  filtradoPaises: Observable<any>;

  public TiposSeguro = [""];

  public TiposPoliza = [""];

  public FuentesReferencia = [""];

  public Roles = [""];

  public Distribuidores = [""];

  public Agentes = [""];

  public Cuentas = [""];

  public Forma_Pago = [""];

  public FormDatos = [""];


  obtenerDatos(Datos, pre = ""){
  	if(Datos[pre + 'cp'].toString() === "" || Datos[pre + 'id_pais'] != 1)
  		return 0;
  	if(Datos[pre + 'cp'].toString().length == 5){
  		console.log(Datos[pre + 'cp']);
  		this.HTTP.cp_buscarDatos(Datos[pre + 'cp'])
  		.subscribe(
  			datos => {
  				console.log(datos);
  				Datos[pre + 'ciudad'] = datos.municipio;
  				Datos[pre + 'estado'] = datos.estado;
  				this.Colonias = datos.colonias;
  			},

  			error => {
  				console.log(error);
  			}
  		);
  	}
  }

  public InsertID = -1;

  guardar(tabla, datos, restricciones: any = "", alTerminar = null, conMensaje = true){
  	this.Progreso.CargandoGlobal = true;
  	if(("nombre_completo" in datos))
    	datos.nombre_completo = datos.nombres + " " + datos.apellidos;
    console.log(datos);
		this.HTTP.guardar(tabla, datos, restricciones).subscribe(
			resultado => {
				console.log(resultado)
				if(resultado.guardado){
					if(conMensaje)
      			this.Alerta.open(tabla.toUpperCase() + ' guardado', 'Cerrar');
      		if(alTerminar)
      			alTerminar(resultado.id);
				}
				else{
					console.log(resultado.error);
					if(conMensaje)
						this.Alerta.open("Error: " + resultado.error, 'Cerrar');
				}
				// console.log(resultado);
  			this.Progreso.CargandoGlobal = false;
			},
			error => {console.log(error);this.Progreso.CargandoGlobal = false;}
		);
  }

  borrar(tabla, datos, alTerminar = function(){}){
		this.HTTP.borrar(tabla, datos).subscribe(
			resultado => {
				if(resultado.borrado){
      		this.Alerta.open(tabla.toUpperCase() + ' borrado', 'Cerrar');
				}
				else{
					console.log(resultado.error);
					this.Alerta.open("Error al borrar", 'Cerrar');
				}
				// console.log(resultado);
      		alTerminar();
			},
			error => {console.log(error);
      		alTerminar();}
		);
  }

  borrarCond(tabla, condiciones, alTerminar = function(){}){
		this.HTTP.borrarCond(tabla, condiciones).subscribe(
			resultado => {
				if(resultado.borrado){
      		// this.Alerta.open(tabla.toUpperCase() + ' borrado', 'Cerrar');
      		console.log("Borrado");
      		alTerminar();
				}
				else{
					console.log(resultado.error);
					// this.Alerta.open("Error al borrar", 'Cerrar');
				}
				// console.log(resultado);
			},
			error => {console.log(error);      		alTerminar();}
		);
  }

  buscar(tabla, campos, condicion, filas){
  	this.HTTP.buscar(tabla, campos, condicion, filas).subscribe(
  		resultado => {
  			// console.log("Buscar", resultado);
  			// variable = resultado[0];
  			// console.log(variable);
  		},

  		error => {
  			console.log(error);
  		}
  	);
  }

  alerta(texto, cerrar) {
    this.Alerta.open(texto, cerrar);
  }

  limpiar(variable){
  	for(var Clave in variable)
  		variable[Clave] = null;
  	// return variable;
  }

  coberturaPoliza(tipoSeguro, dias, zonaOrigen, zonaDestino, datos, alTerminar = null){  	
    this.HTTP.coberturaPoliza(tipoSeguro, dias, zonaOrigen, zonaDestino).subscribe(
      resultado => {
		console.log(resultado);
        this.FormDatos['Tipo_Poliza'] = resultado.Coberturas;
        datos.numero_dias = resultado.Datos.Dias;
				datos.numero_semanas_meses = Math.floor(resultado.Datos.SemanasMeses);
		if(alTerminar)
			alTerminar();
      },

      error => {
      	console.log(error);
      }
    );
  }

  obtener(tabla: string, valor: any = '', alTerminar = null){
  	// console.log(valor);
		this.HTTP.dformulario(tabla, valor).subscribe(
			datos => {
				this.FormDatos[tabla] = datos;

				if(alTerminar)
					alTerminar();
				// console.log(this.FormDatos);
			},
			error => {console.log(error);}
		);
  }

  subirExcel(archivo, tabla, datos = null, alTerminar = null){
  	console.log(datos);

  	// let file = {filename: archivo.name, filetype: archivo.type, value: }
		this.Progreso.CargandoGlobal = true;
		
		// this.

    this.HTTP.subirExcel(archivo, tabla, datos).subscribe(
	      datos => {
  				this.Progreso.CargandoGlobal = false;
					location.reload();
					console.log("Datos", datos);
					if(datos.subido)
					{
						if(alTerminar) alTerminar();
						this.alerta("Subido", "Cerrar");
					}
					else
					{
						this.alerta("No se pudo subir", "Cerrar");
					}
	      }, 

	      error => {
	        console.log("Error", error);
	        if(alTerminar) alTerminar();
  				this.Progreso.CargandoGlobal = false;
					this.alerta("Hay un error en el excel", "Cerrar");
	      }
      );
  }

  subirArchivo(archivo, carpeta, nombre, alTerminar = null){
  	// console.log(archivo);

  	// let file = {filename: archivo.name, filetype: archivo.type, value: }
  	this.Progreso.CargandoGlobal = true;

    this.HTTP.subirArchivo(archivo, carpeta, nombre).subscribe(
	      datos => {
	        // console.log("Datos", datos);
	        if(alTerminar) alTerminar();
  				this.Progreso.CargandoGlobal = false;
	      }, 

	      error => {
	        // console.log("Error", error);
	        if(alTerminar) alTerminar();
  				this.Progreso.CargandoGlobal = false;
	      }
      );
  }

  onSelectFile(event, fileObj, alTerminar = null) {
    if (event.target.files && event.target.files[0]) {
      let file = event.target.files[0];
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = (event) => {
        fileObj.URL_TMP = reader.result;

        fileObj.Formato = file.name.split(".").pop();

        fileObj.Archivo = {
          filename: file.name,
          filetype: file.type,
          value: reader.result.split(',')[1]
        }

        fileObj.Nombre = file.name;

        if(alTerminar)
        	alTerminar();

      }
    }
  }

  formElementoPorId(tabla: string, id: any){
  	for(var Clave in this.FormDatos[tabla]) {
		  if(this.FormDatos[tabla][Clave]['id'] == id)
		  	return Clave;
		}
		return -1;
  }

  formValorPorId(tabla: string, id: any){
  	for(var Clave in this.FormDatos[tabla]) {
  		// console.log(this.FormDatos[tabla][Clave][tabla.], id);
		  if(this.FormDatos[tabla][Clave]['id'] == id)
		  	return this.FormDatos[tabla][Clave][tabla.toLowerCase()];
		}
		return "";
  }

  formValorPPorId(tabla: string, valor: string, id: any){
  	for(var Clave in this.FormDatos[tabla]) {
  		// console.log(this.FormDatos[tabla][Clave][tabla.], id);
		  if(this.FormDatos[tabla][Clave]['id'] == id)
		  	return this.FormDatos[tabla][Clave][valor];
		}
		return "";
  }

  obtenerElementoPor(clave: string, Arreglo: any, valor: any){
  	for(var Subarreglo in Arreglo) {
  		// console.log(Arreglo[Subarreglo][clave], valor);
		  if(Arreglo[Subarreglo][clave] === valor)
		  	return Subarreglo;
		}
		return -1;
  }

	redondear(value, precision) {
	    var multiplier = Math.pow(10, precision || 0);
	    return Math.round(value * multiplier) / multiplier;
	}

	aFechaSQL(fecha: Date){
		fecha = new Date(fecha);
		return fecha.getFullYear() + "-" + (fecha.getMonth() + 1) + "-" + fecha.getDate();
	}

	parseDate(input) {
	  var parts = input.match(/(\d+)/g);
	  // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
	  return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
	}


  // ESTADOS

  public MX_Estados = [
		{
			"nombre": "Aguascalientes",
			"codigo": "MX-AGU",
			"subdivision": "estado"
		},
		{
			"nombre": "Baja California",
			"codigo": "MX-BCN",
			"subdivision": "estado"
		},
		{
			"nombre": "Baja California Sur",
			"codigo": "MX-BCS",
			"subdivision": "estado"
		},
		{
			"nombre": "Campeche",
			"codigo": "MX-CAM",
			"subdivision": "estado"
		},
		{
			"nombre": "Chiapas",
			"codigo": "MX-CHP",
			"subdivision": "estado"
		},
		{
			"nombre": "Chihuahua",
			"codigo": "MX-CHH",
			"subdivision": "estado"
		},
		{
			"nombre": "Ciudad de México",
			"codigo": "MX-DIF",
			"subdivision": "estado"
		},
		{
			"nombre": "Coahuila",
			"codigo": "MX-COA",
			"subdivision": "estado"
		},
		{
			"nombre": "Colima",
			"codigo": "MX-COL",
			"subdivision": "estado"
		},
		{
			"nombre": "Durango",
			"codigo": "MX-DUR",
			"subdivision": "estado"
		},
		{
			"nombre": "Guanajuato",
			"codigo": "MX-GUA",
			"subdivision": "estado"
		},
		{
			"nombre": "Guerrero",
			"codigo": "MX-GRO",
			"subdivision": "estado"
		},
		{
			"nombre": "Hidalgo",
			"codigo": "MX-HID",
			"subdivision": "estado"
		},
		{
			"nombre": "Jalisco",
			"codigo": "MX-JAL",
			"subdivision": "estado"
		},
		{
			"nombre": "Michoacán",
			"codigo": "MX-MIC",
			"subdivision": "estado"
		},
		{
			"nombre": "Morelos",
			"codigo": "MX-MOR",
			"subdivision": "estado"
		},
		{
			"nombre": "México",
			"codigo": "MX-MEX",
			"subdivision": "estado"
		},
		{
			"nombre": "Nayarit",
			"codigo": "MX-NAY",
			"subdivision": "estado"
		},
		{
			"nombre": "Nuevo León",
			"codigo": "MX-NLE",
			"subdivision": "estado"
		},
		{
			"nombre": "Oaxaca",
			"codigo": "MX-OAX",
			"subdivision": "estado"
		},
		{
			"nombre": "Puebla",
			"codigo": "MX-PUE",
			"subdivision": "estado"
		},
		{
			"nombre": "Querétaro",
			"codigo": "MX-QUE",
			"subdivision": "estado"
		},
		{
			"nombre": "Quintana Roo",
			"codigo": "MX-ROO",
			"subdivision": "estado"
		},
		{
			"nombre": "San Luis Potosí",
			"codigo": "MX-SLP",
			"subdivision": "estado"
		},
		{
			"nombre": "Sinaloa",
			"codigo": "MX-SIN",
			"subdivision": "estado"
		},
		{
			"nombre": "Sonora",
			"codigo": "MX-SON",
			"subdivision": "estado"
		},
		{
			"nombre": "Tabasco",
			"codigo": "MX-TAB",
			"subdivision": "estado"
		},
		{
			"nombre": "Tamaulipas",
			"codigo": "MX-TAM",
			"subdivision": "estado"
		},
		{
			"nombre": "Tlaxcala",
			"codigo": "MX-TLA",
			"subdivision": "estado"
		},
		{
			"nombre": "Veracruz",
			"codigo": "MX-VER",
			"subdivision": "estado"
		},
		{
			"nombre": "Yucatán",
			"codigo": "MX-YUC",
			"subdivision": "estado"
		},
		{
			"nombre": "Zacatecas",
			"codigo": "MX-ZAC",
			"subdivision": "estado"
		}
	];

}
