import { Component, OnInit } from '@angular/core';
import { HTTPService }    from '../http.service';

import {VentanasService} from '../servicios/ventanas.service'
import {DocumentosService} from '../servicios/documentos.service'
import {SesionService} from '../sesion.service'
import { DatosFormularioService } from '../servicios/datos-formulario.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDatepickerInputEvent, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS} from '../clases/FechaAdaptador';

@Component({
  selector: 'app-asegurados',
  templateUrl: './asegurados.component.html',
  styleUrls: ['./asegurados.component.css'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class AseguradosComponent implements OnInit {

  constructor(
    public ventana: VentanasService,
    public Formulario: DatosFormularioService,
    private Ruta: ActivatedRoute,
    private Link: Router,
    private HTTP: HTTPService,
    private Documento: DocumentosService,
    public sesion: SesionService) { }

  public IVASeguro = false;

  // public DatosSeguroC = {
  //   id: -1,
  //   liquidado: null,
  //   id_cliente: null,
  //   activo_inactivo: 1,
  //   id_tipo_seguro: null,
  //   id_subagente: null,
  //   origen: null,
  //   destino: null,
  //   salida: null,
  //   regreso: null,
  //   codigo_promocion: null,
  //   id_tipo_poliza: null,
  //   numero_dias: null,
  //   numero_semanas_meses: null,
  //   precioMXN: 0,
  //   precioMXN_descuento: 0,
  //   precioMXN_descuentoSA: 0,
  //   precioMXN_ajuste: 0,
  //   precioMXN_iva: 0,
  //   precioMXN_subtotal: 0,
  //   precioMXN_comision: 0,
  //   precioMXN_total: 0,
  //   precioUSD: 0,
  //   precioUSD_descuento: 0,
  //   precioUSD_descuentoSA: 0,
  //   precioUSD_ajuste: 0,
  //   precioUSD_iva: 0,
  //   precioUSD_subtotal: 0,
  //   precioUSD_comision: 0,
  //   precioUSD_total: 0,
  //   tipo_cambio: 18,
  //   descripcion: null,
  //   fecha_hecha: null,
  //   folio: null,
  //   comentario: null,
  //   saldo_liquidado_MXN: null,
  //   saldo_liquidado_USD: null,
  //   saldo_pendiente_MXN: null,
  //   saldo_pendiente_USD: null,
  //   destino_cp: null,
  //   destino_ciudad: null,
  //   origen_cp: null,
  //   extendido: null,
  //   origen_ciudad: null,
  //   estado_t: null,
  // };
  public DatosSeguro = {
    id: -1,
    liquidado: null,
    id_cliente: null,
    activo_inactivo: 1,
    id_tipo_seguro: null,
    id_subagente: null,
    origen: null,
    destino: null,
    salida: null,
    regreso: null,
    codigo_promocion: null,
    id_tipo_poliza: null,
    numero_dias: null,
    numero_semanas_meses: null,
    precioMXN: null,
    precioMXN_descuento: 0,
    precioMXN_descuentoSA: 0,
    precioMXN_ajuste: 0,
    precioMXN_iva: 0,
    precioMXN_subtotal: 0,
    precioMXN_comision: 0,
    precioMXN_total: 0,
    precioUSD: 0,
    precioUSD_descuento: 0,
    precioUSD_descuentoSA: 0,
    precioUSD_ajuste: 0,
    precioUSD_iva: 0,
    precioUSD_subtotal: 0,
    precioUSD_comision: 0,
    precioUSD_total: 0,
    tipo_cambio: 18,
    descripcion: null,
    fecha_hecha: null,
    folio: null,
    comentario: null,
    saldo_liquidado_MXN: null,
    saldo_liquidado_USD: null,
    saldo_pendiente_MXN: null,
    saldo_pendiente_USD: null,
    destino_cp: null,
    destino_ciudad: null,
    origen_cp: null,
    extendido: null,
    origen_ciudad: null,
    estado_t: null,
    comision: null,
    descuento: null,
  };

  DatosAdicionales = {
    precioUSD_descuento_pre: null,   
    precioMXN_descuento_pre: null,    
  };

   DatosCliente = {
    id: null,
    id_subagente: null,
    id_rol: null,
    id_tipo_cliente: null,
    activo_inactivo: null,
    fecha_registro_prospecto: null,
    universidad: null,
    fecha_registro_asegurado: null,
    nombres: null,
    apellidos: null,
    nombre_completo: null,
    correo_electronico: null,
    contrasena: null,
    telefono: null,
    fecha_nacimiento: null,
    id_fuente_referencia: null,
    id_pais: null,
    estado: null,
    cp: null,
    ciudad: null,
    calle_numero: null,
    razon_social: null,
    rfc: null,
    fac_metodo: null,
    fac_forma: null,
    fac_correo_electronico: null,
    fac_telefono: null,
    fac_id_pais: null,
    fac_estado: null,
    fac_cp: null,
    fac_ciudad: null,
    fac_calle_numero: null,
    colonia: null,
    fac_colonia: null,
    id_cliente: null,
    id_cotizacion: null,
    sexo: null
  };

  DatosPrecio = {
    precio: null,
    descuento: null,
    ajuste: null,
    iva: null
  };

  DatosDistribuidor = {
    descuento: null,
  };

  DatosSubagente = {
    comision: null,
    descuento: null,
  };

  DatosSeguros = [{}];

  DatosExtra = {    
    fecha_asegurado: null,
    cliente: null,
    precioUSD: null,
    precioUSD_descuento: null,
    precioUSD_ajuste: null,
    precioUSD_iva: null,
    precioUSD_subtotal: null,
    precioUSD_comision: null,
    precioUSD_total: null,
    precioMXN_total: null,
    costo: null,
    dias: null,
    costo_final: null,
    margen_usd: null,
    costo_final_MXN: null,
    margen_MXN: null,
    fecha_nacimiento: null,
  }

  Codigo_Poliza = "";
  Zona_Pais = null;
  Folio = null;
  LiquidarPagos = true;
  public Idioma = 'ES';

  obtenerCotizacion(){
    // Datos del cliente cotizaciÃ³n final
    // NOT folio = ''
    // "(id = ? or id = ?)", Array(6,2)
    // console.log(this.DatosSeguro)
    this.HTTP.datosDe("cotizacion", [{campo: "id_cliente", valor: this.DatosCliente.id}, {campo: "NOT folio", valor: ''}])
    // this.HTTP.datosDe("cotizacion")
      .subscribe(
        datos => {
          // this.DatosSeguro = datos[0];
          // this.DatosSeguro = {};
          this.DatosSeguro = JSON.parse(JSON.stringify(datos[0]));
          this.DatosSeguro = JSON.parse(JSON.stringify(datos[0]));
          // this.DatosSeguroC = JSON.parse(JSON.stringify(datos[0]));
          let datoss = JSON.parse(JSON.stringify(datos[0]));
          // this.DatosSeguro = JSON.parse(datoss);
          // this.DatosSeguro = JSON.parse(JSON.stringify(datoss));
          // Object.assign(this.DatosSeguro, this.DatosSeguroC);
          // console.log(datos)
          // console.log(JSON.parse(JSON.stringify(datos)))
          // console.log(JSON.parse(JSON.stringify(datos[0])))
          // console.log(this.DatosSeguro, datoss, this.DatosSeguroC)
          // console.log(this.DatosSeguro)
          this.DatosSeguros = datos;
          // this.Codigo_Poliza = this.Formulario.FormDatos['Tipo_Poliza'][this.Formulario.formElementoPorId("Tipo_Poliza", this.DatosSeguro.id_tipo_poliza)]['codigo_poliza'];
          // console.log(this.Formulario.FormDatos['Tipo_Poliza'][this.Formulario.formElementoPorId("Tipo_Poliza", this.DatosSeguro.id_tipo_poliza)]);
          this.Codigo_Poliza = this.Formulario.FormDatos['Tipo_Poliza'][this.Formulario.formElementoPorId("Tipo_Poliza", this.DatosSeguro.id_tipo_poliza)]['a_codigo'] + this.Formulario.FormDatos['Tipo_Poliza'][this.Formulario.formElementoPorId("Tipo_Poliza", this.DatosSeguro.id_tipo_poliza)]['codigo_poliza'];
          // console.log(this.Codigo_Poliza)
          this.Folio = this.DatosSeguro.id;
          // console.log(this.DatosSeguro);
          // console.log(this.Folio);

          if(this.VAR && this.VAR >= 0){
            this.Folio = this.VAR;
            this.cambioSeguro();
          }
          else
            this.setSeguro();
          

        },

        error => {
          console.log(error);
        }
      );
  }

  VerDatosDatos = {
    tipo_seguro: '- - - - -',
    origen: '- - - - -',
    destino: '- - - - -',
    salida: null,
    regreso: null,
    tipo_poliza: '- - - - -',
    codigo: '- - - - -',
    tiempo: '- - - - -',
  };

  setSeguro(){
    // this.DatosSeguro.salida = this.Formulario.parseDate(this.DatosSeguro.salida);
    // this.DatosSeguro.regreso = this.Formulario.parseDate(this.DatosSeguro.regreso);
    // console.log(this.Datos);
    // console.log(this.Formulario.FormDatos['Tipo_Poliza']);

    this.DatosSeguro.salida = new Date(new Date(this.DatosSeguro.salida).getTime() + 18000000);
    this.DatosSeguro.regreso = new Date(new Date(this.DatosSeguro.regreso).getTime() + 18000000);

    
    this.VerDatosDatos.tipo_seguro = this.Formulario.FormDatos['Tipo_Seguro'][this.Formulario.formElementoPorId("Tipo_Seguro", this.DatosSeguro.id_tipo_seguro)]['tipo_seguro'];
    this.VerDatosDatos.origen = this.Formulario.FormDatos['Pais'][this.Formulario.formElementoPorId("Pais", this.DatosSeguro.origen)]['pais'];
    this.VerDatosDatos.destino = this.Formulario.FormDatos['Pais'][this.Formulario.formElementoPorId("Pais", this.DatosSeguro.destino)]['pais'];

    let Fecha = new Date(this.DatosSeguro.salida);
    // this.VerDatosDatos.salida = this.Formulario.aFechaSQL(this.DatosSeguro.salida);
    this.VerDatosDatos.salida = `${Fecha.getDate()}/${Fecha.getMonth() + 1}/${Fecha.getFullYear()}`;
    Fecha = new Date(this.DatosSeguro.regreso);
    // this.VerDatosDatos.regreso = this.Formulario.aFechaSQL(this.DatosSeguro.regreso);
    this.VerDatosDatos.regreso = `${Fecha.getDate()}/${Fecha.getMonth() + 1}/${Fecha.getFullYear()}`;
    
    this.VerDatosDatos.tipo_poliza = this.Formulario.FormDatos['Tipo_Poliza'][this.Formulario.formElementoPorId("Tipo_Poliza", this.DatosSeguro.id_tipo_poliza)]['tipo_poliza'];
    this.VerDatosDatos.codigo = this.DatosSeguro.codigo_promocion;

    console.log(this.VerDatosDatos, this.DatosSeguro)

    this.obtenerTiposPoliza();
    this.obtenerExtras();
    this.obtenerComision();

    this.Zona_Pais = this.Formulario.FormDatos['Pais'][this.Formulario.formElementoPorId("Pais", this.DatosSeguro.destino)]['zona'];
    this.DSM = this.Formulario.FormDatos['Tipo_Poliza'][this.Formulario.formElementoPorId("Tipo_Poliza", this.DatosSeguro.id_tipo_poliza)]['dsm'];

    // this.actualizarDatosPrecio();
    // this.DatosSeguro.numero_semanas_meses = Math.floor(this.DatosSeguro.numero_semanas_meses);
    // console.log(this.DatosSeguro.numero_semanas_meses);

    this.obtenerPagos();
    this.obtenerIncidentes();
    // this.actualizarDatosPrecio();

    if(this.DatosSeguro.destino == 4)
      this.Idioma = 'AL';
    else if(this.DatosSeguro.destino == 81)
      this.Idioma = 'FR';
    else if(this.DatosSeguro.destino == 73)
      this.Idioma = 'EN';
    else if(this.DatosSeguro.destino == 72)
      this.Idioma = 'ES';
    else
      this.Idioma = 'EN';

    // this.cambioTipoPoliza();
  }

  obtenerDistribuidor(){
    this.HTTP.distribuidor("subagente", this.DatosCliente.id_subagente)
      .subscribe(
        datos => {
          this.DatosDistribuidor = datos.Distribuidor[0];
          this.actualizarDatosPrecio();
          // console.log(this.DatosDistribuidor);
        },

        error => {
          console.log(error);
        }
      );
  }

  obtenerSubagente(){
    this.HTTP.datosDe("subagente", [{campo: "id", valor: this.DatosCliente.id_subagente}])
      .subscribe(
        datos => {
          this.DatosSubagente = datos[0];

          //PRECIOS
          // if(this.DatosSeguro.precioMXN_total == 0){
            this.actualizarDatosPrecio();
          // }
          this.DatosComision.comision = (this.DatosSubagente.comision / 100) * this.DatosSeguro.precioMXN;
          this.DatosComision.impuestos = 0;
          for(var Num in this.DatosImpuestos){
            this.DatosComision.impuestos += (this.DatosImpuestos[Num].porcentaje / 100) * this.DatosComision.comision;
          }
          this.DatosComision.comision_final = this.DatosComision.comision - this.DatosComision.impuestos;
          this.DatosComision.folio_seguro = this.DatosSeguro.folio;
          this.DatosComision.id_subagente = this.DatosSeguro.id_subagente;
          // console.log("asd");
            this.Formulario.guardar("comision", this.DatosComision, null, 
              () => {
                // this.Rutas.navigate(['Asegurados', this.DatosCliente.id]);
              }, false)
            this.DSM = this.Formulario.FormDatos['Tipo_Poliza'][this.Formulario.formElementoPorId("Tipo_Poliza", this.DatosSeguro.id_tipo_poliza)]['dsm'];
          },

        error => {
          console.log(error);
        }
      );
  }

  // public DatosExtra;
  obtenerExtras(){
    let condCampo = "cotizacion.precioUSD";
    if(this.DatosSeguro.estado_t == 2)
      condCampo = "(cotizacion.precioUSD * -1)";
    this.HTTP.paginar("pagMargen", 0, 1, null, false, [{Campo: "cotizacion.id", Valor: this.DatosSeguro.id}, {Campo: condCampo, Valor: "cobertura.precio", Operador: "=="}])
      .subscribe(
        datos => {
          console.log(datos);
          this.DatosExtra = datos.Filas[0];
          this.DatosExtra.dias = this.DatosSeguro.numero_dias;
          this.DatosExtra.costo_final_MXN = this.Formulario.redondear((this.DatosExtra.costo_final * this.DatosSeguro.tipo_cambio), 2);
          this.DatosExtra.margen_MXN = this.Formulario.redondear(((this.DatosExtra.precioUSD_total - this.DatosExtra.costo_final) * this.DatosSeguro.tipo_cambio), 2);
          this.DatosExtra.fecha_nacimiento = new Date(new Date(this.DatosCliente.fecha_nacimiento).getTime() + 1000 * 60 * 60 * 24);
        },

        error => {
          console.log(error);
        }
      );
  }

  Diferencia = {
    Segundos: null,
    Minutos: null,
    Horas: null,
    Dias: null,
    Semanas: null,
  };

  asignarDiferencia(){
    // Calcudora de dÃ­as
    this.DatosSeguro.regreso = new Date(this.DatosSeguro.regreso);
    this.DatosSeguro.salida = new Date(this.DatosSeguro.salida);
    this.Diferencia = {
      Segundos: 
        ((this.DatosSeguro.regreso.getTime() - this.DatosSeguro.salida.getTime())/1000),
      Minutos: 
        (((this.DatosSeguro.regreso.getTime() - this.DatosSeguro.salida.getTime())/1000) / 60),
      Horas: 
        ((((this.DatosSeguro.regreso.getTime() - this.DatosSeguro.salida.getTime())/1000) / 60) / 60),
      Dias: 
        ((((((this.DatosSeguro.regreso.getTime() - this.DatosSeguro.salida.getTime())/1000) / 60) / 60) / 24) + 1),
      Semanas:
        ((((((this.DatosSeguro.regreso.getTime() - this.DatosSeguro.salida.getTime())/1000) / 60) / 60) / 24) / 7)
    }
  }

  // cambioIVA(){
  //   this.DatosSeguro.precioMXN_iva = this.DatosSeguro.precioMXN_subtotal - this.DatosSeguro.precioMXN_comision;
  // }
  public Descuento = 0;
  public Comision = 0;
  actualizarDatosPrecio(){
    if(this.DatosSeguro.estado_t == 2) return 1;
    // console.log(this.DatosSeguro.precioMXN);
    if(!this.DatosSeguro.precioMXN) return 1;

    // this.DatosAdicionales.precioUSD_descuento_pre = this.Formulario.redondear((this.DatosDistribuidor.descuento / 100) * this.DatosSeguro.precioUSD, 2);
    // this.DatosSeguro.precioUSD_descuento = this.Formulario.redondear((this.DatosSeguro.precioUSD * (1-(this.DatosDistribuidor.descuento/100))), 2);

    // this.DatosAdicionales.precioMXN_descuento_pre = this.Formulario.redondear((this.DatosDistribuidor.descuento / 100) * this.DatosSeguro.precioMXN, 2);
    // this.DatosSeguro.precioMXN_descuento = this.Formulario.redondear((this.DatosSeguro.precioMXN * (1-(this.DatosDistribuidor.descuento/100))), 2);

    this.Descuento = this.DatosSubagente.descuento;
    if(this.DatosSeguro.descuento != 0 || this.DatosSeguro.comision != 0)
      this.Descuento = this.DatosSeguro.descuento;

    // this.DatosAdicionales.precioUSD_descuento_pre = this.Formulario.redondear((this.DatosSubagente.descuento / 100) * this.DatosSeguro.precioUSD, 2);
    // this.DatosSeguro.precioUSD_descuento = this.Formulario.redondear((this.DatosSeguro.precioUSD * (1-(this.DatosSubagente.descuento/100))), 2);

    // this.DatosAdicionales.precioMXN_descuento_pre = this.Formulario.redondear((this.DatosSubagente.descuento / 100) * this.DatosSeguro.precioMXN, 2);
    // this.DatosSeguro.precioMXN_descuento = this.Formulario.redondear((this.DatosSeguro.precioMXN * (1-(this.DatosSubagente.descuento/100))), 2);

    this.DatosAdicionales.precioUSD_descuento_pre = this.Formulario.redondear((this.Descuento / 100) * this.DatosSeguro.precioUSD, 2);
    this.DatosSeguro.precioUSD_descuento = this.Formulario.redondear((this.DatosSeguro.precioUSD * (1-(this.Descuento/100))), 2);

    this.DatosAdicionales.precioMXN_descuento_pre = this.Formulario.redondear((this.Descuento / 100) * this.DatosSeguro.precioMXN, 2);
    this.DatosSeguro.precioMXN_descuento = this.Formulario.redondear((this.DatosSeguro.precioMXN * (1-(this.Descuento/100))), 2);
    // console.log(this.DatosAdicionales);

    this.DatosSeguro.precioMXN_descuentoSA = this.DatosAdicionales.precioMXN_descuento_pre;
    this.DatosSeguro.precioUSD_descuentoSA = this.DatosAdicionales.precioUSD_descuento_pre;


    if(this.IVASeguro){
      this.DatosSeguro.precioMXN_iva = this.Formulario.redondear(this.DatosSeguro.precioMXN * 0.16, 2);
      this.DatosSeguro.precioUSD_iva = this.Formulario.redondear(this.DatosSeguro.precioUSD * 0.16, 2);
    }
    else{
      this.DatosSeguro.precioMXN_iva = 0;
      this.DatosSeguro.precioUSD_iva = 0;
    }

    this.Comision = this.Formulario.redondear(this.DatosSubagente.comision, 0);
    // console.log(this.Comision)
    console.log(this.DatosSubagente)
    if(this.DatosSeguro.comision != 0 || this.DatosSeguro.descuento != 0)
      this.Comision = this.Formulario.redondear(this.DatosSeguro.comision, 0);

    this.DatosSeguro.precioUSD_subtotal = this.Formulario.redondear(this.DatosSeguro.precioUSD_descuento - this.DatosSeguro.precioUSD_ajuste + this.DatosSeguro.precioUSD_iva, 2);
    this.DatosSeguro.precioUSD_comision = this.Formulario.redondear(this.DatosSeguro.precioUSD * (this.Comision / 100), 2);
    console.log(this.DatosSeguro.precioUSD +"* ("+this.Comision +"/ 100)")
    this.DatosSeguro.precioUSD_total = this.DatosSeguro.precioUSD_subtotal - this.DatosSeguro.precioUSD_comision;
    // this.DatosSeguro.precioUSD_total = this.Formulario.redondear(this.DatosSeguro.precioUSD_subtotal, 2);

    this.DatosSeguro.precioMXN_subtotal = this.Formulario.redondear(this.DatosSeguro.precioMXN_descuento - this.DatosSeguro.precioMXN_ajuste + this.DatosSeguro.precioMXN_iva, 2);
    this.DatosSeguro.precioMXN_comision = this.Formulario.redondear(this.DatosSeguro.precioMXN  * (this.Comision / 100), 2);
    this.DatosSeguro.precioMXN_total = this.Formulario.redondear(this.DatosSeguro.precioMXN_subtotal - this.DatosSeguro.precioMXN_comision, 2);
    // this.DatosSeguro.precioMXN_total = this.Formulario.redondear(this.DatosSeguro.precioMXN_subtotal, 2);
    // console.log(this.DatosSeguro);
    this.actualizarPagos();
  }

  obtenerTiposPoliza(){
    this.asignarDiferencia();
    if(this.DatosSeguro.origen && this.DatosSeguro.destino && this.DatosSeguro.salida && this.DatosSeguro.regreso && this.DatosSeguro.id_tipo_seguro)
    {
      // console.log("DÃ­as: "+this.Diferencia.Dias +
      //   "\nZona origen: "+this.Formulario.FormDatos['Pais'][this.Formulario.formElementoPorId("Pais", this.DatosSeguro.origen)]['zona']+
      //   "\nZona destino: "+this.Formulario.FormDatos['Pais'][this.Formulario.formElementoPorId("Pais", this.DatosSeguro.destino)]['zona']+
      //   "\nTipo seguro: "+this.DatosSeguro.id_tipo_seguro);
      // this.BloquearTipoPoliza = false;
      this.DatosSeguro.regreso = new Date(this.DatosSeguro.regreso);
      this.Formulario.coberturaPoliza(this.DatosSeguro.id_tipo_seguro, 
        this.Diferencia.Dias, this.Formulario.FormDatos['Pais'][this.Formulario.formElementoPorId("Pais", this.DatosSeguro.origen)]['zona'],
        this.Formulario.FormDatos['Pais'][this.Formulario.formElementoPorId("Pais", this.DatosSeguro.destino)]['zona'],
        this.DatosSeguro, () => {        
          // console.log(this.Formulario.FormDatos['Tipo_Poliza']);
          // console.log(this.Formulario.FormDatos['Tipo_Poliza']);
        });
    }
  }

  DatosComision = {
    id: null,
    id_subagente: null,
    folio_seguro: null,
    estado: null,
    comision: null,
    impuestos: null,
    ajuste: null,
    comision_final: null,
    estado_factura: null,
    factura: null,
    fecha_pago: null,
    id_forma_pago: null,
    id_cuenta: null,
    subcomision: null,
  };
  
  obtenerComision(){
    this.HTTP.datosDe("comision", [{campo: "folio_seguro", valor: this.DatosSeguro.folio}]).subscribe(
      datos => {
        this.obtenerSubagente();
        if(datos)
          this.DatosComision = datos[0];
        // else
        // {
        //   this.DatosComision.folio_seguro = this.DatosSeguro.folio;
        // }
      }, error => {

      }
    )
  }

  DSM = null;
  cambioTipoPoliza(){
    
    
    this.DatosSeguro.precioUSD = this.Formulario.FormDatos['Tipo_Poliza'][this.Formulario.formElementoPorId("Tipo_Poliza", this.DatosSeguro.id_tipo_poliza)]['precio'];
    this.DatosSeguro.precioMXN = this.Formulario.redondear(this.DatosSeguro.precioUSD * this.DatosSeguro.tipo_cambio, 2);
    
    
    let a_codigo = this.Formulario.FormDatos['Tipo_Poliza'][this.Formulario.formElementoPorId("Tipo_Poliza", this.DatosSeguro.id_tipo_poliza)]['a_codigo'];
    let codigo_poliza = this.Formulario.FormDatos['Tipo_Poliza'][this.Formulario.formElementoPorId("Tipo_Poliza", this.DatosSeguro.id_tipo_poliza)]['codigo_poliza'];
    this.DatosCliente.fecha_registro_asegurado = new Date(this.DatosCliente.fecha_registro_asegurado);
    this.DatosSeguro.folio = 
      this.DatosCliente.fecha_registro_asegurado.getFullYear() + ("0" + (this.DatosCliente.fecha_registro_asegurado.getMonth() + 1)).slice(-2) + ("0" + this.DatosCliente.fecha_registro_asegurado.getDate()).slice(-2) + "." +
      a_codigo + codigo_poliza + "." + this.DatosCliente.id_cliente;
  }

  cambioSeguro(){
    // console.log(this.Formulario.obtenerElementoPor("id", this.DatosSeguros, this.Folio));
    this.DatosSeguro = this.DatosSeguros[this.Formulario.obtenerElementoPor("id", this.DatosSeguros, this.Folio)];
    this.obtenerPagos();
    this.obtenerExtras();
    this.obtenerIncidentes();
    this.obtenerTiposPoliza();
    this.setSeguro();
  }

  // -----------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------
  // SECCIÃ“N PAGOS  
  // -----------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------

  public DatosPagos = [
  ];

  public OpcionesPagos: boolean[] = [];

  public DatosPagosExtra = {
    suma_cargo_administrativo_MXN: 0,
    suma_envio_MXN: 0,
    cantidad_pagada_MXN: 0,
    saldo_pendiente_MXN: 0,
    suma_cargo_administrativo_USD: 0,
    suma_envio_USD: 0,
    cantidad_pagada_USD: 0,
    saldo_pendiente_USD: 0,
    total_MXN: 0,
    total_USD: 0,
  }

  obtenerPagos(){    
    this.Formulario.obtener("Forma_Pago");
    // this.HTTP.datosDe("pago", [{campo: "id_cotizacion", valor: this.DatosSeguro.id}])
    this.HTTP.datosPagos({campo: "id_cotizacion", valor: this.DatosSeguro.id})
      .subscribe(
        datos => {
          // console.log(datos);
          // this.DatosPagos = datos;
          // this.DatosPagos = datos.Pagos;
          this.DatosPagos = datos.PagosCompleto;
          // console.log(this.DatosPagos);

          // this.actualizarPagos();
          this.actualizarDatosPrecio();
          

        },

        error => {
          console.log(error);
        }
      );
  }

  opcionCambio(caso, datos){
    if(caso === "Pagos"){
      if(this.OpcionesPagos[datos] == false)
        this.OpcionesPagos[datos] = true;
      else
        this.OpcionesPagos[datos] = false;
    }
    if(caso === "Incidencias"){
      if(this.OpcionesIncidencias[datos] == false)
        this.OpcionesIncidencias[datos] = true;
      else
        this.OpcionesIncidencias[datos] = false;
    }
  }

  liquidarPagos(){
    this.DatosSeguro.liquidado = 1;
    this.Formulario.guardar("cotizacion", this.DatosSeguro);
  }

  actualizarPagos(){
    // this.actualizarDatosPrecio();
    this.LiquidarPagos = false;

    let SumaPagos = 0;

    this.DatosPagosExtra.suma_cargo_administrativo_MXN = 0;
    this.DatosPagosExtra.cantidad_pagada_MXN = 0;
    this.DatosPagosExtra.saldo_pendiente_MXN = 0;
    this.DatosPagosExtra.suma_envio_MXN = 0;
    this.DatosPagosExtra.total_MXN = 0;

    this.DatosPagosExtra.suma_cargo_administrativo_USD = 0;
    this.DatosPagosExtra.cantidad_pagada_USD = 0;
    this.DatosPagosExtra.saldo_pendiente_USD = 0;
    this.DatosPagosExtra.suma_envio_USD = 0;
    this.DatosPagosExtra.total_USD = 0;


    for(var Numero in this.DatosPagos){
      // console.log(this.Formulario.FormDatos['Forma_Pago'], this.DatosPagos[Numero]);
      this.OpcionesPagos[this.DatosPagos[Numero]['id']] = false;

      if(this.DatosPagos[Numero]['cuenta'] === 'MX'){ // ES MXN, tranformar a USD
        this.DatosPagosExtra.suma_cargo_administrativo_MXN += parseInt(this.Formulario.FormDatos['Forma_Pago'][this.Formulario.formElementoPorId("Forma_Pago", this.DatosPagos[Numero]['id_forma_pago'])]['cargo_administrativo']);
        this.DatosPagosExtra.suma_cargo_administrativo_USD += parseInt(this.Formulario.FormDatos['Forma_Pago'][this.Formulario.formElementoPorId("Forma_Pago", this.DatosPagos[Numero]['id_forma_pago'])]['cargo_administrativo']) / this.DatosPagos[Numero]['tipo_cambio'];

        // this.DatosPagosExtra.cantidad_pagada_MXN += parseInt(this.DatosPagos[Numero]['cantidad_abonada']);
        // this.DatosPagosExtra.cantidad_pagada_USD += parseInt(this.DatosPagos[Numero]['cantidad_abonada']) / this.DatosPagos[Numero]['tipo_cambio'];
      }

      else if(this.DatosPagos[Numero]['cuenta'] === 'USD'){ // ES USD, tranformar a MXN
        this.DatosPagosExtra.suma_cargo_administrativo_USD += parseInt(this.Formulario.FormDatos['Forma_Pago'][this.Formulario.formElementoPorId("Forma_Pago", this.DatosPagos[Numero]['id_forma_pago'])]['cargo_administrativo']);
        this.DatosPagosExtra.suma_cargo_administrativo_MXN += parseInt(this.Formulario.FormDatos['Forma_Pago'][this.Formulario.formElementoPorId("Forma_Pago", this.DatosPagos[Numero]['id_forma_pago'])]['cargo_administrativo']) * this.DatosPagos[Numero]['tipo_cambio'];

        // this.DatosPagosExtra.cantidad_pagada_USD += parseInt(this.DatosPagos[Numero]['cantidad_abonada']);
        // this.DatosPagosExtra.cantidad_pagada_MXN += parseInt(this.DatosPagos[Numero]['cantidad_abonada']) * this.DatosPagos[Numero]['tipo_cambio'];
      } 

      if(this.DatosPagos[Numero]['estado'] == 1){
        this.DatosPagosExtra.cantidad_pagada_MXN += parseFloat(this.DatosPagos[Numero]['cantidad_abonada_MXN']);
        this.DatosPagosExtra.cantidad_pagada_USD += parseFloat(this.DatosPagos[Numero]['cantidad_abonada_USD']);
      }



      if(this.DatosPagos[Numero]['envio'] == 1){
        this.DatosPagosExtra.suma_envio_MXN = 100;
        this.DatosPagosExtra.suma_envio_USD = 100 / this.DatosPagos[Numero]['tipo_cambio'];
      }

      if(this.DatosPagos[Numero]['estado'] == 0){
        // console.log(this.DatosPagos[Numero]);
        this.LiquidarPagos = true;
      }

    }

    if(this.DatosPagos.length == 0)
      this.LiquidarPagos = true;

    if(this.DatosPagosExtra.cantidad_pagada_USD < this.DatosSeguro.precioUSD)
      this.LiquidarPagos = true;

    this.DatosPagosExtra.total_MXN = parseFloat((this.DatosPagosExtra.suma_cargo_administrativo_MXN + this.DatosPagosExtra.suma_envio_MXN).toString()) + parseFloat(this.DatosSeguro.precioMXN.toString());
    this.DatosPagosExtra.total_USD = parseFloat((this.DatosPagosExtra.suma_cargo_administrativo_USD + this.DatosPagosExtra.suma_envio_USD).toString()) + parseFloat(this.DatosSeguro.precioUSD.toString());
    
    // this.DatosPagosExtra.total_MXN = (this.DatosPagosExtra.suma_cargo_administrativo_MXN) + (this.DatosPagosExtra.suma_envio_MXN) + (this.DatosSeguro.precioMXN);
    // this.DatosPagosExtra.total_USD = (this.DatosPagosExtra.suma_cargo_administrativo_USD) + (this.DatosPagosExtra.suma_envio_USD) + (this.DatosSeguro.precioUSD);

    this.DatosPagosExtra.saldo_pendiente_MXN = this.DatosPagosExtra.total_MXN - this.DatosPagosExtra.cantidad_pagada_MXN;
    this.DatosPagosExtra.saldo_pendiente_USD = this.DatosPagosExtra.total_USD - this.DatosPagosExtra.cantidad_pagada_USD;



    this.DatosSeguro.saldo_pendiente_MXN = this.DatosPagosExtra.saldo_pendiente_MXN;
    this.DatosSeguro.saldo_pendiente_USD = this.DatosPagosExtra.saldo_pendiente_USD;  

    this.DatosSeguro.saldo_liquidado_MXN = this.DatosPagosExtra.cantidad_pagada_MXN;  
    this.DatosSeguro.saldo_liquidado_USD = this.DatosPagosExtra.cantidad_pagada_USD;

    // console.log(this.DatosPagosExtra);
    // this.DatosSeguro.precioMXN_comision = this.DatosSeguro.precioMXN_comision.toString();
    let Seguro = this.DatosSeguro;
    // delete Seguro["precioMXN_comision"];
    // delete Seguro["precioUSD_comision"];
    // delete Seguro["precioUSD_descuentoSA"];
    // delete Seguro["precioUSD_descuentoSA"];
    // console.log(Seguro);
    
    if(this.DatosSeguro.precioMXN_comision == 0)
    {
      console.log("sdf");
      delete Seguro["precioMXN_comision"];
      delete Seguro["precioUSD_comision"];
      delete Seguro["precioUSD_descuentoSA"];
      delete Seguro["precioUSD_descuentoSA"];
    }
    
    this.Formulario.guardar('cotizacion', Seguro, null, null, false);
    // this.Formulario.guardar('cotizacion', this.DatosSeguro, null, null, false);
  
    if(!this.LiquidarPagos)
      this.liquidarPagos();
  
  }

  agregarPago(){
    this.ventana.agregarPago({DatosSeguro: this.DatosSeguro, DatosPago: null, DatosCliente: this.DatosCliente, DatosPagos: this.DatosPagos}, () => {
      this.obtenerPagos();
    });
  }

  modificarPago(datosDelPago){
    this.ventana.agregarPago({DatosSeguro: this.DatosSeguro, DatosPago: datosDelPago, DatosCliente: this.DatosCliente, DatosPagos: this.DatosPagos}, 
      () => {
        this.obtenerPagos();
        this.DatosSeguro.liquidado = 0;
        this.guardarSin();
      });
  }

  eliminarPago(datosDelPago){
    this.ventana.Confirmar({Mensaje: "¿Elminar pago?"}, 
      () => {
        // console.log("SÃ");
        this.Formulario.borrar("Pago", datosDelPago, () => {
          this.obtenerPagos();
          this.DatosSeguro.liquidado = 0;
          this.guardarSin();
        });
      }, 
      () => {
        // console.log("NO");
      });
  }

  verificarPago(datosDelPago){
    datosDelPago.estado = 1;

    this.HTTP.datosDe("pago", [{campo: "id", valor: datosDelPago.id}])
      .subscribe(
        datos => {
          datos[0].estado = 1;

          this.Formulario.guardar("Pago", datos[0], null, () => {this.obtenerPagos();});
        },

        error => {
          console.log(error);
        }
      );
  }

  // -----------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------
  // SECCIÃ“N Incidentes
  // -----------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------

  public DatosIncidentes = [];
  public OpcionesIncidencias: boolean[] = [];

  obtenerIncidentes(){
    this.HTTP.datosIncidentes({campo: "id_cotizacion", valor: this.DatosSeguro.id})
      .subscribe(
        datos => {
          // console.log(datos);
          this.DatosIncidentes = datos.IncidenteCompleto;
          for(var N in this.DatosIncidentes){
            this.OpcionesIncidencias[this.DatosIncidentes[N].id] = false;
          }
        },

        error => {
          console.log(error);
        }
      );
  }

  agregarIncidente(){
    this.ventana.agregarIncidente({DatosSeguro: this.DatosSeguro, DatosIncidente: null}, () => {this.obtenerIncidentes();});
  }

  modificarIncidente(datosDelIncidente){
    this.ventana.agregarIncidente({DatosSeguro: this.DatosSeguro, DatosIncidente: datosDelIncidente, Creador: datosDelIncidente.nombre_completo}, () => {this.obtenerIncidentes();});
  }

  eliminarIncidente(datosDelIncidente){
    this.ventana.Confirmar({Mensaje: "Â¿Elminar incidente?"}, 
      () => {
        // console.log("SÃ");
        this.Formulario.borrar("Incidente", datosDelIncidente, () => {this.obtenerIncidentes();});
      }, 
      () => {
        // console.log("NO");
      });
  }



  public TAB = 0;
  public VAR = -1;
  public Cliente = false;
  public Agente = false;
  public Admin = false;

  ngOnInit() {
    this.Formulario.obtener("Fuente_Referencia");
    this.Formulario.obtener("Tipo_Seguro");
    this.Formulario.obtener("Forma_Pago");
    this.Formulario.obtener("Cuenta");
    
    this.Formulario.obtener("Tipo_Poliza", null, () => 
    {
      // console.log(this.Formulario.FormDatos);
    });

    this.obtenerImpuestos();
    
    if(this.sesion.cliente())
      this.Cliente = true;
    if(!this.sesion.admin())
      this.Agente = true;
    if(this.sesion.admin())
      this.Admin = true;

    this.sesion.SesionIniciada.subscribe(
      iniciada => {
        if(iniciada && this.sesion.cliente())
          this.Cliente = true;
        if(iniciada && this.sesion.admin())
          this.Agente = false;
        if(this.sesion.admin())
          this.Admin = true;
        console.log(this.Cliente, this.Agente)
      }
    );

    // console.log(this.Cliente);

    // Datos del cliente
    this.Ruta.params.subscribe(
      params => {
        this.DatosCliente.id = +params['id']; 
        if(params['tab'])
        {
          if(params['tab'] === 'Perfil') this.TAB = 0;
          // if(params['tab'] === 'Seguros') this.TAB = 1;
          if(params['tab'] === 'Pagos') this.TAB = 1;
          if(params['tab'] === 'Incidencias') this.TAB = 2;
          if(params['tab'] === 'Margen') this.TAB = 3;
        }

        this.VAR = +params['var'];

        this.HTTP.datosDe("cliente", [{campo: "id", valor: this.DatosCliente.id}])
          .subscribe(
            datos => {
              this.DatosCliente = datos[0];
              // console.log(this.Datos);
              this.obtenerAsignarSeguro(this.DatosCliente.id);
              this.obtenerCotizacion();
              // console.log(this.DatosSeguro);
              this.obtenerDistribuidor();
              this.DatosExtra.fecha_nacimiento = new Date(new Date(this.DatosCliente.fecha_nacimiento).getTime() + 1000 * 60 * 60 * 24);
              // console.log(this.DatosExtra.fecha_nacimiento);
            },

            error => {
              console.log(error);
            }
          );
      }
    );

  }

  public DatosImpuestos = [];
  obtenerImpuestos(){
    this.HTTP.datosDe("impuesto")
      .subscribe(
        datos => {
          this.DatosImpuestos = datos;
          // console.log(datos);
          // console.log(this.DatosCotizacion);
        },
        error => {
          console.log(error);
        }
      );
  }

  // ASIGNAR SEGURO
  public DatosAsignar = {
    subagente: null,
    subagente_id: null,
    distribuidor: null,
    distribuidor_id: null,
    id_distribuidores: null,
    agente: null,
    agente_id: null,
    tipo_distribuidor: null,
    tipo_distribuidor_id: null,
    cliente_id: null,
  }

  obtenerAsignarSeguro(id_cliente){
    this.Formulario.obtener("Tipo_Distribuidor");
    this.Formulario.obtener("Distribuidor");
    this.Formulario.obtener("Agente");
    this.Formulario.obtener("Subagente");


    this.HTTP.aseguradoAsignarSeguro(id_cliente).subscribe(
      (datos) => {
        this.DatosAsignar = datos.Seguro[0];
        // console.log(this.DatosAsignar)
      },

      (error) => {
        console.log(error);
      }
    );
  }

  AsignarGuardar(){
    let Distr = {
      id: this.DatosAsignar.distribuidor_id,
      id_distribuidores: this.DatosAsignar.id_distribuidores,
    }
    this.DatosCliente.id_subagente = this.DatosAsignar.subagente_id;
    this.DatosSeguro.id_subagente = this.DatosAsignar.subagente_id;
    this.Formulario.guardar("distribuidor", Distr);
    this.Formulario.guardar("cliente", this.DatosCliente);
    this.Formulario.guardar("cotizacion", this.DatosSeguro);
    window.location.reload();
  }

  cambioAsignarSeguro(cual){
    if(cual === "Tipo_Distribuidor")
      this.Formulario.obtener("Distribuidor", {Campo: 'id_tipo_distribuidor', Valor: this.DatosAsignar.tipo_distribuidor_id});
    else if(cual === "Distribuidor")
      this.Formulario.obtener("Agente", {Campo: 'id_distribuidor', Valor: this.DatosAsignar.distribuidor_id});
    else if(cual === "Agente")
      this.Formulario.obtener("Subagente", {Campo: 'id_agente', Valor: this.DatosAsignar.agente_id});
  }


  cancelar()
  {
    this.DatosSeguro.id = null;
    this.DatosSeguro.precioMXN = -this.DatosSeguro.precioMXN;
    this.DatosSeguro.precioMXN_descuento = -this.DatosSeguro.precioMXN_descuento;
    this.DatosSeguro.precioMXN_subtotal = -this.DatosSeguro.precioMXN_subtotal;
    this.DatosSeguro.precioMXN_total = -this.DatosSeguro.precioMXN_total;
    this.DatosSeguro.precioMXN_comision = -this.DatosSeguro.precioMXN_comision;

    this.DatosSeguro.precioUSD = -this.DatosSeguro.precioUSD;
    this.DatosSeguro.precioUSD_descuento = -this.DatosSeguro.precioUSD_descuento;
    this.DatosSeguro.precioUSD_subtotal = -this.DatosSeguro.precioUSD_subtotal;
    this.DatosSeguro.precioUSD_comision = -this.DatosSeguro.precioUSD_comision;

    this.DatosSeguro.estado_t = 2;
    this.DatosSeguro.precioUSD_total = -this.DatosSeguro.precioUSD_total;
    this.Formulario.guardar("Cotizacion", this.DatosSeguro, null, (id) => {
      this.Link.navigate(['Asegurados', this.DatosCliente.id, 'Seguros', id]);
    });
    // location.reload();
  }
  extender()
  {
    this.DatosSeguro.id = null;
    this.DatosSeguro.regreso = new Date((new Date(this.DatosSeguro.regreso).getTime() + (30 * 60 * 60 * 1000)) + 
      (new Date(this.DatosSeguro.regreso).getTime() - new Date(this.DatosSeguro.salida).getTime()));
    this.DatosSeguro.salida = new Date(new Date(this.DatosSeguro.regreso).getTime() + (30 * 60 * 60 * 1000));
    // this.DatosSeguro.salida = this.DatosSeguro.salida
    // this.DatosSeguro.regreso = null;
    this.DatosSeguro.estado_t = 1;
    this.Formulario.guardar("Cotizacion", this.DatosSeguro, null, (id) => {
      this.Link.navigate(['Asegurados', this.DatosCliente.id, 'Seguros', id]);
    });
    // location.reload();
  }

  folioColor(){
    if(this.DatosSeguro.estado_t == 2) // cancelar
      return '#d68f8f';
    else if(this.DatosSeguro.estado_t == 1) // extender
      return '#b785c6';
    else if(this.DatosSeguro.estado_t == 3) // correccion
      return '#f4f442';
    else
      return '#EAEAEA';
  }

  PerfilVerMas = false;

  guardarSeguro()
  {
    this.DatosSeguro.estado_t = 3;
    this.Formulario.guardar('cotizacion', this.DatosSeguro)
    window.location.reload();
  }

  guardarCliente() {
    this.DatosCliente.fecha_nacimiento = this.DatosExtra.fecha_nacimiento;
    this.Formulario.guardar('cliente', this.DatosCliente);
  }

  guardarSin()
  {
    // this.DatosSeguro.estado_t = 3;
    this.Formulario.guardar('cotizacion', this.DatosSeguro)
    window.location.reload();
  }

  contrasena()
  {
    this.HTTP.datosDe("usuario", {campo: "correo_electronico", valor: this.DatosCliente.correo_electronico}).subscribe(
      (datos) => {
        this.Documento.email(this.DatosCliente.correo_electronico, "RegistroUsuario", datos[0], null);
      },
      error => {}
    )
  }

}
