import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
// import {SelectionModel} from '@angular/cdk/collections';
// import {MatTableDataSource, PageEvent} from '@angular/material';

// import { HTTPService }    from '../http.service';
// import {SesionService} from '../sesion.service'
// import { DatosFormularioService } from '../servicios/datos-formulario.service';
import { Router, ActivatedRoute } from '@angular/router';
// import { DocumentosService } from '../servicios/documentos.service';
// import { VentanasService } from '../servicios/ventanas.service';
import { BarraProgresoService } from '../servicios/barra-progreso.service';
import { MatDatepickerInputEvent, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS} from '../clases/FechaAdaptador';

import * as Tabla from '../tablas'
import {Tablas} from '../clases/tablas'

@Component({
  selector: 'app-prospectos',
  templateUrl: './prospectos.component.html',
  styleUrls: ['./prospectos.component.css'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class ProspectosComponent extends Tablas implements OnInit {

  constructor(
    private _HTTP: Tabla.HTTPService,
    public _sesion: Tabla.SesionService,
    public _dialog: Tabla.MatDialog,
    public _Ventana: Tabla.VentanasService,
    public _Formulario: Tabla.DatosFormularioService,
    public _ServcioExcel: Tabla.ExcelService,
    private Ruta: ActivatedRoute,
    public Progreso: BarraProgresoService,
    private Link: Router,
    ) {
    super(_HTTP, _sesion, _dialog, _Ventana, _Formulario, _ServcioExcel);
  }

	 DatosCliente = {
    id: null,
    id_subagente: null,
    universidad: null,
    id_rol: null,
    id_tipo_cliente: null,
    activo_inactivo: null,
    fecha_registro_prospecto: null,
    fecha_registro_asegurado: null,
    nombres: null,
    apellidos: null,
    nombre_completo: null,
    correo_electronico: null,
    contrasena: null,
    telefono: null,
    fecha_nacimiento: null,
    id_fuente_referencia: null,
    id_pais: null,
    estado: null,
    cp: null,
    ciudad: null,
    calle_numero: null,
    razon_social: null,
    rfc: null,
    fac_correo_electronico: null,
    fac_telefono: null,
    fac_id_pais: null,
    fac_estado: null,
    fac_cp: null,
    fac_ciudad: null,
    fac_calle_numero: null,
    colonia: null,
    fac_colonia: null,
    sexo: null,
  };

    // - - -- - - - - -- - - -  TABLA

  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - -- - - - - -- - - -  SECCIÓN TABLA
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -

  // Columnas = ;
  // displayedColumns: string[] = ;
  
  // displayedColumns = this.Columnas.map(c => c.id);  
  public TAB = 0;
  private COT = null;

  ngOnInit() {
    // this.Formulario.obtener("Pais");
    this.Formulario.obtener("Fuente_Referencia");

    // OBTENER DATOS DEL CLIENTE
    this.Ruta.params.subscribe(
      params => {
        this.DatosCliente.id = +params['id'];
        let LaTAB = params['tab'];
        this.COT = params['cot'];
        if(LaTAB === "Cotizaciones")
          this.TAB = 1;
          // console.log(this.DatosCliente.id);
        this._HTTP.datosDe("cliente", [{campo: "id", valor: this.DatosCliente.id}])
          .subscribe(
            datos => {
              this.DatosCliente = datos[0];
              this.obtenerAsignarSeguro(this.DatosCliente.id);
              // console.log(this.Datos);
              if(this.COT){
                this.TAB = 1;
                this.asegurar(this.COT)
              }

            },

            error => {
              console.log(error);
            }
          );

      }
    );

    if(this.sesion.cliente())
      this.Cliente = true;
    if(!this.sesion.admin())
      this.Agente = true;

      this.sesion.SesionIniciada.subscribe(
        iniciada => {
          if(iniciada && this.sesion.cliente())
            this.Cliente = true;
          if(iniciada && !this.sesion.admin())
            this.Agente = true;
          console.log(this.Cliente)
        }
      );

    // OBTENER COTIZACIONES DEL CLIENTE
    // this.HTTP.pagCotizaciones(this.DatosCliente.id)
    //   .subscribe(
    //     datos => {
    //       // console.log(datos);
    //       this.DatosCols = datos.Cotizaciones;
    //       // this.DatosCantidad = this.DatosCliente.length;
    //       this.dataSource.data = datos.Cotizaciones;
    //       // this.DatosCantidad = datos.Total;
    //       // console.log(this.Datos);
    //       // this.changeDetectorRefs.detectChanges();
    //     },
    //     error => {
    //       console.log(error);
    //     }
    //   );

    this.agregarColumnas(
      [
        // {id: 'fecha',         nombre: 'Fecha de cotización',   ocultar: false},
        {id: 'tipo_seguro',     nombre: 'Tipo de seguro',       ocultar: false},
        {id: 'tipo_poliza',     nombre: 'Cobertura',            ocultar: false}, 
        {id: 'pais_origen',     nombre: 'Origen',               ocultar: false}, 
        {id: 'pais_destino',    nombre: 'Destino',              ocultar: false}, 
        {id: 'salida',          nombre: 'Salida',               ocultar: false}, 
        {id: 'regreso',         nombre: 'Regreso',              ocultar: false}, 
        {id: 'precioMXN',       nombre: 'Total MXN',            ocultar: false}, 
        {id: 'precioUSD',       nombre: 'Total USD',            ocultar: false}, 
        {id: 'tipo_cambio',     nombre: 'Tipo de cambio',       ocultar: false}, 
      ]
    );

    this.agregarColIds(
      ['select', 'tipo_seguro', 'tipo_poliza', 'pais_origen', 'pais_destino', 'salida', 'regreso', 'precioMXN', 'precioUSD', 'tipo_cambio', 'acciones']
      );

    this.tablaConfig("pagCotizaciones", "Cotizaciones", "cotizacion", "cotizacion", [{Campo: "id_cliente", Valor: this.DatosCliente.id}]);

    this.tabla();

  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("cambio", changes);
  }

  accionCargando = false;

  // imprimirCotizacion(id){
  //   // console.log(id);
  //   this.Progreso.Cargando = true;
  //   this.Documento.documento("Cotizacion", {id_cotizacion: id}, true, () => {this.Progreso.Cargando = false;});
  // }

  // descargarCotizacion(id){
  //   // console.log(id);
  //   this.Progreso.Cargando = true;
  //   this.Documento.documento("Cotizacion", {id_cotizacion: id}, false, () => {this.Progreso.Cargando = false;});
  // }

  eliminarCotizacion(id){
    this.Progreso.Cargando = true;
    this.Ventana.Confirmar({Mensaje: "¿Eliminar cotización?"},
      () => {
        this.Formulario.borrar("cotizacion", {id: id}, () => {this.Progreso.Cargando = false;})
      },

      () => {

        this.Progreso.Cargando = false;
      });
  }

  // enviarCotizacion(id){
  //   this.Progreso.Cargando = true;
  //   this.Documento.email(this.DatosCliente.correo_electronico, "Cotizacion", {id_cotizacion: id}, () => {this.Progreso.Cargando = false;});
  // }
    
  guardar(){
    console.log(this.DatosCliente);
    this.Formulario.guardar('cliente', this.DatosCliente);
  }

  asegurar(id){
    this._Ventana.confirmarCotizacion({ID: id}, 
      (resultado) => {
        if(resultado){
          // this.tabla();
          this.Formulario.alerta("Asegurado", "Cerar");
          this.Link.navigate(["Asegurados", this.DatosCliente.id, "Pagos", id]);
        }
      })
  }


    // ASIGNAR SEGURO
    public DatosAsignar = {
      subagente: null,
      subagente_id: null,
      distribuidor: null,
      distribuidor_id: null,
      id_distribuidores: null,
      agente: null,
      agente_id: null,
      tipo_distribuidor: null,
      tipo_distribuidor_id: null,
      cliente_id: null,
    }
    public Cliente = false;
    public Agente = false;

    obtenerAsignarSeguro(id_cliente){
      this.Formulario.obtener("Tipo_Distribuidor");
      this.Formulario.obtener("Distribuidor");
      this.Formulario.obtener("Agente");
      this.Formulario.obtener("Subagente");
  
  
      this._HTTP.aseguradoAsignarSeguro(id_cliente).subscribe(
        (datos) => {
          this.DatosAsignar = datos.Seguro[0];
          console.log(this.DatosAsignar)
        },
  
        (error) => {
          console.log(error);
        }
      );
    }
  
    AsignarGuardar(){
      let Distr = {
        id: this.DatosAsignar.distribuidor_id,
        id_distribuidores: this.DatosAsignar.id_distribuidores,
      }
      this.DatosCliente.id_subagente = this.DatosAsignar.subagente_id;
      this.Formulario.guardar("distribuidor", Distr);
      this.Formulario.guardar("cliente", this.DatosCliente);
    }
  
    cambioAsignarSeguro(cual){
      if(cual === "Tipo_Distribuidor")
        this.Formulario.obtener("Distribuidor", {Campo: 'id_tipo_distribuidor', Valor: this.DatosAsignar.tipo_distribuidor_id});
      else if(cual === "Distribuidor")
        this.Formulario.obtener("Agente", {Campo: 'id_distribuidor', Valor: this.DatosAsignar.distribuidor_id});
      else if(cual === "Agente")
        this.Formulario.obtener("Subagente", {Campo: 'id_agente', Valor: this.DatosAsignar.agente_id});
    }

}
