<?php
// class LZW {
//     function compress($uncompressed) {
//         $dictSize = 256;
//         $dictionary = array();
//         for ($i = 0; $i < 256; $i++) {
//             $dictionary[chr($i)] = $i;
//         }
//         $w = "";
//         $result = "";
//         for ($i = 0; $i < strlen($uncompressed); $i++) {
//             $c = $this->charAt($uncompressed, $i);
//             $wc = $w.$c;
//             if (isset($dictionary[$wc])) {
//                 $w = $wc;
//             } else {
//                 if ($result != "") {
//                     $result .= ",".$dictionary[$w];
//                 } else {
//                     $result .= $dictionary[$w];
//                 }
//                 $dictionary[$wc] = $dictSize++;
//                 $w = "".$c;
//             }
//         }
//         if ($w != "") {
//             if ($result != "") {
//                 $result .= ",".$dictionary[$w];
//             } else {
//                 $result .= $dictionary[$w];
//             }
//         }
//         return $result;
//     }
//     function decompress($compressed) {
//         $compressed = explode(",", $compressed);
//         $dictSize = 256;
//         $dictionary = array();
//         for ($i = 1; $i < 256; $i++) {
//             $dictionary[$i] = chr($i);
//         }
//         $w = chr($compressed[0]);
//         $result = $w;
//         for ($i = 1; $i < count($compressed); $i++) {
//             $entry = "";
//             $k = $compressed[$i];
//             if (isset($dictionary[$k])) {
//                 $entry = $dictionary[$k];
//             } else if ($k == $dictSize) {
//                 $entry = $w.$this->charAt($w, 0);
//             } else {
//                 return null;
//             }
//             $result .= $entry;
//             $dictionary[$dictSize++] = $w.$this->charAt($entry, 0);
//             $w = $entry;
//         }
//         return $result;
//     }
//     function charAt($string, $index){
//         if($index < mb_strlen($string)){
//             return mb_substr($string, $index, 1);
//         } else{
//             return -1;
//         }
//     }

//     function decompress2($s) {
//         mb_internal_encoding('UTF-8');
        
//         $dict = array();
//         $currChar = mb_substr($s, 0, 1);
//         $oldPhrase = $currChar;
//         $out = array($currChar);
//         $code = 256;
//         $phrase = '';
        
//         for ($i=1; $i < mb_strlen($s); $i++) {
//             $currCode = implode(unpack('N*', str_pad(iconv('UTF-8', 'UTF-16BE', mb_substr($s, $i, 1)), 4, "\x00", STR_PAD_LEFT)));
//             if($currCode < 256) {
//                 $phrase = mb_substr($s, $i, 1);
//             } else {
//                 $phrase = $dict[$currCode] ? $dict[$currCode] : ($oldPhrase.$currChar);
//             }
//             $out[] = $phrase;
//             $currChar = mb_substr($phrase, 0, 1);
//             $dict[$code] = $oldPhrase.$currChar;
//             $code++;
//             $oldPhrase = $phrase;
//         }
//         var_dump($dict);
//         return(implode($out));
//     }
// }

class LZW
{
    function compress($unc) {
        $i;$c;$wc;
        $w = "";
        $dictionary = array();
        $result = array();
        $dictSize = 256;
        for ($i = 0; $i < 256; $i += 1) {
            $dictionary[chr($i)] = $i;
        }
        for ($i = 0; $i < strlen($unc); $i++) {
            $c = $unc[$i];
            $wc = $w.$c;
            if (array_key_exists($w.$c, $dictionary)) {
                $w = $w.$c;
            } else {
                array_push($result,$dictionary[$w]);
                $dictionary[$wc] = $dictSize++;
                $w = (string)$c;
            }
        }
        if ($w !== "") {
            array_push($result,$dictionary[$w]);
        }
        return implode(",",$result);
    }
 
    function decompress($com) {
        // $com = explode(",",$com);
        $i;$w;$k;$result;
        $dictionary = array();
        $entry = "";
        $dictSize = 256;
        for ($i = 0; $i < 256; $i++) {
            $dictionary[$i] = chr($i);
        }
        $w = chr($com[0]);
        $result = $w;
        // $result = count($com);
        for ($i = 1; $i < count($com);$i++) {
            // $result = "q";
            $k = $com[$i];
            if ($dictionary[$k]) {
                $entry = $dictionary[$k];
                // $entry = "dictionary[k];";
            } else {
                if ($k === $dictSize) {
                // if ($k == $dictSize) {
                    $entry = $w.$w[0];
                    $entry = "w.w[0]";
                } else {
                    // return null;
                    // return "null";
                }
            }
            $result .= $entry;
            // $result .= "entry";
            $dictionary[$dictSize++] = $w . $entry[0];
            $w = $entry;
        }
        return $result;
    }
}
?>