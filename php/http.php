<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");


header('Content-Type: *');
// use mailer\PHPMailer\PHPMailer;
// use mailer\Exception\Exception;
// require_once('mailer/PHPMailer.php');
require("mailer/PHPMailer.php");
require("lzw.php");
require("mailer/SMTP.php");
require("mailer/Exception.php");
require_once 'SimpleXLSX.php';

// header('Access-Control-Allow-Origin: http://187.162.123.252:4200');
// header('Content-Type: application/x-www-form-urlencoded');
// header('Content-Type: application/json');

require_once('MysqliDb.php');
// $db = new MysqliDb ('host', 'username', 'password', 'databaseName');
// $DB = new MysqliDb ('localhost', 'usuario', 'usuario', 'miseguro');
$DB = new MysqliDb ('localhost', 'miseguro_sistemac', 'msisistema321', 'miseguro_sistemac');


$JSON = json_decode(file_get_contents("php://input"));
if(!empty($JSON))
{
	// $Datos = $JSON->json;
	// $Datos = $JSON;
	// require_once('MysqliDb.php');
	// $db = new MysqliDb ('host', 'username', 'password', 'databaseName');
	// $DB = new MysqliDb ('localhost', 'usuario', 'usuario', 'miseguro');
	// $DB = new MysqliDb ('localhost', 'miseguro_sistemac', 'msisistema321', 'miseguro_sistemac');

	if($JSON->Caso == "haySesion")
	{
		session_cache_expire(1440);
		session_id($JSON->Datos);
		session_start();
		if(isset($_SESSION) && !empty($_SESSION))
		{
			// ACTUALIZAR DATOS DE SESIÓN
			$DB->join("rol", "subagente.id_rol=rol.id", "INNER");
			$DB->where("correo_electronico", $_SESSION['correo']);
			$Usuario = $DB->get("subagente", 1, Array("subagente.id", "subagente.correo_electronico", "subagente.id_rol", "rol.rol"));
			if ($DB->count > 0){
				$_SESSION['id'] = $Usuario[0]["id"];
				$_SESSION['correo'] = $Usuario[0]["correo_electronico"];
				$_SESSION['rol'] = $Usuario[0]["rol"];
			}
			////////////////////////

			$Resultado = ["existe" => true, "id" => $_SESSION['id'], "correo" => $_SESSION['correo'], "rol" => $_SESSION['rol']];

		}
		else
			$Resultado = ["existe" => false];
		echo json_encode($Resultado);
	}

	if($JSON->Caso == "verificarSesion")
	{
		session_cache_expire(1440);
		session_id($JSON->Datos);
		session_start();
		$Resultado = ["iniciada" => false, "id" => null, "correo_electronico" => null, "rol" => null];
		// if(isset($_COOKIE["SESION_ID"]) && !empty($_COOKIE["SESION_ID"]))
		if(isset($_SESSION["SESION_ID"]) && !empty($_SESSION["SESION_ID"]))
		{
			$Resultado["iniciada"] = true;
			$Resultado["id"] = $_SESSION["SESION_ID"];
			$Resultado["correo_electronico"] = $_SESSION["SESION_CE"];
			// ACTUALIZAR DATOS DE SESIÓN
			// $DB->join("rol", "subagente.id_rol=rol.id", "INNER");
			$DB->join("rol", "usuario.id_rol=rol.id", "INNER");
			$DB->where("correo_electronico", $_SESSION["SESION_CE"]);
			// $Usuario = $DB->get("subagente", 1, Array("subagente.id", "subagente.correo_electronico", "subagente.id_rol", "rol.rol"));
			$Usuario = $DB->get("usuario", 1, Array("usuario.id", "usuario.correo_electronico", "usuario.id_rol", "usuario.id_datos", "rol.rol"));
			$Resultado["sql"] = $DB->getLastQuery();
			if ($DB->count > 0)
			{
				$Resultado["rol"] = $Usuario[0]["rol"];
				$Resultado["id_datos"] = $Usuario[0]["id_datos"];
			}

			$DB->where("correo_electronico", $_SESSION["SESION_CE"]);

			if($Resultado["rol"] != "Cliente")
			{
				$Usuario = $DB->get("subagente", 1);
				$DB->join("agente", "agente.id=subagente.id_agente", "INNER");
				$DB->join("distribuidor", "agente.id_distribuidor=distribuidor.id", "INNER");
				$DB->where("subagente.correo_electronico", $_SESSION["SESION_CE"]);			
				$Resultado["datos"] = $DB->getOne("subagente", Array("subagente.*", "agente.id AS id_agente", "distribuidor.id AS id_distribuidor"));
				$Resultado["datossql"] = $DB->getLastQuery();
			}
			else
			{
				$Usuario = $DB->get("cliente", 1);
			}

			// if($DB->count > 0)
			// 	$Resultado["id_datos"] = $Usuario[0]["id"];
			// $Resultado["id_datos"] = $Usuario[0]["id"];

			// $Usuario = $DB->rawQuery();

			////////////////////////
		}
		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "iniciarSesion")
	{
		session_cache_expire(1440);
		session_start();
		$Resultado = ["iniciada" => false, "id" => null, "correo_electronico" => null, "sid" => null];
		$DB->where("correo_electronico", $JSON->Datos);
		$Usuario = $DB->get("usuario", 1);
		// $Usuario = $DB->get("subagente", 1);
		if ($DB->count > 0){
			$Resultado["id"] = $Usuario[0]["id"];
			$Resultado["correo_electronico"] = $Usuario[0]["correo_electronico"];
			// setcookie("SESION_ID", $Usuario[0]["id"]);
			// setcookie("SESION_CE", $Usuario[0]["correo_electronico"]);

			// $codigo_enlace = crypt($Resultado["correo_electronico"], $Timestamp);

			$_SESSION['SESION_ID'] = $Usuario[0]["id"];
			$_SESSION['SESION_CE'] = $Usuario[0]["correo_electronico"];

			$Resultado["sid"] = session_id();
			$Resultado["iniciada"] = true;
			// $Resultado["cookie"] = $_COOKIE;
		}
		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "cerrarSesion")
	{
		session_id($JSON->Datos);
		session_start();
		$_SESSION = array();
		session_destroy();
		$Resultado = ["cerrada" => true];

		unset($_COOKIE["SESION_ID"]);
		unset($_COOKIE["SESION_CE"]);

		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "buscarUsuario")
	{
		$Resultado = ["existe" => 0, "id" => -1, "correo" => ""];
		// $DB->join("rol", "subagente.id_rol=rol.id", "INNER");
		// $DB->where("contrasena", $JSON->Datos->PW);
		// $DB->where("correo_electronico", $JSON->Datos->Usuario);
		// $Usuario = $DB->get("subagente", 1, Array("subagente.id", "subagente.correo_electronico", "subagente.id_rol", "rol.rol", "subagente.activo_inactivo"));
		
		
		
		// echo json_encode($Usuario);
		// echo json_encode($Usuario[0]["id"]);
		if ($DB->count > 0){
		    // echo json_encode($Usuario);
		    $Resultado["existe"] = 1;
		    $Resultado["id"] = $Usuario[0]["id"];
		    $Resultado["correo"] = $Usuario[0]["correo_electronico"];
		    $Resultado["rol"] = $Usuario[0]["rol"];
		    $Resultado["activo_inactivo"] = $Usuario[0]["activo_inactivo"];
		}
		if($JSON->Extra && $Resultado["existe"] > 0) //conSesion y usuario encontrado
		{
			if($Resultado["activo_inactivo"] == 0)
			{
				$Resultado = array_merge($Resultado, array("sesion" => "desactivado"));
			}
			else
			{
				session_start();
				$_SESSION['id'] = $Resultado['id'];
				$_SESSION['correo'] = $Resultado['correo'];
				$_SESSION['rol'] = $Resultado['rol'];
				$Resultado = array_merge($Resultado, array("sesion" => "iniciada", "sesion_id" => session_id(), "ss" => $_SESSION));
			}
		}
		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "buscarUsuario2")
	{
		$Resultado = ["existe" => false, "id" => null, "correo_electronico" => null, "caso" => "error"];
		$DB->where("contrasena", $JSON->Datos->PW);
		$DB->where("correo_electronico", $JSON->Datos->Usuario);
		// $Usuario = $DB->get("subagente", 1);
		$Usuario = $DB->get("usuario", 1);


		if ($DB->count > 0){
			$Resultado["existe"] = true;
			$Resultado["id"] = $Usuario[0]["id"];
			$Resultado["correo_electronico"] = $Usuario[0]["correo_electronico"];
			$Resultado["caso"] = "existe";
			$Resultado["activo_inactivo"] = $Usuario[0]["activo_inactivo"];
		}
		else
			$Resultado["caso"] = "noexiste";
		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "buscar")
	{
		$Campos = (array) $JSON->Datos->Campos;

		foreach ($Campos as $Campo => $Valor)
			$DB->where($Campo, '%'.$Valor.'%', 'LIKE', $JSON->Datos->Condicion);
		
		// echo json_encode($JSON);

		$Datos = $DB->get(strtolower($JSON->Datos->Tabla), $JSON->Datos->Filas);
		if ($DB->count > 0){
			echo json_encode($Datos);
		}
		// echo json_encode($DB->getLastQuery());	

	}

	else if($JSON->Caso == "datosDe")
	{
		// echo json_encode($JSON->Datos);

		// if(empty($JSON->Datos->SesionID))
		// 	exit;

		// session_id($JSON->Datos->SesionID);
		// session_start();

		// if(strtolower($JSON->Datos->Tabla) == "subagente")
		// 	$DB->where("id", $_SESSION['id']);

		if(!empty($JSON->Datos->Where)){
			// echo json_encode($JSON->Datos->Where);
			$Condiciones = (array)$JSON->Datos->Where;
			foreach ($Condiciones as $Condicion) {
			// echo json_encode($Condicion);
			$operador = (isset($Condicion->operador)) ? $Condicion->operador : "=";
			$valor = (isset($Condicion->operador) && $Condicion->operador == "LIKE") ? "%".$Condicion->valor."%" : $Condicion->valor;
			if(isset($Condicion->or))
				$DB->orWhere($Condicion->campo, $valor, $operador);
			else
				$DB->where($Condicion->campo, $valor, $operador);
			}
		}

		if(!empty($JSON->Datos->Orden)){
			$DB->orderBy("id", $JSON->Datos->Orden);
		}
		

		$Datos = null;

		if(!empty($JSON->Datos->Pagina)){
			$page = $JSON->Datos->Pagina;	
			// set page limit to 2 results per page. 20 by default
			$DB->pageLimit = $JSON->Datos->Cantidad;
			$Datos = $DB->arraybuilder()->paginate(strtolower($JSON->Datos->Tabla), $page);
		}
		else {
			// if(strtolower($JSON->Datos->Tabla) == "cotizacion")
			$Datos = $DB->get(strtolower($JSON->Datos->Tabla));
			// else
			// $Datos = $DB->get(strtolower($JSON->Datos->Tabla));
		}

		if ($DB->count > 0){
			echo json_encode($Datos);
			// echo json_encode($DB->getLastQuery());
			// echo $Datos;
			// echo json_encode(1856.30);
			// echo json_encode($DB->getLastQuery());
		}	
	}

	else if($JSON->Caso == "datosPagos")
	{
		$Where = "";
		if(!empty($JSON->Datos->Where)){
			$Where = " WHERE " . $JSON->Datos->Where->campo . " = " . $JSON->Datos->Where->valor;
		}

		$Consulta = $DB->rawQuery("
			SELECT pago.*, forma_pago.forma_pago, cuenta.cuenta FROM pago 
				INNER JOIN cuenta ON pago.id_cuenta = cuenta.id
                	INNER JOIN forma_pago ON cuenta.id_forma_pago = forma_pago.id".
			$Where);	

		$Resultado ['PagosCompleto'] = $Consulta;

		$Consulta = $DB->rawQuery("
			SELECT * FROM pago ". $Where);

		$Resultado ['Pagos'] = $Consulta;

		echo json_encode($Resultado);
		// echo json_encode($DB->getLastQuery());
	}

	else if($JSON->Caso == "datosIncidentes")
	{
		$Where = "";
		if(!empty($JSON->Datos->Where)){
			$Where = " WHERE " . $JSON->Datos->Where->campo . " = " . $JSON->Datos->Where->valor;
		}

		$Consulta = $DB->rawQuery("
			SELECT incidente.*, cotizacion.folio, pais.pais FROM incidente 
			    INNER JOIN cotizacion ON incidente.id_cotizacion = cotizacion.id
			    INNER JOIN pais ON incidente.id_pais = pais.id".
			$Where);	
			// , subagente.nombre_completo
			// INNER JOIN subagente ON incidente.id_subagente = subagente.id

		$Resultado ['IncidenteCompleto'] = $Consulta;

		$Consulta = $DB->rawQuery("
			SELECT * FROM incidente ". $Where);

		$Resultado ['Incidente'] = $Consulta;

		echo json_encode($Resultado);
		// echo json_encode($DB->getLastQuery());
	}

	else if($JSON->Caso == "datosComentarios")
	{
		$Where = "";
		if(!empty($JSON->Datos->Where)){
			$Where = " WHERE " . $JSON->Datos->Where->campo . " = " . $JSON->Datos->Where->valor;
		}

		$Consulta = $DB->rawQuery("
			SELECT historial_comentarios.*, subagente.nombre_completo FROM 
			historial_comentarios INNER JOIN subagente ON historial_comentarios.id_subagente = subagente.id".
			$Where);	

		$Resultado ['HistorialComentariosCompleto'] = $Consulta;

		$Consulta = $DB->rawQuery("
			SELECT * FROM historial_comentarios ". $Where);

		$Resultado ['HistorialComentarios'] = $Consulta;

		echo json_encode($Resultado);
		// echo json_encode($JSON->Datos->Where);
		// echo json_encode($DB->getLastQuery());
	}

	else if($JSON->Caso == "datosRel")
	{
		$SELECT = 'SELECT * ';
		$FROM = 'FROM ' . strtolower(strtolower($JSON->Datos->Tabla)) . ' ';
		$INNERJOIN = '';
		$WHERE = 'WHERE 1 ';

		if(!empty($JSON->Datos->Where))
		{
			// echo json_encode($JSON->Datos->Where);
			$WHERE = 'WHERE ';
			$Condiciones = (array)$JSON->Datos->Where;
			foreach ($Condiciones as $Condicion) 
			{
			// echo json_encode($Condicion);
				// FALTA AGREGAR ANDs
				$WHERE .= $Condicion->campo . ' = ' . $Condicion->valor . ' ';
				// $DB->where($Condicion->campo, $Condicion->valor);
			}
		}

		if(strtolower($JSON->Datos->Tabla) == "subagente")
		{
			$INNERJOIN = '
			INNER JOIN rol ON rol.id = subagente.id_rol
		    INNER JOIN agente ON agente.id = subagente.id_agente
		    	INNER JOIN distribuidor ON distribuidor.id = agente.id_distribuidor
		        	INNER JOIN tipo_distribuidor ON tipo_distribuidor.id = distribuidor.id_tipo_distribuidor
		    INNER JOIN pais AS pais_normal ON pais_normal.id = subagente.id_pais
		    INNER JOIN pais AS pais_fac ON pais_fac.id = subagente.fac_id_pais ';
		}

		$Consulta = $DB->rawQuery($SELECT . $FROM . $INNERJOIN . $WHERE);	


		// $Cotizaciones = $DB->rawQuery('SELECT tipo_seguro.tipo_seguro, tipo_poliza.tipo_poliza, cotizacionOrigen.pais AS pais_origen, cotizacionDestino.pais AS pais_destino, 
		// 	cotizacion.salida, cotizacion.regreso, cotizacion.precioMXN, cotizacion.precioUSD, cotizacion.tipo_cambio, cotizacion.id
		// FROM cotizacion
		// 	INNER JOIN tipo_poliza 
		//     	ON cotizacion.id_tipo_poliza = tipo_poliza.id
		// 	INNER JOIN tipo_seguro 
		//     	ON cotizacion.id_tipo_seguro = tipo_seguro.id
		//     INNER JOIN pais AS cotizacionOrigen
		//     	ON cotizacion.origen = cotizacionOrigen.id 
		//     INNER JOIN pais AS cotizacionDestino
		//     	ON cotizacion.destino = cotizacionDestino.id
		// WHERE id_cliente = '.$JSON->Datos->ID_Cliente.'	ORDER BY cotizacion.id ASC');	


		$Resultado = ['Datos' => $Consulta];

		echo json_encode($Resultado);
	}

	// CÓDIGO POSTAL

	// else if($JSON->Caso == "dir_Paises")
	// {
	// 	$DB->where("activo_inactivo", 1);
	// 	$Paises = $DB->get("pais");
	// 	if ($DB->count > 0)
	// 	    echo json_encode($Paises);
	// }

	// else if($JSON->Caso == "TiposSeguro")
	// {
	// 	$DB->where("activo_inactivo", 1);
	// 	$TiposSeguro = $DB->get("tiposeguro");
	// 	if ($DB->count > 0)
	// 	    echo json_encode($TiposSeguro);
	// }

	// else if($JSON->Caso == "TiposPoliza")
	// {
	// 	$DB->where("activo_inactivo", 1);
	// 	$TiposPoliza = $DB->get("tipopoliza");
	// 	if ($DB->count > 0)
	// 	    echo json_encode($TiposPoliza);
	// }

	// else if($JSON->Caso == "FuentesReferencia")
	// {
	// 	$DB->where("activo_inactivo", 1);
	// 	$FuentesReferencia = $DB->get("fuentereferencia");
	// 	if ($DB->count > 0)
	// 	    echo json_encode($FuentesReferencia);
	// }

	else if($JSON->Caso == "guardar")
	{
		if(!empty($JSON->Extra))
		{
			if($JSON->Extra->id != null) // UPDATE
			{			
				$DatosArray = (array)$JSON->Extra;	
				$DB->where('id', $JSON->Extra->id);
				if ($DB->update(strtolower($JSON->Datos), $DatosArray))
	    			echo json_encode(["guardado" => true, "id" => $JSON->Extra->id, "query" => $DB->getLastQuery(), "datos" => $JSON]);
				else
	    			echo json_encode(["guardado" => false, "error" => $DB->getLastError()]);
			}
			else // INSERT
			{
				foreach ($JSON->Extra as $Campo => $Valor)
					if($Valor == null)
						unset($JSON->Extra->$Campo);

				$DatosArray = (array)$JSON->Extra;

				if(!empty($JSON->Extra2)) // Hay restricción
				{
					$DB->where($JSON->Extra2, $DatosArray[$JSON->Extra2]);
					$DB->get(strtolower($JSON->Datos));
					if($DB->count > 0)  // Ya existe restricción
					{ 
						echo json_encode(["guardado" => false, "id" => null, "error" => "Ya existe el correo"]);
						exit;
					}
				}
				// echo json_encode(["guardado" => "safdsfs", "id" => $DatosArray]);

				$InsertID = $DB->insert(strtolower($JSON->Datos), $DatosArray);
				// echo json_encode(["guardado" => true, "id" => $InsertID, "da" => $DatosArray, "sql" => $DB->getLastQuery()]);

				if($InsertID)
	    			echo json_encode(["guardado" => true, "id" => $InsertID]);
			}

		}
	}

	else if($JSON->Caso == "borrar")
	{
		if(!empty($JSON->Extra))
		{
			$Tabla = $JSON->Datos;
			$Datos = $JSON->Extra;

			$DB->where('id', $Datos->id);

			$Resultado = ["borrado" => false];

			if($DB->delete(strtolower($Tabla))) 
				$Resultado["borrado"] = true;
			else
				$Resultado["error"] = "Error";

			echo json_encode($Resultado);
		}
	}

	else if($JSON->Caso == "borrarCond")
	{
		if(!empty($JSON->Datos->Condiciones)){
			// echo json_encode($JSON->Datos->Where);
			$Condiciones = (array)$JSON->Datos->Condiciones;
			foreach ($Condiciones as $Condicion) {
			// echo json_encode($Condicion);
				$DB->where($Condicion->Campo, $Condicion->Valor);
			}
		}

		$Resultado = ["borrado" => false];

		if($DB->delete(strtolower($JSON->Datos->Tabla))) 
			$Resultado["borrado"] = true;
		else
			$Resultado["error"] = "Error";

		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "pagUsuarios")
	{
		$DB_Where = '';
		$DB_Limit = '';
		$DB_Select = 'SELECT subagente.nombre_completo, subagente.activo_inactivo, agente.agente, distribuidor.distribuidor, tipo_distribuidor.tipo_distribuidor, rol.rol,
			subagente.correo_electronico, subagente.id AS subagente_id, agente.id AS agente_id, distribuidor.id AS distribuidor_id, tipo_distribuidor.id AS tipo_distribuidor_id, 
			rol.id AS rol_id ';

		$DB_Where .= filtrar($JSON->Datos->Where);

		$DB_Where .= filtrar($JSON->Datos->Filtro);

		if(!$JSON->Datos->Excel)
			$DB_Limit = ' LIMIT '.($JSON->Datos->Pagina * $JSON->Datos->Cantidad).', '.$JSON->Datos->Cantidad;

		if($JSON->Datos->Excel)
			$DB_Select = "SELECT subagente.nombre_completo AS 'Subagente', agente.agente AS 'Agente', distribuidor.distribuidor AS 'Distribuidor', 
			tipo_distribuidor.tipo_distribuidor AS 'Tipo de distribuidor', rol.rol AS 'Rol', subagente.correo_electronico AS 'Correo electrónico' ";

		$DB_FROM = '
		FROM subagente 
			INNER JOIN agente 
		    	ON subagente.id_agente = agente.id 
		    INNER JOIN distribuidor
		    	ON agente.id_distribuidor = distribuidor.id
		    INNER JOIN tipo_distribuidor
		    	ON distribuidor.id_tipo_distribuidor = tipo_distribuidor.id
		    INNER JOIN rol
		    	ON subagente.id_rol = rol.id
		';

		$Usuarios = $DB->rawQuery($DB_Select . $DB_FROM. $DB_Where.'
		ORDER BY subagente.id DESC' . $DB_Limit);	

		// $UsuariosTotal = $DB->get("subagente");
		// $UsuariosTotal = $DB->getOne ("subagente", "count(*) as Total");
		// $UsuariosTotal['Total'];

		$UsuariosTotal = $DB->rawQuery($DB_Select . $DB_FROM. $DB_Where.'
		ORDER BY subagente.id DESC');	

		$Resutlado = ['Filas' => $Usuarios, 'Total' => $DB->count];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resutlado);
	}

	else if($JSON->Caso == "pagAgentes")
	{
		$DB_Where = '';
		$DB_Limit = '';
		$DB_Select = 'SELECT agente.id, agente.agente, distribuidor.distribuidor, tipo_distribuidor.tipo_distribuidor,
			agente.correo_electronico, agente.id AS agente_id, distribuidor.id AS distribuidor_id, tipo_distribuidor.id AS tipo_distribuidor_id';

		$DB_Where .= filtrar($JSON->Datos->Where);

		$DB_Where .= filtrar($JSON->Datos->Filtro);

		if($JSON->Datos->Excel)
			$DB_Select = 'SELECT agente.agente, distribuidor.distribuidor, tipo_distribuidor.tipo_distribuidor,
			agente.correo_electronico, agente.id AS agente_id, distribuidor.id AS distribuidor_id, tipo_distribuidor.id AS tipo_distribuidor_id';

		$DB_FROM = '
		FROM agente 
		    INNER JOIN distribuidor
		    	ON agente.id_distribuidor = distribuidor.id
		    INNER JOIN tipo_distribuidor
		    	ON distribuidor.id_tipo_distribuidor = tipo_distribuidor.id
		';

		$Usuarios = $DB->rawQuery($DB_Select . $DB_FROM. $DB_Where.'
		ORDER BY agente.id DESC' . $DB_Limit);	

		// $UsuariosTotal = $DB->get("subagente");
		// $UsuariosTotal = $DB->getOne ("subagente", "count(*) as Total");
		// $UsuariosTotal['Total'];

		$UsuariosTotal = $DB->rawQuery($DB_Select . $DB_FROM. $DB_Where.'
		ORDER BY agente.id DESC');	

		$Resutlado = ['Filas' => $Usuarios, 'Total' => $DB->count];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resutlado);
	}

	else if($JSON->Caso == "pagDistribuidores")
	{
		$DB_Where = '';
		$DB_Limit = '';
		$DB_Select = 'SELECT distribuidor.distribuidor, tipo_distribuidor.tipo_distribuidor,
			distribuidor.correo_electronico, distribuidor.id AS distribuidor_id, tipo_distribuidor.id AS tipo_distribuidor_id';

		$DB_Where .= filtrar($JSON->Datos->Where);

		$DB_Where .= filtrar($JSON->Datos->Filtro);

		if($JSON->Datos->Excel)
			$DB_Select = 'SELECT distribuidor.distribuidor, tipo_distribuidor.tipo_distribuidor,
			distribuidor.correo_electronico, distribuidor.id AS distribuidor_id, tipo_distribuidor.id AS tipo_distribuidor_id';

		$DB_FROM = '
		FROM distribuidor 
		    INNER JOIN tipo_distribuidor
		    	ON distribuidor.id_tipo_distribuidor = tipo_distribuidor.id
		';

		$Usuarios = $DB->rawQuery($DB_Select . $DB_FROM. $DB_Where.'
		ORDER BY distribuidor.id DESC' . $DB_Limit);	

		// $UsuariosTotal = $DB->get("subagente");
		// $UsuariosTotal = $DB->getOne ("subagente", "count(*) as Total");
		// $UsuariosTotal['Total'];

		$UsuariosTotal = $DB->rawQuery($DB_Select . $DB_FROM. $DB_Where.'
		ORDER BY distribuidor.id DESC');	

		$Resutlado = ['Filas' => $Usuarios, 'Total' => $DB->count];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resutlado);
	}

	else if($JSON->Caso == "pagProspectos")
	{
		$DB_Where = '';
		$DB_Limit = '';
		$DB_Select = 'SELECT cliente.id,	DATE_FORMAT(cliente.fecha_registro_prospecto, "%d/%m/%Y") AS fecha_registro_prospecto, cliente.nombre_completo,	
			DATE_FORMAT(cliente.fecha_nacimiento, "%d/%m/%Y") AS fecha_nacimiento, cliente.correo_electronico, cliente.telefono,
			subagente.nombre_completo AS sub_agente, agente.agente, distribuidor.distribuidor, tipo_distribuidor.tipo_distribuidor, fuente_referencia.fuente_referencia,
			subagente.id AS subagente_id, agente.id AS agente_id, distribuidor.id AS distribuidor_id, tipo_distribuidor.id AS tipo_distribuidor_id ';

		// if(!empty($JSON->Datos->Filtro))
		// 	$DB_Where = " AND " . $JSON->Datos->Filtro->Campo . " LIKE '%" . $JSON->Datos->Filtro->Valor . "%'";

		$DB_Where .= filtrar($JSON->Datos->Where);

		$DB_Where .= filtrar($JSON->Datos->Filtro);

		if(!$JSON->Datos->Excel)
			$DB_Limit = ' LIMIT '.($JSON->Datos->Pagina * $JSON->Datos->Cantidad).', '.$JSON->Datos->Cantidad;

		if($JSON->Datos->Excel)
			$DB_Select = "SELECT	DATE_FORMAT(cliente.fecha_registro_prospecto, '%d/%m/%Y') AS 'Fecha de registro', cliente.nombre_completo AS 'Cliente',	DATE_FORMAT(cliente.fecha_nacimiento, '%d/%m/%Y') AS 'Fecha de nacimiento',
				cliente.correo_electronico AS 'Correo electrónico', cliente.telefono AS 'Teléfono', subagente.nombre_completo AS 'Subagente', agente.agente AS 'Agente', 
				distribuidor.distribuidor AS 'Distribuidor', tipo_distribuidor.tipo_distribuidor AS 'Tipo de distribuidor', fuente_referencia.fuente_referencia AS 'Fuente de referencia' ";

		$DB_FROM = '
		FROM cliente
        	INNER JOIN subagente
            	ON cliente.id_subagente = subagente.id
			INNER JOIN agente 
		    	ON subagente.id_agente = agente.id 
		    INNER JOIN distribuidor
		    	ON agente.id_distribuidor = distribuidor.id
		    INNER JOIN tipo_distribuidor
		    	ON distribuidor.id_tipo_distribuidor = tipo_distribuidor.id
		    INNER JOIN fuente_referencia
		    	ON cliente.id_fuente_referencia = fuente_referencia.id
            INNER JOIN cotizacion 
            	ON cliente.id = cotizacion.id_cliente
		WHERE (cotizacion.folio = "" OR cotizacion.folio IS NULL)'.$DB_Where.'
		GROUP BY cliente.id
		ORDER BY cliente.id DESC';

		$Prospectos = $DB->rawQuery($DB_Select . $DB_FROM . $DB_Limit);
		// LIMIT '.($JSON->Datos->Pagina * $JSON->Datos->Cantidad).', '.$JSON->Datos->Cantidad);	

		// $UsuariosTotal = $DB->get("subagente");
		// $DB->where("folio", "");
		// $DB->groupBy ("id_cliente");
		// $ProspectosTotal = $DB->get("cotizacion");
		// $ProspectosTotal = $DB->getOne("cotizacion", "count(*) as Total");
		// $UsuariosTotal['Total'];

		$ProspectosTotal = $DB->rawQuery($DB_Select . $DB_FROM);

		$Resutlado = ['Filas' => $Prospectos, 'Total' => $DB->count, 'Consulta' => $DB->getLastQuery()];
		// $Resutlado = ['Prospectos' => $Prospectos, 'Total' => $ProspectosTotal['Total']];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resutlado);
	}

	else if($JSON->Caso == "pagAsegurados")
	{
		$DB_Where = '';
		$DB_Limit = '';
		$DB_Select = 'SELECT cotizacion.id, cotizacion.id_tipo_poliza,
		    DATE_FORMAT(cliente.fecha_registro_asegurado, "%d/%m/%Y") AS fecha_registro_asegurado, 
			cliente.nombre_completo, DATE_FORMAT(cliente.fecha_nacimiento, "%d/%m/%Y") AS fecha_nacimiento, cliente.id AS cliente_id,
		    DATE_FORMAT(cotizacion.salida, "%d/%m/%Y") as salida, DATE_FORMAT(cotizacion.regreso, "%d/%m/%Y") as regreso, cotizacion.id AS cotizacion_id, cotizacion.liquidado, cotizacion.saldo_pendiente_USD,
		    cotizacion.saldo_pendiente_MXN, cotizacion.precioMXN, cotizacion.precioUSD,
		    cotizacion_destino.pais AS pais_destino,
		    tipo_poliza.tipo_poliza,
		    tipo_distribuidor.tipo_distribuidor, tipo_distribuidor.id AS tipo_distribuidor_id,
		    distribuidor.distribuidor, distribuidor.id AS distribuidor_id,
		    agente.agente, agente.id AS agente_id,
		    subagente.nombre_completo AS subagente_nombre_completo, subagente.id AS subagente_id,
			cotizacion.estado_t, 
		    fuente_referencia.fuente_referencia ';

		// if(!empty($JSON->Datos->Filtro))
		// 	$DB_Where = " AND " . $JSON->Datos->Filtro->Campo . " LIKE '%" . $JSON->Datos->Filtro->Valor . "%'";

		$DB_Where .= filtrar($JSON->Datos->Where);

		$DB_Where .= filtrar($JSON->Datos->Filtro);

		if(!$JSON->Datos->Excel)
			$DB_Limit = ' LIMIT '.($JSON->Datos->Pagina * $JSON->Datos->Cantidad).', '.$JSON->Datos->Cantidad;

		if($JSON->Datos->Excel)
			$DB_Select = "SELECT 
			    DATE_FORMAT(cliente.fecha_registro_asegurado, '%d/%m/%Y'), AS 'Fecha de registro', cliente.nombre_completo AS 'Cliente', DATE_FORMAT(cliente.fecha_nacimiento, '%d/%m/%Y'), AS 'Fecha de nacimiento',
			    DATE_FORMAT(cotizacion.salida, '%d/%m/%Y') AS 'Fecha de salida', DATE_FORMAT(cotizacion.regreso, '%d/%m/%Y') AS 'Fecha de regreso',
			    cotizacion_destino.pais AS 'País destino',
			    tipo_poliza.tipo_poliza AS 'Tipo de póliza',
			    tipo_distribuidor.tipo_distribuidor AS 'Tipo de distribuidor',
			    distribuidor.distribuidor AS 'Distribuidor',
			    agente.agente AS 'Agente',
			    subagente.nombre_completo AS 'Subagente',
			    fuente_referencia.fuente_referencia AS 'Fuente de referencia', cotizacion.estado_t ";

		$DB_FROM = '
		FROM 
		    cliente
		    INNER JOIN cotizacion
		    	ON cliente.id = cotizacion.id_cliente
		    INNER JOIN pais AS cotizacion_destino
		        ON cotizacion.destino = cotizacion_destino.id
		    INNER JOIN tipo_poliza
		        ON cotizacion.id_tipo_poliza = tipo_poliza.id
		    INNER JOIN subagente
		        ON cliente.id_subagente = subagente.id
		    INNER JOIN agente
		        ON subagente.id_agente = agente.id
		    INNER JOIN distribuidor
		        ON agente.id_distribuidor = distribuidor.id
		    INNER JOIN tipo_distribuidor
		        ON distribuidor.id_tipo_distribuidor = tipo_distribuidor.id
		    INNER JOIN fuente_referencia
		        ON cliente.id_fuente_referencia = fuente_referencia.id
		WHERE
			(NOT cotizacion.folio = "" AND cotizacion.folio IS NOT NULL) ';

		// $Asegurados = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		// GROUP BY cliente.id
		// ORDER BY cliente.id ASC '.$DB_Limit);
		$Asegurados = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		ORDER BY cliente.id DESC '.$DB_Limit);
		// LIMIT '.($JSON->Datos->Pagina * $JSON->Datos->Cantidad).', '.$JSON->Datos->Cantidad);	

		// $UsuariosTotal = $DB->get("subagente");
		// $DB->where("NOT folio", "");
		// $DB->groupBy ("id_cliente");
		// $AseguradosTotal = $DB->get("cotizacion");
		// $UsuariosTotal['Total'];


		$AseguradosTotal = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		GROUP BY cliente.id
		ORDER BY cliente.id ASC ');

		foreach($Asegurados as &$Fila)
		{
			// $Resultado = ['Filas' => $Fila];
			// echo json_encode($Fila);
			if($Fila['estado_t'] == 0)
				$Fila['estado_t'] = 'NEW';
			if($Fila['estado_t'] == 1)
				$Fila['estado_t'] = 'EXT';
			if($Fila['estado_t'] == 2)
				$Fila['estado_t'] = 'CAN';
			if($Fila['estado_t'] == 3)
				$Fila['estado_t'] = 'CORR';
		}

		// $Resutlado = ['Asegurados' => $Asegurados, 'Total' => $AseguradosTotal['Total'], 'Consulta' => $DB->getLastQuery()];
		$Resutlado = ['Filas' => $Asegurados, 'Total' => $DB->count, 'Consulta' => $DB->getLastQuery()];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resutlado);
	}

	else if($JSON->Caso == "pagADMPagos") // PAGOS
	{
		$DB_Where = '';
		$DB_Limit = '';
		$DB_Select = 'SELECT pago.id, pago.no_recibo, pago.fecha_pago, cliente.nombre_completo AS cliente, cotizacion.id AS cotizacion_id, pago.archivo,
		cuenta.cuenta, forma_pago.forma_pago, pago.cantidad_abonada_MXN, pago.cantidad_abonada_USD, pago.tipo_cambio, cotizacion.saldo_pendiente_MXN, cliente.id AS cliente_id, 
		tipo_distribuidor.tipo_distribuidor, distribuidor.distribuidor, agente.agente, subagente.nombre_completo AS subagente ';


		$DB_Where .= filtrar($JSON->Datos->Where);

		$DB_Where .= filtrar($JSON->Datos->Filtro);

		if(!$JSON->Datos->Excel)
			$DB_Limit = ' LIMIT '.($JSON->Datos->Pagina * $JSON->Datos->Cantidad).', '.$JSON->Datos->Cantidad;

		if($JSON->Datos->Excel)
			$DB_Select = "SELECT pago.no_recibo AS 'No. recibo', pago.fecha_pago AS 'Fecha de pago', cliente.nombre_completo AS 'Cliente', 
				cuenta.cuenta AS 'Cuenta', forma_pago.forma_pago AS 'Forma de pago', pago.cantidad_abonada_MXN AS 'Cantidad abonada', pago.cantidad_abonada_USD AS 'Cantidad abonada (USD)',
				pago.tipo_cambio AS 'Tipo de cambio', cotizacion.saldo_pendiente_MXN AS 'Saldo pendiente (MXN)', 
				tipo_distribuidor.tipo_distribuidor AS 'Tipo de distribuidor', distribuidor.distribuidor AS 'Distribuidor', agente.agente AS 'Agente',
				subagente.nombre_completo AS 'Subagente'  ";

		$DB_FROM = '
		FROM pago
			INNER JOIN cotizacion ON pago.id_cotizacion = cotizacion.id
		    INNER JOIN cliente ON cotizacion.id_cliente = cliente.id
		    INNER JOIN cuenta ON pago.id_cuenta = cuenta.id
		    INNER JOIN forma_pago ON pago.id_forma_pago = forma_pago.id
		    INNER JOIN subagente ON pago.id_subagente = subagente.id
		    	INNER JOIN agente ON subagente.id_agente = agente.id
		        	INNER JOIN distribuidor ON agente.id_distribuidor = distribuidor.id
						INNER JOIN tipo_distribuidor ON distribuidor.id_tipo_distribuidor = tipo_distribuidor.id 
		WHERE 1 ';
		// WHERE
		// 	((cotizacion.saldo_pendiente_MXN > 0) OR (cotizacion.saldo_pendiente_USD > 0)) ';

		$Cuentas = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		ORDER BY pago.id DESC '.$DB_Limit);


		$CuetnasTotal = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		ORDER BY pago.id DESC ');

		$Resultado = ['Filas' => $Cuentas, 'Total' => $DB->count, 'Consulta' => $DB->getLastQuery()];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "pagADMComisionesPendiente") 
	{
		$DB_Where = '';
		$DB_Limit = '';
		// $DB_Select = "SELECT comision.id, cotizacion.id AS comision_id, DATE_FORMAT(cotizacion.fecha_asegurado, '%d/%m/%Y') AS fecha_asegurado,
		// subagente.nombre_completo AS 'subagente', agente.agente, distribuidor.distribuidor, tipo_distribuidor.tipo_distribuidor, 
		// cliente.nombre_completo AS 'cliente', cotizacion.precioMXN, comision.factura, comision.comision, comision.impuestos, comision.comision_final ";
		$DB_Select = "
		SELECT cotizacion.id as comision_id, cotizacion.estado_t, cliente.nombres, cliente.apellidos, DATE_FORMAT(cliente.fecha_nacimiento, '%d/%m/%Y') AS fecha_nacimiento, 
		pais_origen.pais AS pais_origen, pais_origen.abreviacion AS pais_origen_ab, DATE_FORMAT(cotizacion.salida, '%d/%m/%Y') AS salida, 
		pais_destino.pais AS pais_destino, pais_destino.abreviacion AS pais_destino_ab, 
		DATE_FORMAT(cotizacion.regreso, '%d/%m/%Y') AS regreso, tipo_poliza.dsm, cotizacion.precioUSD,  cotizacion.precioUSD_descuentoSA, 
		cotizacion.precioUSD_comision, cotizacion.precioUSD_total, cotizacion.precioMXN, cotizacion.precioMXN_descuentoSA, 
		cotizacion.precioMXN_comision, cotizacion.precioMXN_total, cotizacion.fecha_asegurado, comision.id,
		subagente.nombre_completo AS 'subagente', agente.agente, distribuidor.distribuidor, tipo_distribuidor.tipo_distribuidor 
		";

		// if(!empty($JSON->Datos->Where))
		// 	$DB_Where .= " AND " . $JSON->Datos->Where->Campo . " = " . $JSON->Datos->Where->Valor;

		// if(!empty($JSON->Datos->Filtro))
		// 	$DB_Where .= " AND " . $JSON->Datos->Filtro->Campo . " LIKE '%" . $JSON->Datos->Filtro->Valor . "%'";

		$DB_Where .= filtrar($JSON->Datos->Where);

		$DB_Where .= filtrar($JSON->Datos->Filtro);

		if(!$JSON->Datos->Excel && !$JSON->Datos->ExcelIngles)
			$DB_Limit = ' LIMIT '.($JSON->Datos->Pagina * $JSON->Datos->Cantidad).', '.$JSON->Datos->Cantidad;

		if($JSON->Datos->Excel || $JSON->Datos->ExcelIngles)
			// $DB_Select = "SELECT  DATE_FORMAT(cotizacion.fecha_asegurado, '%d/%m/%Y') AS 'Fecha de venta',
			// subagente.nombre_completo AS 'Subagente', agente.agente AS 'Agente', distribuidor.distribuidor AS 'Dsitribuidor', tipo_distribuidor.tipo_distribuidor AS 'Tipo de distribuidor', 
			// cliente.nombre_completo AS 'Cliente', cotizacion.precioMXN AS 'Precio', comision.comision AS 'Comision', 
			// comision.impuestos AS Impuestos, comision.comision_final AS 'Comisión final', comision.factura AS 'Factura'";
			$DB_Select = "
			SELECT cotizacion.estado_t AS 'Transaccion', cliente.nombres AS 'Nombres', cliente.apellidos AS 'Apellidos', 
			DATE_FORMAT(cliente.fecha_nacimiento, '%d/%m/%Y') AS 'Fecha de nacimiento', 
			pais_origen.abreviacion AS 'Pais origen', 
			DATE_FORMAT(cotizacion.salida, '%d/%m/%Y') AS 'Salida', 
			pais_destino.abreviacion AS 'Pais destino', 
			DATE_FORMAT(cotizacion.regreso, '%d/%m/%Y') AS 'Regreso', tipo_poliza.dsm AS 'Tipo de póliza', 
			cotizacion.precioUSD AS 'Precio USD',  cotizacion.precioUSD_descuento as 'Descuento USD', 
			cotizacion.precioUSD_comision as 'Comision USD', cotizacion.precioUSD_total as 'Precio final USD', 
			cotizacion.precioMXN as 'Precio MXN', cotizacion.precioMXN_descuento as 'Descuento MXN', 
			cotizacion.precioMXN_comision as 'Comision MXN', cotizacion.precioMXN_total as 'Precio final MXN'
		";

		// $DB_FROM = '		
		// FROM cotizacion
		// INNER JOIN cliente ON cotizacion.id_cliente = cliente.id
        // INNER JOIN comision ON cotizacion.folio = comision.folio_seguro
		// INNER JOIN subagente ON cotizacion.id_subagente = subagente.id
		// 	INNER JOIN agente ON subagente.id_agente = agente.id
		// 		INNER JOIN distribuidor ON agente.id_distribuidor = distribuidor.id
		// 			INNER JOIN tipo_distribuidor ON distribuidor.id_tipo_distribuidor = tipo_distribuidor.id
		// WHERE (cotizacion.fecha_asegurado > "0000-00-00" AND comision.fecha_pago = "0000-00-00" AND cotizacion.estado_t < 2) ';
		$DB_FROM = '
			FROM cotizacion
				INNER JOIN cliente ON cotizacion.id_cliente = cliente.id
				INNER JOIN subagente ON cotizacion.id_subagente = subagente.id
					INNER JOIN agente ON subagente.id_agente = agente.id
						INNER JOIN distribuidor ON agente.id_distribuidor = distribuidor.id
							INNER JOIN tipo_distribuidor ON distribuidor.id_tipo_distribuidor = tipo_distribuidor.id
				INNER JOIN pais AS pais_origen ON pais_origen.id = cotizacion.origen
				INNER JOIN pais AS pais_destino ON pais_destino.id = cotizacion.destino
				INNER JOIN tipo_poliza ON cotizacion.id_tipo_poliza = tipo_poliza.id
				INNER JOIN comision ON comision.folio_seguro = cotizacion.folio
			WHERE (cotizacion.fecha_asegurado > "0000-00-00" AND comision.fecha_pago = "0000-00-00" AND cotizacion.estado_t < 2) 
		';

		$Cuentas = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		ORDER BY cotizacion.id DESC '.$DB_Limit);


		$CuetnasTotal = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		ORDER BY cotizacion.id DESC ');

		if(!$JSON->Datos->Excel && !$JSON->Datos->ExcelIngles)
			foreach($Cuentas as &$Fila)
			{
				// $Resultado = ['Filas' => $Fila];
				// echo json_encode($Fila);
				if($Fila['estado_t'] == 0)
					$Fila['estado_t'] = 'NEW';
				if($Fila['estado_t'] == 1)
					$Fila['estado_t'] = 'EXT';
				if($Fila['estado_t'] == 2)
					$Fila['estado_t'] = 'CAN';
				if($Fila['estado_t'] == 3)
					$Fila['estado_t'] = 'CORR';
			}
		else
			foreach($Cuentas as &$Fila)
			{
				// $Resultado = ['Filas' => $Fila];
				// echo json_encode($Fila);
				if($Fila['Transaccion'] == 0)
					$Fila['Transaccion'] = 'NEW';
				if($Fila['Transaccion'] == 1)
					$Fila['Transaccion'] = 'EXT';
				if($Fila['Transaccion'] == 2)
					$Fila['Transaccion'] = 'CAN';
				if($Fila['Transaccion'] == 3)
					$Fila['Transaccion'] = 'CORR';
			}

		$Resultado = ['Filas' => $Cuentas, 'Total' => $DB->count, 'Consulta' => $DB->getLastQuery()];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "pagADMComisionesPagado")
	{
		$DB_Where = '';
		$DB_Limit = '';
		// $DB_Select = "SELECT comision.id, cotizacion.id AS comision_id,  DATE_FORMAT(cotizacion.fecha_asegurado, '%d/%m/%Y') AS fecha_asegurado,
		// subagente.nombre_completo AS 'subagente', agente.agente, distribuidor.distribuidor, tipo_distribuidor.tipo_distribuidor, 
		// cliente.nombre_completo AS 'cliente', cotizacion.precioMXN, comision.factura, comision.comision, comision.impuestos, comision.comision_final ";
		$DB_Select = "
			SELECT cotizacion.id as comision_id, cotizacion.estado_t, cliente.nombres, cliente.apellidos, DATE_FORMAT(cliente.fecha_nacimiento, '%d/%m/%Y') AS fecha_nacimiento, 
			pais_origen.pais AS pais_origen, pais_origen.abreviacion AS pais_destino_ab, DATE_FORMAT(cotizacion.salida, '%d/%m/%Y') AS salida, 
			pais_destino.pais AS pais_destino, pais_destino.abreviacion AS pais_destino_ab, 
			DATE_FORMAT(cotizacion.regreso, '%d/%m/%Y') AS regreso, tipo_poliza.dsm, cotizacion.precioUSD,  cotizacion.precioUSD_descuentoSA, 
			cotizacion.precioUSD_comision, cotizacion.precioUSD_total, cotizacion.precioMXN, cotizacion.precioMXN_descuentoSA, 
			cotizacion.precioMXN_comision, cotizacion.precioMXN_total, cotizacion.fecha_asegurado, comision.id,
			subagente.nombre_completo AS 'subagente', agente.agente, distribuidor.distribuidor, tipo_distribuidor.tipo_distribuidor 
		";

		// if(!empty($JSON->Datos->Where))
		// 	$DB_Where .= " AND " . $JSON->Datos->Where->Campo . " = " . $JSON->Datos->Where->Valor;

		// if(!empty($JSON->Datos->Filtro))
		// 	$DB_Where .= " AND " . $JSON->Datos->Filtro->Campo . " LIKE '%" . $JSON->Datos->Filtro->Valor . "%'";

		$DB_Where .= filtrar($JSON->Datos->Where);

		$DB_Where .= filtrar($JSON->Datos->Filtro);

		// if(!$JSON->Datos->Excel)
		if(!$JSON->Datos->Excel && !$JSON->Datos->ExcelIngles)
			$DB_Limit = ' LIMIT '.($JSON->Datos->Pagina * $JSON->Datos->Cantidad).', '.$JSON->Datos->Cantidad;

		if($JSON->Datos->Excel || $JSON->Datos->ExcelIngles)
			// $DB_Select = "SELECT  DATE_FORMAT(cotizacion.fecha_asegurado, '%d/%m/%Y') AS 'Fecha de venta',
			// subagente.nombre_completo AS 'Subagente', agente.agente AS 'Agente', distribuidor.distribuidor AS 'Dsitribuidor', tipo_distribuidor.tipo_distribuidor AS 'Tipo de distribuidor', 
			// cliente.nombre_completo AS 'Cliente', cotizacion.precioMXN AS 'Precio', comision.comision AS 'Comision', 
			// comision.impuestos AS Impuestos, comision.comision_final AS 'Comisión final', comision.factura AS 'Factura'";
			$DB_Select = "
				SELECT cotizacion.estado_t AS 'Transaccion', cliente.nombres AS 'Nombres', cliente.apellidos AS 'Apellidos', 
				DATE_FORMAT(cliente.fecha_nacimiento, '%d/%m/%Y') AS 'Fecha de nacimiento', 
				pais_origen.abreviacion AS 'Origen', 
				DATE_FORMAT(cotizacion.salida, '%d/%m/%Y') AS 'Salida', 
				pais_destino.abreviacion AS 'Destino', 
				DATE_FORMAT(cotizacion.regreso, '%d/%m/%Y') AS 'Regreso', tipo_poliza.dsm AS 'Tipo de póliza', 
				cotizacion.precioUSD AS 'Precio USD',  cotizacion.precioUSD_descuento as 'Descuento USD', 
				cotizacion.precioUSD_comision as 'Comision USD', cotizacion.precioUSD_total as 'Precio final USD', 
				cotizacion.precioMXN as 'Precio MXN', cotizacion.precioMXN_descuento as 'Descuento MXN', 
				cotizacion.precioMXN_comision as 'Comision MXN', cotizacion.precioMXN_total as 'Precio final MXN'
			";

		// $DB_FROM = '		
		// FROM cotizacion
		// INNER JOIN cliente ON cotizacion.id_cliente = cliente.id
        // INNER JOIN comision ON cotizacion.folio = comision.folio_seguro
		// INNER JOIN subagente ON cotizacion.id_subagente = subagente.id
		// 	INNER JOIN agente ON subagente.id_agente = agente.id
		// 		INNER JOIN distribuidor ON agente.id_distribuidor = distribuidor.id
		// 			INNER JOIN tipo_distribuidor ON distribuidor.id_tipo_distribuidor = tipo_distribuidor.id
		// WHERE (cotizacion.fecha_asegurado > "0000-00-00" AND comision.fecha_pago > "0000-00-00" AND cotizacion.estado_t < 2) ';
		
		$DB_FROM = '
			FROM cotizacion
				INNER JOIN cliente ON cotizacion.id_cliente = cliente.id
				INNER JOIN subagente ON cotizacion.id_subagente = subagente.id
					INNER JOIN agente ON subagente.id_agente = agente.id
						INNER JOIN distribuidor ON agente.id_distribuidor = distribuidor.id
							INNER JOIN tipo_distribuidor ON distribuidor.id_tipo_distribuidor = tipo_distribuidor.id
				INNER JOIN pais AS pais_origen ON pais_origen.id = cotizacion.origen
				INNER JOIN pais AS pais_destino ON pais_destino.id = cotizacion.destino
				INNER JOIN tipo_poliza ON cotizacion.id_tipo_poliza = tipo_poliza.id
				INNER JOIN comision ON comision.folio_seguro = cotizacion.folio
			WHERE (cotizacion.fecha_asegurado > "0000-00-00" AND comision.fecha_pago > "0000-00-00" AND cotizacion.estado_t < 2) 
		';

		$Cuentas = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		ORDER BY cotizacion.id DESC '.$DB_Limit);


		$CuetnasTotal = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		ORDER BY cotizacion.id DESC ');		

		if(!$JSON->Datos->Excel && !$JSON->Datos->ExcelIngles)
			foreach($Cuentas as &$Fila)
			{
				// $Resultado = ['Filas' => $Fila];
				// echo json_encode($Fila);
				if($Fila['estado_t'] == 0)
					$Fila['estado_t'] = 'NEW';
				if($Fila['estado_t'] == 1)
					$Fila['estado_t'] = 'EXT';
				if($Fila['estado_t'] == 2)
					$Fila['estado_t'] = 'CAN';
				if($Fila['estado_t'] == 3)
					$Fila['estado_t'] = 'CORR';
			}
		else
			foreach($Cuentas as &$Fila)
			{
				// $Resultado = ['Filas' => $Fila];
				// echo json_encode($Fila);
				if($Fila['Transaccion'] == 0)
					$Fila['Transaccion'] = 'NEW';
				if($Fila['Transaccion'] == 1)
					$Fila['Transaccion'] = 'EXT';
				if($Fila['Transaccion'] == 2)
					$Fila['Transaccion'] = 'CAN';
				if($Fila['Transaccion'] == 3)
					$Fila['Transaccion'] = 'CORR';
			}

		$Resultado = ['Filas' => $Cuentas, 'Total' => $DB->count, 'Consulta' => $DB->getLastQuery()];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "pagIncidencias")
	{
		$DB_Where = '';
		$DB_Limit = '';
		$DB_Select = 'SELECT incidente.*, cliente.nombre_completo, cotizacion.folio, cotizacion.id AS cotizacion_id, subagente.nombre_completo AS subagente,  cotizacion.id_cliente AS cliente_id,
		pais.pais, agente.agente, distribuidor.distribuidor, tipo_distribuidor.tipo_distribuidor  ';

		// if(!empty($JSON->Datos->Where))
		// 	$DB_Where .= " AND " . $JSON->Datos->Where->Campo . " = " . $JSON->Datos->Where->Valor;

		// if(!empty($JSON->Datos->Filtro))
		// 	$DB_Where .= " AND " . $JSON->Datos->Filtro->Campo . " LIKE '%" . $JSON->Datos->Filtro->Valor . "%'";

		$DB_Where .= filtrar($JSON->Datos->Where);

		$DB_Where .= filtrar($JSON->Datos->Filtro);

		if(!$JSON->Datos->Excel)
			$DB_Limit = ' LIMIT '.($JSON->Datos->Pagina * $JSON->Datos->Cantidad).', '.$JSON->Datos->Cantidad;

		// echo json_encode($DB_Where);


		if($JSON->Datos->Excel)
			$DB_Select = "SELECT incidente.*, cotizacion.folio AS 'Folio', subagente.nombre_completo AS 'Subagente', 
		pais.pais AS 'País', agente.agente AS 'Agente', distribuidor.distribuidor AS 'Distribuidor', tipo_distribuidor.tipo_distribuidor AS 'Tipo de distribuidor' ";

		$DB_FROM = '
		FROM incidente 
			INNER JOIN subagente ON incidente.id_subagente = subagente.id
		    	INNER JOIN agente on subagente.id_agente = agente.id
		        	INNER JOIN distribuidor ON agente.id_distribuidor = distribuidor.id
		            	INNER JOIN tipo_distribuidor ON distribuidor.id_tipo_distribuidor = tipo_distribuidor.id
			INNER JOIN cotizacion ON incidente.id_cotizacion = cotizacion.id
				INNER JOIN cliente ON cotizacion.id_cliente = cliente.id
			INNER JOIN pais ON incidente.id_pais = pais.id
		WHERE 1 ';

		$Incidentes = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		ORDER BY incidente.id DESC '.$DB_Limit);


		$CuetnasTotal = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		ORDER BY incidente.id DESC ');

		$Resultado = ['Filas' => $Incidentes, 'Total' => $DB->count, 'Consulta' => $DB->getLastQuery()];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "pagConciliar")
	{
		$DB_Where = '';
		$DB_Limit = '';
		$DB_Select = 'SELECT cotizacion.id, DATE_FORMAT(cotizacion.fecha_asegurado, "%d/%m/%Y") AS fecha_asegurado, cliente.nombre_completo, cotizacion.estado_t,
		DATE_FORMAT(cliente.fecha_nacimiento, "%d/%m/%Y") AS fecha_nacimiento, cotizacion.precioMXN, cotizacion.precioUSD,
		cotizacion.factura_numero, cotizacion.factura_monto, DATE_FORMAT(cotizacion.factura_fecha, "%d/%m/%Y") AS factura_fecha, cotizacion.factura_archivo, cotizacion.numero_dias,
		pais_origen.pais AS pais_origen, cliente.ciudad, cliente.cp, pais_destino.pais AS pais_destino, 
		DATE_FORMAT(cotizacion.salida, "%d/%m/%Y") AS salida, DATE_FORMAT(cotizacion.regreso, "%d/%m/%Y") AS regreso, tipo_poliza.tipo_poliza, pais_destino.zona ';

		// if(!empty($JSON->Datos->Where))
		// 	$DB_Where .= " AND " . $JSON->Datos->Where->Campo . " = " . $JSON->Datos->Where->Valor;

		// if(!empty($JSON->Datos->Filtro))
		// 	$DB_Where .= " AND " . $JSON->Datos->Filtro->Campo . " LIKE '%" . $JSON->Datos->Filtro->Valor . "%'";

		$DB_Where .= filtrar($JSON->Datos->Where);

		$DB_Where .= filtrar($JSON->Datos->Filtro);

		if(!$JSON->Datos->Excel)
			$DB_Limit = ' LIMIT '.($JSON->Datos->Pagina * $JSON->Datos->Cantidad).', '.$JSON->Datos->Cantidad;

		// echo json_encode($DB_Where);


		if($JSON->Datos->Excel)
			$DB_Select = "SELECT cotizacion.id, DATE_FORMAT(cotizacion.fecha_asegurado, '%d/%m/%Y') AS fecha_asegurado, cliente.nombre_completo, cliente.fecha_nacimiento, cotizacion.precioMXN, cotizacion.precioUSD,
			cotizacion.factura_numero, cotizacion.factura_monto,, DATE_FORMAT(cotizacion.factura_fecha, '%d/%m/%Y') AS factura_fecha,
			pais_origen.pais AS pais_origen, cliente.ciudad, cliente.cp, pais_destino.pais AS pais_destino, DATE_FORMAT(cotizacion.salida, '%d/%m/%Y') AS salida, 
			DATE_FORMAT(cotizacion.regreso, '%d/%m/%Y') AS regreso, tipo_poliza.tipo_poliza, pais_destino.zona,
			cotizacion.estado_t as 'Transaccion'";

		$DB_FROM = '
		FROM cotizacion
			INNER JOIN cliente ON cotizacion.id_cliente = cliente.id
			INNER JOIN pais AS pais_destino ON cotizacion.destino = pais_destino.id
			INNER JOIN pais AS pais_origen ON cotizacion.origen = pais_origen.id
			INNER JOIN tipo_poliza ON cotizacion.id_tipo_poliza = tipo_poliza.id 
		WHERE 
			(NOT cotizacion.folio = "" AND cotizacion.folio IS NOT NULL)  
			 ';

		$Filas = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		ORDER BY cotizacion.id DESC '.$DB_Limit);


		$CuetnasTotal = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		ORDER BY cotizacion.id DESC ');

		if(!$JSON->Datos->Excel && !$JSON->Datos->ExcelIngles)
			foreach($Filas as &$Fila)
			{
				// $Resultado = ['Filas' => $Fila];
				// echo json_encode($Fila);
				if($Fila['estado_t'] == 0)
					$Fila['estado_t'] = 'NEW';
				if($Fila['estado_t'] == 1)
					$Fila['estado_t'] = 'EXT';
				if($Fila['estado_t'] == 2)
					$Fila['estado_t'] = 'CAN';
				if($Fila['estado_t'] == 3)
					$Fila['estado_t'] = 'CORR';
			}
		else
			foreach($Cuentas as &$Fila)
			{
				// $Resultado = ['Filas' => $Fila];
				// echo json_encode($Fila);
				if($Fila['Transaccion'] == 0)
					$Fila['Transaccion'] = 'NEW';
				if($Fila['Transaccion'] == 1)
					$Fila['Transaccion'] = 'EXT';
				if($Fila['Transaccion'] == 2)
					$Fila['Transaccion'] = 'CAN';
				if($Fila['Transaccion'] == 3)
					$Fila['Transaccion'] = 'CORR';
			}

		$Resultado = ['Filas' => $Filas, 'Total' => $DB->count, 'Consulta' => $DB->getLastQuery()];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "pagMargen")
	{
		$DB_Where = '';
		$DB_Limit = '';
		// $DB_Select = 'SELECT cotizacion.fecha_asegurado, cliente.nombres, cliente.apellidos, cotizacion.precioUSD, cotizacion.precioUSD_descuento, 
		// cotizacion.precioUSD_ajuste, cotizacion.precioUSD_iva, cotizacion.precioUSD_subtotal, cotizacion.precioUSD_comision, 
		// cotizacion.precioUSD_total, cotizacion.precioMXN_total, cobertura.costo, cobertura.dias, cobertura.costo_final, 
		// cotizacion.salida, DATE_FORMAT(cotizacion.regreso, "%d/%m/%Y") AS regreso, cotizacion.destino_cp, pais_destino.pais AS pais_destino, pais_destino.zona, pais_origen.pais AS pais_origen,
		// (cotizacion.precioUSD_total - cobertura.costo_final) AS "margen_usd",
		// cliente.fecha_nacimiento, cliente.sexo, cliente.correo_electronico, cliente.cp,
		// subagente.nombre_completo AS subagente, agente.agente, distribuidor.distribuidor, distribuidor.id_distribuidores, tipo_distribuidor.tipo_distribuidor,
		// tipo_poliza.tipo_poliza, CONCAT(tipo_poliza.a_codigo, tipo_poliza.codigo_poliza) AS codigo, tipo_seguro.tipo_seguro, "NEW" AS "tt"  ';
		$DB_Select = 'SELECT cliente.nombres, cliente.apellidos, 
		cobertura.dias, cobertura.costo_final, cotizacion.precioUSD, cotizacion.precioUSD_descuento, 
		cotizacion.precioUSD_ajuste, cotizacion.precioUSD_iva, cotizacion.precioUSD_subtotal, cotizacion.precioUSD_comision, 
		cotizacion.precioMXN_total,

		cotizacion.precioUSD_total, DATE_FORMAT(cotizacion.salida, "%d/%m/%Y") AS salida, DATE_FORMAT(cotizacion.regreso, "%d/%m/%Y") AS regreso, 
		cotizacion.destino_cp, cotizacion.destino_ciudad, pais_destino.pais AS pais_destino, pais_destino.zona, pais_origen.pais AS pais_origen,
		-- DATE_FORMAT(cliente.fecha_nacimiento, "%d/%m/%Y") AS fecha_nacimiento, cliente.sexo, cliente.correo_electronico, cliente.cp,
		DATE_FORMAT(cliente.fecha_nacimiento, "%d/%m/%Y") AS fecha_nacimiento, cliente.sexo, subagente.correo_electronico, cliente.cp,
		subagente.nombre_completo AS subagente, agente.agente, distribuidor.distribuidor, distribuidor.id_distribuidores, tipo_distribuidor.tipo_distribuidor,
		tipo_poliza.tipo_poliza, CONCAT(tipo_poliza.a_codigo, tipo_poliza.codigo_poliza) AS codigo, tipo_seguro.tipo_seguro, "NEW" AS "tt", cotizacion.fecha_asegurado ';

		// if(!empty($JSON->Datos->Where))
		// 	$DB_Where .= " AND " . $JSON->Datos->Where->Campo . " = " . $JSON->Datos->Where->Valor;

		$DB_Where .= filtrar($JSON->Datos->Where);

		$DB_Where .= filtrar($JSON->Datos->Filtro);

		if(!$JSON->Datos->Excel && !$JSON->Datos->ExcelIngles)
			$DB_Limit = ' LIMIT '.($JSON->Datos->Pagina * $JSON->Datos->Cantidad).', '.$JSON->Datos->Cantidad;

		// echo json_encode($DB_Where);


		if($JSON->Datos->Excel)
			$DB_Select = "SELECT cliente.nombres AS 'Nombres', cliente.apellidos AS 'Apellidos', cobertura.dias AS 'Días', cotizacion.salida AS 'Salida', cotizacion.regreso AS 'Regreso', 
			cotizacion.destino_cp AS 'Destino CP', cotizacion.destino_ciudad AS 'Destino Ciudad', pais_destino.pais AS 'País destino', 
			pais_destino.zona AS 'Zona', pais_origen.pais AS 'País destino',
			cliente.fecha_nacimiento AS 'Fecha de nacimiento', cliente.sexo AS 'Sexo', subagente.correo_electronico AS 'Correo electrónico', cliente.cp AS 'CP Origen',
			subagente.nombre_completo AS subagente, agente.agente, distribuidor.distribuidor, distribuidor.id_distribuidores, tipo_distribuidor.tipo_distribuidor,
			tipo_poliza.tipo_poliza AS 'Tipo Póliza', CONCAT(tipo_poliza.a_codigo, tipo_poliza.codigo_poliza) AS 'Código', tipo_seguro.tipo_seguro AS 'Tipo seguro', 'NEW' AS 'tt' ";

// DATE_FORMAT(cliente.fecha_nacimiento,'%d/%m/%Y')
		if($JSON->Datos->ExcelIngles)
			$DB_Select = "SELECT distribuidor.id_distribuidores AS 'Client Number', CONCAT(tipo_poliza.a_codigo, tipo_poliza.codigo_poliza) AS 'Policy Code', cotizacion.estado_t AS 'Transaction Type',
			cliente.apellidos AS 'Last Name', cliente.nombres AS 'First Name', DATE_FORMAT(cliente.fecha_nacimiento, '%d-%b-%Y') AS 'Birth Date',
			cliente.sexo AS 'Gender', subagente.correo_electronico AS 'E-Mail', DATE_FORMAT(cotizacion.salida, '%d-%b-%Y') AS 'Start Date', DATE_FORMAT(cotizacion.regreso, '%d-%b-%Y') AS 'End Date', 
			pais_origen.abreviacion AS 'Country of Origin', 
			cliente.ciudad AS 'Origin City', cliente.cp AS 'Origin Zip Code', 
			pais_destino.abreviacion AS 'Country of Destination', 
			cotizacion.destino_ciudad AS 'Destination City', cotizacion.destino_cp AS 'Destination ZIP Code', 
			CONCAT('Zone ', pais_destino.zona) AS 'Policy Territory', '' AS 'Internal Reference'";

		$DB_FROM = '
		FROM cotizacion
			INNER JOIN cliente ON cotizacion.id_cliente = cliente.id
			INNER JOIN tipo_poliza ON cotizacion.id_tipo_poliza = tipo_poliza.id
			INNER JOIN cobertura ON tipo_poliza.id = cobertura.id_tipo_poliza 
			INNER JOIN tipo_seguro ON tipo_poliza.id_tipo_seguro = tipo_seguro.id
			INNER JOIN subagente ON cotizacion.id_subagente = subagente.id
			INNER JOIN agente ON subagente.id_agente = agente.id
			INNER JOIN distribuidor ON agente.id_distribuidor = distribuidor.id
			INNER JOIN tipo_distribuidor ON distribuidor.id_tipo_distribuidor = tipo_distribuidor.id
		    INNER JOIN pais AS pais_origen ON cotizacion.origen = pais_origen.id 
		    INNER JOIN pais AS pais_destino ON cotizacion.destino = pais_destino.id 
		    INNER JOIN fuente_referencia ON cliente.id_fuente_referencia = fuente_referencia.id
		WHERE
			(NOT cotizacion.folio = "" AND cotizacion.folio IS NOT NULL)'
			 ;

		$Filas = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		GROUP BY cotizacion.id 
		ORDER BY cotizacion.id DESC '.$DB_Limit);


		$CuetnasTotal = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		GROUP BY cotizacion.id 
		ORDER BY cotizacion.id DESC ');

		// foreach($Filas as $Fila)
		// {
		// 	if($Fila['Transaction Type'] == 0)
		// 		$Fila['Transaction Type'] = 'NEW';
		// 	if($Fila['Transaction Type'] == 1)
		// 		$Fila['Transaction Type'] = 'EXT';
		// 	if($Fila['Transaction Type'] == 2)
		// 		$Fila['Transaction Type'] = 'CAN';
		// }
		if(($JSON->Datos->Excel || $JSON->Datos->ExcelIngles))
			foreach($Filas as &$Fila)
			{
				// $Resultado = ['Filas' => $Fila];
				// echo json_encode($Fila);
				if($Fila['Transaction Type'] == 0)
					$Fila['Transaction Type'] = 'NEW';
				if($Fila['Transaction Type'] == 1)
					$Fila['Transaction Type'] = 'EXT';
				if($Fila['Transaction Type'] == 2)
					$Fila['Transaction Type'] = 'CAN';
				if($Fila['Transaction Type'] == 3)
					$Fila['Transaction Type'] = 'CORR';

				if($Fila['Gender'] == "Masculino")
					$Fila['Gender'] = 'M';
				if($Fila['Gender'] == "Femenino")
					$Fila['Gender'] = 'F';
			}
		else
			foreach($Filas as &$Fila)
			{
				// $Resultado = ['Filas' => $Fila];
				// echo json_encode($Fila);
				if($Fila['estado_t'] == 0)
					$Fila['estado_t'] = 'NEW';
				if($Fila['estado_t'] == 1)
					$Fila['estado_t'] = 'EXT';
				if($Fila['estado_t'] == 2)
					$Fila['estado_t'] = 'CAN';
				if($Fila['estado_t'] == 3)
					$Fila['estado_t'] = 'CORR';
			}
		$Resultado = ['Filas' => $Filas, 'Total' => $DB->count, 'Consulta' => $DB->getLastQuery()];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "aseguradosExtra")
	{
		$DB_Where = '';
		$DB_Limit = '';
		$DB_Select = 'SELECT cobertura.costo, cobertura.dias, cobertura.costo_final ';

		if(!empty($JSON->Datos->Where))
			$DB_Where .= " WHERE " . $JSON->Datos->Where->Campo . " = " . $JSON->Datos->Where->Valor;

		// echo json_encode($DB_Where);

		$DB_FROM = 'FROM cotizacion 
		INNER JOIN tipo_poliza ON cotizacion.id_tipo_poliza = tipo_poliza.id
		INNER JOIN cobertura ON tipo_poliza.id = cobertura.id_tipo_poliza
		GROUP BY cotizacion.id
			 ';

		$Filas = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		ORDER BY cotizacion.id DESC ');


		// $CuetnasTotal = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		// ORDER BY cotizacion.id ASC ');

		$Resultado = ['Filas' => $Filas, 'Consulta' => $DB->getLastQuery()];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "coberturaCotizacion")
	{
		// $DB_Select = 'SELECT * FROM cotizacion INNER JOIN cobertura ON cotizacion.id_tipo_poliza = cobertura.id_tipo_poliza WHERE cotizacion.id = '.$JSON->Datos->Cotizacion.' AND cotizacion.numero_dias = cobertura.dias';
		$DB_Select = 'SELECT * FROM cotizacion INNER JOIN cobertura ON cotizacion.id_tipo_poliza = cobertura.id_tipo_poliza WHERE cotizacion.id = '.$JSON->Datos->Cotizacion.' AND cotizacion.precioUSD = cobertura.precio ';

		$Filas = $DB->rawQuery($DB_Select);


		// $CuetnasTotal = $DB->rawQuery($DB_Select.$DB_FROM.$DB_Where.'
		// ORDER BY cotizacion.id ASC ');

		$Resultado = ['Cobertura' => $Filas, 'Consulta' => $DB->getLastQuery()];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "pagCotizaciones")
	{

		$DB_Where = filtrar($JSON->Datos->Where);

		$Cotizaciones = $DB->rawQuery('SELECT tipo_seguro.tipo_seguro, tipo_poliza.tipo_poliza, cotizacionOrigen.pais AS pais_origen, cotizacionDestino.pais AS pais_destino, 
			DATE_FORMAT(cotizacion.salida, "%d/%m/%Y") AS salida, DATE_FORMAT(cotizacion.regreso, "%d/%m/%Y") AS regreso, cotizacion.precioMXN, cotizacion.precioUSD, cotizacion.tipo_cambio, cotizacion.id
		FROM cotizacion
			INNER JOIN tipo_poliza 
		    	ON cotizacion.id_tipo_poliza = tipo_poliza.id
			INNER JOIN tipo_seguro 
		    	ON cotizacion.id_tipo_seguro = tipo_seguro.id
		    INNER JOIN pais AS cotizacionOrigen
		    	ON cotizacion.origen = cotizacionOrigen.id 
		    INNER JOIN pais AS cotizacionDestino
		    	ON cotizacion.destino = cotizacionDestino.id
		WHERE (folio = "" OR folio IS NULL)	'.$DB_Where.'
		ORDER BY cotizacion.id DESC');	
		// WHERE id_cliente = '.$JSON->Datos->ID_Cliente.' AND (folio = "" OR folio IS NULL)	ORDER BY cotizacion.id ASC');	

		// $UsuariosTotal = $DB->get("subagente");
		// $DB->where("id_tipo_cliente", 1);
		// $ProspectosTotal = $DB->getOne("cliente", "count(*) as Total");
		// $UsuariosTotal['Total'];

		// $Resutlado = ['Cotizaciones' => $Cotizaciones];
		$Resutlado = ['Filas' => $Cotizaciones];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resutlado);
	}

	else if($JSON->Caso == "cotizacionConfirmar")
	{
		$InnersExtra = '
			INNER JOIN subagente
				ON cotizacion.id_subagente = subagente.id
			INNER JOIN agente
				ON subagente.id_agente = agente.id 
		';
		$SelectExtra = '		
		, subagente.nombre_completo AS subagente_nombre_completo, subagente.correo_electronico AS subagente_correo_electronico,
		agente.agente, subagente.id AS id_subagente  
		';
		// if(isset($JSON->Datos->Externa) && $JSON->Datos->Externa == true)
		// {
		// 	$InnersExtra = '';
		// 	$SelectExtra = '';
		// }
		$Cotizacion = $DB->rawQuery('SELECT 
			cliente.nombre_completo AS cliente_nombre_completo, cliente.correo_electronico AS cliente_correo_electronico, cliente.id AS id_cliente, 
		    cotizacion.fecha_hecha, DATE_FORMAT(cotizacion.salida, "%d/%m/%Y") AS salida, DATE_FORMAT(cotizacion.regreso, "%d/%m/%Y") AS regreso, cotizacion.descripcion, cotizacion.precioMXN, cotizacion.precioUSD, cotizacion.tipo_cambio, cotizacion.id,
		    tipo_seguro.tipo_seguro, tipo_seguro.id AS id_tipo_seguro, tipo_poliza.tipo_poliza, tipo_poliza.codigo_poliza, tipo_poliza.a_codigo, tipo_poliza.id AS id_tipo_poliza, 
		    cotizacionOrigen.pais AS pais_origen, cotizacionDestino.pais AS pais_destino '.$SelectExtra.'  
		FROM cotizacion
			INNER JOIN tipo_poliza 
		    	ON cotizacion.id_tipo_poliza = tipo_poliza.id
			INNER JOIN tipo_seguro 
		    	ON cotizacion.id_tipo_seguro = tipo_seguro.id
		    INNER JOIN pais AS cotizacionOrigen
		    	ON cotizacion.origen = cotizacionOrigen.id 
		    INNER JOIN pais AS cotizacionDestino
		    	ON cotizacion.destino = cotizacionDestino.id
		    INNER JOIN cliente
		    	ON cotizacion.id_cliente = cliente.id  
			'.$InnersExtra.'		    
		 WHERE cotizacion.id = '.$JSON->Datos->ID_Cotizacion);

		$Resutlado = ['Cotizacion' => $Cotizacion, 'asd' => $DB->getLastQuery()];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resutlado);
	}

	else if($JSON->Caso == "datosPoliza")
	{

		$Poliza = $DB->rawQuery('SELECT cotizacion.*, tipo_poliza.*, cliente.id_cliente AS cliente_idp, cliente.nombre_completo AS cliente_nombre, 
		cliente.nombres AS cliente_nombres, cliente.apellidos AS cliente_apellidos, cliente.id AS id_cliente,
		cliente.fecha_nacimiento AS cliente_nacimiento, cliente.correo_electronico AS cliente_correo, 
		pais_destino.pais AS pais_destino, pais_origen.pais AS pais_origen 
		FROM cotizacion
		INNER JOIN cliente ON cotizacion.id_cliente = cliente.id
		INNER JOIN tipo_poliza ON cotizacion.id_tipo_poliza = tipo_poliza.id
        INNER JOIN pais AS pais_destino ON cotizacion.destino = pais_destino.id
        INNER JOIN pais AS pais_origen ON cotizacion.origen = pais_origen.id
		WHERE cotizacion.id = '.$JSON->Datos->Cotizacion);	

		// $UsuariosTotal = $DB->get("subagente");
		// $DB->where("id_tipo_cliente", 1);
		// $ProspectosTotal = $DB->getOne("cliente", "count(*) as Total");
		// $UsuariosTotal['Total'];

		$Resultado = ['Poliza' => $Poliza, 'SQL' => $DB->getLastQuery()];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "aseguradoAsignarSeguro")
	{

		$Seguro = $DB->rawQuery('SELECT subagente.nombre_completo AS subagente, subagente.id AS subagente_id, distribuidor.distribuidor, distribuidor.id AS distribuidor_id, distribuidor.id_distribuidores, agente.agente, agente.id AS agente_id, tipo_distribuidor.tipo_distribuidor, tipo_distribuidor.id AS tipo_distribuidor_id, cliente.id AS cliente_id FROM subagente
		INNER JOIN agente on subagente.id_agente = agente.id
		INNER JOIN distribuidor on agente.id_distribuidor = distribuidor.id
		INNER JOIN tipo_distribuidor on distribuidor.id_tipo_distribuidor = tipo_distribuidor.id
		INNER JOIN cliente on cliente.id_subagente = subagente.id
		WHERE cliente.id = '.$JSON->Datos->Cliente);

		$Resultado = ['Seguro' => $Seguro, 'Q' => $DB->getLastQuery()];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "coberturaPoliza")
	{
		$DSM = 'Días';
		$DiasRedoneados = 7;
		$SemanasMeses = 1;

		// $JSON->Datos->Dias = floor($JSON->Datos->Dias);
		$JSON->Datos->Dias = round($JSON->Datos->Dias);

		$DiasRedoneados = ceil($JSON->Datos->Dias / 7) * 7;

		if($JSON->Datos->Dias >= 1 && $JSON->Datos->Dias <= 9)
		{
			$DSM = 'Días';
			$DiasRedoneados = $JSON->Datos->Dias;
			if($JSON->Datos->TipoSeguro == 3 || $JSON->Datos->TipoSeguro == 2)
			{
				$DSM = 'Semanas';
				// $DiasRedoneados = 1;
				if($JSON->Datos->Dias <= 7)
					$DiasRedoneados = 1;
				else
					$DiasRedoneados = ceil($JSON->Datos->Dias / 7) * 7;
			}
		}
		else if($JSON->Datos->Dias >= 10 && $JSON->Datos->Dias <= 63)
		{
			$DSM = 'Semanas';
			// if($JSON->Datos->Dias < 21)
			if($JSON->Datos->Dias < 15)
				$DiasRedoneados = 10;
				// $DiasRedoneados = ceil($JSON->Datos->Dias / 7);
			// $DiasRedoneados = ceil($JSON->Datos->Dias / 7) * 7;
			else
			{
				$SemanasMeses = $DiasRedoneados / 7;
				$DiasRedoneados = ceil($JSON->Datos->Dias / 7) * 7;
				// $DiasRedoneados = ceil($JSON->Datos->Dias / 7);
				// $DiasRedoneados = $DiasRedoneados / 7;
			}
			if($JSON->Datos->TipoSeguro == 3 || $JSON->Datos->TipoSeguro == 2)
			{
				$DSM = 'Semanas';
				$DiasRedoneados = ceil($JSON->Datos->Dias / 7) * 7;
			}
		}
		else if($JSON->Datos->Dias >= 64)
		{
			$DSM = 'Meses';
			// if($JSON->Datos->Dias >= 64 && $JSON->Datos->Dias <= 92)
			// {
			// 	$DiasRedoneados = 64;
			// 	$SemanasMeses = 3;
			// 	// $SemanasMeses = $DiasRedoneados / 30;
			// }
			// else // Redondear en meses, desde 4 (120 dias)
			// {
				// $DiasRedoneados = ceil($JSON->Datos->Dias/30) * 30;
				$SemanasMeses = $DiasRedoneados / 30;
				// $DiasRedoneados = ceil(round($JSON->Datos->Dias/(30.4),1)) * 30;
				$DiasRedoneados = ceil(round(($JSON->Datos->Dias-3)/(30.4),1)) * 30;
				if($DiasRedoneados <= 90)
					$DiasRedoneados = 64;
			// }
		}

		// if($JSON->Datos->TipoSeguro == 2 && $JSON->Datos->DSM == "Días")
		// if($JSON->Datos->TipoSeguro == 2 || $JSON->Datos->TipoSeguro == 3)
		// {
		// 	$DSM = 'Semanas';
		// }

		// if($JSON->Datos->Dias < 10)


		$TiposPolizas = $DB->rawQuery("
			SELECT * FROM cobertura
				INNER JOIN tipo_poliza
					ON cobertura.id_tipo_poliza = tipo_poliza.id
			WHERE tipo_poliza.zona = ".$JSON->Datos->ZonaDestino."
				AND cobertura.dias = ".$DiasRedoneados." 
			    AND tipo_poliza.id_tipo_seguro = ".$JSON->Datos->TipoSeguro." 
				AND tipo_poliza.dsm = '".$DSM."'");	
				
		$SQL = "SELECT * FROM cobertura
		INNER JOIN tipo_poliza
			ON cobertura.id_tipo_poliza = tipo_poliza.id
	WHERE tipo_poliza.zona = ".$JSON->Datos->ZonaDestino."
		AND cobertura.dias = ".$DiasRedoneados." 
		AND tipo_poliza.id_tipo_seguro = ".$JSON->Datos->TipoSeguro." 
		AND tipo_poliza.dsm = '".$DSM."'";

		// $Resutlado = ['Usuarios' => $Usuarios, 'Total' => $UsuariosTotal['Total']];

		// echo json_encode($DB->getLastQuery());
		$Resultado = ["Coberturas" => $TiposPolizas, 
			"Datos" => 
			[
				"Dias" => $JSON->Datos->Dias, "DiasRedoneados" => $DiasRedoneados, "DSM" => $DSM, "SemanasMeses" => $SemanasMeses,
				"SQL" => $SQL
			]
		];

		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "dFormulario")
	{
		$CampoExtraId = ($JSON->Datos == "Subagente" ? "nombre_completo" : $JSON->Datos);
		// $CampoExtra = ($JSON->Datos == "Tipo_Poliza" ? "codigo_poliza" : $JSON->Datos);
		// $CampoExtra = ($JSON->Datos == "Cuenta" ? "id_forma_pago" : $JSON->Datos);
		// $CampoExtra = ($JSON->Datos == "Forma_Pago" ? "cargo_administrativo" : $JSON->Datos);
		$CampoExtra = $JSON->Datos;
		if($JSON->Datos == "Subagente")
			$CampoExtra = "nombre_completo";
			
		if($JSON->Datos == "Tipo_Poliza") $CampoExtra = "codigo_poliza";
		else if($JSON->Datos == "Cuenta") $CampoExtra = "id_forma_pago";
		else if($JSON->Datos == "Forma_Pago") $CampoExtra = "cargo_administrativo";

		if($JSON->Datos == "Tipo_Poliza")
			$Campos = ["id", strtolower($CampoExtraId), strtolower($CampoExtra), "a_codigo"];
		else
			$Campos = ["id", strtolower($CampoExtraId), strtolower($CampoExtra)];

		if($JSON->Datos == "Pais")
			$Campos[2] = "zona";
		dFormulario($DB, strtolower($JSON->Datos), null, $Campos, $JSON->Extra);
	}

	else if($JSON->Caso == "tipoCambio")
	{	
		$Filas = 1;
		$Orden = 'DESC';
		if(!empty($JSON->Datos->Fecha))
		{
			$DB->where("fecha", $JSON->Datos->Fecha);
			$Filas = null;
			$Orden = 'ASC';
		}

		if(!empty($JSON->Datos->Orden))
			$Orden = $JSON->Datos->Orden;

		$DB->orderBy("tipo_cambio.id", $Orden);
		$DB->join("subagente", "tipo_cambio.id_subagente=subagente.id", "INNER");
		$Datos = $DB->get("tipo_cambio", $Filas);
		if ($DB->count > 0)
		    echo json_encode($Datos);
		else
			echo json_encode([["tipo_cambio" => "No existe", "query" => $DB->getLastQuery()]]);
	}

	else if($JSON->Caso == "email")
	{	
		// echo json_encode("dsadad");
		$Para = "";
		$Asunto = "";
		$Contenido = "";
		$Headers = "From: micuenta@misegurointernacional.com\r\n";
		// $Headers = (isset($JSON->Datos->De)) ? ("From: ".$JSON->Datos->De."\r\n") : "From: msi@misegurointernacional.com\r\n";
		$Headers .= "MIME-Version: 1.0\r\n";
		$Headers .= "Content-Type: text/html; charset=UTF-8\r\n";

		$Enviado = false;
		$Extra = null;

		
		// $EMAIL = new PHPMailer\PHPMailer\PHPMailer();

		if($JSON->Datos->Caso == "Cotizacion")
		{
			$Para = $JSON->Datos->Para;
			$Asunto = 'Mi Seguro Internacional';
			$Contenido = generarHTML($DB, "Cotizacion", $JSON->Datos->Datos);
			$Enviado = mail($Para, $Asunto, $Contenido, $Headers);

			// $DB->where("correo_electronico", $Para);
			// $DatosCliente = $DB->get("cliente");
			// if ($DB->count <= 0) exit;

			// $DB->where("id", $JSON->Datos->Datos->id_cotizacion);
			// $DatosCotizacion = $DB->get("cotizacion");

		}

		else if($JSON->Datos->Caso == "RegistroUsuario")
		{
			$Para = $JSON->Datos->Para;
			$Para .= ", ecantu@estudiantesembajadores.com";
			$Asunto = 'Mi Seguro Internacional | Registro';

			$Contenido = "
				<html>
				<head>
					<title>Usuario registrado</title>
					<style>
						body{
							font-family: Helvetica, sans-serif;
						}
					</style>
				</head>
				<body style='background-color: #fff;'>
					<table>
						<tr>
							<td><img src='https://misegurointernacional.com/ADMIN/assets/img/logo.png' alt='LOGO' width='140px'></td>
							<td><div style='margin-left: 100px;'>Mi Seguro Internacional</div></td>
						</tr>
					</table>		
					
					<div style='background-color: #fff;padding:10px 20px;margin:20px;'>
						<h1>Usuario registrado</h1>
						<h3>Se ha registrado un usuario en Mi Seguro Internacional para este correo:</h3>
						<p>Usuario: <b>".$JSON->Datos->Para."</b></p>
						<br>
						<p>La <b>contraseña</b> debe ser creada en el siguiente enlace:</p>

						<table align='center' width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr>
							<td>
								<table align='center' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td align='center' style='border-radius: 2px;' bgcolor='#7cdb46'><a href='https://admin.misegurointernacional.com/Registrar/".$JSON->Datos->Datos->contrasena."' target='_blank' style='font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; text-decoration: none;border-radius: 3px; padding: 12px 40px; border: 1px solid #7cdb46; display: inline-block;'>Entrar &rarr;</a></td>
								</tr>
								</table>
							</td>
							</tr>
						</table>

						<p>En caso de que no funcione el botón copiar y pegar el siguiente link en el navegador:<br>
						<a href='https://admin.misegurointernacional.com/Registrar/".$JSON->Datos->Datos->contrasena."'>https://admin.misegurointernacional.com/Registrar/".$JSON->Datos->Datos->contrasena."</a></p>

						<hr>
						<p>Si tú no solicitaste una cotización en Mi Seguro Internacional, puedes hacer caso omiso a este mensaje.</p>

						<hr>
						<p style='text-align: center;'>Mi Seguro Internacional</p>
					</div>
				</body>
				</html>
			";


			$Enviado = mail($Para, $Asunto, $Contenido, $Headers);
		}

		else if($JSON->Datos->Caso == "RecordatorioUsuario")
		{
			$Para = $JSON->Datos->Para;
			$Para .= ", ecantu@estudiantesembajadores.com";
			$Asunto = 'Mi Seguro Internacional | Usuario';

			$Contenido = "
				<html>
				<head>
					<title>Usuario registrado</title>
					<style>
						body{
							font-family: Helvetica, sans-serif;
						}
					</style>
				</head>
				<body style='background-color: #fff;'>
					<table>
						<tr>
							<td><img src='https://misegurointernacional.com/ADMIN/assets/img/logo.png' alt='LOGO' width='140px'></td>
							<td><div style='margin-left: 100px;'>Mi Seguro Internacional</div></td>
						</tr>
					</table>		
					
					<div style='background-color: #fff;padding:10px 20px;margin:20px;'>
						<h1>Cotización</h1>
						<h3>Se ha hecho otra cotización en Mi Seguro Internacional para este usuario:</h3>
						<p>Usuario: <b>".$JSON->Datos->Para."</b></p>
						<br>

						<table align='center' width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr>
							<td>
								<table align='center' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td align='center' style='border-radius: 2px;' bgcolor='#7cdb46'><a href='https://misegurointernacional.com/ADMIN/' target='_blank' style='font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; text-decoration: none;border-radius: 3px; padding: 12px 40px; border: 1px solid #7cdb46; display: inline-block;'>Entrar &rarr;</a></td>
								</tr>
								</table>
							</td>
							</tr>
						</table>

						<p>En caso de que no funcione el botón copiar y pegar el siguiente link en el navegador:<br>
						<a href='https://misegurointernacional.com/ADMIN/'>https://misegurointernacional.com/ADMIN/</a></p>

						<hr>
						<p>Si tú no solicitaste una cotización en Mi Seguro Internacional, puedes hacer caso omiso a este mensaje.</p>

						<hr>
						<p style='text-align: center;'>Mi Seguro Internacional</p>
					</div>
				</body>
				</html>
			";


			$Enviado = mail($Para, $Asunto, $Contenido, $Headers);
		}

		else if($JSON->Datos->Caso == "Poliza")
		{
			// $email = new PHPMailer\PHPMailer\PHPMailer();
			// $EMAIL->SetFrom('poliza@misegurointernacional.com'); //Name is optional
			// $EMAIL->Subject   = 'Poliza de seguro';
			// $EMAIL->Body      = "fdsfsdfdsfs";
			// $EMAIL->AddAddress($JSON->Datos->Para);
			// $EMAIL->AddAddress('ecantu@estudiantesembajadores.com');

			// $file_to_attach = $JSON->Datos->Datos->Archivo;
			// $Extra = $JSON->Datos->Datos->Archivo;

			// $carpeta = "../archivos/Polizas";
			// if(!is_dir($carpeta)) mkdir($carpeta);
			// $ruta = $carpeta . "/Asd.png";	
			// if(file_put_contents($ruta, base64_decode($JSON->Datos->Datos->Archivo)))
			// 	$Resultado["Subido"] = true;
			// $file_to_attach = $ruta;
			// $Extra = $file_to_attach;
			// // $file_to_attach = base64_decode($JSON->Datos->Datos->Archivo);

			// $EMAIL->AddAttachment( $file_to_attach , 'Poliza.png' );

			// $Enviado = $EMAIL->Send();

			$EMAIL = new PHPMailer\PHPMailer\PHPMailer(true);
			$Extra['file'] = $_FILES;
			
			try {
				$EMAIL->SetFrom('polizas@misegurointernacional.com');
				$EMAIL->SetFrom((isset($JSON->Datos->Datos->De)) ? $JSON->Datos->Datos->De : "polizas@misegurointernacional.com");

				$EMAIL->CharSet = 'UTF-8';

				$EMAIL->Subject = (isset($JSON->Datos->Datos->Asunto)) ? 
									$JSON->Datos->Datos->Asunto : 'Póliza Mi Seguro Internacional';
				// $EMAIL->Subject = 'Cotización Mi Seguro Internacional';
				// $mensaje = utf8_decode($subject);
				
				$Contenido = "
						<html>
						<head>
							<title>Póliza</title>
							<style>
								body{
									font-family: Helvetica, sans-serif;
								}
							</style>
						</head>
						<body style='background-color: #fff;'>
							<table>
								<tr>
									<td><img src='https://misegurointernacional.com/ADMIN/assets/img/logo.png' alt='LOGO' width='140px'></td>
									<td><div style='margin-left: 100px;'>Mi Seguro Internacional</div></td>
								</tr>
							</table>		
							
							<div style='background-color: #fff;padding:10px 20px;margin:20px;'>
								<h1>Póliza de seguro</h1>
								<br>

								".(isset($JSON->Datos->Datos->Mensaje)) ? $JSON->Datos->Datos->Mensaje : "Póliza Mi Seguro Internacional"."

								<hr>
								<p>Si tú no solicitaste una cotización en Mi Seguro Internacional, puedes hacer caso omiso a este mensaje.</p>

								<hr>
								<p style='text-align: center;'>Mi Seguro Internacional</p>
							</div>
						</body>
						</html>
					";

				// $EMAIL->Body = (isset($JSON->Datos->Datos->Mensaje)) ? 
				// 				$JSON->Datos->Datos->Mensaje : 'Póliza Mi Seguro Internacional';
				$EMAIL->Body = $Contenido;

				// $EMAIL->Body = 'Cotización Mi Seguro Internacional';
				$Correos = '';
				if(isset($JSON->Datos->Datos->CC) && $JSON->Datos->Datos->CC != null){
					$Correos = explode(",", $JSON->Datos->Datos->CC);
					foreach($Correos as $Correo)
						$EMAIL->AddCC($Correo);
					// $EMAIL->AddCC($JSON->Datos->Datos->CC);
				}

				// $EMAIL->
				// $mail->addCC('cc@example.com');

				// $EMAIL->Body      = "fdsfsdfdsfs";
				$EMAIL->AddAddress($JSON->Datos->Para);
				$EMAIL->AddAddress('ecantu@estudiantesembajadores.com');
				// $Extra = $JSON->Datos->Datos;
				$Extra['correos'] = $Correos;

				if(isset($JSON->Datos->Datos->Archivo) && $JSON->Datos->Datos->Archivo != null)
				{

					// $lzw = new LZW();
					// $file_to_attach = $JSON->Datos->Datos->Archivo;
					// $Extra = $JSON->Datos->Datos->Archivo;
					// $carpeta = "../archivos/Polizas";
					// if(!is_dir($carpeta)) mkdir($carpeta);
					// $ruta = $carpeta . "/Asd.png";
					// if(file_put_contents($ruta, base64_decode($JSON->Datos->Datos->Archivo)))
					// 	$Resultado["Subido"] = true;
					// $file_to_attach = $ruta;
					// $Extra = $lzw->decompress($JSON->Datos->Datos->Archivo);
					// $EMAIL->AddAttachment( $file_to_attach , 'Poliza.png' );



					$base = explode('data:application/pdf;base64,', $JSON->Datos->Datos->Archivo);
					$base = base64_decode($base[1]);
					$EMAIL->addStringAttachment($base, 'Poliza.pdf');

					// $EMAIL->AddAttachment($_FILES['PDF']['tmp_name'],
					// 					 'Poliza.pdf');



					// echo json_encode($_FILES);

				}

				$Enviado = $EMAIL->Send();
			}
			catch (Exception $e) {
				$Extra["errror"] = $EMAIL->ErrorInfo;
				// echo 'Message could not be sent. Mailer Error: ', $EMAIL->ErrorInfo;
			}

		}

		else if($JSON->Datos->Caso == "CotizacionConfirmacion")
		{
			// $email = new PHPMailer\PHPMailer\PHPMailer();
			$EMAIL->SetFrom('cotizacion@misegurointernacional.com'); //Name is optional
			$EMAIL->Subject = (isset($JSON->Datos->Datos->Asunto)) ? 
								$JSON->Datos->Datos->Asunto : 'Cotización Mi Seguro Internacional';

			$EMAIL->Body = (isset($JSON->Datos->Datos->Mensaje)) ? 
								$JSON->Datos->Datos->Mensaje : 'Cotización Mi Seguro Internacional';

			if(isset($JSON->Datos->Datos->CC))
				$EMAIL->AddCC($JSON->Datos->Datos->CC);
				
			// $EMAIL->Body      = "fdsfsdfdsfs";
			$EMAIL->AddAddress($JSON->Datos->Para);
			$EMAIL->AddAddress('ecantu@estudiantesembajadores.com');

			$file_to_attach = $JSON->Datos->Datos->Archivo;
			$Extra = $JSON->Datos->Datos->Archivo;

			$carpeta = "../archivos/CotizacionConfirmacion";
			if(!is_dir($carpeta)) mkdir($carpeta);
			$ruta = $carpeta . "/Asd.png";	
			if(file_put_contents($ruta, base64_decode($JSON->Datos->Datos->Archivo)))
				$Resultado["Subido"] = true;
			$file_to_attach = $ruta;
			$Extra = $file_to_attach;
			// $file_to_attach = base64_decode($JSON->Datos->Datos->Archivo);

			$EMAIL->AddAttachment( $file_to_attach , 'Cotizacion.png' );

			$Enviado = $EMAIL->Send();
		}

		else if($JSON->Datos->Caso == "CotizacionVer")
		{
			$EMAIL = new PHPMailer\PHPMailer\PHPMailer(true);
			
			try {
				$EMAIL->SetFrom('cotizacion@misegurointernacional.com');
				$EMAIL->SetFrom((isset($JSON->Datos->Datos->De)) ? $JSON->Datos->Datos->De : "cotizacion@misegurointernacional.com");

				$EMAIL->Subject = (isset($JSON->Datos->Datos->Asunto)) ? 
									$JSON->Datos->Datos->Asunto : 'Cotización Mi Seguro Internacional';
				// $EMAIL->Subject = 'Cotización Mi Seguro Internacional';

				$EMAIL->Body = (isset($JSON->Datos->Datos->Mensaje)) ? 
									$JSON->Datos->Datos->Mensaje : 'Cotización Mi Seguro Internacional';
				// $EMAIL->Body = 'Cotización Mi Seguro Internacional';
				$Correos = '';
				if(isset($JSON->Datos->Datos->CC) && $JSON->Datos->Datos->CC != null){
					$Correos = explode(",", $JSON->Datos->Datos->CC);
					foreach($Correos as $Correo)
						$EMAIL->AddCC($Correo);
					// $EMAIL->AddCC($JSON->Datos->Datos->CC);
				}

				// $EMAIL->
				// $mail->addCC('cc@example.com');

				// $EMAIL->Body      = "fdsfsdfdsfs";
				$EMAIL->AddAddress($JSON->Datos->Para);
				$EMAIL->AddAddress('ecantu@estudiantesembajadores.com');
				// $Extra = $JSON->Datos->Datos;
				$Extra = $Correos;

				if(isset($JSON->Datos->Datos->Archivo) && $JSON->Datos->Datos->Archivo != null)
				{
					$file_to_attach = $JSON->Datos->Datos->Archivo;
					$Extra = $JSON->Datos->Datos->Archivo;

					$carpeta = "../archivos/Cotizacion";
					if(!is_dir($carpeta)) mkdir($carpeta);
					$ruta = $carpeta . "/Asd.png";
					// $ruta = $carpeta . "/Asd.pdf";
					if(file_put_contents($ruta, base64_decode($JSON->Datos->Datos->Archivo)))
						$Resultado["Subido"] = true;
					$file_to_attach = $ruta;
					$Extra = $file_to_attach;
					// $file_to_attach = base64_decode($JSON->Datos->Datos->Archivo);

					$EMAIL->AddAttachment( $file_to_attach , 'Cotizacion.png' );
					// $EMAIL->AddAttachment( $file_to_attach , 'Cotizacion.pdf' );

				}

				$Enviado = $EMAIL->Send();
			}
			catch (Exception $e) {
				$Extra['error'] = $EMAIL->ErrorInfo;
				// echo 'Message could not be sent. Mailer Error: ', $EMAIL->ErrorInfo;
			}
		}
		else if($JSON->Datos->Caso == "Factura")
		{
			// $Para = $JSON->Datos->Para;
			$Para = 'facturas@misegurointerncional.com';
			$Para .= ", ecantu@estudiantesembajadores.com";
			$Asunto = 'Mi Seguro Internacional | Factura';

			$DB->where("id", $JSON->Datos->Datos->id);
			$Cliente = $DB->get("cliente")[0];

			$Contenido = "
				<html>
				<head>
					<title>Factura</title>
					<style>
						body{
							font-family: Helvetica, sans-serif;
						}
					</style>
				</head>
				<body style='background-color: #fff;'>
					<table>
						<tr>
							<td><img src='https://misegurointernacional.com/ADMIN/assets/img/logo.png' alt='LOGO' width='140px'></td>
							<td><div style='margin-left: 100px;'>Mi Seguro Internacional</div></td>
						</tr>
					</table>		
					
					<div style='background-color: #fff;padding:10px 20px;margin:20px;'>
						<h1>Solicitud de factura</h1>
						<h3>El cliente ".$Cliente['nombre_completo']." ha solicitado su factura:</h3>

						<table align='center' width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr>
							<td>
								<table align='center' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td align='center' style='border-radius: 2px;' bgcolor='#7cdb46'><a href='https://misegurointernacional.com/ADMIN/Asegurados/".$JSON->Datos->Datos->id."' target='_blank' style='font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; text-decoration: none;border-radius: 3px; padding: 12px 40px; border: 1px solid #7cdb46; display: inline-block;'>Ver datos &rarr;</a></td>
								</tr>
								</table>
							</td>
							</tr>
						</table>

						<p>En caso de que no funcione el botón copiar y pegar el siguiente link en el navegador:<br>
						<a href='https://misegurointernacional.com/ADMIN/Asegurados/".$JSON->Datos->Datos->id."'>https://misegurointernacional.com/ADMIN/Asegurados/".$JSON->Datos->Datos->id."</a></p>

						<hr>

						<hr>
						<p style='text-align: center;'>Mi Seguro Internacional</p>
					</div>
				</body>
				</html>
			";


			$Enviado = mail($Para, $Asunto, $Contenido, $Headers);
		}

		// if($JSON->Datos->Caso != "Poliza")

		if($Enviado)
			echo json_encode(["Enviado" => true, "Query" => $DB->getLastQuery(), "Extra" => $Extra]);
		else
			echo json_encode(["Enviado" => false, "Query" => $DB->getLastQuery(), "Extra" => $Extra]);

	}

	else if($JSON->Caso == "subirArchivo")
	{
		$Resultado = ["Subido" => false];

		$frags = explode(".", $JSON->Datos->Archivo->filename);
		$extension = $frags[count($frags) - 1];
		$carpeta = "../archivos/" . $JSON->Datos->Carpeta;
		$ruta = $carpeta . "/" . $JSON->Datos->Nombre . "." . $extension;
		// file_put_contents(filename, contenido, flags);

		$Resultado["Ruta"] = $ruta;
		$Resultado["base64"] = $JSON->Datos->Archivo->value;

		// if(file_force_contents($ruta, base64_decode($JSON->Datos->Archivo->value)))
		// 	$Resultado["Subido"] = true;


        if(!is_dir($carpeta)) mkdir($carpeta);

		if(file_put_contents($ruta, base64_decode($JSON->Datos->Archivo->value)))
			$Resultado["Subido"] = true;

		
		// echo json_encode($JSON);
		echo json_encode($Resultado);
		// echo json_encode($ruta);
		// echo json_encode("fdsfsdf");
	}

	else if($JSON->Caso == "subirExcel")
	{
		$Resultado = ["Subido" => false];

		$frags = explode(".", $JSON->Datos->Archivo->filename);
		$extension = $frags[count($frags) - 1];
		$carpeta = "../archivos/Exceles";
		$ruta = $carpeta . "/" . $JSON->Datos->Tabla . "." . $extension;
		// file_put_contents(filename, contenido, flags);

		$Resultado["Ruta"] = $ruta;
		$Resultado["base64"] = $JSON->Datos->Archivo->value;

		// if(file_force_contents($ruta, base64_decode($JSON->Datos->Archivo->value)))
		// 	$Resultado["Subido"] = true;


        if(!is_dir($carpeta)) mkdir($carpeta);

		if(file_put_contents($ruta, base64_decode($JSON->Datos->Archivo->value)))
			$Resultado["Subido"] = true;

		
		if ( $xlsx = SimpleXLSX::parse($ruta) ) 
		{
			// print_r( $xlsx->rows() );
			$Datos1 = array();
			$Datos2 = array();
			$Datos3 = array();
			$Filas = $xlsx->rows();
			$Resultado["FilasN"] = count($xlsx->rows());
			$Resultado["FilasEXC"] = $xlsx->rows();
			
			if($JSON->Datos->Tabla == "asegurados")
			{				
				$Resultado["subido"] = ($Filas[0][12] == "") ? false : true;

				if($Resultado["subido"] == true)
					for($f = 1; $f < count($xlsx->rows()); $f++)
					{
						if($Filas[$f][0] == "" || $Filas[$f][0] == null) continue;
						$DB->where("abreviacion", $Filas[$f][6]);
						$PaisOrigen = $DB->getOne("pais");

						$DB->where("abreviacion", $Filas[$f][9]);
						$PaisDestino = $DB->getOne("pais");

						$PolicyCode = $Filas[$f][14];
						$CodigoPoliza = substr($PolicyCode, 5) . "-Setmex";
						
						$Dias = DiferenciaFechas($Filas[$f][12], $Filas[$f][13]);
						// $Dias = abs($Dias) + 1;
						// $Dias = abs($Dias) + 1;
						// $Dias = round($Dias);
						// $Dias = abs($Dias) + 1;
						$Dias = ($Dias < 0) ? $Dias * -1 : $Dias;
						// json_encode($Dias);

						$DB->where("codigo_poliza", $CodigoPoliza);
						if($Dias < 10) $DB->where("dsm", "Días");
						else if($Dias >= 10 && $Dias <= 64) $DB->where("dsm", "Semanas");
						else if($Dias > 64) $DB->where("dsm", "Meses");
						$TipoPoliza = $DB->getOne("tipo_poliza");
						$Resultado['QueryPoliza'] = $DB->getLastQuery();
						// exit;
						$DB->orderBy("id", "Desc");
						$TipoCambio = $DB->getOne("tipo_cambio");
						$TC = $TipoCambio['tipo_cambio'];
						// echo json_encode($Resultado);

						$Impuestos = $DB->get("impuesto");

						$DB->where("id", $JSON->Datos->Datos->Subagente);
						$Subagente = $DB->getOne("subagente");
						$Resultado["LosDatos"] = $JSON;

						// $Distribuidor = $DB->rawQuery('SELECT *
						// FROM distribuidor 
						// INNER JOIN agente ON agente.id_distribuidor = distribuidor.id INNER JOIN subagente ON subagente.id_agente = agente.id 
						// WHERE subagente.id = '.$Subagente['id']);	
						// $Resultado['QueryDistribuidor'] = $DB->getLastQuery();

						// $Dias = $Filas[$f][9] - $Filas[$f][8];						
						// $Dias = $Filas[$f][8] - $Filas[$f][9];		
						// $Dias = round($Dias / (60 * 60 * 24));
						
						// $DB->where("id_tipo_poliza", $TipoPoliza['id']);
						// $DB->where("dias", $Dias);
						// $Cobertura = $DB->getOne("cobertura");
						// $Resultado['QueryCober'][$f] = $DB->getLastQuery();

						// function Cobertura($DB, $inicio, $fin, $tipo_seguro, $zona_destino, $tipo_poliza = "no")
						// exit;
						$Coberturas = Cobertura($DB, $Filas[$f][12], $Filas[$f][13], $TipoPoliza['id_tipo_seguro'], $PaisDestino['zona'], $TipoPoliza['tipo_poliza']);
						
						// echo json_encode($Coberturas);
						$Cobertura = $Coberturas['Coberturas'][0];
						$Resultado['Cober'][$f] = $Coberturas;

						$Folio = date("Y") . date("m") . date("d") . ".";
						$Folio .= $TipoPoliza['a_codigo'] . $CodigoPoliza . ".";
						$Folio .= strtoupper(substr($Filas[$f][2], 0, 3));
						$Folio .= strtoupper(substr($Filas[$f][1], 0, 3));
						$Folio .= date("Y", strtotime($Filas[$f][5]));
						$Folio .= date("m", strtotime($Filas[$f][5]));
						$Folio .= date("d", strtotime($Filas[$f][5]));

						$Folio2 = date("Y") . date("m") . date("d") . ".";
						$Folio2 .= $TipoPoliza['a_codigo'] . $CodigoPoliza . ".";

						$IDCliente = strtoupper(substr($Filas[$f][2], 0, 3)) . strtoupper(substr($Filas[$f][1], 0, 3));
						$IDCliente .= date("Y", strtotime($Filas[$f][5]));
						$IDCliente .= date("m", strtotime($Filas[$f][5]));
						$IDCliente .= date("d", strtotime($Filas[$f][5]));

						$Datos2[$f-1]['apellidos'] = $Filas[$f][2];
						$Datos2[$f-1]['nombres'] = $Filas[$f][1];
						$Datos2[$f-1]['nombre_completo'] = $Filas[$f][1] . " " . $Filas[$f][2];
						$Datos2[$f-1]['sexo'] = $Filas[$f][4];
						$Datos2[$f-1]['correo_electronico'] = $Filas[$f][3];
						// $Datos2[$f-1]['id_subagente'] = 18;
						$Datos2[$f-1]['id_subagente'] = $Subagente['id'];
						$Datos2[$f-1]['id_cliente'] = $IDCliente;
						$Datos2[$f-1]['fecha_nacimiento'] = date("Y-n-j", strtotime($Filas[$f][5]));
						$Datos2[$f-1]['fecha_registro_asegurado'] = date("Y-n-j");
						$Resultado['Cliente'] = $Datos2;
						$cid = $DB->insert("cliente", $Datos2[$f-1]);
						
						if($Filas[$f][3] != "") {
							$Datos3[$f-1]['correo_electronico'] = $Filas[$f][3];
							$Datos3[$f-1]['contrasena'] = rand(100, 1000);
							$Datos3[$f-1]['id_rol'] = 5;
							$Datos3[$f-1]['id_datos'] = $cid;
							$DB->insert("usuario", $Datos3[$f-1]);
						}
						// CorreoRegistro($Filas[$f][7], $Datos3[$f-1]['contrasena']);						

						$Comision['comision'] = ($Cobertura['precio'] * $TC) * ($Subagente['comision'] / 100.0);
						$Comision['impuestos'] = (($Impuestos[0]['porcentaje'] / 100) * $Comision['comision']) + (($Impuestos[1]['porcentaje'] / 100) * $Comision['comision']);
						$Comision['comision_final'] = $Comision['comision'] - $Comision['impuestos'];
						$Comision['folio_seguro'] = $Folio;
						$Comision['id_subagente'] = $Subagente['id'];

						// $Datos1[$f-1]['id_distribuidores'] = $Filas[$f][0];
						// $Datos1[$f-1]['estado_t'] = $Filas[$f][2];
						// $EstadoT = "NEW";
						// if($Filas[$f][0] == "Corr app") $EstadoT = 3;
						// if($Filas[$f][0] == "Corr app") $EstadoT = 3;
						// if($Filas[$f][0] == "Corr app") $EstadoT = 3;
						// $Datos1[$f-1]['estado_t'] = EstadoT($Filas[$f][0]);
						$Datos1[$f-1]['estado_t'] = 0;
						// $Datos1[$f-1]['salida'] = $Filas[$f][8];
						// $Datos1[$f-1]['regreso'] = $Filas[$f][9];
						$Datos1[$f-1]['salida'] = date("Y-n-j", strtotime($Filas[$f][12]));
						$Datos1[$f-1]['regreso'] = date("Y-n-j", strtotime($Filas[$f][13]));
						$Datos1[$f-1]['origen'] = $PaisOrigen['id'];
						$Datos1[$f-1]['origen_ciudad'] = $Filas[$f][7];
						$Datos1[$f-1]['origen_cp'] = $Filas[$f][8];	
						$Datos1[$f-1]['destino'] = $PaisDestino['id'];
						$Datos1[$f-1]['destino_ciudad'] = $Filas[$f][10];
						// $Datos1[$f-1]['destino_cp'] = $Filas[$f][15];
						$Datos1[$f-1]['id_cliente'] = $cid;
						$Datos1[$f-1]['fecha_hecha'] = date("Y-n-j");
						$Datos1[$f-1]['fecha_asegurado'] = date("Y-n-j");
						// $Datos1[$f-1]['id_subagente'] = 18;
						$Datos1[$f-1]['id_subagente'] = $Subagente['id'];
						$Datos1[$f-1]['id_tipo_poliza'] = $TipoPoliza['id'];
						$Datos1[$f-1]['id_tipo_seguro'] = $TipoPoliza['id_tipo_seguro'];
						$Datos1[$f-1]['tipo_cambio'] = $TC;
						$Datos1[$f-1]['folio'] = $Folio;
						$Datos1[$f-1]['folio2'] = $Folio2;
						$Datos1[$f-1]['numero_dias'] = $Cobertura['dias'];
						$Datos1[$f-1]['precioUSD'] = $Cobertura['precio'];
						$Datos1[$f-1]['precioMXN'] = $Cobertura['precio'] * $TC;
						$Datos1[$f-1]['saldo_pendiente_USD'] = $Cobertura['precio'];
						$Datos1[$f-1]['saldo_pendiente_MXN'] = $Cobertura['precio'] * $TC;
						$Datos1[$f-1]['precioMXN_comision'] = $Comision['comision_final'];
						$Datos1[$f-1]['precioUSD_comision'] = $Comision['comision_final'] / $TC;
						// $Datos1[$f-1]['precioMXN_descuentoSA'] = ($Distribuidor['descuento'] / 100) * $Datos1[$f-1]['precioMXN'];
						// $Datos1[$f-1]['precioUSD_descuentoSA'] = ($Distribuidor['descuento'] / 100) * $Datos1[$f-1]['precioUSD'];
						$Datos1[$f-1]['precioMXN_descuentoSA'] = ($Subagente['descuento'] / 100) * $Datos1[$f-1]['precioMXN'];
						$Datos1[$f-1]['precioUSD_descuentoSA'] = ($Subagente['descuento'] / 100) * $Datos1[$f-1]['precioUSD'];
						$Datos1[$f-1]['precioUSD_total'] = $Datos1[$f-1]['precioUSD'] - $Datos1[$f-1]['precioUSD_comision'] - $Datos1[$f-1]['precioUSD_descuentoSA'];
						$Datos1[$f-1]['precioMXN_total'] = $Datos1[$f-1]['precioMXN'] - $Datos1[$f-1]['precioMXN_comision'] - $Datos1[$f-1]['precioMXN_descuentoSA'];
						// $Datos1[$f-1]['precioMXN_total'] = $Cobertura['precio'] * $TC;
						$DB->insert("cotizacion", $Datos1[$f-1]);
						$Resultado['Cot'] = $Datos1;
						$Resultado['Query'][$f] = $DB->getLastQuery();
						
						$DB->insert("comision", $Comision);
						$Resultado['QueryComs'] = $DB->getLastQuery();

					}
				// $Resultado["subido"] = true;
			}
		} 
		else 
		{
			// echo SimpleXLSX::parse_error();
			$Resultado["FilasN"] = 0;
		}		

		
		// echo json_encode($JSON);
		echo json_encode($Resultado);
		// echo json_encode($ruta);
		// echo json_encode("fdsfsdf");
	}

	else if($JSON->Caso == "documento")
	{	
		$Resultado = ["HTML" => null];
		if($JSON->Datos->Caso == "Cotizacion")
		{
			// $HTMLDocumento = 
			$Resultado["HTML"] = generarHTML($DB, "Cotizacion", $JSON->Datos->Datos);
		}
		echo json_encode($Resultado);
	}

	else if($JSON->Caso == "recuperarContrasena")
	{	
		// echo json_encode("dsadad");
		if($JSON->Datos->Paso == 1) // ENVIAR CORREO CON LINK
		{
			$Resultado = ["enviado" => false, "razon" => null, "correo_electronico" => null];

			$DB->where("correo_electronico", $JSON->Datos->Para);
			// $Datos = $DB->get("subagente");
			$Datos = $DB->get("usuario");
			if($DB->count > 0)
			{
				$Resultado["correo_electronico"] = $Datos[0]["correo_electronico"];

				$Fecha = new DateTime();
				$Timestamp = $Fecha->getTimestamp() + (24 * 60 * 60);

				$codigo_enlace = crypt($Resultado["correo_electronico"], $Timestamp);
				$DB->where("correo_electronico", $Resultado["correo_electronico"]);
				// if($DB->update("subagente", ["contrasena_link" => $codigo_enlace, "contrasena_tiempo" => $Timestamp]))
				if($DB->update("usuario", ["contrasena_link" => $codigo_enlace, "contrasena_tiempo" => $Timestamp]))
				{		
					$FechaExpira = new DateTime();
					$FechaExpira->setTimestamp($Timestamp);
					$FechaPrint = $FechaExpira->format('d/m/Y');
					

					$para = $JSON->Datos->Para;
					$para .= ', ecantu@estudiantesembajadores.com';

					$asunto = 'MSI | Recuperar contraseña';

					$enlace = "https://misegurointernacional.com/ADMIN/Recuperar/" . $codigo_enlace;
					// $enlace = "http://localhost:4200/Recuperar/" . $codigo_enlace;

					$contenido = "
					<html>
					<head>
						<title>Recuperar contraseña</title>
						<style>
							body{
								font-family: Helvetica, sans-serif;
							}
						</style>
					</head>
					<body style='background-color: #fff;'>
						<table>
							<tr>
								<td><img src='https://misegurointernacional.com/ADMIN/assets/img/logo.png' alt='LOGO' width='140px'></td>
								<td><div style='margin-left: 100px;'>Mi Seguro Internacional</div></td>
							</tr>
						</table>		
						
						<div style='background-color: #fff;padding:10px 20px;margin:20px;'>
							<h1>Recuperar contraseña</h1>
							<h3>Has solicitado reestablecer la contraseña de tu cuenta. Haz clic en el siguiente botón:</h3>

							<table align='center' width='100%' border='0' cellspacing='0' cellpadding='0'>
								<tr>
								<td>
									<table align='center' border='0' cellspacing='0' cellpadding='0'>
									<tr>
										<td align='center' style='border-radius: 2px;' bgcolor='#7cdb46'><a href='".$enlace."' target='_blank' style='font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; text-decoration: none;border-radius: 3px; padding: 12px 40px; border: 1px solid #7cdb46; display: inline-block;'>Recuperar contraseña &rarr;</a></td>
									</tr>
									</table>
								</td>
								</tr>
							</table>

							<p>El enlace expira el <b>".$FechaPrint."</b>.</p>
							<p>En caso de que no funcione el botón copiar y pegar el siguiente link en el navegador:<br>
							<a href='".$enlace."'>".$enlace."</a></p>

							<hr>
							<p>Si tú no solicitaste reestablecer tu contraseña, puedes hacer caso omiso a este mensaje.</p>

							<hr>
							<p style='text-align: center;'>Mi Seguro Internacional</p>
						</div>
					</body>
					</html>
					";

					$headers = "From: micuenta@misegurointernacional.com\r\n";
					$headers .= "MIME-Version: 1.0\r\n";
					$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

					if(mail($para, $asunto, $contenido, $headers))
						$Resultado["enviado"] = true;
					else
						$Resultado["razon"] = "Error con el servidor";
				}
				else
					$Resultado["razon"] = "Error con el servidor";

			}
			else
				$Resultado["razon"] = "El correo no existe";

			echo json_encode($Resultado);
		}
		else if($JSON->Datos->Paso == 2) // CAMBIAR CONTRASEÑA
		{
			$Resultado = ["enviado" => false, "razon" => null, "correo_electronico" => null];

			$DB->where("contrasena_link", $JSON->Datos->Para);
			$DatosUsuario = $DB->get("usuario");
			$Fecha = new DateTime();
			$Timestamp = $Fecha->getTimestamp();
			if($DatosUsuario[0]['contrasena_tiempo'] >= $Timestamp)
			{
				$DB->where("contrasena_link", $JSON->Datos->Para);
				// if($DB->update("subagente", ["contrasena" => $JSON->Datos->NuevaCon, "contrasena_link" => "", "contrasena_tiempo" => 0]))
				if($DB->update("usuario", ["contrasena" => $JSON->Datos->NuevaCon, "contrasena_link" => "", "contrasena_tiempo" => 0]))
				{		
					$Resultado["enviado"] = 1;
					// $Resultado["enviado"] = $DB->getLastQuery();
				}
			}
			else
			{
				$Resultado["razon"] = ['db' => $DatosUsuario[0]['contrasena_tiempo'], 'ts' => $Timestamp];
				$Resultado["enviado"] = 2;
			}


			echo json_encode($Resultado);
		}
	}

	else if($JSON->Caso == "distribuidor")
	{		
		$InnerJOIN = "";
		if($JSON->Datos->Dado = "subagente")
			$InnerJOIN = " INNER JOIN agente ON agente.id_distribuidor = distribuidor.id INNER JOIN subagente ON subagente.id_agente = agente.id ";

		$Distribuidor = $DB->rawQuery('SELECT *
		FROM distribuidor 
			'.$InnerJOIN.' 
		WHERE '.$JSON->Datos->Dado.'.id = '.$JSON->Datos->ID);	

		$Resutlado = ['Distribuidor' => $Distribuidor];

		// echo json_encode($DB->getLastQuery());
		echo json_encode($Resutlado);
	}

	else if($JSON->Caso == "enviarArchivo")
	{
		// $Resultado = ["Enviado" => false];

		// $email = new PHPMailer();
		// $email->SetFrom('poliza@misegurointernacional.com'); //Name is optional
		// $email->Subject   = 'Póliza';
		// $email->Body      = "fdsfsdfdsfs";
		// $email->AddAddress(  );

		// $file_to_attach = 'PATH_OF_YOUR_FILE_HERE';

		// $email->AddAttachment( $file_to_attach , 'NameOfFile.pdf' );

		// return $email->Send();

		// $Para = "";
		// $Asunto = "";
		// $Contenido = "";
		// $Headers = "From: msi@misegurointernacional.com\r\n";
		// $Headers .= "MIME-Version: 1.0\r\n";
		// $Headers .= "Content-Type: text/html; charset=UTF-8\r\n";

		// if($JSON->Datos->Caso == "Cotizacion")
		// {
		// 	$Para = $JSON->Datos->Para;
		// 	$Asunto = 'Mi Seguro Internacional';
		// 	$Contenido = generarHTML($DB, "Cotizacion", $JSON->Datos->Datos);

		// }

		// if(mail($Para, $Asunto, $Contenido, $Headers))
		// 	$Resultado["Enviado"] = true;
		// // else
		// // 	$Resultado["Enviado"] = false;
		// echo json_encode($Resultado);

	}

	else if($JSON->Caso == "generarPDF")
	{	
		generarPDF();
	}

	else if($JSON->Caso == "cotizarTarjeta")
	{
		// echo json_encode("fsfdsf");
		// $string ='<ArnRequest><Availability DisplayCurrency="USD" SearchTimeout="15"><HotelAvailability InDate="2007-04-26" OutDate="2007-04-29" Rooms="1" Adults="2" Children="0"><Hotel HotelID="8800"/></HotelAvailability></Availability></ArnRequest>';

		$xmlrequest = '
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Header>
          <OpHeader xmlns="https://servertest.operacionesenlinea.com/">
            <OpKey1>kld8hfhkjsf</OpKey1>
            <OpKey2>8346765hdhryyu</OpKey2>
          </OpHeader>
        </soap:Header>
        <soap:Body>
          <ecommerceVenta xmlns="https://servertest.operacionesenlinea.com/">
            <request>
              <comercioId>1</comercioId>
              <contrasena>34242</contrasena>
              <modo>A</modo>
              <monto>'.$JSON->Datos->Cotizacion->precioMXN.'</monto>
              <transaccionId>'.$JSON->Datos->Cotizacion->id.'</transaccionId>
              <referencia>'.$JSON->Datos->Cotizacion->id.'</referencia>
              <tarjeta>'.$JSON->Datos->Tarjeta->tarjeta.'</tarjeta>
              <tipo>1</tipo>
              <mes>'.$JSON->Datos->Tarjeta->mes.'</mes>
              <anio>'.$JSON->Datos->Tarjeta->anio.'</anio>
              <digitos>'.$JSON->Datos->Tarjeta->digitos.'</digitos>
              <titular>'.$JSON->Datos->Tarjeta->titular.'</titular>
              <dirIp>192.192.192.255</dirIp>
              <diferimiento>00</diferimiento>
              <numeroPagos>00</numeroPagos>
              <plan>00</plan>
            </request>
          </ecommerceVenta>
        </soap:Body>
	  </soap:Envelope>
	  ';


		//Change this variables.
		// $location_URL = 'https://servertest.operacionesenlinea.com/gateway/OpGateway.asmx?wsdl';
		// $action_URL = "https://servertest.operacionesenlinea.com/ecommerceVenta";

		
		// $location_URL = 'https://servertest.operacionesenlinea.com/gateway/OpGateway.asmx?wsdl';|
		$location_URL = 'https://servidorseguro.operacionesenlinea.com/gateway/OpGateway.asmx?wsdl';

		// $client = new SoapClient(null, array(
		// 'location' => $location_URL,
		// 'trace'    => 1,
		// ));

		$client = new SoapClient($location_URL);
		
		$headerbody = array(
			'OpKey1' => 'kld8hfhkjsf', 
			'OpKey2' => '8346765hdhryyu',
			// 'OpKey1' => '51', 
			// 'OpKey2' => 'ja9sduj',
			// 'OpKey1' => '8346765hdhryyu',
			// 'OpKey2' => 'kld8hfhkjsf', 
		);   
		$header = new SOAPHeader('https://servidorseguro.operacionesenlinea.com/', 'OpHeader', $headerbody);
		$client->__setSoapHeaders($header); 



		// $order_return = $client->__doRequest($xmlrequest,$location_URL,$action_URL,1);
		// $order_return = $client->__doRequest($xmlrequest,$location_URL);

		// echo json_encode($order_return);
		// echo json_encode($xmlrequest);
		// echo json_encode("fsfdsf");


		$respuesta = $client->ecommerceVenta(
			// [
			array('request' => 
				[
					"comercioId" => 51,
					"contrasena" => 'ja9sduj',
					"modo" => 'A',
					"monto" => $JSON->Datos->Cotizacion->precioMXN,
					"transaccionId" => $JSON->Datos->Cotizacion->id,
					"referencia" => $JSON->Datos->Cotizacion->id,
					"tarjeta" => $JSON->Datos->Tarjeta->tarjeta,
					"tipo" => 1,
					"mes" => $JSON->Datos->Tarjeta->mes,
					"anio" => $JSON->Datos->Tarjeta->anio,
					"digitos" => $JSON->Datos->Tarjeta->digitos,
					"titular" => $JSON->Datos->Tarjeta->titular,
					"dirIp" => '192.192.192.255',
					"diferimiento" => 00,
					"numeroPagos" => 00,
					"plan" => 00,
				]
			)
			// ]
		);

		$Resultado = 
		[
			// "return" => $order_return,
			"return" => $respuesta,
			"c" => $client->__getFunctions(),
			"t" => $client->__getLastResponseHeaders(),
			"xml" => $xmlrequest
		];

		echo json_encode($Resultado);
	}


}

function dFormulario($DB, $tabla, $num_filas = null, $campos = '*', $restriccion)
{
	    // echo json_encode("dsadsad");
	$DB->where("activo_inactivo", 1);
	if(!empty($restriccion))
		$DB->where($restriccion->Campo, $restriccion->Valor);
	$Datos = $DB->get($tabla, $num_filas, $campos);
	if ($DB->count > 0)
	    // echo json_encode($DB->getLastQuery());
	    echo json_encode($Datos);
	    // echo json_encode("dsadsad");
}

function filtrar($Filtro)
{
	$DB_Where = "";
	if(!empty($Filtro))
	{
		// $DB_Where .= " AND " . $Filtro->Campo . " LIKE '%" . $Filtro->Valor . "%'";
		$Arreglo = null;
		if(isset($Filtro->Condiciones))
			$Arreglo = $Filtro->Condiciones;
		else
			$Arreglo = $Filtro;
		foreach($Arreglo as $Condicion)
		{
			// if(!is_object($Condicion)) break;
			if(!empty($Condicion) && $Condicion->Campo != null && $Condicion->Campo != "")
			{
				// echo json_encode($Condicion->Valor);
				$Logico = (isset($Condicion->Condicion)) ? $Condicion->Condicion : "AND";
				$Operador = (isset($Condicion->Operador)) ? $Condicion->Operador : "LIKE";
				// $Pre = (isset($Condicion->Pre)) ? $Condicion->Pre : "";
				$Valor = ($Operador == "LIKE") ? '%'.$Condicion->Valor.'%' : $Condicion->Valor;
				// $Valor = ($Operador == "==") ? $Condicion->Valor : $Condicion->Valor;

				if($Operador == "BETWEEN")
				{
					if(strpos($Condicion->Campo, "fecha") || strpos($Condicion->Campo, "salida") || strpos($Condicion->Campo, "regrso"))
					{
						$DB_Where .= " ".$Logico." " . $Condicion->Campo . " ".$Operador." str_to_date('".$Valor."', '%d/%m/%Y') AND  str_to_date('".$Condicion->Valor2."', '%d/%m/%Y')";
					}
					else
						$DB_Where .= " ".$Logico." " . $Condicion->Campo . " ".$Operador." '".$Valor."' AND '".$Condicion->Valor2."'";
				}
				// echo json_encode($Condicion->Valor);
				else
				{
					
					if(strpos($Condicion->Campo, "fecha") || strpos($Condicion->Campo, "salida") || strpos($Condicion->Campo, "regrso"))
					{
						$Valor = str_replace("%", " ", $Valor);
						$DB_Where .= " ".$Logico." " . $Condicion->Campo . " ".$Operador." str_to_date('".$Valor."', '%d/%m/%Y')";
					}
					else
					{
						if($Operador == "==")
							$DB_Where .= " ".$Logico." " . $Condicion->Campo . " = ".$Valor;
						else
							$DB_Where .= " ".$Logico." " . $Condicion->Campo . " ".$Operador." '".$Valor."'";
					}
				}
			}
		}
	}
	// echo $DB_Where;
	return $DB_Where;
}

function file_force_contents($dir, $contents){
    $parts = explode('/', $dir);
    $file = array_pop($parts);
    $dir = '';
    foreach($parts as $part)
        if(!is_dir($dir .= "/$part")) mkdir($dir);
    return file_put_contents("$dir/$file", $contents);
}

function generarHTML($DB, $caso, $datos)
{
	$HTML = '';
	if($caso == "Cotizacion")
	{
		$Cotizacion = $DB->rawQuery('SELECT 
			cliente.nombre_completo AS cliente_nombre_completo, cliente.correo_electronico AS cliente_correo_electronico, cliente.id AS id_cliente, 
			subagente.nombre_completo AS subagente_nombre_completo, subagente.correo_electronico AS subagente_correo_electronico,
			agente.agente, cotizacion.comentario,
			cotizacion.fecha_hecha, DATE_FORMAT(cotizacion.salida, "%d/%m/%Y") AS salida, DATE_FORMAT(cotizacion.regreso, "%d/%m/%Y") AS regreso, cotizacion.descripcion, cotizacion.precioMXN, cotizacion.precioUSD, cotizacion.tipo_cambio, cotizacion.id,
			tipo_seguro.tipo_seguro, tipo_poliza.tipo_poliza, tipo_poliza.codigo_poliza, 
			cotizacionOrigen.pais AS pais_origen, cotizacionDestino.pais AS pais_destino
		FROM cotizacion
			INNER JOIN tipo_poliza 
				ON cotizacion.id_tipo_poliza = tipo_poliza.id
			INNER JOIN tipo_seguro 
				ON cotizacion.id_tipo_seguro = tipo_seguro.id
			INNER JOIN pais AS cotizacionOrigen
				ON cotizacion.origen = cotizacionOrigen.id 
			INNER JOIN pais AS cotizacionDestino
				ON cotizacion.destino = cotizacionDestino.id
			INNER JOIN cliente
				ON cotizacion.id_cliente = cliente.id
			INNER JOIN subagente
				ON cotizacion.id_subagente = subagente.id
			INNER JOIN agente
				ON subagente.id_agente = agente.id
		WHERE cotizacion.id = '.$datos->id_cotizacion);


		$HTML = '
			<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
			<style>
			 .bold,label{font-weight:700}table{margin-top:10px;margin-left:40px}td{padding-right:30px}.row{margin-bottom:20px}.cerrado{border:1px solid #404040}button{height:50px;font-size:16pt;color:#fff!important}i{font-size:22pt;margin:0 10px;float:right}a{color:#000;cursor:pointer}a:hover{color:#4B4B4B}
			</style>
			<div class="row">
				<div class="col-sm-3"><img width="100px" src="../assets/img/logo.png"></div>
				<div class="col-sm-6">
					<table>
						<tr>
						<td class="bold">Cotización para:</td>
							<td>'.$Cotizacion[0]['cliente_nombre_completo'].'</td>
						</tr>
						<tr>
							<td class="bold">Correo electrónico:</td>
							<td>'.$Cotizacion[0]['cliente_correo_electronico'].'</td>
						</tr>
						<tr>
							<td class="bold">Cotizado por</td>
							<td>'.$Cotizacion[0]['subagente_nombre_completo'].'</td>
						</tr>
						<tr>
							<td class="bold">Email:</td>
							<td>'.$Cotizacion[0]['subagente_correo_electronico'].'</td>
						</tr>
						<tr>
							<td class="bold">Agente</td>
							<td>'.$Cotizacion[0]['agente'].'</td>
						</tr>
					</table>		
				</div>
				<div class="col-sm-3">
					<p style="margin-top: 100px;"><label>Cotización realizada el</label> '.$Cotizacion[0]['fecha_hecha'].'</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4 cerrado">
					'.$Cotizacion[0]['tipo_seguro'].'<br>
					'.$Cotizacion[0]['tipo_poliza'].'<br>
					'.$Cotizacion[0]['pais_origen'].'<br>
					'.$Cotizacion[0]['pais_destino'].'<br>
					'.$Cotizacion[0]['salida'].'<br>
					'.$Cotizacion[0]['regreso'].'
				</div>
				<div class="col-sm-8 cerrado">
					'.$Cotizacion[0]['descripcion'].'
				</div>
			</div>
			<div class="row">
				<div class="col-sm-10 cerrado">
					'.$Cotizacion[0]['comentario'].'

					<p style="font-size: 10pt; margin-top: 160px;">Los precios son estimados y son sujetos a cambios sin previo aviso debido a los tipos de seguro y fluctuaciones del tipo de cambio</p>
				</div>
				<div class="col-sm-2 cerrado">
					<h2>TOTAL</h2>

					<p>$'.$Cotizacion[0]['precioMXN'].' MXN</p>
					<p>$'.$Cotizacion[0]['precioUSD'].' USD</p>
					<p>Tipo de cambio $'.$Cotizacion[0]['tipo_cambio'].'</p>
				</div>
			</div>
		';
		// $HTML = "dsad";
	}

	return $HTML;
}

function generarPDF(){
	require_once 'fpdm/fpdm.php';

	$fields = array(
		'campo1'    => 'My name',
		'campo2' => 'My address',
		'campo3'    => 'My city',
		'campo4'   => 'My phone number'
	);
	
	$pdf = new FPDM('Campos.pdf');
	$pdf->Load($fields, false); // second parameter: false if field values are in ISO-8859-1, true if UTF-8
	$pdf->Merge();
	$pdf->Output('F');
	
}

function EstadoT($e)
{
	if(is_numeric($e))
	{
		switch($e)
		{
			case 0: return "NEW";
			case 1: return "EXT";
			case 2: return "CAN";
			case 3: return "CORR";
		}
	}
	else
	{
		switch($e)
		{
			case "NEW": return 0;
			case "EXT": return 1;
			case "CAN": return 2;
			case "CORR": return 3;
		}
	}
	return 0;
}

function Cobertura($DB, $inicio, $fin, $tipo_seguro, $zona_destino, $tipo_poliza = "no")
{
	// switch($tp)
	// {
	// 	case "Oro": $tp = 1;
	// 	case "Oro": $tp = 1;
	// 	case "Oro": $tp = 1;
	// }
	// $Dias = $inicio - $fin;
	// $Dias = $inicio - $fin;
	// echo json_encode("fdsfsdf");
	$Dias = DiferenciaFechas($inicio, $fin);
	$DSM = 'Días';
	$DiasRedoneados = 7;
	$SemanasMeses = 1;


	// echo json_encode($Dias);
	// $JSON->Datos->Dias = floor($JSON->Datos->Dias);
	$Dias = round($Dias);

	$Dias = abs($Dias) + 1;

	$DiasRedoneados = ceil($Dias / 7) * 7;

	if($Dias >= 1 && $Dias <= 9)
	{
		$DSM = 'Días';
		$DiasRedoneados = $Dias;
		if($tipo_seguro == 3 || $tipo_seguro == 2)
		{
			$DSM = 'Semanas';
			$DiasRedoneados = 1;
		}
	}
	else if($Dias >= 10 && $Dias <= 63)
	{
		$DSM = 'Semanas';
		// if($Dias < 21)
		if($Dias < 15)
			$DiasRedoneados = 10;
		// $DiasRedoneados = ceil($Dias / 7) * 7;
		else
			// $SemanasMeses = $DiasRedoneados / 7;
			$DiasRedoneados = ceil($Dias / 7) * 7;
			// $DiasRedoneados = $DiasRedoneados / 7;
		if($tipo_seguro == 3 || $tipo_seguro == 2)
		{
			$DSM = 'Semanas';
			$DiasRedoneados = ceil($Dias / 7) * 7;
		}
	}
	else if($Dias >= 64)
	{
		$DSM = 'Meses';
		// if($Dias >= 64 && $Dias <= 90)
		// {
		// 	$DiasRedoneados = 64;
		// 	$SemanasMeses = 3;
		// }
		// else // Redondear en meses, desde 4 (120 dias)
		// {
		// 	$DiasRedoneados = ceil($Dias/30) * 30;
		// 	$SemanasMeses = $DiasRedoneados / 30;
		// }
		$SemanasMeses = $DiasRedoneados / 30;
		// $DiasRedoneados = ceil(round($JSON->Datos->Dias/(30.4),1)) * 30;
		$DiasRedoneados = ceil(round(($JSON->Datos->Dias-3)/(30.4),1)) * 30;
		if($DiasRedoneados <= 90)
			$DiasRedoneados = 64;
	}

	// if($tipo_seguro == 2 && $JSON->Datos->DSM == "Días")
	// if($tipo_seguro == 2 || $tipo_seguro == 3)
	// {
	// 	$DSM = 'Semanas';
	// }

	// if($Dias < 10)

	$Where = "WHERE tipo_poliza.zona = ".$zona_destino."
	AND cobertura.dias = ".$DiasRedoneados." 
	AND tipo_poliza.id_tipo_seguro = ".$tipo_seguro." 
	AND tipo_poliza.dsm = '".$DSM."'";
	if($tipo_poliza != "no")
	{
		$Where .= " AND tipo_poliza.tipo_poliza = '".$tipo_poliza."'";
	}
	// echo json_encode([$Where, $inicio, $fin, $Dias]);

	$TiposPolizas = $DB->rawQuery("
		SELECT * FROM cobertura
			INNER JOIN tipo_poliza
				ON cobertura.id_tipo_poliza = tipo_poliza.id "
			.$Where
		);	

	// $Resutlado = ['Usuarios' => $Usuarios, 'Total' => $UsuariosTotal['Total']];

	// echo json_encode($DB->getLastQuery());
	$Resultado = ["Coberturas" => $TiposPolizas, 
		"Datos" => 
		[
			"Dias" => $Dias, "DiasRedoneados" => $DiasRedoneados, "DSM" => $DSM, "SemanasMeses" => $SemanasMeses,
			"SQL" => $DB->getLastQuery()
		]
	];

	// echo json_encode($Resultado);
	return $Resultado;
}

function DiferenciaFechas($date_1 , $date_2 , $differenceFormat = '%d' )
{
    // $datetime1 = date_create($date_1);
    // $datetime2 = date_create($date_2);
    
	// $interval = date_diff($datetime1, $datetime2);
	
	// // echo json_encode($interval);
    
	// return $interval->format($differenceFormat);
	
	
	//Our "then" date.
	// $then = "2009-02-04";
	
	//Convert it into a timestamp.
	// $then = $date_2;
	$then = strtotime($date_2);
	
	//Get the current timestamp.
	$now = strtotime($date_1);
	
	//Calculate the difference.
	$difference = $now - $then;
	// $difference = $then - $now;
	
	//Convert seconds into days.
	$days = floor($difference / (60*60*24) );

	// $da = abs($days);
	
	// return $da;
	return $days;
    
}

function CorreoRegistro($para, $contra)
{	
	$Para = $para;
	$Para .= ", ecantu@estudiantesembajadores.com";
	$Asunto = 'Mi Seguro Internacional | Registro';

	$Contenido = "
		<html>
		<head>
			<title>Usuario registrado</title>
			<style>
				body{
					font-family: Helvetica, sans-serif;
				}
			</style>
		</head>
		<body style='background-color: #fff;'>
			<table>
				<tr>
					<td><img src='https://misegurointernacional.com/ADMIN/assets/img/logo.png' alt='LOGO' width='140px'></td>
					<td><div style='margin-left: 100px;'>Mi Seguro Internacional</div></td>
				</tr>
			</table>		
			
			<div style='background-color: #fff;padding:10px 20px;margin:20px;'>
				<h1>Usuario registrado</h1>
				<h3>Se ha registrado un usuario en Mi Seguro Internacional para este correo:</h3>
				<p>Usuario: <b>".$para."</b></p>
				<br>
				<p>La <b>contraseña</b> debe ser creada en el siguiente enlace:</p>

				<table align='center' width='100%' border='0' cellspacing='0' cellpadding='0'>
					<tr>
					<td>
						<table align='center' border='0' cellspacing='0' cellpadding='0'>
						<tr>
							<td align='center' style='border-radius: 2px;' bgcolor='#7cdb46'><a href='https://misegurointernacional.com/ADMIN/Registrar/".$contra."' target='_blank' style='font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; text-decoration: none;border-radius: 3px; padding: 12px 40px; border: 1px solid #7cdb46; display: inline-block;'>Entrar &rarr;</a></td>
						</tr>
						</table>
					</td>
					</tr>
				</table>

				<p>En caso de que no funcione el botón copiar y pegar el siguiente link en el navegador:<br>
				<a href='https://misegurointernacional.com/ADMIN/Registrar/".$contra."'>https://misegurointernacional.com/ADMIN/Registrar/".$contra."</a></p>

				<hr>
				<p>Si tú no solicitaste una cotización en Mi Seguro Internacional, puedes hacer caso omiso a este mensaje.</p>

				<hr>
				<p style='text-align: center;'>Mi Seguro Internacional</p>
			</div>
		</body>
		</html>
	";

	$Headers = "From: micuenta@misegurointernacional.com\r\n";
	$Headers .= "MIME-Version: 1.0\r\n";
	$Headers .= "Content-Type: text/html; charset=UTF-8\r\n";


	$Enviado = mail($Para, "Registro de usuario", $Contenido, $Headers);
}

// echo json_encode($Datos->Datos->PW);

if(isset($_GET['CASO']))
{
	// require_once ('MysqliDb.php');
	// $DB = new MysqliDb ('localhost', 'usuario', 'usuario', 'angular_dos');

	// $JSON = json_decode(file_get_contents("php://input"));
	// if(!empty($JSON))
	// 	$Datos = $JSON->json;

}


?>