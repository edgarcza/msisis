[33mcommit e7b7c12c31203ab1e67abfbe6155793f9a30901a[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m
Author: Edgar <edgarcantu.za@gmail.com>
Date:   Tue May 7 12:21:07 2019 -0500

    HTTP

[1mdiff --git a/php/http.php b/php/http.php[m
[1mindex a1dd729..8dd4731 100644[m
[1m--- a/php/http.php[m
[1m+++ b/php/http.php[m
[36m@@ -881,7 +881,7 @@[m [mif(!empty($JSON))[m
 [m
 		$DB_Where .= filtrar($JSON->Datos->Filtro);[m
 [m
[31m-		if(!$JSON->Datos->Excel)[m
[32m+[m		[32mif(!$JSON->Datos->Excel && !$JSON->Datos->ExcelIngles)[m
 			$DB_Limit = ' LIMIT '.($JSON->Datos->Pagina * $JSON->Datos->Cantidad).', '.$JSON->Datos->Cantidad;[m
 [m
 		if($JSON->Datos->Excel || $JSON->Datos->ExcelIngles)[m
[36m@@ -994,7 +994,8 @@[m [mif(!empty($JSON))[m
 [m
 		$DB_Where .= filtrar($JSON->Datos->Filtro);[m
 [m
[31m-		if(!$JSON->Datos->Excel)[m
[32m+[m		[32m// if(!$JSON->Datos->Excel)[m
[32m+[m		[32mif(!$JSON->Datos->Excel && !$JSON->Datos->ExcelIngles)[m
 			$DB_Limit = ' LIMIT '.($JSON->Datos->Pagina * $JSON->Datos->Cantidad).', '.$JSON->Datos->Cantidad;[m
 [m
 		if($JSON->Datos->Excel || $JSON->Datos->ExcelIngles)[m
[36m@@ -2241,7 +2242,8 @@[m [mif(!empty($JSON))[m
 						$Datos2[$f-1]['nombre_completo'] = $Filas[$f][1] . " " . $Filas[$f][2];[m
 						$Datos2[$f-1]['sexo'] = $Filas[$f][3];[m
 						// $Datos2[$f-1]['correo_electronico'] = $Filas[$f][7];[m
[31m-						$Datos2[$f-1]['id_subagente'] = 18;[m
[32m+[m						[32m// $Datos2[$f-1]['id_subagente'] = 18;[m
[32m+[m						[32m$Datos2[$f-1]['id_subagente'] = $Subagente['id'];[m
 						$Datos2[$f-1]['id_cliente'] = $IDCliente;[m
 						$Datos2[$f-1]['fecha_nacimiento'] = date("Y-n-j", strtotime($Filas[$f][4]));[m
 						$Datos2[$f-1]['fecha_registro_asegurado'] = date("Y-n-j");[m
[36m@@ -2282,7 +2284,8 @@[m [mif(!empty($JSON))[m
 						$Datos1[$f-1]['id_cliente'] = $cid;[m
 						$Datos1[$f-1]['fecha_hecha'] = date("Y-n-j");[m
 						$Datos1[$f-1]['fecha_asegurado'] = date("Y-n-j");[m
[31m-						$Datos1[$f-1]['id_subagente'] = 18;[m
[32m+[m						[32m// $Datos1[$f-1]['id_subagente'] = 18;[m
[32m+[m						[32m$Datos1[$f-1]['id_subagente'] = $Subagente['id'];[m
 						$Datos1[$f-1]['id_tipo_poliza'] = $TipoPoliza['id'];[m
 						$Datos1[$f-1]['id_tipo_seguro'] = $TipoPoliza['id_tipo_seguro'];[m
 						$Datos1[$f-1]['tipo_cambio'] = $TC;[m
[1mdiff --git a/src/app/asegurados/asegurados.component.ts b/src/app/asegurados/asegurados.component.ts[m
[1mindex b93fc96..fc784b4 100644[m
[1m--- a/src/app/asegurados/asegurados.component.ts[m
[1m+++ b/src/app/asegurados/asegurados.component.ts[m
[36m@@ -423,7 +423,7 @@[m [mexport class AseguradosComponent implements OnInit {[m
 [m
     this.DatosSeguro.precioUSD_subtotal = this.Formulario.redondear(this.DatosSeguro.precioUSD_descuento - this.DatosSeguro.precioUSD_ajuste + this.DatosSeguro.precioUSD_iva, 2);[m
     this.DatosSeguro.precioUSD_comision = this.Formulario.redondear(this.DatosSeguro.precioUSD * (this.DatosSubagente.comision / 100), 2);[m
[31m-    // console.log(this.DatosSeguro.precioUSD +"* ("+this.DatosSubagente.comision +"/ 100)")[m
[32m+[m[32m    console.log(this.DatosSeguro.precioUSD +"* ("+this.DatosSubagente.comision +"/ 100)")[m
     this.DatosSeguro.precioUSD_total = this.DatosSeguro.precioUSD_subtotal - this.DatosSeguro.precioUSD_comision;[m
     // this.DatosSeguro.precioUSD_total = this.Formulario.redondear(this.DatosSeguro.precioUSD_subtotal, 2);[m
 [m
