import {SelectionModel} from '@angular/cdk/collections';
import { Component, OnInit, Inject } from '@angular/core';
import {MatTableDataSource, PageEvent, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { HTTPService }    from '../../http.service';
import {SesionService} from '../../sesion.service'
import {VentanasService} from '../../servicios/ventanas.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';
import {ExcelService} from '../../servicios/excel.service';

@Component({
  selector: 'app-por-pagar',
  templateUrl: './por-pagar.component.html',
  styleUrls: ['./por-pagar.component.css']
})
export class PorPagarComponent implements OnInit {

  constructor(
  	private HTTP: HTTPService,
    public sesion: SesionService,
    public dialog: MatDialog,
    public ventana: VentanasService,
    public Formulario: DatosFormularioService,
    public ServcioExcel: ExcelService
  	) { }

  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - -- - - - - -- - - -  SECCIÓN FILTROS
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -

  Filtros = {
    Campo: null,
    Valor: null,
    pageIndex: 0,
    pageSize: 5
  };

  Excel = false;

  filtrar(){
    if(this.Filtros.Campo && this.Filtros.Valor){
      if(this.Filtros.Valor.length > 1)
        this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize, this.Filtros);
      else
        this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize);
    }
    else{
        this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize);
    }
    // var s = this.Filtros.Valor;
    // console.log(this.Datos.filter(e => console.log(e));
  }

  filtroTexto(texto: string){
    // console.log(texto);
    this.Filtros.Valor = texto;
    this.filtrar();
  }

  filtroEliminar(texto: string){
    // console.log(texto);
    if(this.selection.selected){
      for(var DatosEn in this.selection.selected){

        this.ventana.Confirmar({Mensaje: "¿Eliminar pago "+this.selection.selected[DatosEn].no_recibo+"?"}, 
          () => {
            this.Formulario.borrar("pago", this.selection.selected[DatosEn], () => {
              this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize);});
          }, 
          () => { // console.log("NO");
           });
        // this.Formulario.borrar("pago", this.selection.selected[DatosEn]);
      }
    }
  }

  filtroSeleccionar(valor){
    // console.log(valor);
    this.Filtros.Campo = valor;
    this.filtrar();
  }

  filtroExcel(valor){
    console.log(valor);
    this.Excel = true;
    this.filtrar();
  }

  columnas(texto: string){
    this.ventana.columnas(this.Columnas);
  }

  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - -- - - - - -- - - -  SECCIÓN PÁGINA
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -

  cambioPagina(event){
    console.log(event);
    this.Filtros.pageIndex = event.pageIndex;
    this.Filtros.pageSize = event.pageSize;
    this.tabla(event.pageIndex, event.pageSize);
  }


  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - -- - - - - -- - - -  SECCIÓN TABLA
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  Columnas = [
    {id: 'no_recibo',    					nombre: 'No. recibo',      				ocultar: false, tabla: 'pago.no_recibo'},
    {id: 'fecha_pago',            nombre: 'Fecha de pago',         	ocultar: false, tabla: 'pago.fecha_pago'},
    {id: 'cliente',            		nombre: 'Cliente',    						ocultar: false, tabla: 'cliente.nombre_completo'},
    {id: 'cuenta',               	nombre: 'Cuenta',        					ocultar: false, tabla: 'cuenta.cuenta'}, 
    {id: 'forma_pago',           	nombre: 'Forma de pago',         	ocultar: false, tabla: 'forma_pago.forma_pago'}, 
    {id: 'cantidad_abonada_MXN',	nombre: 'Monto (MXN)',           	ocultar: false, tabla: 'pago.cantidad_abonada_MXN'}, 
    {id: 'tipo_cambio',          	nombre: 'TC',         						ocultar: false, tabla: 'pago.tipo_cambio'}, 
    {id: 'saldo_pendiente_MXN',   nombre: 'Saldo pendiente (MXN)',  ocultar: false, tabla: 'cotizacion.saldo_pendiente_MXN'}, 
    {id: 'tipo_distribuidor',    	nombre: 'Tipo de distribuidor',   ocultar: false, tabla: 'tipo_distribuidor.tipo_distribuidor'}, 
    {id: 'distribuidor',         	nombre: 'Distribuidor',           ocultar: false, tabla: 'distribuidor.distribuidor'}, 
    {id: 'agente',               	nombre: 'Agente',                 ocultar: false, tabla: 'agente.agente'}, 
    {id: 'subagente',   					nombre: 'Sub Agente',             ocultar: false, tabla: 'sub_agente.nombre_completo'},
  ];
  displayedColumns: string[] = ['select', 'no_recibo', 'fecha_pago', 'cliente', 
    'cuenta', 'forma_pago', 'cantidad_abonada_MXN', 'tipo_cambio', 'saldo_pendiente_MXN', 'tipo_distribuidor', 'distribuidor', 
    'agente', 'subagente', 'acciones'];

  public Datos = [];
  public DatosExcel = [];
  public DatosCantidad = 0;

  ngOnInit() {
    this.tabla();
  }

  tabla(pagina = 0, cantidad = 5, filtro: any = null){
    // this.Formulario.Progreso.Cargando = true;
    // this.HTTP.pagCuentasPorPagar(pagina, cantidad, filtro, this.Excel)
    //   .subscribe(
    //     datos => {
    //       if(this.Excel)
    //       {
    //         // exportar
    //         this.DatosExcel = datos.Cuentas;
    //         console.log(this.DatosExcel);
    //         this.ServcioExcel.exportAsExcelFile(this.DatosExcel, "Cuentas-Por-Pagar");
    //         this.Excel = false;
    //         return 0;
    //       }
    //       // console.log(datos);
    //       this.Datos = datos.Cuentas;
    //       this.dataSource.data = datos.Cuentas;
    //       this.DatosCantidad = datos.Total;
    //     },
    //     error => {
    //       console.log(error);
    //     }
    //   );
  }

  dataSource = new MatTableDataSource(this.Datos);
  selection = new SelectionModel(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** Actualizar número de casillas seleccionadas */
  public Seleccionados = 0;
  actualizarSeleccion(){
    this.Seleccionados = this.selection.selected.length;
  }

}
