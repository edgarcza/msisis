import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PorPagarComponent } from './por-pagar.component';

describe('PorPagarComponent', () => {
  let component: PorPagarComponent;
  let fixture: ComponentFixture<PorPagarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PorPagarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PorPagarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
