import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PorCobrarComponent } from './por-cobrar.component';

describe('PorCobrarComponent', () => {
  let component: PorCobrarComponent;
  let fixture: ComponentFixture<PorCobrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PorCobrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PorCobrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
