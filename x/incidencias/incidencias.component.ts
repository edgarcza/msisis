import {SelectionModel} from '@angular/cdk/collections';
import { Component, OnInit, Inject } from '@angular/core';
import {MatTableDataSource, PageEvent, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { HTTPService }    from '../../http.service';
import {SesionService} from '../../sesion.service'
import {VentanasService} from '../../servicios/ventanas.service';
import { DatosFormularioService } from '../../servicios/datos-formulario.service';
import {ExcelService} from '../../servicios/excel.service';

@Component({
  selector: 'app-incidencias',
  templateUrl: './incidencias.component.html',
  styleUrls: ['./incidencias.component.css']
})
export class IncidenciasComponent implements OnInit {

constructor(
  	private HTTP: HTTPService,
    public sesion: SesionService,
    public dialog: MatDialog,
    public ventana: VentanasService,
    public Formulario: DatosFormularioService,
    public ServcioExcel: ExcelService
  	) { }

  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - -- - - - - -- - - -  SECCIÓN FILTROS
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -

  Filtros = {
    Campo: null,
    Valor: null,
    pageIndex: 0,
    pageSize: 5
  };

  Excel = false;

  filtrar(){
    if(this.Filtros.Campo && this.Filtros.Valor){
      if(this.Filtros.Valor.length > 1)
        this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize, this.Filtros);
      else
        this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize);
    }
    else{
        this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize);
    }
    // var s = this.Filtros.Valor;
    // console.log(this.Datos.filter(e => console.log(e));
  }

  filtroTexto(texto: string){
    // console.log(texto);
    this.Filtros.Valor = texto;
    this.filtrar();
  }

  filtroEliminar(texto: string){
    // console.log(texto);
    if(this.selection.selected){
      for(var DatosEn in this.selection.selected){

        this.ventana.Confirmar({Mensaje: "¿Eliminar pago "+this.selection.selected[DatosEn].no_recibo+"?"}, 
          () => {
            this.Formulario.borrar("pago", this.selection.selected[DatosEn], () => {
              this.tabla(this.Filtros.pageIndex, this.Filtros.pageSize);});
          }, 
          () => { // console.log("NO");
           });
        // this.Formulario.borrar("pago", this.selection.selected[DatosEn]);
      }
    }
  }

  filtroSeleccionar(valor){
    // console.log(valor);
    this.Filtros.Campo = valor;
    this.filtrar();
  }

  filtroExcel(valor){
    console.log(valor);
    this.Excel = true;
    this.filtrar();
  }

  columnas(texto: string){
    this.ventana.columnas(this.Columnas);
  }

  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - -- - - - - -- - - -  SECCIÓN PÁGINA
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -

  cambioPagina(event){
    console.log(event);
    this.Filtros.pageIndex = event.pageIndex;
    this.Filtros.pageSize = event.pageSize;
    this.tabla(event.pageIndex, event.pageSize);
  }


  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - -- - - - - -- - - -  SECCIÓN TABLA
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  // - - - - - - - - - - - -- -  --  --  - - - - - - - - -
  Columnas = [
    {id: 'fecha_creacion',    		nombre: 'Fecha de creación',     	ocultar: false, tabla: 'incidente.fecha_creacion'},
    {id: 'fecha_incidente',      	nombre: 'Fecha de incidente',   	ocultar: false, tabla: 'incidente.fecha_incidente'},
    {id: 'status',            		nombre: 'Status',    							ocultar: false, tabla: 'incidente.status'},
    {id: 'fecha_envio',          	nombre: 'Fecha de envío',       	ocultar: false, tabla: 'incidente.fecha_envio'}, 
    {id: 'monto_solicitado',    	nombre: 'Cantidad solicitada',   	ocultar: false, tabla: 'incidente.monto_solicitado'}, 
    {id: 'fecha_autorizacion',		nombre: 'Fecha de pago',         	ocultar: false, tabla: 'incidente.fecha_autorizacion'}, 
    {id: 'monto_pagado',         	nombre: 'Cantidad pagada',       	ocultar: false, tabla: 'incidente.monto_pagado'}, 
    {id: 'resultado',   					nombre: 'Resultado',  						ocultar: false, tabla: 'incidente.resultado'}, 
    {id: 'tipo_distribuidor',    	nombre: 'Tipo de distribuidor',   ocultar: false, tabla: 'tipo_distribuidor.tipo_distribuidor'}, 
    {id: 'distribuidor',         	nombre: 'Distribuidor',           ocultar: false, tabla: 'distribuidor.distribuidor'}, 
    {id: 'agente',               	nombre: 'Agente',                 ocultar: false, tabla: 'agente.agente'}, 
    {id: 'subagente',   					nombre: 'Sub Agente',             ocultar: false, tabla: 'sub_agente.nombre_completo'},
  ];
  displayedColumns: string[] = ['select', 'fecha_creacion', 'fecha_incidente', 'status', 
    'fecha_envio', 'monto_solicitado', 'fecha_autorizacion', 'monto_pagado', 'resultado', 'tipo_distribuidor', 'distribuidor', 
    'agente', 'subagente', 'acciones'];

  public Datos = [];
  public DatosExcel = [];
  public DatosCantidad = 0;

  ngOnInit() {
    this.tabla();
  }

  tabla(pagina = 0, cantidad = 5, filtro: any = null){
    // this.Formulario.Progreso.Cargando = true;
    this.HTTP.pagIncidencias(pagina, cantidad, filtro, this.Excel)
      .subscribe(
        datos => {
          if(this.Excel)
          {
            // exportar
            this.DatosExcel = datos.Incidentes;
            console.log(this.DatosExcel);
            this.ServcioExcel.exportAsExcelFile(this.DatosExcel, "Incidentes");
            this.Excel = false;
            return 0;
          }
          // console.log(datos);
          this.Datos = datos.Incidentes;
          this.dataSource.data = datos.Incidentes;
          this.DatosCantidad = datos.Total;
        },
        error => {
          console.log(error);
        }
      );
  }

  dataSource = new MatTableDataSource(this.Datos);
  selection = new SelectionModel(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** Actualizar número de casillas seleccionadas */
  public Seleccionados = 0;
  actualizarSeleccion(){
    this.Seleccionados = this.selection.selected.length;
  }

}
